/*
Dealer Name: i.e.SmartSytems
System Name: IESS SSH Client v0.1
System Number:
Programmer: Matthew Laletas
Comments: IESS SSH Client v0.1
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
#CRESTRON_SIMPLSHARP_LIBRARY "SSH_Interface_v1_3"
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT Connect, _SKIP_;
ANALOG_INPUT IP_Port;
STRING_INPUT IP_Address[16], _SKIP_;
STRING_INPUT Login_Name[32], Login_Password[32], _SKIP_;
STRING_INPUT TX$[511];

DIGITAL_OUTPUT Connect_FB;
ANALOG_OUTPUT Connect_Status_FB, _SKIP_;
STRING_OUTPUT RX$;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
INTEGER_PARAMETER _SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,_SKIP_,Unique_ID;
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
SSH_Interface UserClass;
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER	iConnectRequest;
STRING	sStringToSend[250];
STRING  sConnectionStatus[10];
INTEGER iConnectionStatus;
INTEGER Connection_Status;
STRING gvIP_Address[16];
INTEGER gvIP_Port;
STRING gvLogin_Name[32], gvLogin_Password[32];
/*******************************************************************************************
  Functions
*******************************************************************************************/
Function RegDelegates()
{
	RegisterDelegate(UserClass, SendFromDevice, ProcessFromDevice);
	RegisterDelegate(UserClass, SendConnectionStatus, ProcessConnectionStatus);
	RegisterDelegate(UserClass, SendFingerprint, ProcessFingerprint);
}         
Callback Function ProcessFromDevice(STRING ReturnFromDevice)
{
	RX$ = ReturnFromDevice;
}

Callback Function ProcessFingerprint(STRING Fingerprint)
{
	//New_Key_Text = Fingerprint;
}

Function RetryConnection()
{
	UserClass.Connect(gvIP_Address, gvIP_Port, gvLogin_Name, gvLogin_Password);
}
FUNCTION fnAccept_New_Key()
{
	Connection_Status = 1;
	Connect_Status_FB = Connection_Status;
	//Connection_Status_Text = "Trying To Connect";    	
	iConnectRequest = 1;
	UserClass.Accept_New_Key();
	//Accept_New_Key_Notice = 0;
   	UserClass.Connect(gvIP_Address, gvIP_Port, gvLogin_Name, gvLogin_Password);
}
Callback Function ProcessConnectionStatus(STRING ConnectedValue)
{
	processlogic();
	if(ConnectedValue = "True")
	{
		if(Connection_Status <> 2)
		{	
			Connection_Status = 2;
			Connect_Status_FB = Connection_Status;
			//Connection_Status_Text = "Connected";
		}
		Connect_FB = 1;
		iConnectionStatus = 1;	
		
    }
	else if(ConnectedValue = "False")
	{
		if(Connection_Status <> 3)
		{
			Connection_Status = 3;
			Connect_Status_FB = Connection_Status;
			//Connection_Status_Text = "Not Connected";    
		}
		Connect_FB = 0;
		iConnectionStatus = 0;
		processlogic();

		if(iConnectRequest = 1)
		{
			Connection_Status = 1;
			Connect_Status_FB = Connection_Status;
			//Connection_Status_Text = "Trying To Connect";
			wait(5)
			{
     			RetryConnection();
			}       
		}
	}
	else if(ConnectedValue = "Non-Matching Fingerprint")
	{
		fnAccept_New_Key();
		iConnectRequest = 0;
	}
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
RELEASE Connect
{
	iConnectRequest = 0;  
    UserClass.Disconnect();  
	//Accept_New_Key_Notice = 0;
}
PUSH Connect
{
	Connection_Status = 1;
	Connect_Status_FB = Connection_Status;
	//Connection_Status_Text = "Trying To Connect";
	iConnectRequest = 1;
   	UserClass.Connect(gvIP_Address, gvIP_Port, gvLogin_Name, gvLogin_Password);
}
CHANGE TX$
{
	IF( LEN( TX$ ) )
   		UserClass.Command_In( TX$ );
}
CHANGE IP_Address
{
	gvIP_Address = IP_Address;
}
CHANGE IP_Port
{
	gvIP_Port = IP_Port;
}
CHANGE Login_Name
{
	gvLogin_Name = Login_Name;
}
CHANGE Login_Password
{
	gvLogin_Password = Login_Password;
}
/*******************************************************************************************
  Main()
*******************************************************************************************/
Function Main()
{
	waitforinitializationcomplete();
	RegDelegates();
	UserClass.Accept_Any_Key(1);
	UserClass.Unique_ID(Unique_ID);
    if(Connection_Status <> 1 && Connection_Status <> 2)
	{ 
		Connection_Status = 3;
		Connect_Status_FB = Connection_Status;
		//Connection_Status_Text = "Not Connected";    
		Connect_FB = 0;
		iConnectionStatus = 0;
	}
	UserClass.LoadSettings();
}
