/*
Dealer Name: i.e.SmartSytems
System Name: IESS Display Routing v0.6
System Number:
Programmer: Matthew Laletas, v0.6 edited by Jesus Barrera
Comments: IESS Display Routing v0.6
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_INPUT 16
#DEFINE_CONSTANT NUM_OUTPUT 16
#HELP_PDF_FILE "IESS Display Routing v0_6.pdf"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT PowerOff, _SKIP_, Inputs[NUM_INPUT], _SKIP_, Outputs[NUM_OUTPUT], _SKIP_;
ANALOG_INPUT NumberDisplays, _SKIP_, RoutingMode;
ANALOG_OUTPUT LastSelectedInput, _SKIP_, VideoSwitch[NUM_OUTPUT], _SKIP_, VideoControl[NUM_OUTPUT];
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER GLBL_LastInput, GLBL_LastOutput, GLBL_Outputs[NUM_OUTPUT], GLBL_Countdown, GLBL_NumberDisplays, GLBL_RoutingMode;
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION RouteAllDisplays()
{
	INTEGER lvCounter;
	FOR( lvCounter = 1 TO GLBL_NumberDisplays )
	{
		VideoSwitch[lvCounter] = GLBL_LastInput;
		IF( GLBL_LastInput )
			VideoControl[lvCounter] = 1;
		ELSE
			VideoControl[lvCounter] = 99;		
	}
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
PUSH Inputs
{
	GLBL_LastInput = GETLASTMODIFIEDARRAYINDEX();
	LastSelectedInput = GLBL_LastInput;
	IF( GLBL_RoutingMode = 1 )
		RouteAllDisplays();
	ELSE IF( GLBL_RoutingMode = 3 )
	{
		VideoSwitch[GLBL_LastOutput] = GLBL_LastInput;
		IF( GLBL_LastInput )
			VideoControl[GLBL_LastOutput] = 1;
		ELSE
			VideoControl[GLBL_LastOutput] = 99;
		GLBL_Outputs[GLBL_LastOutput] = GLBL_LastInput;
	}
	WAIT( 400, SourceHold )
	{
		IF( Inputs[GLBL_LastInput] )
			RouteAllDisplays();
	}
}
RELEASE Inputs
{
	CancelWait( SourceHold );	
}
CHANGE NumberDisplays
{
	GLBL_NumberDisplays = NumberDisplays;
}
CHANGE RoutingMode
{
	GLBL_RoutingMode = RoutingMode;
}
PUSH Outputs
{
	GLBL_LastOutput = GETLASTMODIFIEDARRAYINDEX();
	IF( GLBL_RoutingMode = 2 )
	{
		VideoSwitch[GLBL_LastOutput] = GLBL_LastInput;
		IF( GLBL_LastInput )
			VideoControl[GLBL_LastOutput] = 1;
		ELSE
			VideoControl[GLBL_LastOutput] = 99;

		GLBL_Outputs[GLBL_LastOutput] = GLBL_LastInput;
	}
}
PUSH PowerOff
{
	GLBL_LastInput = 0;
	LastSelectedInput = 0;
	IF( GLBL_RoutingMode = 1 )
		RouteAllDisplays();
	ELSE IF( GLBL_RoutingMode = 3 )
	{
		VideoSwitch[GLBL_LastOutput] = GLBL_LastInput;
		IF( GLBL_LastInput )
			VideoControl[GLBL_LastOutput] = 1;
		ELSE
			VideoControl[GLBL_LastOutput] = 99;
		GLBL_Outputs[GLBL_LastOutput] = GLBL_LastInput;
	}
}
/*******************************************************************************************
  Main()
*******************************************************************************************/
FUNCTION MAIN()
{
	GLBL_RoutingMode = 2;
}
