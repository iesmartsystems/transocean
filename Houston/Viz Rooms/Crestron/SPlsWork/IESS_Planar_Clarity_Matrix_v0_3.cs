using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_PLANAR_CLARITY_MATRIX_V0_3
{
    public class UserModuleClass_IESS_PLANAR_CLARITY_MATRIX_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput AUTOPOWER_ENABLE;
        Crestron.Logos.SplusObjects.DigitalInput AUTOPOWER_PREVIOUSSTATE;
        Crestron.Logos.SplusObjects.DigitalInput AUTOPOWER_DISABLE;
        Crestron.Logos.SplusObjects.DigitalInput SYSTEMPOWER_TOGGLE;
        Crestron.Logos.SplusObjects.DigitalInput SYSTEMPOWER_ON;
        Crestron.Logos.SplusObjects.DigitalInput SYSTEMPOWER_OFF;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_SAVE;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_LOAD;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_DELETE;
        Crestron.Logos.SplusObjects.DigitalInput ROUTE_DP;
        Crestron.Logos.SplusObjects.DigitalInput ROUTE_DVI;
        Crestron.Logos.SplusObjects.DigitalInput ROUTE_VGA;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> ROUTE_HDMI;
        Crestron.Logos.SplusObjects.AnalogInput PRESET_SELECT;
        Crestron.Logos.SplusObjects.AnalogInput ZONE_SELECT;
        Crestron.Logos.SplusObjects.AnalogInput BP_CONTROLLER_SELECT;
        Crestron.Logos.SplusObjects.AnalogInput BP_SOURCE_SELECT;
        Crestron.Logos.SplusObjects.StringInput PLANAR_RX;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_POWERON;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_POWEROFF;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_PRESET_SAVE;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_PRESET_LOAD;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_PRESET_DELETE;
        Crestron.Logos.SplusObjects.StringOutput PLANAR_TX;
        Crestron.Logos.SplusObjects.AnalogOutput STATUS_PRESET_ACTIVE;
        Crestron.Logos.SplusObjects.AnalogOutput STATUS_ZONE_ACTIVE;
        Crestron.Logos.SplusObjects.AnalogOutput STATUS_INPUTX_BP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> STATUS_INPUTX_ZONE;
        SPLANARCOMMANDS COMMAND;
        SIRCOMMANDS IR;
        CrestronString GVRXBUFFER;
        CrestronString GVWINDOWSELECTEDID;
        ushort GVWINDOWSELECTED = 0;
        ushort GVPRESETSELECTED = 0;
        ushort GVZONESELECTED = 0;
        private void SENDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
            { 
            
            __context__.SourceCodeLine = 134;
            PLANAR_TX  .UpdateValue ( LVINCOMING + COMMAND . ETX  ) ; 
            
            }
            
        object POLL_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                CrestronString LVSTRING;
                LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
                
                
                __context__.SourceCodeLine = 143;
                LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CGET  ) ; 
                __context__.SourceCodeLine = 144;
                SENDSTRING (  __context__ , LVSTRING) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object AUTOPOWER_ENABLE_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 149;
            LVSTRING  .UpdateValue ( COMMAND . AUTOPOWER + COMMAND . CSET + COMMAND . AUTOPOWERENABLE  ) ; 
            __context__.SourceCodeLine = 150;
            SENDSTRING (  __context__ , LVSTRING) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object AUTOPOWER_PREVIOUSSTATE_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 155;
        LVSTRING  .UpdateValue ( COMMAND . AUTOPOWER + COMMAND . CSET + COMMAND . AUTOPOWERPREVIOUS  ) ; 
        __context__.SourceCodeLine = 156;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object AUTOPOWER_DISABLE_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 161;
        LVSTRING  .UpdateValue ( COMMAND . AUTOPOWER + COMMAND . CSET + COMMAND . AUTOPOWERDISABLE  ) ; 
        __context__.SourceCodeLine = 162;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SYSTEMPOWER_ON_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 167;
        LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWERON  ) ; 
        __context__.SourceCodeLine = 168;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SYSTEMPOWER_OFF_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 173;
        LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWEROFF  ) ; 
        __context__.SourceCodeLine = 174;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SYSTEMPOWER_TOGGLE_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 179;
        if ( Functions.TestForTrue  ( ( STATUS_POWERON  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 180;
            LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWEROFF  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 181;
            if ( Functions.TestForTrue  ( ( STATUS_POWEROFF  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 182;
                LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWERON  ) ; 
                }
            
            }
        
        __context__.SourceCodeLine = 183;
        if ( Functions.TestForTrue  ( ( Functions.Length( LVSTRING ))  ) ) 
            {
            __context__.SourceCodeLine = 184;
            SENDSTRING (  __context__ , LVSTRING) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_SAVE_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 189;
        if ( Functions.TestForTrue  ( ( STATUS_POWERON  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 191;
            LVSTRING  .UpdateValue ( COMMAND . PRESETSAVE + "(" + Functions.ItoA (  (int) ( GVPRESETSELECTED ) ) + ")"  ) ; 
            __context__.SourceCodeLine = 192;
            SENDSTRING (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_LOAD_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 198;
        LVSTRING  .UpdateValue ( COMMAND . PRESETRECALL + "(" + Functions.ItoA (  (int) ( GVPRESETSELECTED ) ) + ")"  ) ; 
        __context__.SourceCodeLine = 199;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 200;
        if ( Functions.TestForTrue  ( ( STATUS_POWEROFF  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 202;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_22__" , 15 , __SPLS_TMPVAR__WAITLABEL_22___Callback ) ;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_22___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            __context__.SourceCodeLine = 205;
            LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWERON  ) ; 
            __context__.SourceCodeLine = 206;
            SENDSTRING (  __context__ , LVSTRING) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object PRESET_DELETE_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 213;
        LVSTRING  .UpdateValue ( COMMAND . PRESETDELETE + "(" + Functions.ItoA (  (int) ( GVPRESETSELECTED ) ) + ")"  ) ; 
        __context__.SourceCodeLine = 214;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTE_DP_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 219;
        LVSTRING  .UpdateValue ( COMMAND . IR + COMMAND . CSET + IR . DP  ) ; 
        __context__.SourceCodeLine = 220;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 221;
        STATUS_INPUTX_ZONE [ GVZONESELECTED]  .Value = (ushort) ( (4 + 1) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTE_DVI_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 226;
        LVSTRING  .UpdateValue ( COMMAND . IR + COMMAND . CSET + IR . DVI  ) ; 
        __context__.SourceCodeLine = 227;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 228;
        STATUS_INPUTX_ZONE [ GVZONESELECTED]  .Value = (ushort) ( (4 + 2) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTE_VGA_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 233;
        LVSTRING  .UpdateValue ( COMMAND . IR + COMMAND . CSET + IR . VGA  ) ; 
        __context__.SourceCodeLine = 234;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 235;
        STATUS_INPUTX_ZONE [ GVZONESELECTED]  .Value = (ushort) ( (4 + 3) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTE_HDMI_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 241;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 242;
        LVSTRING  .UpdateValue ( COMMAND . IR + COMMAND . CSET + IR . HDMI [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 243;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 244;
        STATUS_INPUTX_ZONE [ GVZONESELECTED]  .Value = (ushort) ( LVINDEX ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_SELECT_OnChange_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 248;
        GVPRESETSELECTED = (ushort) ( PRESET_SELECT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZONE_SELECT_OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 253;
        GVZONESELECTED = (ushort) ( ZONE_SELECT  .UshortValue ) ; 
        __context__.SourceCodeLine = 254;
        STATUS_ZONE_ACTIVE  .Value = (ushort) ( GVZONESELECTED ) ; 
        __context__.SourceCodeLine = 255;
        LVSTRING  .UpdateValue ( COMMAND . IR + COMMAND . CSET + IR . ZONE [ GVZONESELECTED ]  ) ; 
        __context__.SourceCodeLine = 256;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BP_CONTROLLER_SELECT_OnChange_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object BP_SOURCE_SELECT_OnChange_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 265;
        LVSTRING  .UpdateValue ( COMMAND . BP + COMMAND . CSET + "VC" + Functions.ItoA (  (int) ( BP_CONTROLLER_SELECT  .UshortValue ) ) + ".IN" + Functions.ItoA (  (int) ( BP_SOURCE_SELECT  .UshortValue ) )  ) ; 
        __context__.SourceCodeLine = 266;
        SENDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 267;
        if ( Functions.TestForTrue  ( ( STATUS_POWEROFF  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 269;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_23__" , 15 , __SPLS_TMPVAR__WAITLABEL_23___Callback ) ;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_23___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            __context__.SourceCodeLine = 272;
            LVSTRING  .UpdateValue ( COMMAND . POWER + COMMAND . CSET + COMMAND . POWERON  ) ; 
            __context__.SourceCodeLine = 273;
            SENDSTRING (  __context__ , LVSTRING) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object PLANAR_RX_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVSTRING;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        ushort LVCOUNTER = 0;
        ushort LVPRESET = 0;
        
        
        __context__.SourceCodeLine = 282;
        GVRXBUFFER  .UpdateValue ( GVRXBUFFER + PLANAR_RX  ) ; 
        __context__.SourceCodeLine = 283;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GVRXBUFFER ))  ) ) 
            { 
            __context__.SourceCodeLine = 285;
            LVRX  .UpdateValue ( Functions.Remove ( "\u000D" , GVRXBUFFER )  ) ; 
            __context__.SourceCodeLine = 286;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 287;
            if ( Functions.TestForTrue  ( ( Functions.Find( "SYSTEM.POWER:" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 289;
                LVTRASH  .UpdateValue ( Functions.Remove ( "SYSTEM.POWER:" , LVRX )  ) ; 
                __context__.SourceCodeLine = 290;
                if ( Functions.TestForTrue  ( ( Functions.Find( "ON" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 292;
                    STATUS_POWEROFF  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 293;
                    STATUS_POWERON  .Value = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 295;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "OFF" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 297;
                        STATUS_POWERON  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 298;
                        STATUS_POWEROFF  .Value = (ushort) ( 1 ) ; 
                        } 
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 302;
                if ( Functions.TestForTrue  ( ( Functions.Find( "AUTO.ON:" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 304;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "AUTO.ON:" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 305;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "POWER_ON" , LVRX ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 308;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "POWER_OFF" , LVRX ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 313;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "PRESET.SAVE" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 315;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "@ACK" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 317;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "PRESET.RECALL(" , LVRX )  ) ; 
                            __context__.SourceCodeLine = 318;
                            LVSTRING  .UpdateValue ( Functions.Remove ( 1, LVRX )  ) ; 
                            __context__.SourceCodeLine = 319;
                            STATUS_PRESET_ACTIVE  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                            __context__.SourceCodeLine = 320;
                            Functions.Pulse ( 200, STATUS_PRESET_SAVE ) ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 323;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "PRESET.RECALL" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 325;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "@ACK" , LVRX ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 327;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "PRESET.RECALL(" , LVRX )  ) ; 
                                __context__.SourceCodeLine = 328;
                                LVSTRING  .UpdateValue ( Functions.Remove ( 1, LVRX )  ) ; 
                                __context__.SourceCodeLine = 329;
                                LVPRESET = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                __context__.SourceCodeLine = 330;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVPRESET > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVPRESET <= 4 ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 332;
                                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                    ushort __FN_FOREND_VAL__1 = (ushort)4; 
                                    int __FN_FORSTEP_VAL__1 = (int)1; 
                                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                        {
                                        __context__.SourceCodeLine = 333;
                                        STATUS_INPUTX_ZONE [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 332;
                                        }
                                    
                                    __context__.SourceCodeLine = 334;
                                    STATUS_PRESET_ACTIVE  .Value = (ushort) ( LVPRESET ) ; 
                                    __context__.SourceCodeLine = 335;
                                    Functions.Pulse ( 200, STATUS_PRESET_LOAD ) ; 
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 339;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "PRESET.DELETE" , LVRX ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 341;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "@ACK" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 343;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "PRESET.RECALL(" , LVRX )  ) ; 
                                    __context__.SourceCodeLine = 344;
                                    LVSTRING  .UpdateValue ( Functions.Remove ( 1, LVRX )  ) ; 
                                    __context__.SourceCodeLine = 345;
                                    STATUS_PRESET_ACTIVE  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                    __context__.SourceCodeLine = 346;
                                    Functions.Pulse ( 200, STATUS_PRESET_DELETE ) ; 
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 349;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "QCONFIG:" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 351;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "IN" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 353;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "IN" , LVRX )  ) ; 
                                        __context__.SourceCodeLine = 354;
                                        STATUS_INPUTX_BP  .Value = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                        } 
                                    
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 283;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 367;
        COMMAND . POLL  .UpdateValue ( "IPV4.ADDRESS ?"  ) ; 
        __context__.SourceCodeLine = 368;
        COMMAND . AUTOPOWER  .UpdateValue ( "AUTO.ON"  ) ; 
        __context__.SourceCodeLine = 369;
        COMMAND . AUTOPOWERENABLE  .UpdateValue ( "POWER_ON"  ) ; 
        __context__.SourceCodeLine = 370;
        COMMAND . AUTOPOWERPREVIOUS  .UpdateValue ( "PREVIOUS_STATE"  ) ; 
        __context__.SourceCodeLine = 371;
        COMMAND . AUTOPOWERDISABLE  .UpdateValue ( "DISABLED"  ) ; 
        __context__.SourceCodeLine = 372;
        COMMAND . POWER  .UpdateValue ( "SYSTEM.POWER"  ) ; 
        __context__.SourceCodeLine = 373;
        COMMAND . POWERON  .UpdateValue ( "ON"  ) ; 
        __context__.SourceCodeLine = 374;
        COMMAND . POWEROFF  .UpdateValue ( "OFF"  ) ; 
        __context__.SourceCodeLine = 375;
        COMMAND . PRESETSAVE  .UpdateValue ( "PRESET.SAVE"  ) ; 
        __context__.SourceCodeLine = 376;
        COMMAND . PRESETRECALL  .UpdateValue ( "PRESET.RECALL"  ) ; 
        __context__.SourceCodeLine = 377;
        COMMAND . PRESETDELETE  .UpdateValue ( "PRESET.DELETE"  ) ; 
        __context__.SourceCodeLine = 378;
        COMMAND . IR  .UpdateValue ( "KEY"  ) ; 
        __context__.SourceCodeLine = 379;
        COMMAND . BP  .UpdateValue ( "QCONFIG"  ) ; 
        __context__.SourceCodeLine = 380;
        COMMAND . CSET  .UpdateValue ( "="  ) ; 
        __context__.SourceCodeLine = 381;
        COMMAND . CGET  .UpdateValue ( "?"  ) ; 
        __context__.SourceCodeLine = 382;
        COMMAND . INCREMENT  .UpdateValue ( "+"  ) ; 
        __context__.SourceCodeLine = 383;
        COMMAND . DECREMENT  .UpdateValue ( "-"  ) ; 
        __context__.SourceCodeLine = 384;
        COMMAND . ETX  .UpdateValue ( "\u000D"  ) ; 
        __context__.SourceCodeLine = 386;
        IR . ZONE [ 1 ]  .UpdateValue ( "ZONE1"  ) ; 
        __context__.SourceCodeLine = 387;
        IR . ZONE [ 2 ]  .UpdateValue ( "ZONE2"  ) ; 
        __context__.SourceCodeLine = 388;
        IR . ZONE [ 3 ]  .UpdateValue ( "ZONE3"  ) ; 
        __context__.SourceCodeLine = 389;
        IR . ZONE [ 4 ]  .UpdateValue ( "ZONE4"  ) ; 
        __context__.SourceCodeLine = 390;
        IR . HDMI [ 1 ]  .UpdateValue ( "HDMI1"  ) ; 
        __context__.SourceCodeLine = 391;
        IR . HDMI [ 2 ]  .UpdateValue ( "HDMI2"  ) ; 
        __context__.SourceCodeLine = 392;
        IR . HDMI [ 3 ]  .UpdateValue ( "HDMI3"  ) ; 
        __context__.SourceCodeLine = 393;
        IR . HDMI [ 4 ]  .UpdateValue ( "HDMI4"  ) ; 
        __context__.SourceCodeLine = 394;
        IR . DP  .UpdateValue ( "DISPLAY.PORT"  ) ; 
        __context__.SourceCodeLine = 395;
        IR . DVI  .UpdateValue ( "DVI"  ) ; 
        __context__.SourceCodeLine = 396;
        IR . VGA  .UpdateValue ( "VGA"  ) ; 
        __context__.SourceCodeLine = 398;
        GVZONESELECTED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 399;
        STATUS_POWEROFF  .Value = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVRXBUFFER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    GVWINDOWSELECTEDID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
    COMMAND  = new SPLANARCOMMANDS( this, true );
    COMMAND .PopulateCustomAttributeList( false );
    IR  = new SIRCOMMANDS( this, true );
    IR .PopulateCustomAttributeList( false );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    AUTOPOWER_ENABLE = new Crestron.Logos.SplusObjects.DigitalInput( AUTOPOWER_ENABLE__DigitalInput__, this );
    m_DigitalInputList.Add( AUTOPOWER_ENABLE__DigitalInput__, AUTOPOWER_ENABLE );
    
    AUTOPOWER_PREVIOUSSTATE = new Crestron.Logos.SplusObjects.DigitalInput( AUTOPOWER_PREVIOUSSTATE__DigitalInput__, this );
    m_DigitalInputList.Add( AUTOPOWER_PREVIOUSSTATE__DigitalInput__, AUTOPOWER_PREVIOUSSTATE );
    
    AUTOPOWER_DISABLE = new Crestron.Logos.SplusObjects.DigitalInput( AUTOPOWER_DISABLE__DigitalInput__, this );
    m_DigitalInputList.Add( AUTOPOWER_DISABLE__DigitalInput__, AUTOPOWER_DISABLE );
    
    SYSTEMPOWER_TOGGLE = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEMPOWER_TOGGLE__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEMPOWER_TOGGLE__DigitalInput__, SYSTEMPOWER_TOGGLE );
    
    SYSTEMPOWER_ON = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEMPOWER_ON__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEMPOWER_ON__DigitalInput__, SYSTEMPOWER_ON );
    
    SYSTEMPOWER_OFF = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEMPOWER_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEMPOWER_OFF__DigitalInput__, SYSTEMPOWER_OFF );
    
    PRESET_SAVE = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_SAVE__DigitalInput__, PRESET_SAVE );
    
    PRESET_LOAD = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_LOAD__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_LOAD__DigitalInput__, PRESET_LOAD );
    
    PRESET_DELETE = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_DELETE__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_DELETE__DigitalInput__, PRESET_DELETE );
    
    ROUTE_DP = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_DP__DigitalInput__, this );
    m_DigitalInputList.Add( ROUTE_DP__DigitalInput__, ROUTE_DP );
    
    ROUTE_DVI = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_DVI__DigitalInput__, this );
    m_DigitalInputList.Add( ROUTE_DVI__DigitalInput__, ROUTE_DVI );
    
    ROUTE_VGA = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_VGA__DigitalInput__, this );
    m_DigitalInputList.Add( ROUTE_VGA__DigitalInput__, ROUTE_VGA );
    
    ROUTE_HDMI = new InOutArray<DigitalInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ROUTE_HDMI[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( ROUTE_HDMI__DigitalInput__ + i, ROUTE_HDMI__DigitalInput__, this );
        m_DigitalInputList.Add( ROUTE_HDMI__DigitalInput__ + i, ROUTE_HDMI[i+1] );
    }
    
    STATUS_POWERON = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_POWERON__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_POWERON__DigitalOutput__, STATUS_POWERON );
    
    STATUS_POWEROFF = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_POWEROFF__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_POWEROFF__DigitalOutput__, STATUS_POWEROFF );
    
    STATUS_PRESET_SAVE = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_PRESET_SAVE__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_PRESET_SAVE__DigitalOutput__, STATUS_PRESET_SAVE );
    
    STATUS_PRESET_LOAD = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_PRESET_LOAD__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_PRESET_LOAD__DigitalOutput__, STATUS_PRESET_LOAD );
    
    STATUS_PRESET_DELETE = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_PRESET_DELETE__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_PRESET_DELETE__DigitalOutput__, STATUS_PRESET_DELETE );
    
    PRESET_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( PRESET_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( PRESET_SELECT__AnalogSerialInput__, PRESET_SELECT );
    
    ZONE_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( ZONE_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( ZONE_SELECT__AnalogSerialInput__, ZONE_SELECT );
    
    BP_CONTROLLER_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( BP_CONTROLLER_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( BP_CONTROLLER_SELECT__AnalogSerialInput__, BP_CONTROLLER_SELECT );
    
    BP_SOURCE_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( BP_SOURCE_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( BP_SOURCE_SELECT__AnalogSerialInput__, BP_SOURCE_SELECT );
    
    STATUS_PRESET_ACTIVE = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_PRESET_ACTIVE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( STATUS_PRESET_ACTIVE__AnalogSerialOutput__, STATUS_PRESET_ACTIVE );
    
    STATUS_ZONE_ACTIVE = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_ZONE_ACTIVE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( STATUS_ZONE_ACTIVE__AnalogSerialOutput__, STATUS_ZONE_ACTIVE );
    
    STATUS_INPUTX_BP = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_INPUTX_BP__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( STATUS_INPUTX_BP__AnalogSerialOutput__, STATUS_INPUTX_BP );
    
    STATUS_INPUTX_ZONE = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        STATUS_INPUTX_ZONE[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_INPUTX_ZONE__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( STATUS_INPUTX_ZONE__AnalogSerialOutput__ + i, STATUS_INPUTX_ZONE[i+1] );
    }
    
    PLANAR_RX = new Crestron.Logos.SplusObjects.StringInput( PLANAR_RX__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( PLANAR_RX__AnalogSerialInput__, PLANAR_RX );
    
    PLANAR_TX = new Crestron.Logos.SplusObjects.StringOutput( PLANAR_TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PLANAR_TX__AnalogSerialOutput__, PLANAR_TX );
    
    __SPLS_TMPVAR__WAITLABEL_22___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_22___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_23___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_23___CallbackFn );
    
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_0, false ) );
    AUTOPOWER_ENABLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUTOPOWER_ENABLE_OnPush_1, false ) );
    AUTOPOWER_PREVIOUSSTATE.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUTOPOWER_PREVIOUSSTATE_OnPush_2, false ) );
    AUTOPOWER_DISABLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( AUTOPOWER_DISABLE_OnPush_3, false ) );
    SYSTEMPOWER_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( SYSTEMPOWER_ON_OnPush_4, false ) );
    SYSTEMPOWER_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( SYSTEMPOWER_OFF_OnPush_5, false ) );
    SYSTEMPOWER_TOGGLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( SYSTEMPOWER_TOGGLE_OnPush_6, false ) );
    PRESET_SAVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_SAVE_OnPush_7, false ) );
    PRESET_LOAD.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_LOAD_OnPush_8, false ) );
    PRESET_DELETE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_DELETE_OnPush_9, false ) );
    ROUTE_DP.OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_DP_OnPush_10, false ) );
    ROUTE_DVI.OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_DVI_OnPush_11, false ) );
    ROUTE_VGA.OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_VGA_OnPush_12, false ) );
    for( uint i = 0; i < 4; i++ )
        ROUTE_HDMI[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( ROUTE_HDMI_OnPush_13, false ) );
        
    PRESET_SELECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( PRESET_SELECT_OnChange_14, false ) );
    ZONE_SELECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( ZONE_SELECT_OnChange_15, false ) );
    BP_CONTROLLER_SELECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( BP_CONTROLLER_SELECT_OnChange_16, false ) );
    BP_SOURCE_SELECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( BP_SOURCE_SELECT_OnChange_17, false ) );
    PLANAR_RX.OnSerialChange.Add( new InputChangeHandlerWrapper( PLANAR_RX_OnChange_18, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_PLANAR_CLARITY_MATRIX_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_22___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_23___Callback;


const uint POLL__DigitalInput__ = 0;
const uint AUTOPOWER_ENABLE__DigitalInput__ = 1;
const uint AUTOPOWER_PREVIOUSSTATE__DigitalInput__ = 2;
const uint AUTOPOWER_DISABLE__DigitalInput__ = 3;
const uint SYSTEMPOWER_TOGGLE__DigitalInput__ = 4;
const uint SYSTEMPOWER_ON__DigitalInput__ = 5;
const uint SYSTEMPOWER_OFF__DigitalInput__ = 6;
const uint PRESET_SAVE__DigitalInput__ = 7;
const uint PRESET_LOAD__DigitalInput__ = 8;
const uint PRESET_DELETE__DigitalInput__ = 9;
const uint ROUTE_DP__DigitalInput__ = 10;
const uint ROUTE_DVI__DigitalInput__ = 11;
const uint ROUTE_VGA__DigitalInput__ = 12;
const uint ROUTE_HDMI__DigitalInput__ = 13;
const uint PRESET_SELECT__AnalogSerialInput__ = 0;
const uint ZONE_SELECT__AnalogSerialInput__ = 1;
const uint BP_CONTROLLER_SELECT__AnalogSerialInput__ = 2;
const uint BP_SOURCE_SELECT__AnalogSerialInput__ = 3;
const uint PLANAR_RX__AnalogSerialInput__ = 4;
const uint STATUS_POWERON__DigitalOutput__ = 0;
const uint STATUS_POWEROFF__DigitalOutput__ = 1;
const uint STATUS_PRESET_SAVE__DigitalOutput__ = 2;
const uint STATUS_PRESET_LOAD__DigitalOutput__ = 3;
const uint STATUS_PRESET_DELETE__DigitalOutput__ = 4;
const uint PLANAR_TX__AnalogSerialOutput__ = 0;
const uint STATUS_PRESET_ACTIVE__AnalogSerialOutput__ = 1;
const uint STATUS_ZONE_ACTIVE__AnalogSerialOutput__ = 2;
const uint STATUS_INPUTX_BP__AnalogSerialOutput__ = 3;
const uint STATUS_INPUTX_ZONE__AnalogSerialOutput__ = 4;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SPLANARCOMMANDS : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  POLL;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  AUTOPOWER;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  AUTOPOWERENABLE;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  AUTOPOWERPREVIOUS;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  AUTOPOWERDISABLE;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  POWER;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  POWERON;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  POWEROFF;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  PRESETSAVE;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  PRESETRECALL;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  PRESETDELETE;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  IR;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  BP;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  CSET;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  CGET;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  INCREMENT;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  DECREMENT;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  ETX;
    
    
    public SPLANARCOMMANDS( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        POLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        AUTOPOWER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        AUTOPOWERENABLE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        AUTOPOWERPREVIOUS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        AUTOPOWERDISABLE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        POWER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        POWERON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        POWEROFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        PRESETSAVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PRESETRECALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PRESETDELETE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        IR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        BP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        CSET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        CGET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        INCREMENT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        DECREMENT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SIRCOMMANDS : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  [] ZONE;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  [] HDMI;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  DP;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  DVI;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  VGA;
    
    
    public SIRCOMMANDS( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        DP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        DVI  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        VGA  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        ZONE  = new CrestronString[ 5 ];
        for( uint i = 0; i < 5; i++ )
            ZONE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        HDMI  = new CrestronString[ 5 ];
        for( uint i = 0; i < 5; i++ )
            HDMI [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
