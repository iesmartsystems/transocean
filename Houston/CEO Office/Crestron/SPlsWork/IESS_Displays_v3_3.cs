using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DISPLAYS_V3_3
{
    public class UserModuleClass_IESS_DISPLAYS_V3_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_TOGGLE;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_ON;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_MUTE_OFF;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_UP;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_VOLUME_DN;
        Crestron.Logos.SplusObjects.DigitalInput DISPLAY_POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.DigitalInput IP_CONNECT;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_ASPECT;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_VOLUME;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_POWER_TIME;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_TYPE;
        Crestron.Logos.SplusObjects.AnalogInput DISPLAY_OBJ;
        Crestron.Logos.SplusObjects.AnalogInput SHARP_PROTOCOL;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput DISPLAY_ID__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput LOGINNAME__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput LOGINPASSWORD__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        Crestron.Logos.SplusObjects.StringInput GENERIC_POWERON;
        Crestron.Logos.SplusObjects.StringInput GENERIC_POWEROFF;
        Crestron.Logos.SplusObjects.StringInput GENERIC_MUTEON;
        Crestron.Logos.SplusObjects.StringInput GENERIC_MUTEOFF;
        Crestron.Logos.SplusObjects.StringInput GENERIC_VOLUMEUP;
        Crestron.Logos.SplusObjects.StringInput GENERIC_VOLUMEDOWN;
        Crestron.Logos.SplusObjects.StringInput GENERIC_HEADER;
        Crestron.Logos.SplusObjects.StringInput GENERIC_FOOTER;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> GENERIC_INPUT;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_POWER_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_POWER_OFF_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_VOLUME_MUTE_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput DISPLAY_VOLUME_MUTE_OFF_FB;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_VOLUME_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_INPUT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_ASPECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAY_LAMP_HRS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX__DOLLAR__;
        SplusTcpClient TCPCLIENT;
        SplusUdpSocket UDPCLIENT;
        SLOCALDISPLAY GLBL_DISPLAY;
        SDISPLAY GLBL_DISPLAY_COMMANDS;
        CrestronString [] INPUT;
        CrestronString [] ASPECT;
        CrestronString [] INPUTPOLLDATA;
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 132;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 134;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 136;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 137;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 139;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 140;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 142;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 143;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 146;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 148;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 149;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 150;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 152;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 153;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 155;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 156;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 157;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 158;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 160;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 168;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 170;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 172;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 173;
                LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                __context__.SourceCodeLine = 174;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 176;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 177;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 179;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 180;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 184;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                __context__.SourceCodeLine = 185;
                LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                } 
            
            __context__.SourceCodeLine = 188;
            LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 189;
            LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
            __context__.SourceCodeLine = 191;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 192;
                LVVOLUMELEVEL = (uint) ( 0 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 193;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 194;
                    LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 195;
            return (uint)( LVVOLUMELEVEL) ; 
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            
            __context__.SourceCodeLine = 199;
            GLBL_DISPLAY . CTXQUEUE  .UpdateValue ( GLBL_DISPLAY . CTXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 200;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , GLBL_DISPLAY.CTXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 202;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_12__" , 10 , __SPLS_TMPVAR__WAITLABEL_12___Callback ) ;
                __context__.SourceCodeLine = 200;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_12___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 205;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , GLBL_DISPLAY . CTXQUEUE )  ) ; 
            __context__.SourceCodeLine = 206;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 208;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 209;
                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                    { 
                    __context__.SourceCodeLine = 211;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                        {
                        __context__.SourceCodeLine = 212;
                        Functions.SocketSend ( TCPCLIENT , LVTEMP ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 214;
                        Functions.SocketSend ( UDPCLIENT , LVTEMP ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 217;
                    TX__DOLLAR__  .UpdateValue ( LVTEMP  ) ; 
                    }
                
                __context__.SourceCodeLine = 218;
                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.DEBUG)  ) ) 
                    {
                    __context__.SourceCodeLine = 219;
                    Trace( "Display TX: {0}", LVTEMP ) ; 
                    }
                
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
        { 
        short LVSTATUS = 0;
        
        
        __context__.SourceCodeLine = 228;
        if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
            { 
            __context__.SourceCodeLine = 230;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                {
                __context__.SourceCodeLine = 231;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( TCPCLIENT ) ) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 234;
                LVSTATUS = (short) ( Functions.SocketUDP_Disable( UDPCLIENT ) ) ; 
                __context__.SourceCodeLine = 235;
                CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 236;
                CONNECT_STATUS_FB  .Value = (ushort) ( 1 ) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 239;
            if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                { 
                __context__.SourceCodeLine = 241;
                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSCONNECTED ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 243;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.IPADDRESS ) > 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( GLBL_DISPLAY.IPPORT > 0 ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 245;
                        if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                            { 
                            __context__.SourceCodeLine = 247;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue != 15))  ) ) 
                                {
                                __context__.SourceCodeLine = 248;
                                LVSTATUS = (short) ( Functions.SocketConnectClient( TCPCLIENT , GLBL_DISPLAY.IPADDRESS , (ushort)( GLBL_DISPLAY.IPPORT ) , (ushort)( 1 ) ) ) ; 
                                }
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 251;
                                LVSTATUS = (short) ( Functions.SocketUDP_Enable( UDPCLIENT , "255.255.255.255" , (ushort)( GLBL_DISPLAY.IPPORT ) ) ) ; 
                                __context__.SourceCodeLine = 252;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTATUS == 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 254;
                                    CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 255;
                                    CONNECT_STATUS_FB  .Value = (ushort) ( 2 ) ; 
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    } 
                
                } 
            
            }
        
        
        }
        
    private CrestronString SHARPPADDING (  SplusExecutionContext __context__, ushort LVPROTOCOL ) 
        { 
        
        __context__.SourceCodeLine = 266;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 2) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 267;
            return ( "0" ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 268;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 3) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 4) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 269;
                return ( " " ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 271;
                return ( " " ) ; 
                }
            
            }
        
        
        return ""; // default return value (none specified in module)
        }
        
    private CrestronString SHARPBUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING , ushort LVPROTOCOL ) 
        { 
        ushort LVCOUNTER = 0;
        
        CrestronString LVCOMMAND;
        CrestronString LVCOMMANDTEMP;
        CrestronString LVCOMMANDPERM;
        CrestronString LVNUMBER;
        CrestronString LVPADDING;
        LVCOMMAND  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVCOMMANDTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVCOMMANDPERM  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVNUMBER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVPADDING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
        
        
        __context__.SourceCodeLine = 277;
        LVCOMMANDPERM  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 278;
        LVCOMMANDTEMP  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 279;
        LVCOMMAND  .UpdateValue ( Functions.Remove ( 4, LVCOMMANDTEMP )  ) ; 
        __context__.SourceCodeLine = 280;
        LVCOMMAND  .UpdateValue ( Functions.Remove ( LVCOMMAND , LVCOMMANDPERM )  ) ; 
        __context__.SourceCodeLine = 281;
        LVNUMBER  .UpdateValue ( LVCOMMANDPERM  ) ; 
        __context__.SourceCodeLine = 282;
        LVPADDING  .UpdateValue ( SHARPPADDING (  __context__ , (ushort)( LVPROTOCOL ))  ) ; 
        __context__.SourceCodeLine = 283;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 3) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 285;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 287;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                __context__.SourceCodeLine = 285;
                } 
            
            __context__.SourceCodeLine = 289;
            LVCOMMAND  .UpdateValue ( LVCOMMAND + LVNUMBER + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 291;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 2) ) || Functions.TestForTrue ( Functions.BoolToInt (LVPROTOCOL == 4) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 293;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + LVNUMBER  ) ; 
                __context__.SourceCodeLine = 294;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)3; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 296;
                    LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                    __context__.SourceCodeLine = 294;
                    } 
                
                __context__.SourceCodeLine = 298;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 302;
                LVPADDING  .UpdateValue ( " "  ) ; 
                __context__.SourceCodeLine = 303;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( Functions.Length( LVNUMBER ) ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)3; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 305;
                    LVCOMMAND  .UpdateValue ( LVCOMMAND + LVPADDING  ) ; 
                    __context__.SourceCodeLine = 303;
                    } 
                
                __context__.SourceCodeLine = 307;
                LVCOMMAND  .UpdateValue ( LVCOMMAND + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 309;
        GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 310;
        GLBL_DISPLAY . COMMANDACK  .UpdateValue ( LVINCOMING  ) ; 
        __context__.SourceCodeLine = 311;
        return ( LVCOMMAND ) ; 
        
        }
        
    private CrestronString BUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 316;
        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . STX + LVINCOMING + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
        __context__.SourceCodeLine = 317;
        return ( LVSTRING ) ; 
        
        }
        
    private CrestronString CHECKSUMBUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
        { 
        ushort LVCHECKSUMTOTAL = 0;
        
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 324;
        LVCHECKSUMTOTAL = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 325;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6))  ) ) 
            { 
            __context__.SourceCodeLine = 327;
            LVCHECKSUMTOTAL = (ushort) ( Byte( LVINCOMING , (int)( 1 ) ) ) ; 
            __context__.SourceCodeLine = 328;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 2 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( LVINCOMING ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 330;
                LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL ^ Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                __context__.SourceCodeLine = 328;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 333;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 7))  ) ) 
                { 
                __context__.SourceCodeLine = 335;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)Functions.Length( LVINCOMING ); 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 337;
                    LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL | Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                    __context__.SourceCodeLine = 335;
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 342;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)Functions.Length( LVINCOMING ); 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 344;
                    LVCHECKSUMTOTAL = (ushort) ( (LVCHECKSUMTOTAL + Byte( LVINCOMING , (int)( LVCOUNTER ) )) ) ; 
                    __context__.SourceCodeLine = 342;
                    } 
                
                __context__.SourceCodeLine = 346;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 348;
                    LVCHECKSUMTOTAL = (ushort) ( (256 - Functions.Low( (ushort) LVCHECKSUMTOTAL )) ) ; 
                    __context__.SourceCodeLine = 349;
                    GLBL_DISPLAY . COMMANDACK  .UpdateValue ( LVINCOMING  ) ; 
                    } 
                
                } 
            
            }
        
        __context__.SourceCodeLine = 352;
        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . STX + LVINCOMING + Functions.Chr (  (int) ( Functions.Low( (ushort) LVCHECKSUMTOTAL ) ) ) + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
        __context__.SourceCodeLine = 353;
        return ( LVSTRING ) ; 
        
        }
        
    private void SENDSTRING (  SplusExecutionContext __context__, ushort LVTYPE , CrestronString LVINCOMING ) 
        { 
        ushort LVCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 361;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVTYPE == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVTYPE == 2) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 362;
            SETQUEUE (  __context__ , SHARPBUILDSTRING( __context__ , LVINCOMING , (ushort)( SHARP_PROTOCOL  .UshortValue ) )) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 363;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVTYPE >= 3 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVTYPE <= 7 ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 364;
                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVINCOMING )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 365;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 15))  ) ) 
                    { 
                    __context__.SourceCodeLine = 367;
                    LVSTRING  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 368;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)16; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 370;
                        LVSTRING  .UpdateValue ( LVSTRING + GLBL_DISPLAY . IPADDRESS  ) ; 
                        __context__.SourceCodeLine = 368;
                        } 
                    
                    __context__.SourceCodeLine = 372;
                    LVSTRING  .UpdateValue ( LVINCOMING + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 373;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 376;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVINCOMING )) ; 
                    }
                
                }
            
            }
        
        
        }
        
    private void RUNINITIALIZATION (  SplusExecutionContext __context__, ushort LVTYPE ) 
        { 
        CrestronString LVID;
        CrestronString LVSTRING;
        LVID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        __context__.SourceCodeLine = 381;
        GLBL_DISPLAY . NID = (ushort) ( Functions.Atoi( DISPLAY_ID__DOLLAR__ ) ) ; 
        __context__.SourceCodeLine = 382;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( DISPLAY_ID__DOLLAR__ ) < 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 383;
            GLBL_DISPLAY . NID = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 384;
        
            {
            int __SPLS_TMPVAR__SWTCH_1__ = ((int)LVTYPE);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 388;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "POWR1"  ) ; 
                    __context__.SourceCodeLine = 389;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POWR0"  ) ; 
                    __context__.SourceCodeLine = 390;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "IAVD1"  ) ; 
                    __context__.SourceCodeLine = 391;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "IAVD2"  ) ; 
                    __context__.SourceCodeLine = 392;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "IAVD3"  ) ; 
                    __context__.SourceCodeLine = 393;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "IAVD4"  ) ; 
                    __context__.SourceCodeLine = 394;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "IAVD5"  ) ; 
                    __context__.SourceCodeLine = 395;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "WIDE8"  ) ; 
                    __context__.SourceCodeLine = 396;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "WIDE1"  ) ; 
                    __context__.SourceCodeLine = 397;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "WIDE2"  ) ; 
                    __context__.SourceCodeLine = 398;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "WIDE3"  ) ; 
                    __context__.SourceCodeLine = 399;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "WIDE10"  ) ; 
                    __context__.SourceCodeLine = 400;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE1"  ) ; 
                    __context__.SourceCodeLine = 401;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE2"  ) ; 
                    __context__.SourceCodeLine = 402;
                    GLBL_DISPLAY_COMMANDS . COMMANDSLEEP  .UpdateValue ( "RSPW1"  ) ; 
                    __context__.SourceCodeLine = 403;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOLM"  ) ; 
                    __context__.SourceCodeLine = 404;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "POWR????"  ) ; 
                    __context__.SourceCodeLine = 405;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "IAVD????"  ) ; 
                    __context__.SourceCodeLine = 406;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOLM????"  ) ; 
                    __context__.SourceCodeLine = 407;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE????"  ) ; 
                    __context__.SourceCodeLine = 408;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 409;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 410;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 411;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 415;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "POWR1"  ) ; 
                    __context__.SourceCodeLine = 416;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POWR0"  ) ; 
                    __context__.SourceCodeLine = 417;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "INPS9"  ) ; 
                    __context__.SourceCodeLine = 418;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "INPS2"  ) ; 
                    __context__.SourceCodeLine = 419;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "INPS3"  ) ; 
                    __context__.SourceCodeLine = 420;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "INPS4"  ) ; 
                    __context__.SourceCodeLine = 421;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "INPS5"  ) ; 
                    __context__.SourceCodeLine = 422;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "WIDE8"  ) ; 
                    __context__.SourceCodeLine = 423;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "WIDE1"  ) ; 
                    __context__.SourceCodeLine = 424;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "WIDE2"  ) ; 
                    __context__.SourceCodeLine = 425;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "WIDE3"  ) ; 
                    __context__.SourceCodeLine = 426;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "WIDE10"  ) ; 
                    __context__.SourceCodeLine = 427;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE1"  ) ; 
                    __context__.SourceCodeLine = 428;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE0"  ) ; 
                    __context__.SourceCodeLine = 429;
                    GLBL_DISPLAY_COMMANDS . COMMANDSLEEP  .UpdateValue ( "RSPW1"  ) ; 
                    __context__.SourceCodeLine = 430;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOLM"  ) ; 
                    __context__.SourceCodeLine = 431;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "POWR????"  ) ; 
                    __context__.SourceCodeLine = 432;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "INPS????"  ) ; 
                    __context__.SourceCodeLine = 433;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOLM????"  ) ; 
                    __context__.SourceCodeLine = 434;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE????"  ) ; 
                    __context__.SourceCodeLine = 435;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 436;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 437;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 438;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 31 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 442;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0008\u0022\u0000\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 443;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0008\u0022\u0000\u0000\u0000\u0001"  ) ; 
                    __context__.SourceCodeLine = 444;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0000"  ) ; 
                    __context__.SourceCodeLine = 445;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0001"  ) ; 
                    __context__.SourceCodeLine = 446;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0002"  ) ; 
                    __context__.SourceCodeLine = 447;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0008\u0022\u000A\u0000\u0005\u0003"  ) ; 
                    __context__.SourceCodeLine = 448;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 449;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0005"  ) ; 
                    __context__.SourceCodeLine = 450;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0004"  ) ; 
                    __context__.SourceCodeLine = 451;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 452;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0006"  ) ; 
                    __context__.SourceCodeLine = 453;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0008\u0022\u000B\u000A\u0001\u0003"  ) ; 
                    __context__.SourceCodeLine = 454;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0008\u0022\u0002\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 455;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0008\u0022\u0002\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 456;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 457;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0008\u0022\u00F0\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 458;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0008\u0022\u00F0\u0004\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 459;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0008\u0022\u00F0\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 460;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0008\u0022\u00F0\u0002\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 461;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 462;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 463;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 464;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 4) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 468;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 469;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 470;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0021"  ) ; 
                    __context__.SourceCodeLine = 471;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0023"  ) ; 
                    __context__.SourceCodeLine = 472;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0018"  ) ; 
                    __context__.SourceCodeLine = 473;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0025"  ) ; 
                    __context__.SourceCodeLine = 474;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0008"  ) ; 
                    __context__.SourceCodeLine = 475;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 476;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u000B"  ) ; 
                    __context__.SourceCodeLine = 477;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0004"  ) ; 
                    __context__.SourceCodeLine = 478;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0031"  ) ; 
                    __context__.SourceCodeLine = 479;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0018" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 480;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 481;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 482;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0012" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0001"  ) ; 
                    __context__.SourceCodeLine = 483;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0011" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 484;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0014" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 485;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0012" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 486;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0013" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0000"  ) ; 
                    __context__.SourceCodeLine = 487;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u00AA"  ) ; 
                    __context__.SourceCodeLine = 488;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 489;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 490;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 5) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 494;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0002\u0000\u0000\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 495;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0002\u0001\u0000\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 496;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A1"  ) ; 
                    __context__.SourceCodeLine = 497;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A2"  ) ; 
                    __context__.SourceCodeLine = 498;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u00A6"  ) ; 
                    __context__.SourceCodeLine = 499;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u0020"  ) ; 
                    __context__.SourceCodeLine = 500;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0002\u0003\u0000\u0000\u0002\u0001\u0001"  ) ; 
                    __context__.SourceCodeLine = 501;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0002\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 502;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0004\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 503;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0006\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 504;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0003\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 505;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0018\u0000\u0000\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 506;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0002\u0012\u0000\u0000\u0000\u0014"  ) ; 
                    __context__.SourceCodeLine = 507;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0002\u0012\u0000\u0000\u0000\u0015"  ) ; 
                    __context__.SourceCodeLine = 508;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0003\u0010\u0000\u0000\u0005\u0005\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 509;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0000\u00BF\u0000\u0000\u0001\u0002\u00C2"  ) ; 
                    __context__.SourceCodeLine = 510;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 511;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 512;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 513;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 514;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 32 ) ; 
                    __context__.SourceCodeLine = 515;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 516;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 517;
                    INPUTPOLLDATA [ 1 ]  .UpdateValue ( "\u0021"  ) ; 
                    __context__.SourceCodeLine = 518;
                    INPUTPOLLDATA [ 2 ]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 519;
                    INPUTPOLLDATA [ 3 ]  .UpdateValue ( "\u0022"  ) ; 
                    __context__.SourceCodeLine = 520;
                    INPUTPOLLDATA [ 4 ]  .UpdateValue ( "\u0020"  ) ; 
                    __context__.SourceCodeLine = 521;
                    INPUTPOLLDATA [ 5 ]  .UpdateValue ( "\u0001"  ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 6) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 525;
                    LVSTRING  .UpdateValue ( "\u0030\u0041\u0030"  ) ; 
                    __context__.SourceCodeLine = 526;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( LVSTRING + "\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 527;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( LVSTRING + "\u0041\u0030\u0043\u0002\u0043\u0032\u0030\u0033\u0044\u0036\u0030\u0030\u0030\u0034\u0003"  ) ; 
                    __context__.SourceCodeLine = 528;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 529;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 530;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0031\u0033\u0003"  ) ; 
                    __context__.SourceCodeLine = 531;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 532;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0030\u0030\u0030\u0030\u0035\u0003"  ) ; 
                    __context__.SourceCodeLine = 533;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0033\u0003"  ) ; 
                    __context__.SourceCodeLine = 534;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 535;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 536;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0034\u0003"  ) ; 
                    __context__.SourceCodeLine = 537;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0032\u0037\u0030\u0030\u0030\u0030\u0042\u0003"  ) ; 
                    __context__.SourceCodeLine = 538;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0038\u0044\u0030\u0030\u0030\u0031\u0003"  ) ; 
                    __context__.SourceCodeLine = 539;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0038\u0044\u0030\u0030\u0030\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 540;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( LVSTRING + "\u0045\u0030\u0041\u0002\u0030\u0030\u0036\u0032\u0030\u0030"  ) ; 
                    __context__.SourceCodeLine = 541;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( LVSTRING + "\u0041\u0030\u0036\u0002\u0030\u0031\u0044\u0036\u0003"  ) ; 
                    __context__.SourceCodeLine = 542;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0030\u0003"  ) ; 
                    __context__.SourceCodeLine = 543;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0036\u0032\u0003"  ) ; 
                    __context__.SourceCodeLine = 544;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( LVSTRING + "\u0043\u0030\u0036\u0002\u0030\u0030\u0038\u0044\u0003"  ) ; 
                    __context__.SourceCodeLine = 545;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0001"  ) ; 
                    __context__.SourceCodeLine = 546;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 547;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 548;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 7) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 552;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u0017\u002E\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 553;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "\u0017\u002F\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 554;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0002"  ) ; 
                    __context__.SourceCodeLine = 555;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 556;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0004"  ) ; 
                    __context__.SourceCodeLine = 557;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0005"  ) ; 
                    __context__.SourceCodeLine = 558;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "\u0000\u0001\u0000\u0000\u0006"  ) ; 
                    __context__.SourceCodeLine = 559;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "\u0000\u0020\u0000\u0000\u000A"  ) ; 
                    __context__.SourceCodeLine = 560;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0009"  ) ; 
                    __context__.SourceCodeLine = 561;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0003"  ) ; 
                    __context__.SourceCodeLine = 562;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0007"  ) ; 
                    __context__.SourceCodeLine = 563;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "\u0000\u0020\u0000\u0000\u0008"  ) ; 
                    __context__.SourceCodeLine = 564;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "\u0000\u0031\u0000\u0000\u0001"  ) ; 
                    __context__.SourceCodeLine = 565;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "\u0000\u0031\u0000\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 566;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "\u0000\u0016\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 567;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "\u0001\u0002\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 568;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "\u0000\u0001\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 569;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "\u0000\u0016\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 570;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "\u0000\u0031\u0001\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 571;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u00A9"  ) ; 
                    __context__.SourceCodeLine = 572;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u009A"  ) ; 
                    __context__.SourceCodeLine = 573;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 574;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 8) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 578;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "ka 01 01"  ) ; 
                    __context__.SourceCodeLine = 579;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "ka 01 00"  ) ; 
                    __context__.SourceCodeLine = 580;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "xb 01 90"  ) ; 
                    __context__.SourceCodeLine = 581;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "xb 01 91"  ) ; 
                    __context__.SourceCodeLine = 582;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "xb 01 92"  ) ; 
                    __context__.SourceCodeLine = 583;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "xb 01 93"  ) ; 
                    __context__.SourceCodeLine = 584;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "xb 01 40"  ) ; 
                    __context__.SourceCodeLine = 585;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "kc 01 02"  ) ; 
                    __context__.SourceCodeLine = 586;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "kc 01 01"  ) ; 
                    __context__.SourceCodeLine = 587;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "kc 01 06"  ) ; 
                    __context__.SourceCodeLine = 588;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "kc 01 09"  ) ; 
                    __context__.SourceCodeLine = 589;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "kc 01 10"  ) ; 
                    __context__.SourceCodeLine = 590;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "ke 01 00"  ) ; 
                    __context__.SourceCodeLine = 591;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "ke 01 01"  ) ; 
                    __context__.SourceCodeLine = 592;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "kf 01 "  ) ; 
                    __context__.SourceCodeLine = 593;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "ka 01 ff"  ) ; 
                    __context__.SourceCodeLine = 594;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "xb 01 ff"  ) ; 
                    __context__.SourceCodeLine = 595;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "kf 01 ff"  ) ; 
                    __context__.SourceCodeLine = 596;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "ke 01 ff"  ) ; 
                    __context__.SourceCodeLine = 597;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 598;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 599;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 600;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 9) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 604;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "PWR ON"  ) ; 
                    __context__.SourceCodeLine = 605;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "PWR OFF"  ) ; 
                    __context__.SourceCodeLine = 606;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "SOURCE A0"  ) ; 
                    __context__.SourceCodeLine = 607;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "SOURCE A1"  ) ; 
                    __context__.SourceCodeLine = 608;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "SOURCE 11"  ) ; 
                    __context__.SourceCodeLine = 609;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "SOURCE 21"  ) ; 
                    __context__.SourceCodeLine = 610;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "SOURCE 42"  ) ; 
                    __context__.SourceCodeLine = 611;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "ASPECT 20"  ) ; 
                    __context__.SourceCodeLine = 612;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "ASPECT 10"  ) ; 
                    __context__.SourceCodeLine = 613;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "ASPECT 30"  ) ; 
                    __context__.SourceCodeLine = 614;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "ASPECT 40"  ) ; 
                    __context__.SourceCodeLine = 615;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "ASPECT 50"  ) ; 
                    __context__.SourceCodeLine = 616;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "MUTE ON"  ) ; 
                    __context__.SourceCodeLine = 617;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "MUTE OFF"  ) ; 
                    __context__.SourceCodeLine = 618;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "VOL "  ) ; 
                    __context__.SourceCodeLine = 619;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "PWR?"  ) ; 
                    __context__.SourceCodeLine = 620;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "SOURCE?"  ) ; 
                    __context__.SourceCodeLine = 621;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "VOL?"  ) ; 
                    __context__.SourceCodeLine = 622;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "MUTE?"  ) ; 
                    __context__.SourceCodeLine = 623;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 624;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 625;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 626;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 30 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 10) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 631;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "PON"  ) ; 
                    __context__.SourceCodeLine = 632;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "POF"  ) ; 
                    __context__.SourceCodeLine = 633;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "IIS:HD1"  ) ; 
                    __context__.SourceCodeLine = 634;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "IIS:HD2"  ) ; 
                    __context__.SourceCodeLine = 635;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "IIS:DVI"  ) ; 
                    __context__.SourceCodeLine = 636;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "IIS:RG1"  ) ; 
                    __context__.SourceCodeLine = 637;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "IIS:RG2"  ) ; 
                    __context__.SourceCodeLine = 638;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "VSE:2"  ) ; 
                    __context__.SourceCodeLine = 639;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "VSE:1"  ) ; 
                    __context__.SourceCodeLine = 640;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "VSE:3"  ) ; 
                    __context__.SourceCodeLine = 641;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "VSE:5"  ) ; 
                    __context__.SourceCodeLine = 642;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "VSE:6"  ) ; 
                    __context__.SourceCodeLine = 643;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "AMT:1"  ) ; 
                    __context__.SourceCodeLine = 644;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "AMT:0"  ) ; 
                    __context__.SourceCodeLine = 645;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "AVL:"  ) ; 
                    __context__.SourceCodeLine = 646;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "QPW"  ) ; 
                    __context__.SourceCodeLine = 647;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "QIN"  ) ; 
                    __context__.SourceCodeLine = 648;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "QAV"  ) ; 
                    __context__.SourceCodeLine = 649;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "QAM"  ) ; 
                    __context__.SourceCodeLine = 650;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0002"  ) ; 
                    __context__.SourceCodeLine = 651;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u0003"  ) ; 
                    __context__.SourceCodeLine = 652;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 653;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 64 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 11) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 657;
                    LVSTRING  .UpdateValue ( "\u00BE\u00EF\u0003\u0006\u0000"  ) ; 
                    __context__.SourceCodeLine = 658;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( LVSTRING + "\u00BA\u00D2\u0001\u0000\u0000\u0060\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 659;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( LVSTRING + "\u002A\u00D3\u0001\u0000\u0000\u0060\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 660;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( LVSTRING + "\u000E\u00D2\u0001\u0000\u0000\u0020\u0003\u0000"  ) ; 
                    __context__.SourceCodeLine = 661;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( LVSTRING + "\u006E\u00D6\u0001\u0000\u0000\u0020\u000D\u0000"  ) ; 
                    __context__.SourceCodeLine = 662;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( LVSTRING + "\u00FE\u00D2\u0001\u0000\u0000\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 663;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( LVSTRING + "\u00AE\u00DE\u0001\u0000\u0000\u0020\u0011\u0000"  ) ; 
                    __context__.SourceCodeLine = 664;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( LVSTRING + "\u006E\u00D3\u0001\u0000\u0000\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 665;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( LVSTRING + "\u000E\u00D1\u0001\u0000\u0008\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 666;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( LVSTRING + "\u009E\u00D0\u0001\u0000\u0008\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 667;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( LVSTRING + "\u003E\u00D6\u0001\u0000\u0008\u0020\u000A\u0000"  ) ; 
                    __context__.SourceCodeLine = 668;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( LVSTRING + "\u005E\u00D7\u0001\u0000\u0008\u0020\u0008\u0000"  ) ; 
                    __context__.SourceCodeLine = 669;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( LVSTRING + "\u009E\u00C4\u0001\u0000\u0008\u0020\u0030\u0000"  ) ; 
                    __context__.SourceCodeLine = 670;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( LVSTRING + "\u00D2\u00D6\u0001\u0000\u0002\u0020\u0001\u0000"  ) ; 
                    __context__.SourceCodeLine = 671;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( LVSTRING + "\u0046\u00D3\u0001\u0000\u0002\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 672;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 673;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( LVSTRING + "\u0019\u00D3\u0002\u0000\u0000\u0060\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 674;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( LVSTRING + "\u00CD\u00D2\u0002\u0000\u0000\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 675;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( LVSTRING + "\u007A\u00C2\u0005\u0000\u0050\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 676;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( LVSTRING + "\u0075\u00D3\u0002\u0000\u0002\u0020\u0000\u0000"  ) ; 
                    __context__.SourceCodeLine = 677;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 678;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 32 ) ; 
                    __context__.SourceCodeLine = 679;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 680;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 681;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLLAMPHOURS  .UpdateValue ( LVSTRING + "\u00C2\u00FF\u0002\u0000\u0090\u0010\u0000\u0000"  ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 12) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 685;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "(PWR 1)"  ) ; 
                    __context__.SourceCodeLine = 686;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "(PWR 0)"  ) ; 
                    __context__.SourceCodeLine = 687;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "(SIN3)"  ) ; 
                    __context__.SourceCodeLine = 688;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "(SIN4)"  ) ; 
                    __context__.SourceCodeLine = 689;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "(SIN6)"  ) ; 
                    __context__.SourceCodeLine = 690;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "(SIN7)"  ) ; 
                    __context__.SourceCodeLine = 691;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "(SIN1)"  ) ; 
                    __context__.SourceCodeLine = 692;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "(SZP1)"  ) ; 
                    __context__.SourceCodeLine = 693;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "(SZP2)"  ) ; 
                    __context__.SourceCodeLine = 694;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "(SZP0)"  ) ; 
                    __context__.SourceCodeLine = 695;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "(SZP3)"  ) ; 
                    __context__.SourceCodeLine = 696;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "(SZP4)"  ) ; 
                    __context__.SourceCodeLine = 697;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 698;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 699;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 700;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "(PWR ?)"  ) ; 
                    __context__.SourceCodeLine = 701;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 702;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 703;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 704;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 705;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 706;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 707;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 13) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 712;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "pow=on"  ) ; 
                    __context__.SourceCodeLine = 713;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "pow=off"  ) ; 
                    __context__.SourceCodeLine = 714;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "sour=hdmi"  ) ; 
                    __context__.SourceCodeLine = 715;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "sour=hdmi2"  ) ; 
                    __context__.SourceCodeLine = 716;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "sour=rgb"  ) ; 
                    __context__.SourceCodeLine = 717;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "sour=rgb2"  ) ; 
                    __context__.SourceCodeLine = 718;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "sour=vid"  ) ; 
                    __context__.SourceCodeLine = 719;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "asp=16:9"  ) ; 
                    __context__.SourceCodeLine = 720;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "asp=4:3"  ) ; 
                    __context__.SourceCodeLine = 721;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "asp=16:10"  ) ; 
                    __context__.SourceCodeLine = 722;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "asp=AUTO"  ) ; 
                    __context__.SourceCodeLine = 723;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "asp=WIDE"  ) ; 
                    __context__.SourceCodeLine = 724;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "mute=on"  ) ; 
                    __context__.SourceCodeLine = 725;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "mute=off"  ) ; 
                    __context__.SourceCodeLine = 726;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "vol="  ) ; 
                    __context__.SourceCodeLine = 727;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "pow=?"  ) ; 
                    __context__.SourceCodeLine = 728;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "sour=?"  ) ; 
                    __context__.SourceCodeLine = 729;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "vol=?"  ) ; 
                    __context__.SourceCodeLine = 730;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "mute=?"  ) ; 
                    __context__.SourceCodeLine = 731;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u000D*"  ) ; 
                    __context__.SourceCodeLine = 732;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "#\u000D"  ) ; 
                    __context__.SourceCodeLine = 733;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 734;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 30 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 14) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 738;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "CPOWR0000000000000001"  ) ; 
                    __context__.SourceCodeLine = 739;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "CPOWR0000000000000000"  ) ; 
                    __context__.SourceCodeLine = 740;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "CINPT0000000100000001"  ) ; 
                    __context__.SourceCodeLine = 741;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "CINPT0000000100000002"  ) ; 
                    __context__.SourceCodeLine = 742;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "CINPT0000000100000003"  ) ; 
                    __context__.SourceCodeLine = 743;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "CINPT0000000600000001"  ) ; 
                    __context__.SourceCodeLine = 744;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "CINPT0000000400000001"  ) ; 
                    __context__.SourceCodeLine = 745;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 746;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 747;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 748;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 749;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 750;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "CAMUT0000000000000001"  ) ; 
                    __context__.SourceCodeLine = 751;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "CAMUT0000000000000000"  ) ; 
                    __context__.SourceCodeLine = 752;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "CVOLU0000000000000"  ) ; 
                    __context__.SourceCodeLine = 753;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "EPOWR################"  ) ; 
                    __context__.SourceCodeLine = 754;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "EINPT################"  ) ; 
                    __context__.SourceCodeLine = 755;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "EVOLU################"  ) ; 
                    __context__.SourceCodeLine = 756;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "EAMUT################"  ) ; 
                    __context__.SourceCodeLine = 757;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "*S"  ) ; 
                    __context__.SourceCodeLine = 758;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000A"  ) ; 
                    __context__.SourceCodeLine = 759;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 760;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 15) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 764;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "\u00FF\u00FF\u00FF\u00FF\u00FF\u00FF"  ) ; 
                    __context__.SourceCodeLine = 765;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 766;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 767;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 768;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 769;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 770;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 771;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 772;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 773;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 774;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 775;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 776;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 777;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 778;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 779;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 780;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 781;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 782;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 783;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 784;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 785;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 786;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 16) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 790;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( "DISPLAY.POWER=1"  ) ; 
                    __context__.SourceCodeLine = 791;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( "DISPLAY.POWER=0"  ) ; 
                    __context__.SourceCodeLine = 792;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( "SOURCE.SELECT=1"  ) ; 
                    __context__.SourceCodeLine = 793;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( "SOURCE.SELECT=2"  ) ; 
                    __context__.SourceCodeLine = 794;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( "SOURCE.SELECT=3"  ) ; 
                    __context__.SourceCodeLine = 795;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( "SOURCE.SELECT=4"  ) ; 
                    __context__.SourceCodeLine = 796;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( "SOURCE.SELECT=5"  ) ; 
                    __context__.SourceCodeLine = 797;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( "ASPECT=0"  ) ; 
                    __context__.SourceCodeLine = 798;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( "ASPECT=1"  ) ; 
                    __context__.SourceCodeLine = 799;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( "ASPECT=3"  ) ; 
                    __context__.SourceCodeLine = 800;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( "ASPECT=4"  ) ; 
                    __context__.SourceCodeLine = 801;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( "ASPECT=5"  ) ; 
                    __context__.SourceCodeLine = 802;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( "AUDIO.MUTE=1"  ) ; 
                    __context__.SourceCodeLine = 803;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( "AUDIO.MUTE=0"  ) ; 
                    __context__.SourceCodeLine = 804;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( "AUDIO.VOLUME="  ) ; 
                    __context__.SourceCodeLine = 805;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( "DISPLAY.POWER?"  ) ; 
                    __context__.SourceCodeLine = 806;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( "SOURCE.SELECT?"  ) ; 
                    __context__.SourceCodeLine = 807;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( "AUDIO.VOLUME?"  ) ; 
                    __context__.SourceCodeLine = 808;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( "AUDIO.MUTE?"  ) ; 
                    __context__.SourceCodeLine = 809;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 810;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                    __context__.SourceCodeLine = 811;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 812;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 19) ) ) ) 
                    { 
                    __context__.SourceCodeLine = 817;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWERON  .UpdateValue ( GENERIC_POWERON  ) ; 
                    __context__.SourceCodeLine = 818;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOWEROFF  .UpdateValue ( GENERIC_POWEROFF  ) ; 
                    __context__.SourceCodeLine = 819;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  .UpdateValue ( GENERIC_INPUT [ 1 ]  ) ; 
                    __context__.SourceCodeLine = 820;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  .UpdateValue ( GENERIC_INPUT [ 2 ]  ) ; 
                    __context__.SourceCodeLine = 821;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  .UpdateValue ( GENERIC_INPUT [ 3 ]  ) ; 
                    __context__.SourceCodeLine = 822;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  .UpdateValue ( GENERIC_INPUT [ 4 ]  ) ; 
                    __context__.SourceCodeLine = 823;
                    GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  .UpdateValue ( GENERIC_INPUT [ 5 ]  ) ; 
                    __context__.SourceCodeLine = 824;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 825;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 826;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 827;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 828;
                    GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 829;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEON  .UpdateValue ( GENERIC_MUTEON  ) ; 
                    __context__.SourceCodeLine = 830;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMEMUTEOFF  .UpdateValue ( GENERIC_MUTEOFF  ) ; 
                    __context__.SourceCodeLine = 831;
                    GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 832;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 833;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 834;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 835;
                    GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 836;
                    GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( GENERIC_HEADER  ) ; 
                    __context__.SourceCodeLine = 837;
                    GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( GENERIC_FOOTER  ) ; 
                    __context__.SourceCodeLine = 838;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMIN = (short) ( 0 ) ; 
                    __context__.SourceCodeLine = 839;
                    GLBL_DISPLAY_COMMANDS . VOLUMEMAX = (short) ( 100 ) ; 
                    } 
                
                } 
                
            }
            
        
        __context__.SourceCodeLine = 843;
        INPUT [ 1 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT1  ) ; 
        __context__.SourceCodeLine = 844;
        INPUT [ 2 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT2  ) ; 
        __context__.SourceCodeLine = 845;
        INPUT [ 3 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT3  ) ; 
        __context__.SourceCodeLine = 846;
        INPUT [ 4 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT4  ) ; 
        __context__.SourceCodeLine = 847;
        INPUT [ 5 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDINPUT5  ) ; 
        __context__.SourceCodeLine = 848;
        ASPECT [ 1 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT1  ) ; 
        __context__.SourceCodeLine = 849;
        ASPECT [ 2 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT2  ) ; 
        __context__.SourceCodeLine = 850;
        ASPECT [ 3 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT3  ) ; 
        __context__.SourceCodeLine = 851;
        ASPECT [ 4 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT4  ) ; 
        __context__.SourceCodeLine = 852;
        ASPECT [ 5 ]  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDASPECT5  ) ; 
        
        }
        
    private void SETVOLUME (  SplusExecutionContext __context__, short LVVOL , ushort LVDISPLAY ) 
        { 
        CrestronString LVSTRING;
        CrestronString LVTEMP;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
        
        
        __context__.SourceCodeLine = 857;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 2) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 859;
            LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
            __context__.SourceCodeLine = 860;
            SETQUEUE (  __context__ , SHARPBUILDSTRING( __context__ , LVSTRING , (ushort)( SHARP_PROTOCOL  .UshortValue ) )) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 862;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 3) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 4) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 864;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.Chr (  (int) ( LVVOL ) )  ) ; 
                __context__.SourceCodeLine = 865;
                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 867;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 10) ) || Functions.TestForTrue ( Functions.BoolToInt (LVDISPLAY == 14) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 869;
                    MakeString ( LVTEMP , "{0:d3}", (short)LVVOL) ; 
                    __context__.SourceCodeLine = 870;
                    LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP  ) ; 
                    __context__.SourceCodeLine = 871;
                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 873;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 8))  ) ) 
                        { 
                        __context__.SourceCodeLine = 875;
                        MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                        __context__.SourceCodeLine = 876;
                        LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP  ) ; 
                        __context__.SourceCodeLine = 877;
                        SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 879;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 5))  ) ) 
                            { 
                            __context__.SourceCodeLine = 881;
                            MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                            __context__.SourceCodeLine = 882;
                            LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP + "\u0000"  ) ; 
                            __context__.SourceCodeLine = 883;
                            SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 885;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 6))  ) ) 
                                { 
                                __context__.SourceCodeLine = 887;
                                MakeString ( LVTEMP , "{0:X2}", LVVOL) ; 
                                __context__.SourceCodeLine = 888;
                                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.Mid ( LVTEMP ,  (int) ( 1 ) ,  (int) ( 1 ) ) + Functions.Mid ( LVTEMP ,  (int) ( 2 ) ,  (int) ( 1 ) ) + "\u0003"  ) ; 
                                __context__.SourceCodeLine = 889;
                                SETQUEUE (  __context__ , CHECKSUMBUILDSTRING( __context__ , LVSTRING )) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 891;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVDISPLAY == 9))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 893;
                                    MakeString ( LVTEMP , "{0:d2}", (short)LVVOL) ; 
                                    __context__.SourceCodeLine = 894;
                                    LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + LVTEMP  ) ; 
                                    __context__.SourceCodeLine = 895;
                                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 899;
                                    LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDVOLUMELEVEL + Functions.ItoA (  (int) ( LVVOL ) )  ) ; 
                                    __context__.SourceCodeLine = 900;
                                    SETQUEUE (  __context__ , BUILDSTRING( __context__ , LVSTRING )) ; 
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        CrestronString LVSTRING;
        CrestronString LVRECEIVETEST;
        CrestronString LVTRASH;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVRECEIVETEST  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        ushort LVCOUNTER = 0;
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 909;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 923 ))  ) ) 
            {
            __context__.SourceCodeLine = 910;
            GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
            }
        
        __context__.SourceCodeLine = 911;
        if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.DEBUG)  ) ) 
            {
            __context__.SourceCodeLine = 912;
            Trace( "Display RX: {0}", GLBL_DISPLAY . CRXQUEUE ) ; 
            }
        
        __context__.SourceCodeLine = 914;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 916;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Login:" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 918;
                GLBL_DISPLAY . IPLOGIN = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 919;
                LVSTRING  .UpdateValue ( LOGINNAME__DOLLAR__ + "\u000D"  ) ; 
                __context__.SourceCodeLine = 920;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 922;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Password:" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 924;
                    LVSTRING  .UpdateValue ( LOGINPASSWORD__DOLLAR__ + "\u000D"  ) ; 
                    __context__.SourceCodeLine = 925;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 927;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 929;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1))  ) ) 
                    { 
                    __context__.SourceCodeLine = 931;
                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                    __context__.SourceCodeLine = 932;
                    LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 934;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2))  ) ) 
                        { 
                        __context__.SourceCodeLine = 936;
                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 937;
                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 940;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "0"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 943;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                        { 
                        __context__.SourceCodeLine = 945;
                        GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                        __context__.SourceCodeLine = 946;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 947;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 948;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 951;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                            { 
                            __context__.SourceCodeLine = 953;
                            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                            __context__.SourceCodeLine = 954;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 955;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 956;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 959;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                { 
                                __context__.SourceCodeLine = 961;
                                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 962;
                                LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                __context__.SourceCodeLine = 963;
                                GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                __context__.SourceCodeLine = 964;
                                DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                __context__.SourceCodeLine = 965;
                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 967;
                                    GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                    } 
                                
                                } 
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 972;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "1"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 975;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                            { 
                            __context__.SourceCodeLine = 977;
                            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                            __context__.SourceCodeLine = 978;
                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 979;
                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 980;
                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 983;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                { 
                                __context__.SourceCodeLine = 985;
                                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 986;
                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 987;
                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 988;
                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 991;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 993;
                                    GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                    __context__.SourceCodeLine = 994;
                                    LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                    __context__.SourceCodeLine = 995;
                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                    __context__.SourceCodeLine = 996;
                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                    __context__.SourceCodeLine = 997;
                                    if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 999;
                                        GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1003;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1005;
                                        GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                        __context__.SourceCodeLine = 1006;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 1007;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1010;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "OK" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1012;
                            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSCONNECTREQUEST)  ) ) 
                                {
                                __context__.SourceCodeLine = 1013;
                                GLBL_DISPLAY . IPLOGIN = (ushort) ( 1 ) ; 
                                }
                            
                            __context__.SourceCodeLine = 1014;
                            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.COMMANDCONFIRM)  ) ) 
                                { 
                                __context__.SourceCodeLine = 1016;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1018;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1019;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1020;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1022;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1024;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1025;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1026;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1028;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1030;
                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1031;
                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1032;
                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1034;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1036;
                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1037;
                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1038;
                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 1042;
                                                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                                ushort __FN_FOREND_VAL__1 = (ushort)5; 
                                                int __FN_FORSTEP_VAL__1 = (int)1; 
                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                                    { 
                                                    __context__.SourceCodeLine = 1044;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == INPUT[ LVCOUNTER ]))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1046;
                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                        __context__.SourceCodeLine = 1047;
                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                        __context__.SourceCodeLine = 1048;
                                                        break ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 1042;
                                                    } 
                                                
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1052;
                                GLBL_DISPLAY . COMMANDACK  .UpdateValue ( ""  ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1054;
                            GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1056;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "ERR" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1058;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1060;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                __context__.SourceCodeLine = 1062;
                                if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.COMMANDCONFIRM)  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1065;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1067;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1069;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1071;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 1074;
                                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 1075;
                                GLBL_DISPLAY . COMMANDACK  .UpdateValue ( ""  ) ; 
                                __context__.SourceCodeLine = 1076;
                                GLBL_DISPLAY . COMMANDCONFIRM = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                { 
                                __context__.SourceCodeLine = 1081;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1083;
                                    GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                    __context__.SourceCodeLine = 1084;
                                    LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                    __context__.SourceCodeLine = 1085;
                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                    __context__.SourceCodeLine = 1086;
                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                    __context__.SourceCodeLine = 1087;
                                    if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1089;
                                        GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1093;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1095;
                                        GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( ""  ) ; 
                                        __context__.SourceCodeLine = 1096;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 1097;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 927;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1103;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
                { 
                __context__.SourceCodeLine = 1105;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000\u0000\u000E" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1107;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000\u0000\u000E" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                    __context__.SourceCodeLine = 1108;
                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 1109;
                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 1110;
                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1112;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1114;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0000\u0000\u0000\u00F1\u0005\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1115;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1116;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1117;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1119;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1121;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                            __context__.SourceCodeLine = 1122;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1123;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1124;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1126;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1128;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1\u0003\u000C\u00F5\u0008\u00F0\u0002\u0000\u0000\u00F1\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1129;
                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1130;
                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1131;
                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1133;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u000C\u00F1" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1135;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u000C\u00F1" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1136;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWERON))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1138;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1139;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1140;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1142;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDACK == GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1144;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1145;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1146;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1149;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.CRXQUEUE == "\u0000"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1151;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1152;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1153;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1154;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 1156;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1157;
                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1160;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 4))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1162;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0011\u0000"  ) ; 
                    __context__.SourceCodeLine = 1163;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1165;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1166;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1167;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1168;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1169;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_VOLUME_MUTE_ON_FB  .Value == 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1171;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1172;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1173;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1176;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0011\u0001"  ) ; 
                    __context__.SourceCodeLine = 1177;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1179;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1180;
                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1181;
                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1182;
                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1184;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0012"  ) ; 
                    __context__.SourceCodeLine = 1185;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1187;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1188;
                        LVVOL = (short) ( Byte( GLBL_DISPLAY.CRXQUEUE , (int)( 1 ) ) ) ; 
                        __context__.SourceCodeLine = 1189;
                        GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                        __context__.SourceCodeLine = 1190;
                        DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                        __context__.SourceCodeLine = 1191;
                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1193;
                            GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 1195;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_VOLUME_MUTE_ON_FB  .Value == 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1197;
                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 1198;
                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 1199;
                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1202;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0013\u0000"  ) ; 
                    __context__.SourceCodeLine = 1203;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1205;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1206;
                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1207;
                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1208;
                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1210;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0013\u0001"  ) ; 
                    __context__.SourceCodeLine = 1211;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1213;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1214;
                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 1215;
                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 1216;
                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 1218;
                    LVRECEIVETEST  .UpdateValue ( "\u00AA\u00FF" + Functions.Chr (  (int) ( GLBL_DISPLAY.NID ) ) + "\u0003\u0041\u0014"  ) ; 
                    __context__.SourceCodeLine = 1219;
                    if ( Functions.TestForTrue  ( ( Functions.Find( LVRECEIVETEST , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1221;
                        LVTRASH  .UpdateValue ( Functions.Remove ( LVRECEIVETEST , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 1222;
                        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                        ushort __FN_FOREND_VAL__2 = (ushort)5; 
                        int __FN_FORSTEP_VAL__2 = (int)1; 
                        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                            { 
                            __context__.SourceCodeLine = 1224;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Left( GLBL_DISPLAY.CRXQUEUE , (int)( 1 ) ) == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 1 ) )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1226;
                                DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                __context__.SourceCodeLine = 1227;
                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                } 
                            
                            __context__.SourceCodeLine = 1222;
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1231;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 1232;
                        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1235;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 5))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1237;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0020\u00BF" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1239;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0010\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1241;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0010\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1242;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 1 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1243;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVRECEIVETEST == "\u0000") ) || Functions.TestForTrue ( Functions.BoolToInt (LVRECEIVETEST == "\u000F") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1245;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1246;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1247;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1249;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0004"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1251;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1252;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1253;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1255;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0005"))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1257;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1258;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1259;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 2 ) ; 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1261;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 4 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1262;
                                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                                ushort __FN_FOREND_VAL__3 = (ushort)5; 
                                int __FN_FORSTEP_VAL__3 = (int)1; 
                                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                                    { 
                                    __context__.SourceCodeLine = 1264;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (INPUTPOLLDATA[ LVCOUNTER ] == LVRECEIVETEST))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1266;
                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                        __context__.SourceCodeLine = 1267;
                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 1262;
                                    } 
                                
                                __context__.SourceCodeLine = 1270;
                                LVRECEIVETEST  .UpdateValue ( Functions.Mid ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 7 ) ,  (int) ( 1 ) )  ) ; 
                                __context__.SourceCodeLine = 1271;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0000"))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1273;
                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1274;
                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1275;
                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1277;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVRECEIVETEST == "\u0001"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1279;
                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1280;
                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1281;
                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 1285;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                            {
                            __context__.SourceCodeLine = 1286;
                            GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1289;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1291;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1293;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                __context__.SourceCodeLine = 1294;
                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 1295;
                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 1296;
                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1298;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1300;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1301;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1302;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1303;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1305;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1307;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1308;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1309;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1310;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1312;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "0200D60000040004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1314;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "0200D60000040004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1315;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1316;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1317;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1319;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "3D60001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1321;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "3D60001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                __context__.SourceCodeLine = 1322;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1323;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1324;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1326;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "3D60002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1328;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "3D60002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    __context__.SourceCodeLine = 1329;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1330;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1331;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1333;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "3D60003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1335;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "3D60003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1336;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1337;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1338;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1340;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "3D60004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1342;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "3D60004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1343;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1344;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1345;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1347;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "00008D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1349;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "00008D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1352;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1354;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "000060" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1356;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "000060" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1358;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1360;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "000062" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1362;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "000062" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1364;
                                                                        LVVOL = (short) ( Byte( GLBL_DISPLAY.CRXQUEUE , (int)( 1 ) ) ) ; 
                                                                        __context__.SourceCodeLine = 1365;
                                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                        __context__.SourceCodeLine = 1366;
                                                                        DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                        __context__.SourceCodeLine = 1367;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1369;
                                                                            GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                            } 
                                                                        
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            __context__.SourceCodeLine = 1372;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 1373;
                                GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1376;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 7))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1378;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1380;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                    __context__.SourceCodeLine = 1381;
                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                    __context__.SourceCodeLine = 1382;
                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 1383;
                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1385;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1387;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1388;
                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 1389;
                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                        __context__.SourceCodeLine = 1390;
                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1392;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0005" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1394;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0005" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1395;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1396;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1397;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1399;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0006" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1401;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0006" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                __context__.SourceCodeLine = 1402;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1403;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1404;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1406;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0007" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1408;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0007" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    __context__.SourceCodeLine = 1409;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1410;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1411;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1413;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0008" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1415;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0008" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1416;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1417;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1418;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1420;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0001" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1422;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0001" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1423;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1424;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1425;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1427;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1429;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1430;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1431;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1432;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1434;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0001\u0002\u0002\u0000\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1436;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0001\u0002\u0002\u0000\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1437;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1438;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1439;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1441;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1443;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1444;
                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1445;
                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1446;
                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1448;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0031\u0002\u0000\u0001\u0033" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1450;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0031\u0002\u0000\u0000\u0033" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1451;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1452;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1453;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1455;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0005" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1457;
                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0005" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                __context__.SourceCodeLine = 1458;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                __context__.SourceCodeLine = 1459;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1461;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0004" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1463;
                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0004" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                    __context__.SourceCodeLine = 1464;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 1465;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1467;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1469;
                                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                        __context__.SourceCodeLine = 1470;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                                        __context__.SourceCodeLine = 1471;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1473;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0001\u0002\u0000\u0002" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 1475;
                                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0001\u0002\u0000\u0002" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                            __context__.SourceCodeLine = 1476;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                                            __context__.SourceCodeLine = 1477;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 1479;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u00A9\u0000\u0016\u0002\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 1481;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u00A9\u0000\u0016\u0002\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                                __context__.SourceCodeLine = 1482;
                                                                                                LVSTRING  .UpdateValue ( ""  ) ; 
                                                                                                __context__.SourceCodeLine = 1483;
                                                                                                LVSTRING  .UpdateValue ( Functions.Left ( GLBL_DISPLAY . CRXQUEUE ,  (int) ( 1 ) )  ) ; 
                                                                                                __context__.SourceCodeLine = 1484;
                                                                                                LVVOL = (short) ( Functions.HextoI( LVSTRING ) ) ; 
                                                                                                __context__.SourceCodeLine = 1485;
                                                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                                                __context__.SourceCodeLine = 1486;
                                                                                                DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                                                __context__.SourceCodeLine = 1487;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 1489;
                                                                                                    GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                __context__.SourceCodeLine = 1492;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 1493;
                                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1496;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 8))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1498;
                                    while ( Functions.TestForTrue  ( ( Functions.Find( "x" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1500;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "x" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                        __context__.SourceCodeLine = 1501;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 1502;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "a 01 OK00" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1504;
                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 1505;
                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 1506;
                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1508;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "a 01 OK01" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1510;
                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 1511;
                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 1512;
                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1514;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "e 01 OK01" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1516;
                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1517;
                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1518;
                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1520;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "e 01 OK00" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1522;
                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1523;
                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1524;
                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1526;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "f 01 OK" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1528;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "f 01 OK" , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1529;
                                                            LVVOL = (short) ( Functions.HextoI( LVSTRING ) ) ; 
                                                            __context__.SourceCodeLine = 1530;
                                                            GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                            __context__.SourceCodeLine = 1531;
                                                            DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                            __context__.SourceCodeLine = 1532;
                                                            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1534;
                                                                GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                } 
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1537;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "b 01 OK" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1539;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "b 01 OK" , LVSTRING )  ) ; 
                                                                __context__.SourceCodeLine = 1540;
                                                                ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                                                                ushort __FN_FOREND_VAL__4 = (ushort)5; 
                                                                int __FN_FORSTEP_VAL__4 = (int)1; 
                                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__4) && (LVCOUNTER  <= __FN_FOREND_VAL__4) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__4) && (LVCOUNTER  >= __FN_FOREND_VAL__4) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__4) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1542;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 2 ) )))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1544;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                                        __context__.SourceCodeLine = 1545;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                                        } 
                                                                    
                                                                    __context__.SourceCodeLine = 1540;
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        __context__.SourceCodeLine = 1498;
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 1552;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 9))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 1554;
                                        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1556;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                            __context__.SourceCodeLine = 1557;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 1558;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ESC/VP.net" , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 1559;
                                                GLBL_DISPLAY . IPLOGIN = (ushort) ( 1 ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 1560;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PWR" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1562;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "PWR" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 1563;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "01" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "02" , LVSTRING ) )) ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1565;
                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 1566;
                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 1567;
                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1569;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "00" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "03" , LVSTRING ) )) ) ) || Functions.TestForTrue ( Functions.Find( "04" , LVSTRING ) )) ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1571;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1572;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1573;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1576;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "SOURCE" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1578;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "SOURCE" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 1579;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "A0" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1581;
                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1582;
                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1584;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "A1" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1586;
                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                            __context__.SourceCodeLine = 1587;
                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1589;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "11" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1591;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                __context__.SourceCodeLine = 1592;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1594;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "21" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1596;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                    __context__.SourceCodeLine = 1597;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1599;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "42" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1601;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                        __context__.SourceCodeLine = 1602;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1605;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOL" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1607;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "VOL" , LVSTRING )  ) ; 
                                                        __context__.SourceCodeLine = 1608;
                                                        LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                        __context__.SourceCodeLine = 1609;
                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                        __context__.SourceCodeLine = 1610;
                                                        DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                        __context__.SourceCodeLine = 1611;
                                                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1613;
                                                            GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                            } 
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1616;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1618;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "MUTE" , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1619;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ON" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1621;
                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1622;
                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1623;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1625;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "OFF" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1627;
                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1628;
                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1629;
                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            __context__.SourceCodeLine = 1554;
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 1635;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 1637;
                                            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "\u0003" , GLBL_DISPLAY.CRXQUEUE ) ) || Functions.TestForTrue ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1639;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1640;
                                                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1641;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 1642;
                                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0003" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        }
                                                    
                                                    }
                                                
                                                __context__.SourceCodeLine = 1643;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 1644;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0002" , LVSTRING ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1645;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0002" , LVSTRING )  ) ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 1647;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "000"))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1650;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1652;
                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 1653;
                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 1654;
                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1657;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1659;
                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1660;
                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1661;
                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1664;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1666;
                                                                LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                __context__.SourceCodeLine = 1667;
                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                __context__.SourceCodeLine = 1668;
                                                                DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                __context__.SourceCodeLine = 1669;
                                                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1671;
                                                                    GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1676;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "001"))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1679;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1681;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1682;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1683;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1686;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1688;
                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1689;
                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1690;
                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1693;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1695;
                                                                    LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                    __context__.SourceCodeLine = 1696;
                                                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                    __context__.SourceCodeLine = 1697;
                                                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                    __context__.SourceCodeLine = 1698;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1700;
                                                                        GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1704;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POF" , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1706;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1707;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1708;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1710;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PON" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1712;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1713;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1714;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                { 
                                                                __context__.SourceCodeLine = 1719;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1721;
                                                                    LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                    __context__.SourceCodeLine = 1722;
                                                                    GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                    __context__.SourceCodeLine = 1723;
                                                                    DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                    __context__.SourceCodeLine = 1724;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1726;
                                                                        GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1731;
                                                                    ushort __FN_FORSTART_VAL__5 = (ushort) ( 1 ) ;
                                                                    ushort __FN_FOREND_VAL__5 = (ushort)5; 
                                                                    int __FN_FORSTEP_VAL__5 = (int)1; 
                                                                    for ( LVCOUNTER  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__5) && (LVCOUNTER  <= __FN_FOREND_VAL__5) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__5) && (LVCOUNTER  >= __FN_FOREND_VAL__5) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__5) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1733;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == Functions.Right( INPUT[ LVCOUNTER ] , (int)( 3 ) )))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1735;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( LVCOUNTER ) ; 
                                                                            __context__.SourceCodeLine = 1736;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( LVCOUNTER ) ; 
                                                                            } 
                                                                        
                                                                        __context__.SourceCodeLine = 1731;
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                __context__.SourceCodeLine = 1637;
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 1744;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 11))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 1746;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u001D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1748;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1750;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1752;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1753;
                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                            __context__.SourceCodeLine = 1754;
                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                            __context__.SourceCodeLine = 1755;
                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1757;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0002\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1759;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0002\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1760;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1761;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1762;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1764;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1766;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1767;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1768;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1769;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1772;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1774;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1776;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1777;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                __context__.SourceCodeLine = 1778;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1780;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1782;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                    __context__.SourceCodeLine = 1783;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                    __context__.SourceCodeLine = 1784;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1786;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0003\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1788;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0003\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1789;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1790;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1792;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1794;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u000D\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1795;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                            __context__.SourceCodeLine = 1796;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1798;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1800;
                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u000B\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1802;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0011\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1804;
                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0011\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                                    __context__.SourceCodeLine = 1805;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                    __context__.SourceCodeLine = 1806;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                    } 
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1809;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME))  ) ) 
                                                                { 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1812;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1814;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0000\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1816;
                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0000\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 1817;
                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1818;
                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1819;
                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1821;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0001\u0000" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1823;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0001\u0000" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                            __context__.SourceCodeLine = 1824;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1825;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1826;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1829;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_DISPLAY.COMMANDPOLL == GLBL_DISPLAY_COMMANDS.COMMANDPOLLLAMPHOURS))  ) ) 
                                                                        { 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                __context__.SourceCodeLine = 1833;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 32 ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 1834;
                                                    GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( ""  ) ; 
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 1837;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 12))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 1839;
                                                    while ( Functions.TestForTrue  ( ( Functions.Find( ")" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1841;
                                                        LVSTRING  .UpdateValue ( Functions.Remove ( ")" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                        __context__.SourceCodeLine = 1842;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "(SST!003 " , LVSTRING ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1844;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "(SST!003 " , LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1845;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI 1" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1847;
                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1848;
                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1850;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI 2" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1852;
                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                    __context__.SourceCodeLine = 1853;
                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1855;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "DisplayPort" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1857;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                        __context__.SourceCodeLine = 1858;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1860;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Component" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1862;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                            __context__.SourceCodeLine = 1863;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1865;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "VGA" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1867;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                __context__.SourceCodeLine = 1868;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                } 
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 1871;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!0)" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1873;
                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                __context__.SourceCodeLine = 1874;
                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                __context__.SourceCodeLine = 1875;
                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1877;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!10)" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1879;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1880;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1881;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1883;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "(0-1,0)" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1885;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1886;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1887;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1889;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "(PWR!1)" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1891;
                                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1892;
                                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1893;
                                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        __context__.SourceCodeLine = 1839;
                                                        } 
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 1898;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 13))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 1900;
                                                        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1902;
                                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                            __context__.SourceCodeLine = 1903;
                                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                            __context__.SourceCodeLine = 1904;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*pow=" , LVSTRING ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1906;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "POW=ON" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1908;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1909;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1910;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1912;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "POW=OFF" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1914;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1915;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 1916;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 1919;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*sour=" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1921;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI2" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1923;
                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                        __context__.SourceCodeLine = 1924;
                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1926;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1928;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1929;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1931;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "RGB2" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1933;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                __context__.SourceCodeLine = 1934;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 1936;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "RGB" , LVSTRING ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1938;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 1939;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 1941;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VID" , LVSTRING ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 1943;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                        __context__.SourceCodeLine = 1944;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 1946;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "NETWORK" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 1948;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 6 ) ; 
                                                                                            __context__.SourceCodeLine = 1949;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 6 ) ; 
                                                                                            } 
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1952;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*mute=" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1954;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE=ON" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1956;
                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 1957;
                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 1958;
                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 1960;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "MUTE=OFF" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1962;
                                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                __context__.SourceCodeLine = 1963;
                                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                __context__.SourceCodeLine = 1964;
                                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                } 
                                                                            
                                                                            }
                                                                        
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 1967;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*vol=" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 1969;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "*vol=" , LVSTRING )  ) ; 
                                                                            __context__.SourceCodeLine = 1970;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*VOL=" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 1972;
                                                                                LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                __context__.SourceCodeLine = 1973;
                                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                                __context__.SourceCodeLine = 1974;
                                                                                DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                                __context__.SourceCodeLine = 1975;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 1977;
                                                                                    GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                                    } 
                                                                                
                                                                                } 
                                                                            
                                                                            } 
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 1900;
                                                            } 
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 1984;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 14))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 1986;
                                                            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000A" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 1988;
                                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u000A" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                __context__.SourceCodeLine = 1989;
                                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                __context__.SourceCodeLine = 1990;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNPOWR0000000000000001" , LVSTRING ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 1992;
                                                                    DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                    __context__.SourceCodeLine = 1993;
                                                                    DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                    __context__.SourceCodeLine = 1994;
                                                                    GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 1996;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNPOWR0000000000000000" , LVSTRING ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 1998;
                                                                        DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                        __context__.SourceCodeLine = 1999;
                                                                        DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                        __context__.SourceCodeLine = 2000;
                                                                        GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 2002;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000001" , LVSTRING ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 2004;
                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 2005;
                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 2007;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000002" , LVSTRING ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 2009;
                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                                __context__.SourceCodeLine = 2010;
                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 2012;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000100000003" , LVSTRING ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 2014;
                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                    __context__.SourceCodeLine = 2015;
                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 2017;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000600000001" , LVSTRING ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 2019;
                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                        __context__.SourceCodeLine = 2020;
                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 2022;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SNINPT0000000400000001" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 2024;
                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                            __context__.SourceCodeLine = 2025;
                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 2027;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SNVOLU" , LVSTRING ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 2029;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "*SNVOLU" , LVSTRING )  ) ; 
                                                                                                __context__.SourceCodeLine = 2030;
                                                                                                LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                __context__.SourceCodeLine = 2031;
                                                                                                GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                                                __context__.SourceCodeLine = 2032;
                                                                                                DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                                                __context__.SourceCodeLine = 2033;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2035;
                                                                                                    GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            else 
                                                                                                {
                                                                                                __context__.SourceCodeLine = 2038;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SNAMUT0000000000000001" , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2040;
                                                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                    __context__.SourceCodeLine = 2041;
                                                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                    __context__.SourceCodeLine = 2042;
                                                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                                                    } 
                                                                                                
                                                                                                else 
                                                                                                    {
                                                                                                    __context__.SourceCodeLine = 2044;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SNAMUT0000000000000000" , LVSTRING ))  ) ) 
                                                                                                        { 
                                                                                                        __context__.SourceCodeLine = 2046;
                                                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                        __context__.SourceCodeLine = 2047;
                                                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                        __context__.SourceCodeLine = 2048;
                                                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                                        } 
                                                                                                    
                                                                                                    else 
                                                                                                        {
                                                                                                        __context__.SourceCodeLine = 2050;
                                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SAPOWR0000000000000001" , LVSTRING ))  ) ) 
                                                                                                            { 
                                                                                                            __context__.SourceCodeLine = 2052;
                                                                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                            __context__.SourceCodeLine = 2053;
                                                                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                            __context__.SourceCodeLine = 2054;
                                                                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                                                            } 
                                                                                                        
                                                                                                        else 
                                                                                                            {
                                                                                                            __context__.SourceCodeLine = 2056;
                                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SAPOWR0000000000000000" , LVSTRING ))  ) ) 
                                                                                                                { 
                                                                                                                __context__.SourceCodeLine = 2058;
                                                                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                                __context__.SourceCodeLine = 2059;
                                                                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                                __context__.SourceCodeLine = 2060;
                                                                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                                                                } 
                                                                                                            
                                                                                                            else 
                                                                                                                {
                                                                                                                __context__.SourceCodeLine = 2062;
                                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SAINPT0000000100000001" , LVSTRING ))  ) ) 
                                                                                                                    { 
                                                                                                                    __context__.SourceCodeLine = 2064;
                                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                                    __context__.SourceCodeLine = 2065;
                                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 1 ) ; 
                                                                                                                    } 
                                                                                                                
                                                                                                                else 
                                                                                                                    {
                                                                                                                    __context__.SourceCodeLine = 2067;
                                                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SAINPT0000000100000002" , LVSTRING ))  ) ) 
                                                                                                                        { 
                                                                                                                        __context__.SourceCodeLine = 2069;
                                                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( 2 ) ; 
                                                                                                                        __context__.SourceCodeLine = 2070;
                                                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( 2 ) ; 
                                                                                                                        } 
                                                                                                                    
                                                                                                                    else 
                                                                                                                        {
                                                                                                                        __context__.SourceCodeLine = 2072;
                                                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SAINPT0000000100000003" , LVSTRING ))  ) ) 
                                                                                                                            { 
                                                                                                                            __context__.SourceCodeLine = 2074;
                                                                                                                            DISPLAY_INPUT_FB  .Value = (ushort) ( 3 ) ; 
                                                                                                                            __context__.SourceCodeLine = 2075;
                                                                                                                            GLBL_DISPLAY . STATUSINPUT = (ushort) ( 3 ) ; 
                                                                                                                            } 
                                                                                                                        
                                                                                                                        else 
                                                                                                                            {
                                                                                                                            __context__.SourceCodeLine = 2077;
                                                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SAINPT0000000600000001" , LVSTRING ))  ) ) 
                                                                                                                                { 
                                                                                                                                __context__.SourceCodeLine = 2079;
                                                                                                                                DISPLAY_INPUT_FB  .Value = (ushort) ( 4 ) ; 
                                                                                                                                __context__.SourceCodeLine = 2080;
                                                                                                                                GLBL_DISPLAY . STATUSINPUT = (ushort) ( 4 ) ; 
                                                                                                                                } 
                                                                                                                            
                                                                                                                            else 
                                                                                                                                {
                                                                                                                                __context__.SourceCodeLine = 2082;
                                                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "*SAINPT0000000400000001" , LVSTRING ))  ) ) 
                                                                                                                                    { 
                                                                                                                                    __context__.SourceCodeLine = 2084;
                                                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                                                                    __context__.SourceCodeLine = 2085;
                                                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                                                                    } 
                                                                                                                                
                                                                                                                                else 
                                                                                                                                    {
                                                                                                                                    __context__.SourceCodeLine = 2087;
                                                                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "*SAVOLU" , LVSTRING ))  ) ) 
                                                                                                                                        { 
                                                                                                                                        __context__.SourceCodeLine = 2089;
                                                                                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "*SNVOLU" , LVSTRING )  ) ; 
                                                                                                                                        __context__.SourceCodeLine = 2090;
                                                                                                                                        LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                                                        __context__.SourceCodeLine = 2091;
                                                                                                                                        GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                                                                                        __context__.SourceCodeLine = 2092;
                                                                                                                                        DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                                                                                        __context__.SourceCodeLine = 2093;
                                                                                                                                        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                                                                            { 
                                                                                                                                            __context__.SourceCodeLine = 2095;
                                                                                                                                            GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                                                                                            } 
                                                                                                                                        
                                                                                                                                        } 
                                                                                                                                    
                                                                                                                                    else 
                                                                                                                                        {
                                                                                                                                        __context__.SourceCodeLine = 2098;
                                                                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "*SAAMUT0000000000000001" , LVSTRING ))  ) ) 
                                                                                                                                            { 
                                                                                                                                            __context__.SourceCodeLine = 2100;
                                                                                                                                            DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                                                            __context__.SourceCodeLine = 2101;
                                                                                                                                            DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                                                            __context__.SourceCodeLine = 2102;
                                                                                                                                            GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                                                                                            } 
                                                                                                                                        
                                                                                                                                        else 
                                                                                                                                            {
                                                                                                                                            __context__.SourceCodeLine = 2104;
                                                                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "*SAAMUT0000000000000000" , LVSTRING ))  ) ) 
                                                                                                                                                { 
                                                                                                                                                __context__.SourceCodeLine = 2106;
                                                                                                                                                DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                                                                                __context__.SourceCodeLine = 2107;
                                                                                                                                                DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                                                                                __context__.SourceCodeLine = 2108;
                                                                                                                                                GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                                                                                } 
                                                                                                                                            
                                                                                                                                            }
                                                                                                                                        
                                                                                                                                        }
                                                                                                                                    
                                                                                                                                    }
                                                                                                                                
                                                                                                                                }
                                                                                                                            
                                                                                                                            }
                                                                                                                        
                                                                                                                        }
                                                                                                                    
                                                                                                                    }
                                                                                                                
                                                                                                                }
                                                                                                            
                                                                                                            }
                                                                                                        
                                                                                                        }
                                                                                                    
                                                                                                    }
                                                                                                
                                                                                                }
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                __context__.SourceCodeLine = 1986;
                                                                } 
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 2113;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 15))  ) ) 
                                                                { 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 2117;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 16))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 2119;
                                                                    while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , GLBL_DISPLAY.CRXQUEUE ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 2121;
                                                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u000D" , GLBL_DISPLAY . CRXQUEUE )  ) ; 
                                                                        __context__.SourceCodeLine = 2122;
                                                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                                        __context__.SourceCodeLine = 2123;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:0" , LVSTRING ) )) ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 2125;
                                                                            DISPLAY_POWER_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                            __context__.SourceCodeLine = 2126;
                                                                            DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                            __context__.SourceCodeLine = 2127;
                                                                            GLBL_DISPLAY . STATUSPOWER = (ushort) ( 0 ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 2129;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "DISPLAY.POWER:1" , LVSTRING ) )) ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 2131;
                                                                                DISPLAY_POWER_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                __context__.SourceCodeLine = 2132;
                                                                                DISPLAY_POWER_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                __context__.SourceCodeLine = 2133;
                                                                                GLBL_DISPLAY . STATUSPOWER = (ushort) ( 1 ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 2135;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:OFF" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:0" , LVSTRING ) )) ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 2137;
                                                                                    DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 0 ) ; 
                                                                                    __context__.SourceCodeLine = 2138;
                                                                                    DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 1 ) ; 
                                                                                    __context__.SourceCodeLine = 2139;
                                                                                    GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 0 ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 2141;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:ON" , LVSTRING ) ) || Functions.TestForTrue ( Functions.Find( "AUDIO.MUTE:1" , LVSTRING ) )) ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 2143;
                                                                                        DISPLAY_VOLUME_MUTE_OFF_FB  .Value = (ushort) ( 0 ) ; 
                                                                                        __context__.SourceCodeLine = 2144;
                                                                                        DISPLAY_VOLUME_MUTE_ON_FB  .Value = (ushort) ( 1 ) ; 
                                                                                        __context__.SourceCodeLine = 2145;
                                                                                        GLBL_DISPLAY . STATUSVOLUMEMUTE = (ushort) ( 1 ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 2147;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "AUDIO.VOLUME:" , LVSTRING ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 2149;
                                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "AUDIO.VOLUME:" , LVSTRING )  ) ; 
                                                                                            __context__.SourceCodeLine = 2150;
                                                                                            LVVOL = (short) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                            __context__.SourceCodeLine = 2151;
                                                                                            GLBL_DISPLAY . STATUSVOLUME = (short) ( LVVOL ) ; 
                                                                                            __context__.SourceCodeLine = 2152;
                                                                                            DISPLAY_VOLUME_FB  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (short)( LVVOL ) ) ) ; 
                                                                                            __context__.SourceCodeLine = 2153;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 2155;
                                                                                                GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
                                                                                                } 
                                                                                            
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 2158;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "SOURCE.SELECT:" , LVSTRING ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 2160;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "SOURCE.SELECT:" , LVSTRING )  ) ; 
                                                                                                __context__.SourceCodeLine = 2161;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "HDMI." , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2163;
                                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "HDMI." , LVSTRING )  ) ; 
                                                                                                    __context__.SourceCodeLine = 2164;
                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                    __context__.SourceCodeLine = 2165;
                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                    } 
                                                                                                
                                                                                                __context__.SourceCodeLine = 2167;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "DP" , LVSTRING ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2169;
                                                                                                    DISPLAY_INPUT_FB  .Value = (ushort) ( 5 ) ; 
                                                                                                    __context__.SourceCodeLine = 2170;
                                                                                                    GLBL_DISPLAY . STATUSINPUT = (ushort) ( 5 ) ; 
                                                                                                    } 
                                                                                                
                                                                                                else 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 2175;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Atoi( LVSTRING ) <= 5 ) )) ))  ) ) 
                                                                                                        { 
                                                                                                        __context__.SourceCodeLine = 2177;
                                                                                                        DISPLAY_INPUT_FB  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                        __context__.SourceCodeLine = 2178;
                                                                                                        GLBL_DISPLAY . STATUSINPUT = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                                                                                        } 
                                                                                                    
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        __context__.SourceCodeLine = 2119;
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 2185;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 19))  ) ) 
                                                                        { 
                                                                        } 
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        }
        
    object TCPCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            
            __context__.SourceCodeLine = 2195;
            GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2196;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2197;
            CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
object TCPCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2202;
        GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2203;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 2204;
        CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2208;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 2209;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 2211;
            GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2212;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2216;
            GLBL_DISPLAY . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2217;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 2223;
        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( GLBL_DISPLAY . CRXQUEUE + TCPCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 2224;
        Functions.ClearBuffer ( TCPCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 2225;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 2226;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object DISPLAY_OBJ_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2231;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_OBJ  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_OBJ  .UshortValue <= 5 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2233;
            GLBL_DISPLAY . STATUSREADY = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2234;
            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSPOWER ))  ) ) 
                { 
                __context__.SourceCodeLine = 2236;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOWERON) ; 
                __context__.SourceCodeLine = 2237;
                CreateWait ( "DISPPWR" , (DISPLAY_POWER_TIME  .UshortValue * 100) , DISPPWR_Callback ) ;
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 2245;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), INPUT[ DISPLAY_OBJ  .UshortValue ]) ; 
                } 
            
            __context__.SourceCodeLine = 2247;
            GLBL_DISPLAY . STATUSREADY = (ushort) ( 1 ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2249;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_OBJ  .UshortValue == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_OBJ  .UshortValue == 99) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2251;
                GLBL_DISPLAY . STATUSREADY = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 2252;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOWEROFF) ; 
                __context__.SourceCodeLine = 2253;
                GLBL_DISPLAY . STATUSREADY = (ushort) ( 1 ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void DISPPWR_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2239;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), INPUT[ DISPLAY_OBJ  .UshortValue ]) ; 
            __context__.SourceCodeLine = 2240;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), ASPECT[ 1 ]) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_TYPE_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2259;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_TYPE  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_TYPE  .UshortValue <= 19 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2261;
            RUNINITIALIZATION (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVSTRINGVOL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 2268;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) , (short)( GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) , (ushort)( DISPLAY_VOLUME  .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 2269;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 2271;
            GLBL_DISPLAY . INTERNALVOLUME = (short) ( LVVOL ) ; 
            __context__.SourceCodeLine = 2272;
            SETVOLUME (  __context__ , (short)( LVVOL ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_VOLUME_UP_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2277;
        while ( Functions.TestForTrue  ( ( DISPLAY_VOLUME_UP  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2279;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2280;
            CreateWait ( "VOLUP" , 10 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 2277;
            } 
        
        __context__.SourceCodeLine = 2289;
        GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2282;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (GLBL_DISPLAY.INTERNALVOLUME + 3) > GLBL_DISPLAY_COMMANDS.VOLUMEMAX ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2284;
                GLBL_DISPLAY . INTERNALVOLUME = (short) ( (GLBL_DISPLAY.INTERNALVOLUME + 3) ) ; 
                __context__.SourceCodeLine = 2285;
                SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.INTERNALVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_DN_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2293;
        while ( Functions.TestForTrue  ( ( DISPLAY_VOLUME_DN  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2295;
            GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2296;
            CreateWait ( "VOLDN" , 10 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 2293;
            } 
        
        __context__.SourceCodeLine = 2305;
        GLBL_DISPLAY . STATUSVOLUMEHELD = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2298;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (GLBL_DISPLAY.INTERNALVOLUME - 3) < GLBL_DISPLAY_COMMANDS.VOLUMEMIN ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2300;
                GLBL_DISPLAY . INTERNALVOLUME = (short) ( (GLBL_DISPLAY.INTERNALVOLUME - 3) ) ; 
                __context__.SourceCodeLine = 2301;
                SETVOLUME (  __context__ , (short)( GLBL_DISPLAY.INTERNALVOLUME ), (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_MUTE_TOGGLE_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2309;
        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEMUTE ))  ) ) 
            {
            __context__.SourceCodeLine = 2310;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2312;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF) ; 
            }
        
        __context__.SourceCodeLine = 2313;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_13__" , 40 , __SPLS_TMPVAR__WAITLABEL_13___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_13___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 2314;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_MUTE_ON_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2318;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
            { 
            __context__.SourceCodeLine = 2320;
            if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEMUTE ))  ) ) 
                {
                __context__.SourceCodeLine = 2321;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2324;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEON) ; 
            }
        
        __context__.SourceCodeLine = 2325;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_14__" , 40 , __SPLS_TMPVAR__WAITLABEL_14___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_14___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 2326;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_VOLUME_MUTE_OFF_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2330;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 3))  ) ) 
            { 
            __context__.SourceCodeLine = 2332;
            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSVOLUMEMUTE)  ) ) 
                {
                __context__.SourceCodeLine = 2333;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF) ; 
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 2336;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDVOLUMEMUTEOFF) ; 
            }
        
        __context__.SourceCodeLine = 2337;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_15__" , 40 , __SPLS_TMPVAR__WAITLABEL_15___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_15___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 2338;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object DISPLAY_ASPECT_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2342;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_ASPECT  .UshortValue > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( DISPLAY_ASPECT  .UshortValue <= 5 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 2343;
            SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), ASPECT[ DISPLAY_ASPECT  .UshortValue ]) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SHARP_PROTOCOL_OnChange_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_ID__DOLLAR___OnChange_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2350;
        RUNINITIALIZATION (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEBUG_OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2354;
        if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 2355;
            GLBL_DISPLAY . DEBUG = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 2357;
            GLBL_DISPLAY . DEBUG = (ushort) ( 0 ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2361;
        GLBL_DISPLAY . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2365;
        GLBL_DISPLAY . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_POLL_OnPush_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 2370;
        if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_DISPLAY.STATUSVOLUMEHELD ))  ) ) 
            { 
            __context__.SourceCodeLine = 2372;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER  ) ; 
            __context__.SourceCodeLine = 2373;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2375;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLPOWER + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2376;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2378;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 5) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 6) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 2379;
                    SETQUEUE (  __context__ , GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 2381;
                    SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLPOWER) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 2382;
            if ( Functions.TestForTrue  ( ( GLBL_DISPLAY.STATUSPOWER)  ) ) 
                { 
                __context__.SourceCodeLine = 2384;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_16__" , 200 , __SPLS_TMPVAR__WAITLABEL_16___Callback ) ;
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_16___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2387;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT  ) ; 
            __context__.SourceCodeLine = 2388;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2390;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLINPUT + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2391;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2394;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLINPUT) ; 
                }
            
            __context__.SourceCodeLine = 2395;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_17__" , 200 , __SPLS_TMPVAR__WAITLABEL_17___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_17___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2398;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME  ) ; 
            __context__.SourceCodeLine = 2399;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2401;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLVOLUME + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2402;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2405;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLVOLUME) ; 
                }
            
            __context__.SourceCodeLine = 2406;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_18__" , 200 , __SPLS_TMPVAR__WAITLABEL_18___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_18___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            __context__.SourceCodeLine = 2409;
            GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE  ) ; 
            __context__.SourceCodeLine = 2410;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 1) ) || Functions.TestForTrue ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 2) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 2412;
                LVSTRING  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLMUTE + GLBL_DISPLAY_COMMANDS . ETX  ) ; 
                __context__.SourceCodeLine = 2413;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 2416;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLMUTE) ; 
                }
            
            __context__.SourceCodeLine = 2417;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_19__" , 200 , __SPLS_TMPVAR__WAITLABEL_19___Callback ) ;
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_19___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 2419;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 11))  ) ) 
                { 
                __context__.SourceCodeLine = 2421;
                GLBL_DISPLAY . COMMANDPOLL  .UpdateValue ( GLBL_DISPLAY_COMMANDS . COMMANDPOLLLAMPHOURS  ) ; 
                __context__.SourceCodeLine = 2422;
                SENDSTRING (  __context__ , (ushort)( DISPLAY_TYPE  .UshortValue ), GLBL_DISPLAY_COMMANDS.COMMANDPOLLLAMPHOURS) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object IP_CONNECT_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2434;
        if ( Functions.TestForTrue  ( ( IP_CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 2436;
            GLBL_DISPLAY . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 2437;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 9))  ) ) 
                {
                __context__.SourceCodeLine = 2438;
                SETQUEUE (  __context__ , "ESC/VP.net\u0010\u0003\u0000\u0000\u0000\u0000") ; 
                }
            
            __context__.SourceCodeLine = 2439;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                { 
                __context__.SourceCodeLine = 2441;
                GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "00"  ) ; 
                __context__.SourceCodeLine = 2442;
                GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u000D"  ) ; 
                } 
            
            __context__.SourceCodeLine = 2444;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 2448;
            GLBL_DISPLAY . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2449;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            __context__.SourceCodeLine = 2450;
            GLBL_DISPLAY . IPLOGIN = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 2451;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DISPLAY_TYPE  .UshortValue == 10))  ) ) 
                { 
                __context__.SourceCodeLine = 2453;
                GLBL_DISPLAY_COMMANDS . STX  .UpdateValue ( "\u0002"  ) ; 
                __context__.SourceCodeLine = 2454;
                GLBL_DISPLAY_COMMANDS . ETX  .UpdateValue ( "\u0003"  ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX__DOLLAR___OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2461;
        GLBL_DISPLAY . CRXQUEUE  .UpdateValue ( GLBL_DISPLAY . CRXQUEUE + RX__DOLLAR__  ) ; 
        __context__.SourceCodeLine = 2462;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DISPLAY.CRXQUEUE ) > 1 ))  ) ) 
            {
            __context__.SourceCodeLine = 2463;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 2467;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    INPUT  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        INPUT [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
    ASPECT  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        ASPECT [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
    INPUTPOLLDATA  = new CrestronString[ 6 ];
    for( uint i = 0; i < 6; i++ )
        INPUTPOLLDATA [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
    TCPCLIENT  = new SplusTcpClient ( 2047, this );
    UDPCLIENT  = new SplusUdpSocket ( 2047, this );
    GLBL_DISPLAY  = new SLOCALDISPLAY( this, true );
    GLBL_DISPLAY .PopulateCustomAttributeList( false );
    GLBL_DISPLAY_COMMANDS  = new SDISPLAY( this, true );
    GLBL_DISPLAY_COMMANDS .PopulateCustomAttributeList( false );
    
    DISPLAY_VOLUME_MUTE_TOGGLE = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_TOGGLE__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_TOGGLE__DigitalInput__, DISPLAY_VOLUME_MUTE_TOGGLE );
    
    DISPLAY_VOLUME_MUTE_ON = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_ON__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_ON__DigitalInput__, DISPLAY_VOLUME_MUTE_ON );
    
    DISPLAY_VOLUME_MUTE_OFF = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_MUTE_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_MUTE_OFF__DigitalInput__, DISPLAY_VOLUME_MUTE_OFF );
    
    DISPLAY_VOLUME_UP = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_UP__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_UP__DigitalInput__, DISPLAY_VOLUME_UP );
    
    DISPLAY_VOLUME_DN = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_VOLUME_DN__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_VOLUME_DN__DigitalInput__, DISPLAY_VOLUME_DN );
    
    DISPLAY_POLL = new Crestron.Logos.SplusObjects.DigitalInput( DISPLAY_POLL__DigitalInput__, this );
    m_DigitalInputList.Add( DISPLAY_POLL__DigitalInput__, DISPLAY_POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    IP_CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( IP_CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( IP_CONNECT__DigitalInput__, IP_CONNECT );
    
    DISPLAY_POWER_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_POWER_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_POWER_ON_FB__DigitalOutput__, DISPLAY_POWER_ON_FB );
    
    DISPLAY_POWER_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_POWER_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_POWER_OFF_FB__DigitalOutput__, DISPLAY_POWER_OFF_FB );
    
    DISPLAY_VOLUME_MUTE_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__, DISPLAY_VOLUME_MUTE_ON_FB );
    
    DISPLAY_VOLUME_MUTE_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__, DISPLAY_VOLUME_MUTE_OFF_FB );
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    DISPLAY_ASPECT = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_ASPECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_ASPECT__AnalogSerialInput__, DISPLAY_ASPECT );
    
    DISPLAY_VOLUME = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_VOLUME__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_VOLUME__AnalogSerialInput__, DISPLAY_VOLUME );
    
    DISPLAY_POWER_TIME = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_POWER_TIME__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_POWER_TIME__AnalogSerialInput__, DISPLAY_POWER_TIME );
    
    DISPLAY_TYPE = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_TYPE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_TYPE__AnalogSerialInput__, DISPLAY_TYPE );
    
    DISPLAY_OBJ = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_OBJ__AnalogSerialInput__, this );
    m_AnalogInputList.Add( DISPLAY_OBJ__AnalogSerialInput__, DISPLAY_OBJ );
    
    SHARP_PROTOCOL = new Crestron.Logos.SplusObjects.AnalogInput( SHARP_PROTOCOL__AnalogSerialInput__, this );
    m_AnalogInputList.Add( SHARP_PROTOCOL__AnalogSerialInput__, SHARP_PROTOCOL );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    DISPLAY_VOLUME_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_VOLUME_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_VOLUME_FB__AnalogSerialOutput__, DISPLAY_VOLUME_FB );
    
    DISPLAY_INPUT_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_INPUT_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_INPUT_FB__AnalogSerialOutput__, DISPLAY_INPUT_FB );
    
    DISPLAY_ASPECT_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_ASPECT_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_ASPECT_FB__AnalogSerialOutput__, DISPLAY_ASPECT_FB );
    
    DISPLAY_LAMP_HRS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__, DISPLAY_LAMP_HRS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    DISPLAY_ID__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( DISPLAY_ID__DOLLAR____AnalogSerialInput__, 7, this );
    m_StringInputList.Add( DISPLAY_ID__DOLLAR____AnalogSerialInput__, DISPLAY_ID__DOLLAR__ );
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( RX__DOLLAR____AnalogSerialInput__, 255, this );
    m_StringInputList.Add( RX__DOLLAR____AnalogSerialInput__, RX__DOLLAR__ );
    
    LOGINNAME__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( LOGINNAME__DOLLAR____AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINNAME__DOLLAR____AnalogSerialInput__, LOGINNAME__DOLLAR__ );
    
    LOGINPASSWORD__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( LOGINPASSWORD__DOLLAR____AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINPASSWORD__DOLLAR____AnalogSerialInput__, LOGINPASSWORD__DOLLAR__ );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    GENERIC_POWERON = new Crestron.Logos.SplusObjects.StringInput( GENERIC_POWERON__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_POWERON__AnalogSerialInput__, GENERIC_POWERON );
    
    GENERIC_POWEROFF = new Crestron.Logos.SplusObjects.StringInput( GENERIC_POWEROFF__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_POWEROFF__AnalogSerialInput__, GENERIC_POWEROFF );
    
    GENERIC_MUTEON = new Crestron.Logos.SplusObjects.StringInput( GENERIC_MUTEON__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_MUTEON__AnalogSerialInput__, GENERIC_MUTEON );
    
    GENERIC_MUTEOFF = new Crestron.Logos.SplusObjects.StringInput( GENERIC_MUTEOFF__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_MUTEOFF__AnalogSerialInput__, GENERIC_MUTEOFF );
    
    GENERIC_VOLUMEUP = new Crestron.Logos.SplusObjects.StringInput( GENERIC_VOLUMEUP__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_VOLUMEUP__AnalogSerialInput__, GENERIC_VOLUMEUP );
    
    GENERIC_VOLUMEDOWN = new Crestron.Logos.SplusObjects.StringInput( GENERIC_VOLUMEDOWN__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( GENERIC_VOLUMEDOWN__AnalogSerialInput__, GENERIC_VOLUMEDOWN );
    
    GENERIC_HEADER = new Crestron.Logos.SplusObjects.StringInput( GENERIC_HEADER__AnalogSerialInput__, 7, this );
    m_StringInputList.Add( GENERIC_HEADER__AnalogSerialInput__, GENERIC_HEADER );
    
    GENERIC_FOOTER = new Crestron.Logos.SplusObjects.StringInput( GENERIC_FOOTER__AnalogSerialInput__, 7, this );
    m_StringInputList.Add( GENERIC_FOOTER__AnalogSerialInput__, GENERIC_FOOTER );
    
    GENERIC_INPUT = new InOutArray<StringInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        GENERIC_INPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( GENERIC_INPUT__AnalogSerialInput__ + i, GENERIC_INPUT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( GENERIC_INPUT__AnalogSerialInput__ + i, GENERIC_INPUT[i+1] );
    }
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__DOLLAR____AnalogSerialOutput__, TX__DOLLAR__ );
    
    __SPLS_TMPVAR__WAITLABEL_12___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_12___CallbackFn );
    DISPPWR_Callback = new WaitFunction( DISPPWR_CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_13___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_13___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_14___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_14___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_15___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_15___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_16___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_16___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_17___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_17___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_18___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_18___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_19___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_19___CallbackFn );
    
    TCPCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketConnect_0, false ) );
    TCPCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketDisconnect_1, false ) );
    TCPCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketStatus_2, false ) );
    TCPCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketReceive_3, false ) );
    DISPLAY_OBJ.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_OBJ_OnChange_4, false ) );
    DISPLAY_TYPE.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_TYPE_OnChange_5, false ) );
    DISPLAY_VOLUME.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_OnChange_6, false ) );
    DISPLAY_VOLUME_UP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_UP_OnPush_7, false ) );
    DISPLAY_VOLUME_DN.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_DN_OnPush_8, false ) );
    DISPLAY_VOLUME_MUTE_TOGGLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_MUTE_TOGGLE_OnPush_9, false ) );
    DISPLAY_VOLUME_MUTE_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_MUTE_ON_OnPush_10, false ) );
    DISPLAY_VOLUME_MUTE_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_VOLUME_MUTE_OFF_OnPush_11, false ) );
    DISPLAY_ASPECT.OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_ASPECT_OnChange_12, false ) );
    SHARP_PROTOCOL.OnAnalogChange.Add( new InputChangeHandlerWrapper( SHARP_PROTOCOL_OnChange_13, false ) );
    DISPLAY_ID__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( DISPLAY_ID__DOLLAR___OnChange_14, false ) );
    DEBUG.OnDigitalChange.Add( new InputChangeHandlerWrapper( DEBUG_OnChange_15, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_16, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_17, false ) );
    DISPLAY_POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISPLAY_POLL_OnPush_18, false ) );
    IP_CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( IP_CONNECT_OnChange_19, false ) );
    RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( RX__DOLLAR___OnChange_20, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_21, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DISPLAYS_V3_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_12___Callback;
private WaitFunction DISPPWR_Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_13___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_14___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_15___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_16___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_17___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_18___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_19___Callback;


const uint DISPLAY_VOLUME_MUTE_TOGGLE__DigitalInput__ = 0;
const uint DISPLAY_VOLUME_MUTE_ON__DigitalInput__ = 1;
const uint DISPLAY_VOLUME_MUTE_OFF__DigitalInput__ = 2;
const uint DISPLAY_VOLUME_UP__DigitalInput__ = 3;
const uint DISPLAY_VOLUME_DN__DigitalInput__ = 4;
const uint DISPLAY_POLL__DigitalInput__ = 5;
const uint DEBUG__DigitalInput__ = 6;
const uint IP_CONNECT__DigitalInput__ = 7;
const uint DISPLAY_ASPECT__AnalogSerialInput__ = 0;
const uint DISPLAY_VOLUME__AnalogSerialInput__ = 1;
const uint DISPLAY_POWER_TIME__AnalogSerialInput__ = 2;
const uint DISPLAY_TYPE__AnalogSerialInput__ = 3;
const uint DISPLAY_OBJ__AnalogSerialInput__ = 4;
const uint SHARP_PROTOCOL__AnalogSerialInput__ = 5;
const uint IP_PORT__AnalogSerialInput__ = 6;
const uint IP_ADDRESS__AnalogSerialInput__ = 7;
const uint DISPLAY_ID__DOLLAR____AnalogSerialInput__ = 8;
const uint RX__DOLLAR____AnalogSerialInput__ = 9;
const uint LOGINNAME__DOLLAR____AnalogSerialInput__ = 10;
const uint LOGINPASSWORD__DOLLAR____AnalogSerialInput__ = 11;
const uint MANUALCMD__AnalogSerialInput__ = 12;
const uint GENERIC_POWERON__AnalogSerialInput__ = 13;
const uint GENERIC_POWEROFF__AnalogSerialInput__ = 14;
const uint GENERIC_MUTEON__AnalogSerialInput__ = 15;
const uint GENERIC_MUTEOFF__AnalogSerialInput__ = 16;
const uint GENERIC_VOLUMEUP__AnalogSerialInput__ = 17;
const uint GENERIC_VOLUMEDOWN__AnalogSerialInput__ = 18;
const uint GENERIC_HEADER__AnalogSerialInput__ = 19;
const uint GENERIC_FOOTER__AnalogSerialInput__ = 20;
const uint GENERIC_INPUT__AnalogSerialInput__ = 21;
const uint DISPLAY_POWER_ON_FB__DigitalOutput__ = 0;
const uint DISPLAY_POWER_OFF_FB__DigitalOutput__ = 1;
const uint DISPLAY_VOLUME_MUTE_ON_FB__DigitalOutput__ = 2;
const uint DISPLAY_VOLUME_MUTE_OFF_FB__DigitalOutput__ = 3;
const uint CONNECT_FB__DigitalOutput__ = 4;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint DISPLAY_VOLUME_FB__AnalogSerialOutput__ = 1;
const uint DISPLAY_INPUT_FB__AnalogSerialOutput__ = 2;
const uint DISPLAY_ASPECT_FB__AnalogSerialOutput__ = 3;
const uint DISPLAY_LAMP_HRS_FB__AnalogSerialOutput__ = 4;
const uint TX__DOLLAR____AnalogSerialOutput__ = 5;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SDISPLAY : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public short  VOLUMEMIN = 0;
    
    [SplusStructAttribute(1, false, false)]
    public short  VOLUMEMAX = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  COMMANDPOWERON;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  COMMANDPOWEROFF;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  COMMANDINPUT1;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  COMMANDINPUT2;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  COMMANDINPUT3;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  COMMANDINPUT4;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  COMMANDINPUT5;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  COMMANDASPECT1;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  COMMANDASPECT2;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  COMMANDASPECT3;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  COMMANDASPECT4;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  COMMANDASPECT5;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  COMMANDVOLUMELEVEL;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  COMMANDVOLUMEMUTEON;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  COMMANDVOLUMEMUTEOFF;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  COMMANDSLEEP;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  COMMANDPOLLPOWER;
    
    [SplusStructAttribute(19, false, false)]
    public CrestronString  COMMANDPOLLINPUT;
    
    [SplusStructAttribute(20, false, false)]
    public CrestronString  COMMANDPOLLVOLUME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  COMMANDPOLLMUTE;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  COMMANDPOLLLAMPHOURS;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  STX;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  ETX;
    
    
    public SDISPLAY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        COMMANDPOWERON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOWEROFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDINPUT1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDINPUT2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDINPUT3  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDINPUT4  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDINPUT5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDASPECT1  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDASPECT2  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDASPECT3  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDASPECT4  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDASPECT5  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDVOLUMELEVEL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDVOLUMEMUTEON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDVOLUMEMUTEOFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDSLEEP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLPOWER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLINPUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLMUTE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLLAMPHOURS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SLOCALDISPLAY : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSPOWER = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSINPUT = 0;
    
    [SplusStructAttribute(2, false, false)]
    public short  STATUSVOLUME = 0;
    
    [SplusStructAttribute(3, false, false)]
    public short  INTERNALVOLUME = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  STATUSVOLUMEMUTE = 0;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  STATUSASPECT = 0;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  STATUSREADY = 0;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  STATUSVOLUMEHELD = 0;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  COMMANDCONFIRM = 0;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  COMMANDACK;
    
    [SplusStructAttribute(11, false, false)]
    public ushort  IPLOGIN = 0;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(14, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(15, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  CTXQUEUE;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  CRXQUEUE;
    
    [SplusStructAttribute(18, false, false)]
    public ushort  NID = 0;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  DEBUG = 0;
    
    
    public SLOCALDISPLAY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDACK  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        CTXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        CRXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        
        
    }
    
}

}
