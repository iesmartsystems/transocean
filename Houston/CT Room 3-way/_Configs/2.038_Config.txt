//ROOM SETUP
Room: RoomName-203
Room: PhoneNumber-
Room: NoTouchPowerOff-
Room: RoutingMode-2
//DISPLAY(s) SETUP
//Display 1
Display 1: Name-203 Left Projector
Display 1: ControlDeviceType-17
Display 1: ControlID-N/A
Display 1: ControlProtocol-4
Display 1: SourceInputs-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
Display 1: PowerOnTime-10
Display 1: PowerOffTime-10
Display 1: OutputVideo-1
Display 1: ScreenUp-1
Display 1: ScreenDown-2
Display 1: ControlBaud-
Display 1: ControlParity-
Display 1: ControlIP-
Display 1: ControlIPPort-
Display 1: ControlLogin-
Display 1: ControlPassword-
//Display 2
Display 2: Name-203 Right Projector
Display 2: ControlDeviceType-17
Display 2: ControlID-N/A
Display 2: ControlProtocol-4
Display 2: SourceInputs-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
Display 2: PowerOnTime-10
Display 2: PowerOffTime-10
Display 2: OutputVideo-1
Display 2: ScreenUp-3
Display 2: ScreenDown-4
Display 2: ControlBaud-
Display 2: ControlParity-
Display 2: ControlIP-
Display 2: ControlIPPort-
Display 2: ControlLogin-
Display 2: ControlPassword-
//Display 3
Display 3: Name-203 West Projector
Display 3: ControlDeviceType-17
Display 3: ControlID-N/A
Display 3: ControlProtocol-4
Display 3: SourceInputs-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
Display 3: PowerOnTime-10
Display 3: PowerOffTime-10
Display 3: OutputVideo-1
Display 3: ScreenUp-5
Display 3: ScreenDown-6
Display 3: ControlBaud-
Display 3: ControlParity-
Display 3: ControlIP-
Display 3: ControlIPPort-
Display 3: ControlLogin-
Display 3: ControlPassword-
//SWITCHER(s) SETUP
//AUDIO(s) SETUP
//Audio 1
Audio 1: ObejctID-
Audio 1: ControlBaud-
Audio 1: ControlParity-
Audio 1: ControlIP-10.210.2.114
Audio 1: ControlIPPort-1710
Audio 1: ControlLogin-
Audio 1: ControlPassword-
Audio 1: InstanceTags 1-ROOM_CMB-1-RCMB
Audio 1: InstanceTags 2-MIC_CMB-1-RCMB
Audio 1: InstanceTags 3-VOICE_CMB-1-RCMB
Audio 1: InstanceTags 4-USB_TX_CMB-1-RCMB
Audio 1: InstanceTags 5-AEC_CMB-1-RCMB
Audio 1: InstanceTags 6-WX_MIC_LEVEL_1-1-SVOL
Audio 1: InstanceTags 7-PGM_LEVEL_1-1-SVOL
Audio 1: InstanceTags 8-USB_RX_LEVEL_1-1-SVOL
Audio 1: InstanceTags 9-
Audio 1: InstanceTags 10-
Audio 1: InstanceTags 11-
Audio 1: InstanceTags 12-
Audio 1: InstanceTags 13-
Audio 1: InstanceTags 14-
Audio 1: InstanceTags 15-
Audio 1: InstanceTags 16-
//Audio 2 Shure Mic
Audio 2: ObejctID-
Audio 2: ControlBaud-
Audio 2: ControlParity-
Audio 2: ControlIP-10.210.2.156
Audio 2: ControlIPPort-2202
Audio 2: ControlLogin-
Audio 2: ControlPassword-
//Audio 3 Shure Mic
Audio 3: ObejctID-
Audio 3: ControlBaud-
Audio 3: ControlParity-
Audio 3: ControlIP-10.210.2.61
Audio 3: ControlIPPort-2202
Audio 3: ControlLogin-
Audio 3: ControlPassword-
//CODEC(s) SETUP
//SOURCE(s) SETUP
//Source 1
Source 1: Name-203 Laptop
Source 1: InputVideo-1
Source 1: IconType-1
Source 1: ControlBaud-
Source 1: ControlParity-
Source 1: ControlIP-
Source 1: ControlIPPort-
Source 1: ControlLogin-
Source 1: ControlPassword-
//ENVIRONMENT(s) SETUP
//ENV 1
ENV 1: Name-
ENV 1: ControlBaud-
ENV 1: ControlParity-
ENV 1: ControlIP-
ENV 1: ControlIPPort-
ENV 1: ControlLogin-
ENV 1: ControlPassword-
//MISC(s) SETUP
//Strings & Analogs
Misc 1: Strings-
Misc 1: Analogs-
Misc 2: Strings-
Misc 2: Analogs-
