namespace PolycomGroupSeries;
        // class declarations
         class BaseManager;
         class DateFormats;
         class SystemUpdate;
         class SystemManager;
         class GlobalServicesUpdate;
         class GlobalServicesManager;
         class DialingNumber;
         class CalendarMeeting;
         class MultiPointAutoAnswerMode;
         class MoveDirection;
         class ConferenceManager;
         class ConferenceUpdate;
         class Extensions;
         class PolycomGroupSeriesDevice;
         class Logger;
         class Levels;
         class SimplPolycomGroupSeriesDevice;
         class AudioVideoManager;
         class AudioVideoUpdate;
         class SimplTransport;
         class CalenderRemainingTimes;
         class MpMode;
         class SshSecurityModes;
         class SshTransport;
         class GatekeeperMode;
         class Camera;
         class TimeFormats;
         class ConnectionStates;
         class TransportTypes;
         class LdapAuthenticationTypes;
         class AesEncryptionMode;
         class NatConfiguration;
         class SnmpTrapVersions;
         class DirectoryManager;
         class DirectoryUpdate;
         class DirectoryObject;
         class Input;
         class NetworkUpdate;
         class NetworkManager;
         class SipTransportProtocol;
         class LanPort;
         class CallFunctionManager;
         class PolycomCall;
         class SpeedDialEntry;
         class CallFunctionUpdate;
         class CallRates;
         class VideoCallTypes;
         class AudioCallTypes;
         class ConnectionPreferences;
         class DialingMethods;
         class LineOutMode;
         class SleepTime;
         class PolycomMessageType;
         class TransportEventType;
         class VgaQualityMode;
         class CameraContent;
         class PointToPointAutoAnswerMode;
           class SendData;
     class BaseManager 
    {
        // class delegates
        delegate FUNCTION SendData ( STRING data );

        // class events

        // class functions
        FUNCTION ProcessResponse ( STRING response );
        FUNCTION Initialize ();
        FUNCTION Poll ();
        FUNCTION ResetState ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty SendData SendToDevice;
    };

    static class DateFormats // enum
    {
        static SIGNED_LONG_INTEGER MonthDate;
        static SIGNED_LONG_INTEGER DateMonth;
    };

    static class SystemUpdate // enum
    {
        static SIGNED_LONG_INTEGER Version;
        static SIGNED_LONG_INTEGER SerialNumber;
        static SIGNED_LONG_INTEGER Model;
        static SIGNED_LONG_INTEGER DaylightSavingsEnabled;
        static SIGNED_LONG_INTEGER EchoReply;
        static SIGNED_LONG_INTEGER RemoteEnabled;
        static SIGNED_LONG_INTEGER SystemName;
        static SIGNED_LONG_INTEGER BasicMode;
        static SIGNED_LONG_INTEGER SleepMode;
    };

    static class GlobalServicesUpdate // enum
    {
        static SIGNED_LONG_INTEGER CalenderUpdated;
        static SIGNED_LONG_INTEGER CalenderPlayTone;
        static SIGNED_LONG_INTEGER CalenderService;
        static SIGNED_LONG_INTEGER CalenderRemainingTime;
        static SIGNED_LONG_INTEGER MailboxAccountAddress;
        static SIGNED_LONG_INTEGER CalenderServer;
        static SIGNED_LONG_INTEGER PrivateMeetingsDisplayed;
        static SIGNED_LONG_INTEGER CalenderConnection;
        static SIGNED_LONG_INTEGER CalenderUser;
        static SIGNED_LONG_INTEGER LdapAuthenticationType;
        static SIGNED_LONG_INTEGER LdapBaseName;
        static SIGNED_LONG_INTEGER LdapBindDn;
        static SIGNED_LONG_INTEGER LdapDirectoryServer;
        static SIGNED_LONG_INTEGER LdapNtlmDomain;
        static SIGNED_LONG_INTEGER LdapServerAddress;
        static SIGNED_LONG_INTEGER LdapServerPort;
        static SIGNED_LONG_INTEGER LdapSslEnabled;
        static SIGNED_LONG_INTEGER LdapUsername;
        static SIGNED_LONG_INTEGER SnmpEnabled;
        static SIGNED_LONG_INTEGER SnmpAdminName;
        static SIGNED_LONG_INTEGER SnmpCommunityName;
        static SIGNED_LONG_INTEGER SnmpConsoleIpAddress;
        static SIGNED_LONG_INTEGER SnmpLocationName;
        static SIGNED_LONG_INTEGER SnmpSystemDescription;
        static SIGNED_LONG_INTEGER SnmpTrapVersion;
    };

     class DialingNumber 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        VideoCallTypes CallType;
        STRING Uri[];
        INTEGER SimplCallProtocol;
        INTEGER SimplCallType;
    };

     class CalendarMeeting 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING UniqueId[];
        STRING Name[];
        STRING Password[];
        STRING Organizer[];
        STRING Location[];
        STRING Subject[];
        STRING DurationString[];
        STRING StartTimeString[];
        STRING EndTimeString[];
        INTEGER SimplIsPublic;
        INTEGER SimplIsDialable;
        DialingNumber SimplDialingNumbers[];
    };

    static class MultiPointAutoAnswerMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER On;
        static SIGNED_LONG_INTEGER Off;
        static SIGNED_LONG_INTEGER DoNotDisturb;
    };

    static class MoveDirection // enum
    {
        static SIGNED_LONG_INTEGER Left;
        static SIGNED_LONG_INTEGER Right;
        static SIGNED_LONG_INTEGER Up;
        static SIGNED_LONG_INTEGER Down;
        static SIGNED_LONG_INTEGER ZoomIn;
        static SIGNED_LONG_INTEGER ZoomOut;
    };

    static class ConferenceUpdate // enum
    {
        static SIGNED_LONG_INTEGER PointToPointAutoAnswer;
        static SIGNED_LONG_INTEGER DynamicBandwidth;
        static SIGNED_LONG_INTEGER AesEncryptionMode;
        static SIGNED_LONG_INTEGER Pvec;
        static SIGNED_LONG_INTEGER Rsvp;
        static SIGNED_LONG_INTEGER MaxTimeInCall;
        static SIGNED_LONG_INTEGER MultiPointAutoAnswerMode;
        static SIGNED_LONG_INTEGER H239Enabled;
        static SIGNED_LONG_INTEGER MpMode;
        static SIGNED_LONG_INTEGER MuteAutoAnsweredCalls;
        static SIGNED_LONG_INTEGER DisplayIconsInCall;
        static SIGNED_LONG_INTEGER MaxRxBandwidth;
        static SIGNED_LONG_INTEGER MaxTxBandwidth;
        static SIGNED_LONG_INTEGER Transcoding;
    };

    static class Extensions 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION GetWordFromWhiteSpaceSeperatedMessage ( STRING input , SIGNED_LONG_INTEGER index );
        static STRING_FUNCTION TextBetweenQuotes ( STRING input );
        static SIGNED_LONG_INTEGER_FUNCTION NumberInText ( STRING input );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class PolycomGroupSeriesDevice 
    {
        // class delegates
        delegate FUNCTION SendData ( STRING data );

        // class events

        // class functions
        FUNCTION AcceptSshKey ( STRING keyFingerprint );
        FUNCTION RejectSshKey ();
        FUNCTION StopTransport ();
        FUNCTION SetUsername ( STRING username );
        FUNCTION SetPassword ( STRING password );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty SendData BuildAndSendToDevice;
        AudioVideoManager AudioVideoControl;
        CallFunctionManager CallFunctionControl;
        ConferenceManager ConferenceControl;
        DirectoryManager DirectoryControl;
        GlobalServicesManager GlobalServicesControl;
        NetworkManager NetworkControl;
        SystemManager SystemControl;
    };

    static class Logger 
    {
        // class delegates

        // class events

        // class functions
        static FUNCTION Write ( Levels loggingLevel , STRING message );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static Levels LoggingLevel;

        // class properties
    };

    static class Levels // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Regular;
        static SIGNED_LONG_INTEGER Verbose;
    };

     class SimplPolycomGroupSeriesDevice 
    {
        // class delegates
        delegate FUNCTION SimplIntegerUpdated ( INTEGER value );
        delegate FUNCTION SimplSignedIntegerUpdated ( SIGNED_INTEGER value );
        delegate FUNCTION SimplStringUpdated ( SIMPLSHARPSTRING value );
        delegate FUNCTION SimplIntegerStringUpdated ( INTEGER value , SIMPLSHARPSTRING stringValue );
        delegate FUNCTION SimplIntegerStringStringUpdated ( INTEGER value , SIMPLSHARPSTRING stringValue1 , SIMPLSHARPSTRING stringValue2 );
        delegate FUNCTION SimplStringsUpdated ( SIMPLSHARPSTRING stringValue1 , SIMPLSHARPSTRING stringValue2 , SIMPLSHARPSTRING stringValue3 , SIMPLSHARPSTRING stringValue4 , SIMPLSHARPSTRING stringValue5 );
        delegate FUNCTION SimplStringStringUpdated ( SIMPLSHARPSTRING value , SIMPLSHARPSTRING value2 );

        // class events

        // class functions
        FUNCTION Enable_H239 ();
        FUNCTION Disable_H239 ();
        FUNCTION SetInitialLocalDirectoryDownload ( INTEGER value );
        FUNCTION SetMaxLocalEntries ( INTEGER value );
        FUNCTION SetLocalEntriesPerPage ( INTEGER value );
        FUNCTION LocalDirectory_Home ();
        FUNCTION LocalDirectory_DownloadEntries ();
        FUNCTION LocalDirectory_FirstPage ();
        FUNCTION LocalDirectory_NextPage ();
        FUNCTION LocalDirecory_PreviousPage ();
        FUNCTION LocalDirectory_PreviousDirectory ();
        FUNCTION LocalDirectory_SelectEntry ( INTEGER value );
        FUNCTION LocalDirectory_ClearSelectedEntry ();
        FUNCTION LocalDirectory_DialSelectedEntry ( INTEGER value );
        FUNCTION LocalDirectory_Search ( STRING searchString , INTEGER searchAnywhere );
        FUNCTION GlobalDirectory_LiveSearch ( STRING searchTerms , INTEGER searchAnywhere );
        FUNCTION GlobalDirectoryLiveSearch_FirstPage ();
        FUNCTION GlobalDirectoryLiveSearch_NextPage ();
        FUNCTION GlobalDirecoryLiveSearch_PreviousPage ();
        FUNCTION GlobalDirectoryLiveSearch_SelectEntry ( INTEGER value );
        FUNCTION GlobalDirectoryLiveSearch_ClearSelectedEntry ();
        FUNCTION GlobalDirectoryLiveSearch_DialSelectedEntry ( INTEGER value );
        FUNCTION SetGlobalLiveSearchEntriesPerPage ( INTEGER value );
        FUNCTION SetInitialGlobalDirectoryDownload ( INTEGER value );
        FUNCTION SetGlobalMultiTieredDirectory ( INTEGER value );
        FUNCTION SetMaxGlobalEntries ( INTEGER value );
        FUNCTION SetGlobalEntriesPerPage ( INTEGER value );
        FUNCTION GlobalDirectory_Home ();
        FUNCTION GlobalDirectory_DownloadEntries ();
        FUNCTION GlobalDirectory_FirstPage ();
        FUNCTION GlobalDirectory_NextPage ();
        FUNCTION GlobalDirecory_PreviousPage ();
        FUNCTION GlobalDirectory_PreviousDirectory ();
        FUNCTION GlobalDirectory_SelectEntry ( INTEGER value );
        FUNCTION GlobalDirectory_ClearSelectedEntry ();
        FUNCTION GlobalDirectory_DialSelectedEntry ( INTEGER value );
        FUNCTION GlobalDirectory_Search ( STRING searchString , INTEGER searchAnywhere );
        FUNCTION Button_Pound ();
        FUNCTION Button_Asterisk ();
        FUNCTION Button_0 ();
        FUNCTION Button_1 ();
        FUNCTION Button_2 ();
        FUNCTION Button_3 ();
        FUNCTION Button_4 ();
        FUNCTION Button_5 ();
        FUNCTION Button_6 ();
        FUNCTION Button_7 ();
        FUNCTION Button_8 ();
        FUNCTION Button_9 ();
        FUNCTION Button_Period ();
        FUNCTION Button_Down ();
        FUNCTION Button_Left ();
        FUNCTION Button_Right ();
        FUNCTION Button_Select ();
        FUNCTION Button_Up ();
        FUNCTION Button_Back ();
        FUNCTION Button_Call ();
        FUNCTION Button_Graphics ();
        FUNCTION Button_Hangup ();
        FUNCTION Button_Mute ();
        FUNCTION Button_VolumePlus ();
        FUNCTION Button_VolumeMinus ();
        FUNCTION Button_Camera ();
        FUNCTION Button_Delete ();
        FUNCTION Button_Directory ();
        FUNCTION Button_Home ();
        FUNCTION Button_Keyboard ();
        FUNCTION Button_Menu ();
        FUNCTION Button_Pip ();
        FUNCTION Button_Preset ();
        FUNCTION Button_Info ();
        FUNCTION StartCOMTransport ( STRING username , STRING password );
        FUNCTION StopCOMTransport ();
        FUNCTION ReceiveData ( STRING data );
        FUNCTION SendSSHData ( STRING data );
        FUNCTION StartSshTransport ( STRING hostAddress , INTEGER port , INTEGER secureMode , STRING username , STRING password );
        FUNCTION StopSshTransport ();
        FUNCTION AcceptSshKey ( STRING key );
        FUNCTION RejectSshKey ();
        FUNCTION SetLoggingMode ( INTEGER value );
        FUNCTION Enable_NatH323Compatible ();
        FUNCTION Disable_NatH323Compatible ();
        FUNCTION Enable_H323Gateway ();
        FUNCTION Disable_H323Gateway ();
        FUNCTION Enable_IpH323 ();
        FUNCTION Disable_IpH323 ();
        FUNCTION Enable_Dhcp ();
        FUNCTION Disable_Dhcp ();
        FUNCTION Enable_SipDebug ();
        FUNCTION Disable_SipDebug ();
        FUNCTION Enable_Sip ();
        FUNCTION Disable_Sip ();
        FUNCTION Set_E164Extension ( STRING value );
        FUNCTION Set_GateKeeperIp ( STRING value );
        FUNCTION Set_H323Name ( STRING value );
        FUNCTION Set_GatekeeperMode ( INTEGER value );
        FUNCTION Set_DefaultGateway ( STRING value );
        FUNCTION Set_DnsServer ( INTEGER serverNumber , STRING value );
        FUNCTION Set_Hostname ( STRING value );
        FUNCTION Set_IpAddress ( STRING value );
        FUNCTION Set_LanPort ( INTEGER value );
        FUNCTION Set_NatConfiguration ( INTEGER value );
        FUNCTION Set_SubnetMask ( STRING value );
        FUNCTION Set_WanIpAddress ( STRING value );
        FUNCTION Set_SipAccountName ( STRING value );
        FUNCTION Set_SipPassword ( STRING value );
        FUNCTION Set_SipProxyServer ( STRING value );
        FUNCTION Set_SipRegistrarServer ( STRING value );
        FUNCTION Set_SipTransportProtocol ( INTEGER value );
        FUNCTION Set_SipUsername ( STRING value );
        FUNCTION SetCalendarDateFormat ( INTEGER format );
        FUNCTION SetCalendarTimeFormat ( INTEGER format );
        FUNCTION Calendar_EnableInitialPoll ( INTEGER enabled );
        FUNCTION UpdateCalendarMeetings ();
        FUNCTION SelectCalendarMeeting ( INTEGER index );
        FUNCTION SelectCalendarDialingMethod ( INTEGER index );
        FUNCTION DialSelectedCalendarMeeting ( INTEGER meetingIndex , INTEGER callMethodIndex );
        FUNCTION Enable_CalenderPlayTone ();
        FUNCTION Disable_CalenderPlayTone ();
        FUNCTION Enable_CalenderService ();
        FUNCTION Disable_CalenderService ();
        FUNCTION Enable_PrivateMeetingsDisplayed ();
        FUNCTION Disable_PrivateMeetingsDisplayed ();
        FUNCTION Enable_LdapDirectoryServer ();
        FUNCTION Disable_LdapDirectoryServer ();
        FUNCTION Enable_LdapSsl ();
        FUNCTION Disable_LdapSsl ();
        FUNCTION Enable_Snmp ();
        FUNCTION Disable_Snmp ();
        FUNCTION Set_CalenderPassword ( STRING value );
        FUNCTION Set_CalenderRemainingTime ( INTEGER value );
        FUNCTION Set_MailboxAccountAddress ( STRING value );
        FUNCTION Set_CalenderServer ( STRING value );
        FUNCTION Set_CalenderUser ( STRING value );
        FUNCTION Set_LdapAuthenticationType ( INTEGER value );
        FUNCTION Set_LdapBaseName ( STRING value );
        FUNCTION Set_LdapBindDn ( STRING value );
        FUNCTION Set_LdapNtlmDomain ( STRING value );
        FUNCTION Set_LdapPassword ( STRING value );
        FUNCTION Set_NtlmLdapPassword ( STRING value );
        FUNCTION Set_BasicLdapPassword ( STRING value );
        FUNCTION Set_LdapServerAddress ( STRING value );
        FUNCTION Set_LdapServerPort ( INTEGER value );
        FUNCTION Set_LdapUsername ( STRING value );
        FUNCTION Set_SnmpAdminName ( STRING value );
        FUNCTION Set_SnmpCommunityName ( STRING value );
        FUNCTION Set_SnmpConsoleIpAddress ( STRING value );
        FUNCTION Set_SnmpLocationName ( STRING value );
        FUNCTION Set_SnmpSystemDescription ( STRING value );
        FUNCTION Set_SnmpTrapVersion ( INTEGER value );
        FUNCTION SystemUsername ( STRING value );
        FUNCTION SystemPassword ( STRING value );
        FUNCTION Sleep ();
        FUNCTION Wake ();
        FUNCTION Reboot ();
        FUNCTION Enable_DaylightSavings ();
        FUNCTION Disable_DaylightSavings ();
        FUNCTION Enable_EchoReply ();
        FUNCTION Disable_EchoReply ();
        FUNCTION Enable_RemoteEnable ();
        FUNCTION Disable_RemoteEnable ();
        FUNCTION Enable_BasicMode ();
        FUNCTION Disable_BasicMode ();
        FUNCTION Set_SystemName ( STRING value );
        FUNCTION Enable_CameraTracking ();
        FUNCTION Disable_CameraTracking ();
        FUNCTION Enable_SelfView ();
        FUNCTION Disable_SelfView ();
        FUNCTION Auto_Selfview ();
        FUNCTION Set_LineOutMode ( INTEGER value );
        FUNCTION Set_CurrentPlayingSource ( INTEGER value );
        FUNCTION Set_CurrentMappedSource ( INTEGER value );
        FUNCTION Set_Camera1Content ( INTEGER value );
        FUNCTION Set_Camera2Content ( INTEGER value );
        FUNCTION Set_Camera3Content ( INTEGER value );
        FUNCTION Set_Camera4Content ( INTEGER value );
        FUNCTION Set_VgaQualityPreference ( INTEGER value );
        FUNCTION Set_PrimaryCamera ( INTEGER value );
        FUNCTION Set_SleepTime ( INTEGER value );
        FUNCTION Select_NearEndCamera ( INTEGER value );
        FUNCTION ZoomIn_NearEndCamera ();
        FUNCTION ZoomOut_NearEndCamera ();
        FUNCTION MoveUp_NearEndCamera ();
        FUNCTION MoveDown_NearEndCamera ();
        FUNCTION MoveLeft_NearEndCamera ();
        FUNCTION MoveRight_NearEndCamera ();
        FUNCTION ReleaseMovement_NearEndCamera ();
        FUNCTION SelectPreset_NearEndCamera ( INTEGER value );
        FUNCTION SetPreset_NearEndCamera ( INTEGER value );
        FUNCTION Select_FarEndCamera ( INTEGER value );
        FUNCTION ZoomIn_FarEndCamera ();
        FUNCTION ZoomOut_FarEndCamera ();
        FUNCTION MoveUp_FarEndCamera ();
        FUNCTION MoveDown_FarEndCamera ();
        FUNCTION MoveLeft_FarEndCamera ();
        FUNCTION MoveRight_FarEndCamera ();
        FUNCTION ReleaseMovement_FarEndCamera ();
        FUNCTION SelectPreset_FarEndCamera ( INTEGER value );
        FUNCTION SetPreset_FarEndCamera ( INTEGER value );
        FUNCTION SendContentSource ( INTEGER value );
        FUNCTION PlaceVideoCall ( STRING stringToDial , INTEGER callRate , INTEGER callProtocol );
        FUNCTION PlaceAudioCall ( STRING stringToDial , INTEGER callProtocol );
        FUNCTION Dtmf_Tone_0 ();
        FUNCTION Dtmf_Tone_1 ();
        FUNCTION Dtmf_Tone_2 ();
        FUNCTION Dtmf_Tone_3 ();
        FUNCTION Dtmf_Tone_4 ();
        FUNCTION Dtmf_Tone_5 ();
        FUNCTION Dtmf_Tone_6 ();
        FUNCTION Dtmf_Tone_7 ();
        FUNCTION Dtmf_Tone_8 ();
        FUNCTION Dtmf_Tone_9 ();
        FUNCTION Dtmf_Tone_Pound ();
        FUNCTION Dtmf_Tone_Asterisk ();
        FUNCTION AnswerIncomingCall ();
        FUNCTION IgnoreIncomingCall ();
        FUNCTION CancelOutgoingCall ();
        FUNCTION HangUpAllCalls ();
        FUNCTION HangUpCall1 ();
        FUNCTION HangUpCall2 ();
        FUNCTION HangUpCall3 ();
        FUNCTION HangUpCall4 ();
        FUNCTION HangUpCall5 ();
        FUNCTION Set_ConnectionPreference ( INTEGER value );
        FUNCTION Set_DialingMethod ( INTEGER value );
        FUNCTION Set_PointToPointAutoAnswerMode ( INTEGER value );
        FUNCTION Enable_DynamicBandwidth ();
        FUNCTION Disable_DynamicBandwidth ();
        FUNCTION Enable_Pvec ();
        FUNCTION Disable_Pvec ();
        FUNCTION Enable_Rsvp ();
        FUNCTION Disable_Rsvp ();
        FUNCTION Enable_H329 ();
        FUNCTION Disable_H329 ();
        FUNCTION Enable_MuteAutoAnsweredCalls ();
        FUNCTION Disable_MuteAutoAnsweredCalls ();
        FUNCTION Enable_DisplayIconsInCall ();
        FUNCTION Disable_DisplayIconsInCall ();
        FUNCTION Enable_Transcoding ();
        FUNCTION Disable_Transcoding ();
        FUNCTION Set_AesEncryptionMode ( INTEGER value );
        FUNCTION Set_MaxTimeInCall ( INTEGER value );
        FUNCTION Set_MultiPointAutoAnswerMode ( INTEGER value );
        FUNCTION Set_MpMode ( INTEGER value );
        FUNCTION Set_MaxRxBandwidth ( INTEGER value );
        FUNCTION Set_MaxTxBandwidth ( INTEGER value );
        FUNCTION Mute_Volume ();
        FUNCTION Unmute_Volume ();
        FUNCTION StopPlayingSource ();
        FUNCTION Set_AudioTransmitLevel ( SIGNED_INTEGER value );
        FUNCTION Set_LineInLevel ( INTEGER value );
        FUNCTION Set_Volume ( INTEGER value );
        FUNCTION Increase_Volume ();
        FUNCTION Decrease_Volume ();
        FUNCTION Release_Volume ();
        FUNCTION Set_MediaInLevel ( INTEGER value );
        FUNCTION Enable_Mute ();
        FUNCTION Disable_Mute ();
        FUNCTION Enable_EchoCanceller ();
        FUNCTION Disable_EchoCanceller ();
        FUNCTION Enable_KeyboardNoiseReduction ();
        FUNCTION Disable_KeyboardNoiseReduction ();
        FUNCTION Enable_LiveMusicMode ();
        FUNCTION Disable_LiveMuseMode ();
        FUNCTION Enable_PolycomMicrophones ();
        FUNCTION Disable_PolycomMicrophones ();
        FUNCTION Enable_Stereo ();
        FUNCTION Disable_Stereo ();
        FUNCTION Enable_AutoShowContent ();
        FUNCTION Disable_AutoShowContent ();
        FUNCTION Enable_ContentAuto ();
        FUNCTION Disable_ContentAuto ();
        FUNCTION Enable_FarControlNearCamera ();
        FUNCTION Disable_FarControlNearCamera ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        PolycomCall IncomingCall;
        PolycomCall OutgoingCall;
        PolycomCall ConnectedCalls[];
        DirectoryObject LocalDirectory[];
        DirectoryObject GlobalDirectory[];
        DirectoryObject GlobalDirectorySearchResults[];
        CalendarMeeting CalendarMeetings[];
        CalendarMeeting SelectedCalendarMeeting;
        DialingNumber SelectedCalendarDialingNumber;

        // class properties
        DelegateProperty SimplIntegerUpdated ValidInputs_Update;
        DelegateProperty SimplSignedIntegerUpdated AudioTransmitLevel_ValueUpdate;
        DelegateProperty SimplIntegerUpdated LineInLevel_ValueUpdate;
        DelegateProperty SimplIntegerUpdated Volume_ValueUpdate;
        DelegateProperty SimplIntegerUpdated MediaInLevel_ValueUpdate;
        DelegateProperty SimplIntegerUpdated Mute_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated FarEndMute_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated EchoCanceller_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated KeyboardNoiseReduction_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated LiveMusicMode_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated LineOutMode_ValueUpdate;
        DelegateProperty SimplIntegerUpdated PolycomMicrophones_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Stereo_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated SourcePlaying_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated AutoShowContent_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated ContentAuto_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated FarControlNearCamera_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated CameraTracking_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated SelfView_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated SelfView_IsAutoUpdate;
        DelegateProperty SimplIntegerUpdated Selected_NearEndCameraUpdate;
        DelegateProperty SimplIntegerUpdated Selected_FarEndCameraUpdate;
        DelegateProperty SimplIntegerUpdated DynamicBandwidth_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Pvec_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Rsvp_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated MuteAutoAnsweredCalls_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated DisplayIconsInCall_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Transcoding_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated MaxTimeInCall_ValueUpdate;
        DelegateProperty SimplIntegerUpdated MaxRxBandwidth_ValueUpdate;
        DelegateProperty SimplIntegerUpdated MaxTxBandwidth_ValueUpdate;
        DelegateProperty SimplIntegerUpdated AesEncryptionMode_ValueUpdate;
        DelegateProperty SimplIntegerUpdated MultiPointAutoAnswerMode_ValueUpdate;
        DelegateProperty SimplIntegerUpdated MpMode_ValueUpdate;
        DelegateProperty SimplIntegerUpdated NatH323Compatible_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated H323Gateway_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated IpH323_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Dhcp_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated H460FirewallTraversal_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated SipDebug_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Sip_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated GateKeeperMode_ValueUpdate;
        DelegateProperty SimplIntegerUpdated LanPort_ValueUpdate;
        DelegateProperty SimplIntegerUpdated NatConfiguration_ValueUpdate;
        DelegateProperty SimplIntegerUpdated SipTransportProtocol_ValueUpdate;
        DelegateProperty SimplIntegerUpdated CalenderPlayTone_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated CalenderService_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Calender_IsConnectedUpdate;
        DelegateProperty SimplIntegerUpdated CalenderRemainingTime_ValueUpdate;
        DelegateProperty SimplIntegerUpdated PrivateMeetingsDisplay_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated LdapDirectoryServer_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated LdapSsl_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated LdapAuthenticationType_ValueUpdate;
        DelegateProperty SimplIntegerUpdated Snmp_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated SnmpTrapVersion_ValueUpdate;
        DelegateProperty SimplIntegerUpdated Sleep_IsActiveUpdate;
        DelegateProperty SimplIntegerUpdated DaylightSavingsTime_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated EchoReply_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated Remote_IsEnabledUpdate;
        DelegateProperty SimplIntegerUpdated BasicMode_IsEnabledUpdate;
        DelegateProperty SimplStringUpdated Version_ValueUpdate;
        DelegateProperty SimplStringUpdated SerialNumber_ValueUpdate;
        DelegateProperty SimplStringUpdated Model_ValueUpdate;
        DelegateProperty SimplStringUpdated SystemName_ValueUpdate;
        DelegateProperty SimplIntegerUpdated LocalDirectory_IsSearchingUpdate;
        DelegateProperty SimplStringUpdated LocalDirectory_SelectedNameUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectory_IsSearchingUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectoryLiveSearch_IsSearchingUpdate;
        DelegateProperty SimplStringUpdated GlobalDirectory_SelectedNameUpdate;
        DelegateProperty SimplStringUpdated GlobalDirectoryLiveSearch_SelectedNameUpdate;
        DelegateProperty SimplIntegerUpdated COMTransport_ConnectionStateUpdate;
        DelegateProperty SimplIntegerUpdated COMTransport_ProcessingDataUpdate;
        DelegateProperty SimplIntegerUpdated SshTransport_ConnectionStateUpdate;
        DelegateProperty SimplIntegerUpdated SshTransport_ProcessingDataUpdate;
        DelegateProperty SimplIntegerUpdated LdapServerPort;
        DelegateProperty SimplStringUpdated CalenderPasswordUpdate;
        DelegateProperty SimplStringUpdated MailboxAccountAddressUpdate;
        DelegateProperty SimplStringUpdated CalenderServerUpdate;
        DelegateProperty SimplStringUpdated CalenderUserUpdate;
        DelegateProperty SimplStringUpdated LdapBaseNameUpdate;
        DelegateProperty SimplStringUpdated LdapBindDnUpdate;
        DelegateProperty SimplStringUpdated LdapNtlmDomainUpdate;
        DelegateProperty SimplStringUpdated LdapPasswordUpdate;
        DelegateProperty SimplStringUpdated LdapServerAddressUpdate;
        DelegateProperty SimplStringUpdated LdapUsernameUpdate;
        DelegateProperty SimplStringUpdated SnmpAdminNameUpdate;
        DelegateProperty SimplStringUpdated SnmpCommunityNameUpdate;
        DelegateProperty SimplStringUpdated SnmpConsoleIpAddressUpdate;
        DelegateProperty SimplStringUpdated SnmpLocationNameUpdate;
        DelegateProperty SimplStringUpdated SnmpSystemDescriptionUpdate;
        DelegateProperty SimplStringUpdated E164ExtensionUpdate;
        DelegateProperty SimplStringUpdated GateKeeperIpUpdate;
        DelegateProperty SimplStringUpdated H323NameUpdate;
        DelegateProperty SimplStringUpdated DefaultGatewayUpdate;
        DelegateProperty SimplStringUpdated DnsServer1Update;
        DelegateProperty SimplStringUpdated DnsServer2Update;
        DelegateProperty SimplStringUpdated DnsServer3Update;
        DelegateProperty SimplStringUpdated DnsServer4Update;
        DelegateProperty SimplStringUpdated HostnameUpdate;
        DelegateProperty SimplStringUpdated IpAddressUpdate;
        DelegateProperty SimplStringUpdated SubnetMaskUpdate;
        DelegateProperty SimplStringUpdated WanIpAddressUpdate;
        DelegateProperty SimplStringUpdated SipAccountNameUpdate;
        DelegateProperty SimplStringUpdated SipPasswordUpdate;
        DelegateProperty SimplStringUpdated SipProxyServerUpdate;
        DelegateProperty SimplStringUpdated SipRegistrarServerUpdate;
        DelegateProperty SimplStringUpdated SipUsernameUpdate;
        DelegateProperty SimplIntegerUpdated LocalDirectoryUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectoryUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectoryLiveSearchUpdate;
        DelegateProperty SimplIntegerUpdated PointToPointAutoAnswerModeUpdate;
        DelegateProperty SimplIntegerUpdated ConnectionPreference_ValueUpdate;
        DelegateProperty SimplIntegerUpdated DialingMethod_ValueUpdate;
        DelegateProperty SimplStringUpdated TransmitData;
        DelegateProperty SimplIntegerUpdated IncomingCallUpdate;
        DelegateProperty SimplIntegerUpdated OutgoingCallUpdate;
        DelegateProperty SimplIntegerUpdated ConnectedCallUpdate;
        DelegateProperty SimplIntegerUpdated CurrentPlayingSourceUpdate;
        DelegateProperty SimplIntegerUpdated CurrentMappedSourceUpdate;
        DelegateProperty SimplIntegerUpdated Camera1ContentUpdate;
        DelegateProperty SimplIntegerUpdated Camera2ContentUpdate;
        DelegateProperty SimplIntegerUpdated Camera3ContentUpdate;
        DelegateProperty SimplIntegerUpdated Camera4ContentUpdate;
        DelegateProperty SimplIntegerUpdated VgaQualityPreferenceUpdate;
        DelegateProperty SimplIntegerUpdated PrimaryCameraUpdate;
        DelegateProperty SimplIntegerUpdated SleepTimeUpdate;
        DelegateProperty SimplIntegerUpdated H239_IsEnabledUpdate;
        DelegateProperty SimplStringUpdated SshUnknownKeyReceivedUpdate;
        DelegateProperty SimplIntegerUpdated LocalSearchCompleteUpdate;
        DelegateProperty SimplIntegerUpdated GlobalSearchCompleteUpdate;
        DelegateProperty SimplIntegerUpdated LocalDirectory_SelectedItemIsGroupUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectory_SelectedItemIsGroupUpdate;
        DelegateProperty SimplIntegerUpdated GlobalDirectoryLiveSearch_SelectedItemIsGroupUpdate;
        DelegateProperty SimplIntegerUpdated VolumeMute_IsEnabledValueUpdate;
        DelegateProperty SimplIntegerUpdated CalendarMeetingsUpdate;
        DelegateProperty SimplIntegerUpdated CalendarMeetingInfoUpdate;
        DelegateProperty SimplIntegerUpdated CalendarContactMethodUpdate;
        DelegateProperty SimplStringUpdated SSHRxData;
        DelegateProperty SimplIntegerStringUpdated LocalPhonebookNameUpdate;
        DelegateProperty SimplIntegerStringUpdated GlobalPhonebookNameUpdate;
        DelegateProperty SimplIntegerStringUpdated GlobalLiveSearchPhonebookNameUpdate;
        DelegateProperty SimplIntegerStringStringUpdated CalendarNameUpdate;
        DelegateProperty SimplStringsUpdated SelectedCalendarMeetingStringsUpdate;
        DelegateProperty SimplStringStringUpdated IncomingCallNameUpdate;
        DelegateProperty SimplStringStringUpdated OutgoingCallNameUpdate;
        DelegateProperty SimplIntegerStringStringUpdated ConnectedCallsNameUpdate;
    };

    static class AudioVideoUpdate // enum
    {
        static SIGNED_LONG_INTEGER AudioTransmitLevel;
        static SIGNED_LONG_INTEGER MicrophoneMute;
        static SIGNED_LONG_INTEGER FarEndMute;
        static SIGNED_LONG_INTEGER Volume;
        static SIGNED_LONG_INTEGER EchoCanceller;
        static SIGNED_LONG_INTEGER KeyboardNoiseReduction;
        static SIGNED_LONG_INTEGER LiveMusicMode;
        static SIGNED_LONG_INTEGER PolycomMicrophones;
        static SIGNED_LONG_INTEGER LineInLevel;
        static SIGNED_LONG_INTEGER LineOutMode;
        static SIGNED_LONG_INTEGER MediaInLevel;
        static SIGNED_LONG_INTEGER Stereo;
        static SIGNED_LONG_INTEGER PlayingSource;
        static SIGNED_LONG_INTEGER MappedSource;
        static SIGNED_LONG_INTEGER Playing;
        static SIGNED_LONG_INTEGER AutoShowContent;
        static SIGNED_LONG_INTEGER ContentAuto;
        static SIGNED_LONG_INTEGER CameraContent1;
        static SIGNED_LONG_INTEGER CameraContent2;
        static SIGNED_LONG_INTEGER CameraContent3;
        static SIGNED_LONG_INTEGER CameraContent4;
        static SIGNED_LONG_INTEGER VgaQualityPreference;
        static SIGNED_LONG_INTEGER FarControlNearCamera;
        static SIGNED_LONG_INTEGER PrimaryCamera;
        static SIGNED_LONG_INTEGER SleepTime;
        static SIGNED_LONG_INTEGER CameraTracking;
        static SIGNED_LONG_INTEGER NearCameraSelection;
        static SIGNED_LONG_INTEGER FarCameraSelection;
        static SIGNED_LONG_INTEGER SelfView;
        static SIGNED_LONG_INTEGER VolumeMute;
        static SIGNED_LONG_INTEGER ValidInputs;
    };

    static class CalenderRemainingTimes // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER _1;
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER _5;
        static SIGNED_LONG_INTEGER _10;
        static SIGNED_LONG_INTEGER _15;
        static SIGNED_LONG_INTEGER _30;
    };

    static class MpMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER Discussion;
        static SIGNED_LONG_INTEGER Presentation;
        static SIGNED_LONG_INTEGER Fullscreen;
    };

    static class SshSecurityModes // enum
    {
        static SIGNED_LONG_INTEGER Unsecure;
        static SIGNED_LONG_INTEGER Secure;
        static SIGNED_LONG_INTEGER ForceAccept;
    };

    static class GatekeeperMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Off;
        static SIGNED_LONG_INTEGER Specify;
        static SIGNED_LONG_INTEGER Auto;
    };

    static class Camera // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Camera1;
        static SIGNED_LONG_INTEGER Camera2;
        static SIGNED_LONG_INTEGER Camera3;
        static SIGNED_LONG_INTEGER Camera4;
    };

    static class TimeFormats // enum
    {
        static SIGNED_LONG_INTEGER TwelveHourMode;
        static SIGNED_LONG_INTEGER TwentyFourHourMode;
    };

    static class ConnectionStates // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Connecting;
        static SIGNED_LONG_INTEGER Connected;
        static SIGNED_LONG_INTEGER Disconnected;
        static SIGNED_LONG_INTEGER LoggedIn;
    };

    static class TransportTypes // enum
    {
        static SIGNED_LONG_INTEGER Telnet;
        static SIGNED_LONG_INTEGER Com;
        static SIGNED_LONG_INTEGER Ssh;
        static SIGNED_LONG_INTEGER Custom;
    };

    static class LdapAuthenticationTypes // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Anonymous;
        static SIGNED_LONG_INTEGER Basic;
        static SIGNED_LONG_INTEGER Ntlm;
    };

    static class AesEncryptionMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Yes;
        static SIGNED_LONG_INTEGER No;
        static SIGNED_LONG_INTEGER RequiredVideoCallsOnly;
        static SIGNED_LONG_INTEGER RequiredAllCalls;
    };

    static class NatConfiguration // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER Manual;
        static SIGNED_LONG_INTEGER Off;
    };

    static class SnmpTrapVersions // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER V1;
        static SIGNED_LONG_INTEGER V2c;
        static SIGNED_LONG_INTEGER V3;
    };

    static class DirectoryUpdate // enum
    {
        static SIGNED_LONG_INTEGER GlobalDirectoryUpdated;
        static SIGNED_LONG_INTEGER LocalDirectoryUpdated;
        static SIGNED_LONG_INTEGER DownloadingGlobalDirectory;
        static SIGNED_LONG_INTEGER DownloadingLocalDirectory;
        static SIGNED_LONG_INTEGER GlobalDirectoryDownloadComplete;
        static SIGNED_LONG_INTEGER LocalDirectoryDownloadComplete;
        static SIGNED_LONG_INTEGER GlobalDirectorySearchResultsUpdated;
        static SIGNED_LONG_INTEGER LocalDirectorySearchResultsUpdated;
        static SIGNED_LONG_INTEGER PerformingLiveGlobalDirectorySearch;
        static SIGNED_LONG_INTEGER CompletePerformingLiveGlobalDirectorySearch;
        static SIGNED_LONG_INTEGER GlobalDirectoryLiveSearchUpdated;
    };

     class DirectoryObject 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Uid[];
        STRING ParentUid[];
    };

    static class Input // enum
    {
        static SIGNED_LONG_INTEGER Input1;
        static SIGNED_LONG_INTEGER Input2;
        static SIGNED_LONG_INTEGER Input3;
        static SIGNED_LONG_INTEGER Input4;
        static SIGNED_LONG_INTEGER Input5;
        static SIGNED_LONG_INTEGER Input6;
        static SIGNED_LONG_INTEGER Input7;
        static SIGNED_LONG_INTEGER Input8;
        static SIGNED_LONG_INTEGER Input9;
    };

    static class NetworkUpdate // enum
    {
        static SIGNED_LONG_INTEGER E164Extension;
        static SIGNED_LONG_INTEGER GateKeeperIp;
        static SIGNED_LONG_INTEGER H323Name;
        static SIGNED_LONG_INTEGER NatH323Compatible;
        static SIGNED_LONG_INTEGER H323Gateway;
        static SIGNED_LONG_INTEGER IpH323Enabled;
        static SIGNED_LONG_INTEGER GateKeeperMode;
        static SIGNED_LONG_INTEGER DefaultGateway;
        static SIGNED_LONG_INTEGER Dhcp;
        static SIGNED_LONG_INTEGER Dns;
        static SIGNED_LONG_INTEGER H460FirewallTraversal;
        static SIGNED_LONG_INTEGER Hostname;
        static SIGNED_LONG_INTEGER IpAddress;
        static SIGNED_LONG_INTEGER LanPort;
        static SIGNED_LONG_INTEGER NatConfiguration;
        static SIGNED_LONG_INTEGER SubnetMask;
        static SIGNED_LONG_INTEGER WanIpAddress;
        static SIGNED_LONG_INTEGER SipAccountName;
        static SIGNED_LONG_INTEGER SipDebug;
        static SIGNED_LONG_INTEGER SipEnabled;
        static SIGNED_LONG_INTEGER SipProxyServer;
        static SIGNED_LONG_INTEGER SipRegistrarServer;
        static SIGNED_LONG_INTEGER SipTransportProtocol;
        static SIGNED_LONG_INTEGER SipUsername;
    };

    static class SipTransportProtocol // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER Tcp;
        static SIGNED_LONG_INTEGER Udp;
        static SIGNED_LONG_INTEGER Tls;
    };

    static class LanPort // enum
    {
        static SIGNED_LONG_INTEGER Unknwon;
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER _10Hdx;
        static SIGNED_LONG_INTEGER _10Fdx;
        static SIGNED_LONG_INTEGER _100Hdx;
        static SIGNED_LONG_INTEGER _100Ddx;
        static SIGNED_LONG_INTEGER _1000Hdx;
        static SIGNED_LONG_INTEGER _1000Fdx;
    };

     class PolycomCall 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Id[];
        STRING FarSiteName[];
        STRING FarSiteNumber[];
        STRING Speed[];
        STRING ConnectionStatus[];
        STRING MuteStatus[];
        STRING CallDirection[];
        STRING CallType[];
        STRING CallState[];
        INTEGER SimplIsAudioCall;
        INTEGER SimplIsVideoCall;
    };

     class SpeedDialEntry 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING SystemLabel[];
        STRING Type[];
    };

    static class CallFunctionUpdate // enum
    {
        static SIGNED_LONG_INTEGER ConnectionPreference;
        static SIGNED_LONG_INTEGER DialingMethod;
        static SIGNED_LONG_INTEGER CallStatus;
        static SIGNED_LONG_INTEGER IncomingCallUpdated;
        static SIGNED_LONG_INTEGER OutgoingCallUpdated;
        static SIGNED_LONG_INTEGER ConnectedCallUpdated;
    };

    static class CallRates // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER _128;
        static SIGNED_LONG_INTEGER _256;
        static SIGNED_LONG_INTEGER _384;
        static SIGNED_LONG_INTEGER _512;
        static SIGNED_LONG_INTEGER _768;
        static SIGNED_LONG_INTEGER _1024;
        static SIGNED_LONG_INTEGER _1472;
        static SIGNED_LONG_INTEGER _1920;
        static SIGNED_LONG_INTEGER _2048;
        static SIGNED_LONG_INTEGER _3072;
        static SIGNED_LONG_INTEGER _3840;
        static SIGNED_LONG_INTEGER _4096;
        static SIGNED_LONG_INTEGER _6144;
    };

    static class VideoCallTypes // enum
    {
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER H323;
        static SIGNED_LONG_INTEGER IP;
        static SIGNED_LONG_INTEGER SIP;
        static SIGNED_LONG_INTEGER Gateway;
    };

    static class AudioCallTypes // enum
    {
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER SIP;
        static SIGNED_LONG_INTEGER H323;
        static SIGNED_LONG_INTEGER SIPSpeakerphone;
        static SIGNED_LONG_INTEGER POTS;
    };

    static class ConnectionPreferences // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Video;
        static SIGNED_LONG_INTEGER Audio;
    };

    static class DialingMethods // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Auto;
        static SIGNED_LONG_INTEGER Manual;
    };

    static class LineOutMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Fixed;
        static SIGNED_LONG_INTEGER Variable;
    };

    static class SleepTime // enum
    {
        static SIGNED_LONG_INTEGER _0;
        static SIGNED_LONG_INTEGER _1;
        static SIGNED_LONG_INTEGER _3;
        static SIGNED_LONG_INTEGER _15;
        static SIGNED_LONG_INTEGER _30;
        static SIGNED_LONG_INTEGER _60;
        static SIGNED_LONG_INTEGER _120;
        static SIGNED_LONG_INTEGER _240;
        static SIGNED_LONG_INTEGER _480;
        static SIGNED_LONG_INTEGER Unknown;
    };

    static class PolycomMessageType // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER AudioVideo;
        static SIGNED_LONG_INTEGER CallFunction;
        static SIGNED_LONG_INTEGER Conference;
        static SIGNED_LONG_INTEGER Directory;
        static SIGNED_LONG_INTEGER GlobalServices;
        static SIGNED_LONG_INTEGER Network;
        static SIGNED_LONG_INTEGER System;
    };

    static class TransportEventType // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Connected;
        static SIGNED_LONG_INTEGER Connecting;
        static SIGNED_LONG_INTEGER Disconnected;
        static SIGNED_LONG_INTEGER LoggedIn;
        static SIGNED_LONG_INTEGER SSHUnknownHostKeyFingerprint;
    };

    static class VgaQualityMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER People;
        static SIGNED_LONG_INTEGER Content;
        static SIGNED_LONG_INTEGER Both;
    };

    static class CameraContent // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER People;
        static SIGNED_LONG_INTEGER Content;
    };

    static class PointToPointAutoAnswerMode // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER On;
        static SIGNED_LONG_INTEGER Off;
        static SIGNED_LONG_INTEGER DoNotDisturb;
    };

namespace PolycomGroupSeries.Protocol;
        // class declarations
         class ApiData;
     class ApiData 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static SIGNED_LONG_INTEGER DefaultSshPort;
        static STRING Delimiter[];

        // class properties
    };

