using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_PANASONIC_IP_CAMERA_V0_1
{
    public class UserModuleClass_IESS_PANASONIC_IP_CAMERA_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.DigitalInput POWER_ON;
        Crestron.Logos.SplusObjects.DigitalInput POWER_OFF;
        Crestron.Logos.SplusObjects.DigitalInput PAN_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput PAN_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput TILT_UP;
        Crestron.Logos.SplusObjects.DigitalInput TILT_DOWN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_IN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_OUT;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_SAVE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> PRESET;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.AnalogInput CAMERA_SELECT;
        Crestron.Logos.SplusObjects.AnalogInput PRESET_SELECT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        Crestron.Logos.SplusObjects.StringInput LOGINNAME;
        Crestron.Logos.SplusObjects.StringInput LOGINPASSWORD;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        SplusTcpClient TCPCLIENT;
        SDEVICE SOURCEDEVICE;
        ushort GVINDEX = 0;
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 103;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 105;
                if ( Functions.TestForTrue  ( ( LVTYPE)  ) ) 
                    {
                    __context__.SourceCodeLine = 106;
                    Trace( "Panasonic RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 108;
                    Trace( "Panasonic TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 114;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 116;
                if ( Functions.TestForTrue  ( ( SOURCEDEVICE.STATUSCONNECTED)  ) ) 
                    {
                    __context__.SourceCodeLine = 117;
                    LVSTATUS = (short) ( Functions.SocketDisconnectClient( TCPCLIENT ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 119;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 121;
                    if ( Functions.TestForTrue  ( ( Functions.Not( SOURCEDEVICE.STATUSCONNECTED ))  ) ) 
                        {
                        __context__.SourceCodeLine = 122;
                        LVSTATUS = (short) ( Functions.SocketConnectClient( TCPCLIENT , SOURCEDEVICE.IPADDRESS , (ushort)( SOURCEDEVICE.IPPORT ) , (ushort)( 0 ) ) ) ; 
                        }
                    
                    } 
                
                }
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMPSTRING;
            LVTEMPSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 172;
            SOURCEDEVICE . TXQUEUE  .UpdateValue ( SOURCEDEVICE . TXQUEUE + LVSTRING  ) ; 
            __context__.SourceCodeLine = 173;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , SOURCEDEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 175;
                CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
                __context__.SourceCodeLine = 176;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_35__" , 4 , __SPLS_TMPVAR__WAITLABEL_35___Callback ) ;
                __context__.SourceCodeLine = 173;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_35___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            __context__.SourceCodeLine = 179;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , SOURCEDEVICE . TXQUEUE )  ) ; 
            __context__.SourceCodeLine = 180;
            LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
            __context__.SourceCodeLine = 181;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 3 ))  ) ) 
                { 
                __context__.SourceCodeLine = 183;
                Functions.SocketSend ( TCPCLIENT , LVTEMP ) ; 
                __context__.SourceCodeLine = 184;
                SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                __context__.SourceCodeLine = 185;
                CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void BUILDSTRING (  SplusExecutionContext __context__, CrestronString LVTYPE , CrestronString LVINCOMING ) 
        { 
        CrestronString LVHEADER;
        LVHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        CrestronString LVLENGTH;
        LVLENGTH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 194;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == "GET"))  ) ) 
            { 
            __context__.SourceCodeLine = 196;
            LVHEADER  .UpdateValue ( "GET /cgi-bin/aw_ptz?cmd=" + LVINCOMING + "&res=1 HTTP/1.1" + SOURCEDEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 197;
            LVHEADER  .UpdateValue ( LVHEADER + "Host: " + SOURCEDEVICE . IPADDRESS + ":" + Functions.ItoA (  (int) ( SOURCEDEVICE.IPPORT ) ) + SOURCEDEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 198;
            LVHEADER  .UpdateValue ( LVHEADER + "Connection: keep-alive" + SOURCEDEVICE . ETX + "User-Agent: curl/7.45.0" + SOURCEDEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 199;
            LVHEADER  .UpdateValue ( LVHEADER + SOURCEDEVICE . ETX + "\u000B\u000B"  ) ; 
            } 
        
        __context__.SourceCodeLine = 201;
        SETQUEUE (  __context__ , LVHEADER) ; 
        
        }
        
    object TCPCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            
            __context__.SourceCodeLine = 210;
            SOURCEDEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 211;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 212;
            CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
object TCPCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 218;
        SOURCEDEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 219;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 220;
        CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 224;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 225;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 227;
            SOURCEDEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 228;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 232;
            SOURCEDEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 233;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVDATA;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4095, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2047, this );
        LVDATA  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 240;
        SOURCEDEVICE . RXQUEUE  .UpdateValue ( SOURCEDEVICE . RXQUEUE + TCPCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 241;
        SETDEBUG (  __context__ , TCPCLIENT.SocketRxBuf, (ushort)( 1 )) ; 
        __context__.SourceCodeLine = 242;
        Functions.ClearBuffer ( TCPCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 243;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A\u000D\u000A" , SOURCEDEVICE.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 245;
            LVRX  .UpdateValue ( Functions.Remove ( "\u000D\u000A\u000D\u000A" , SOURCEDEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 246;
            SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 247;
            if ( Functions.TestForTrue  ( ( Functions.Find( "HTTP/1.1 200 OK" , LVRX ))  ) ) 
                { 
                } 
            
            __context__.SourceCodeLine = 243;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 269;
        SOURCEDEVICE . STATUSPOLL = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 270;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.COMMANDPOLL) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_LEFT_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 274;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.PANLEFT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_LEFT_OnRelease_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 278;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.PANSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 282;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.PANRIGHT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnRelease_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 286;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.PANSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 290;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.TILTUP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnRelease_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 294;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.TILTSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 298;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.TILTDOWN) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnRelease_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 302;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.TILTSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 306;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.ZOOMIN) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnRelease_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 310;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.ZOOMSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 314;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.ZOOMOUT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnRelease_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 318;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.ZOOMSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_OnPush_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 338;
        GVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 340;
        MakeString ( LVSTRING , "{0}{1:d}", SOURCEDEVICE . PRESETRECALL , (short)(GVINDEX - 1)) ; 
        __context__.SourceCodeLine = 341;
        BUILDSTRING (  __context__ , "GET", LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_SAVE_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 346;
        if ( Functions.TestForTrue  ( ( GVINDEX)  ) ) 
            { 
            __context__.SourceCodeLine = 348;
            MakeString ( LVSTRING , "{0}{1:d}", SOURCEDEVICE . PRESETSAVE , (short)(GVINDEX - 1)) ; 
            __context__.SourceCodeLine = 349;
            BUILDSTRING (  __context__ , "GET", LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWER_ON_OnPush_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 354;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.POWERON) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWER_OFF_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 358;
        BUILDSTRING (  __context__ , "GET", SOURCEDEVICE.POWEROFF) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 363;
        SOURCEDEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 367;
        SOURCEDEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGINNAME_OnChange_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 371;
        SOURCEDEVICE . USERNAME  .UpdateValue ( LOGINNAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGINPASSWORD_OnChange_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 375;
        SOURCEDEVICE . PASSWORD  .UpdateValue ( LOGINPASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 379;
        BUILDSTRING (  __context__ , "GET", MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 386;
        SOURCEDEVICE . IPPORT = (ushort) ( 80 ) ; 
        __context__.SourceCodeLine = 387;
        SOURCEDEVICE . POWERON  .UpdateValue ( "#On"  ) ; 
        __context__.SourceCodeLine = 388;
        SOURCEDEVICE . POWEROFF  .UpdateValue ( "#Of"  ) ; 
        __context__.SourceCodeLine = 389;
        SOURCEDEVICE . PANLEFT  .UpdateValue ( "#P25"  ) ; 
        __context__.SourceCodeLine = 390;
        SOURCEDEVICE . PANRIGHT  .UpdateValue ( "#P75"  ) ; 
        __context__.SourceCodeLine = 391;
        SOURCEDEVICE . PANSTOP  .UpdateValue ( "#P50"  ) ; 
        __context__.SourceCodeLine = 392;
        SOURCEDEVICE . TILTUP  .UpdateValue ( "#T65"  ) ; 
        __context__.SourceCodeLine = 393;
        SOURCEDEVICE . TILTDOWN  .UpdateValue ( "#T35"  ) ; 
        __context__.SourceCodeLine = 394;
        SOURCEDEVICE . TILTSTOP  .UpdateValue ( "#T50"  ) ; 
        __context__.SourceCodeLine = 395;
        SOURCEDEVICE . ZOOMIN  .UpdateValue ( "#Z80"  ) ; 
        __context__.SourceCodeLine = 396;
        SOURCEDEVICE . ZOOMOUT  .UpdateValue ( "#Z20"  ) ; 
        __context__.SourceCodeLine = 397;
        SOURCEDEVICE . ZOOMSTOP  .UpdateValue ( "#Z50"  ) ; 
        __context__.SourceCodeLine = 398;
        SOURCEDEVICE . PRESETRECALL  .UpdateValue ( "#R0"  ) ; 
        __context__.SourceCodeLine = 399;
        SOURCEDEVICE . PRESETSAVE  .UpdateValue ( "#M0"  ) ; 
        __context__.SourceCodeLine = 400;
        SOURCEDEVICE . STX  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 401;
        SOURCEDEVICE . ETX  .UpdateValue ( "\u000D\u000A"  ) ; 
        __context__.SourceCodeLine = 402;
        SOURCEDEVICE . USERNAME  .UpdateValue ( "admin"  ) ; 
        __context__.SourceCodeLine = 403;
        SOURCEDEVICE . PASSWORD  .UpdateValue ( "password"  ) ; 
        __context__.SourceCodeLine = 404;
        SOURCEDEVICE . COMMANDPOLL  .UpdateValue ( "PW?"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    TCPCLIENT  = new SplusTcpClient ( 4095, this );
    SOURCEDEVICE  = new SDEVICE( this, true );
    SOURCEDEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    POWER_ON = new Crestron.Logos.SplusObjects.DigitalInput( POWER_ON__DigitalInput__, this );
    m_DigitalInputList.Add( POWER_ON__DigitalInput__, POWER_ON );
    
    POWER_OFF = new Crestron.Logos.SplusObjects.DigitalInput( POWER_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( POWER_OFF__DigitalInput__, POWER_OFF );
    
    PAN_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_LEFT__DigitalInput__, PAN_LEFT );
    
    PAN_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_RIGHT__DigitalInput__, PAN_RIGHT );
    
    TILT_UP = new Crestron.Logos.SplusObjects.DigitalInput( TILT_UP__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_UP__DigitalInput__, TILT_UP );
    
    TILT_DOWN = new Crestron.Logos.SplusObjects.DigitalInput( TILT_DOWN__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_DOWN__DigitalInput__, TILT_DOWN );
    
    ZOOM_IN = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_IN__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_IN__DigitalInput__, ZOOM_IN );
    
    ZOOM_OUT = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_OUT__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_OUT__DigitalInput__, ZOOM_OUT );
    
    PRESET_SAVE = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_SAVE__DigitalInput__, PRESET_SAVE );
    
    PRESET = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        PRESET[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( PRESET__DigitalInput__ + i, PRESET__DigitalInput__, this );
        m_DigitalInputList.Add( PRESET__DigitalInput__ + i, PRESET[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CAMERA_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( CAMERA_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( CAMERA_SELECT__AnalogSerialInput__, CAMERA_SELECT );
    
    PRESET_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( PRESET_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( PRESET_SELECT__AnalogSerialInput__, PRESET_SELECT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    LOGINNAME = new Crestron.Logos.SplusObjects.StringInput( LOGINNAME__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINNAME__AnalogSerialInput__, LOGINNAME );
    
    LOGINPASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGINPASSWORD__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGINPASSWORD__AnalogSerialInput__, LOGINPASSWORD );
    
    __SPLS_TMPVAR__WAITLABEL_35___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_35___CallbackFn );
    
    TCPCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketConnect_0, false ) );
    TCPCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketDisconnect_1, false ) );
    TCPCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketStatus_2, false ) );
    TCPCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    PAN_LEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnPush_6, false ) );
    PAN_LEFT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnRelease_7, false ) );
    PAN_RIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnPush_8, false ) );
    PAN_RIGHT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnRelease_9, false ) );
    TILT_UP.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_UP_OnPush_10, false ) );
    TILT_UP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_UP_OnRelease_11, false ) );
    TILT_DOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnPush_12, false ) );
    TILT_DOWN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnRelease_13, false ) );
    ZOOM_IN.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnPush_14, false ) );
    ZOOM_IN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnRelease_15, false ) );
    ZOOM_OUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnPush_16, false ) );
    ZOOM_OUT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnRelease_17, false ) );
    for( uint i = 0; i < 16; i++ )
        PRESET[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_OnPush_18, false ) );
        
    PRESET_SAVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_SAVE_OnPush_19, false ) );
    POWER_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWER_ON_OnPush_20, false ) );
    POWER_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWER_OFF_OnPush_21, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_22, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_23, false ) );
    LOGINNAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGINNAME_OnChange_24, false ) );
    LOGINPASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGINPASSWORD_OnChange_25, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_26, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_PANASONIC_IP_CAMERA_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_35___Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint POWER_ON__DigitalInput__ = 3;
const uint POWER_OFF__DigitalInput__ = 4;
const uint PAN_LEFT__DigitalInput__ = 5;
const uint PAN_RIGHT__DigitalInput__ = 6;
const uint TILT_UP__DigitalInput__ = 7;
const uint TILT_DOWN__DigitalInput__ = 8;
const uint ZOOM_IN__DigitalInput__ = 9;
const uint ZOOM_OUT__DigitalInput__ = 10;
const uint PRESET_SAVE__DigitalInput__ = 11;
const uint PRESET__DigitalInput__ = 12;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint CAMERA_SELECT__AnalogSerialInput__ = 1;
const uint PRESET_SELECT__AnalogSerialInput__ = 2;
const uint IP_ADDRESS__AnalogSerialInput__ = 3;
const uint MANUALCMD__AnalogSerialInput__ = 4;
const uint LOGINNAME__AnalogSerialInput__ = 5;
const uint LOGINPASSWORD__AnalogSerialInput__ = 6;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SDEVICE : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  STATUSPOLL = 0;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  POWERON;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  POWEROFF;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  PANLEFT;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  PANRIGHT;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  PANSTOP;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  TILTUP;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  TILTDOWN;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  TILTSTOP;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  ZOOMIN;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  ZOOMOUT;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  ZOOMSTOP;
    
    [SplusStructAttribute(15, false, false)]
    public CrestronString  PRESETRECALL;
    
    [SplusStructAttribute(16, false, false)]
    public CrestronString  PRESETSAVE;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  STX;
    
    [SplusStructAttribute(19, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(20, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(21, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  USERNAME;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  PASSWORD;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(25, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SDEVICE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        POWERON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        POWEROFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PANLEFT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PANRIGHT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PANSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        TILTUP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        TILTDOWN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        TILTSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        ZOOMIN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        ZOOMOUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        ZOOMSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PRESETRECALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PRESETSAVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        USERNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        PASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, Owner );
        
        
    }
    
}

}
