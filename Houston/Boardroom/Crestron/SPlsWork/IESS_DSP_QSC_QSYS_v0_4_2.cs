using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_QSYS_V0_4_2
{
    public class UserModuleClass_IESS_DSP_QSC_QSYS_V0_4_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        SplusTcpClient AUDIOCLIENT;
        SAUDIO AUDIODEVICE;
        ushort GVVOIPCOUNTER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 140;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 142;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                __context__.SourceCodeLine = 143;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 144;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 146;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 148;
                    LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 158;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 160;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 162;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 163;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 165;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 166;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 168;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 169;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 172;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 174;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 175;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 176;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 178;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 179;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 181;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 182;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 183;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 184;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 185;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 193;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 194;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMEINCOMING >= LVMAXIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 195;
                return (uint)( 65535) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 196;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMEINCOMING <= LVMINIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 197;
                    return (uint)( 0) ; 
                    }
                
                else 
                    { 
                    __context__.SourceCodeLine = 201;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 203;
                        LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                        __context__.SourceCodeLine = 204;
                        LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                        __context__.SourceCodeLine = 205;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 207;
                            LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                            __context__.SourceCodeLine = 208;
                            LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 210;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 211;
                                LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 215;
                        LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                        __context__.SourceCodeLine = 216;
                        LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 219;
                    LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
                    __context__.SourceCodeLine = 220;
                    LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
                    __context__.SourceCodeLine = 222;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 223;
                        LVVOLUMELEVEL = (uint) ( 0 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 224;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                            {
                            __context__.SourceCodeLine = 225;
                            LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 226;
                    return (uint)( LVVOLUMELEVEL) ; 
                    } 
                
                }
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 231;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 233;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 234;
                    Trace( "QSYS RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 235;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 0))  ) ) 
                        {
                        __context__.SourceCodeLine = 236;
                        Trace( "QSYS TX: {0}", LVSTRING ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 237;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 3))  ) ) 
                            {
                            __context__.SourceCodeLine = 238;
                            Trace( "QSYS DATA: {0}", LVSTRING ) ; 
                            }
                        
                        }
                    
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            
            __context__.SourceCodeLine = 246;
            LVTEMP  .UpdateValue ( LVSTRING + AUDIODEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 247;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 1710))  ) ) 
                {
                __context__.SourceCodeLine = 248;
                Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 250;
                TX  .UpdateValue ( LVTEMP  ) ; 
                }
            
            __context__.SourceCodeLine = 251;
            SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
            
            }
            
        private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 256;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATE[ LVINDEX ] ))  ) ) 
                {
                __context__.SourceCodeLine = 257;
                AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 260;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ LVINDEX ] ]  ) ; 
                __context__.SourceCodeLine = 261;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.pinpad." + LVNUM + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 262;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 263;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            
            }
            
        private void POLLINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 270;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 271;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 272;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 273;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 274;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 276;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 278;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 280;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                        __context__.SourceCodeLine = 281;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 283;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 286;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 288;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 289;
                                LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 291;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 293;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                    __context__.SourceCodeLine = 294;
                                    LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 296;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 298;
                                        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                        ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                        int __FN_FORSTEP_VAL__1 = (int)1; 
                                        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                            { 
                                            __context__.SourceCodeLine = 300;
                                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                            __context__.SourceCodeLine = 301;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 302;
                                                LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 298;
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 305;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 308;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 309;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 315;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
                { 
                __context__.SourceCodeLine = 317;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AddComponentControl\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022, \u0022Component\u0022 : "  ) ; 
                __context__.SourceCodeLine = 318;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 319;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 321;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                    __context__.SourceCodeLine = 328;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 330;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 332;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                        __context__.SourceCodeLine = 333;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 335;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 337;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 338;
                            LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                            __context__.SourceCodeLine = 339;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 341;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 343;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 344;
                                LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 352;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 354;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 357;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 359;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 360;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 361;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 362;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                                        __context__.SourceCodeLine = 363;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 },"  ) ; 
                                        __context__.SourceCodeLine = 364;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022recent.calls\u0022 }"  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 366;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 368;
                                            GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                            __context__.SourceCodeLine = 369;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 370;
                                                AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 372;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 374;
                                                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                                ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                                int __FN_FORSTEP_VAL__1 = (int)1; 
                                                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                                    { 
                                                    __context__.SourceCodeLine = 376;
                                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                                    __context__.SourceCodeLine = 377;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 378;
                                                        LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 374;
                                                    } 
                                                
                                                __context__.SourceCodeLine = 380;
                                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 382;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } } }"  ) ; 
                __context__.SourceCodeLine = 383;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 384;
                POLLINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            
            }
            
        private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 390;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 392;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
                __context__.SourceCodeLine = 390;
                } 
            
            __context__.SourceCodeLine = 394;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_25__" , 200 , __SPLS_TMPVAR__WAITLABEL_25___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_25___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 395;
            SETQUEUE (  __context__ , AUDIODEVICE.COMMANDAUTOPOLL) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void CHECKINSTANCETAGS (  SplusExecutionContext __context__, CrestronString LVINSTANCETAG , CrestronString LVPARSEDSTRING ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        ushort FVOIPNAME = 0;
        ushort FVOIPNUM = 0;
        
        short LVVOL = 0;
        
        CrestronString LVVOLUME;
        CrestronString LVSTRING;
        CrestronString LVTRASH;
        CrestronString LVINSTANCEINDEX;
        CrestronString LVCOMPARESTRING;
        LVVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        LVINSTANCEINDEX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVCOMPARESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 402;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 404;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINSTANCETAG == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                { 
                __context__.SourceCodeLine = 406;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 408;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022gain\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 410;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 411;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 412;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 414;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 415;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 417;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 418;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                __context__.SourceCodeLine = 419;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 420;
                                    LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 422;
                                    LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                    }
                                
                                __context__.SourceCodeLine = 423;
                                AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                __context__.SourceCodeLine = 424;
                                VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                __context__.SourceCodeLine = 425;
                                if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 426;
                                    AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                    }
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 430;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 432;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 433;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 434;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 436;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 437;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 439;
                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 440;
                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 442;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 444;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 445;
                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 450;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 452;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022output." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 454;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 455;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022output." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 456;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 457;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 458;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 460;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "gain\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 462;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 463;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "gain\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 464;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 466;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 467;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 469;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 470;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 471;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 472;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 474;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 475;
                                            AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            __context__.SourceCodeLine = 476;
                                            VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                            __context__.SourceCodeLine = 477;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 478;
                                                AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 482;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "mute\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 484;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 485;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "mute\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 486;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 488;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 489;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 490;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 492;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 493;
                                            VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 495;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 497;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 498;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 504;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "wall." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 506;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 507;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022wall." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 508;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 509;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 510;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 512;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "open\u0022,\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 514;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 515;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "open\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 516;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 518;
                                        AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 520;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 522;
                                            AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 523;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 524;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 527;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 529;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 530;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 531;
                                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 532;
                                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSWALL[ LVCOUNTER ])  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 537;
                                        AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 538;
                                        GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 545;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 547;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022select." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 549;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 550;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022select." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 551;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 552;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 553;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 555;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 557;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 558;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 560;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 561;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 1), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 562;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVVOLUME ) ) ; 
                                            __context__.SourceCodeLine = 563;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOLUMEMUTE[ LVCOUNTER ] ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 564;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 569;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 571;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 572;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022mute." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 573;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 574;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 575;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 577;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 579;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 580;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 582;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 583;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 585;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 587;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 588;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 594;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 596;
                                LVCOMPARESTRING  .UpdateValue ( "\u0022Name\u0022:\u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) ) + ".gain\u0022"  ) ; 
                                __context__.SourceCodeLine = 597;
                                if ( Functions.TestForTrue  ( ( Functions.Find( LVCOMPARESTRING , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 599;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 600;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( LVCOMPARESTRING , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 601;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 603;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 604;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 606;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 607;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 608;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 609;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 611;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 612;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVVOL == Functions.ToSignedLongInteger( -( 100 ) )))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 614;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 615;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 619;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 620;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 626;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 628;
                                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                                    ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                    int __FN_FORSTEP_VAL__2 = (int)1; 
                                    for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                        { 
                                        __context__.SourceCodeLine = 630;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 632;
                                            LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                            __context__.SourceCodeLine = 633;
                                            break ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 628;
                                        } 
                                    
                                    __context__.SourceCodeLine = 636;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 638;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 639;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 640;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 642;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 643;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 644;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 645;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 647;
                                                AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 648;
                                                VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 650;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 652;
                                                    AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 653;
                                                    VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 657;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 659;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 660;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 661;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 663;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 664;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 665;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 666;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 668;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 669;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 671;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 673;
                                                    AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 674;
                                                    VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 678;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 680;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 681;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 682;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 684;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 685;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 686;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 687;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 689;
                                                AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 690;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 692;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 694;
                                                    AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 695;
                                                    VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 699;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.ringing\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 701;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 703;
                                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                            __context__.SourceCodeLine = 704;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 705;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 706;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 707;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 708;
                                                VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 713;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.status\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 715;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 716;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.status\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 717;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 719;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Dialing - " , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 720;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 722;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Idle" , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 723;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 725;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "Incoming Call" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 727;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "Incoming Call" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 728;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( " - " , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 731;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( " - " , LVSTRING )  ) ; 
                                                        __context__.SourceCodeLine = 732;
                                                        FVOIPNAME = (ushort) ( (Functions.Find( "\u0028" , LVSTRING ) - 1) ) ; 
                                                        __context__.SourceCodeLine = 733;
                                                        VOIP_CALLINCOMINGNAME_FB [ LVVOIPNUM]  .UpdateValue ( Functions.Remove ( FVOIPNAME, LVSTRING )  ) ; 
                                                        __context__.SourceCodeLine = 734;
                                                        FVOIPNUM = (ushort) ( (Functions.Find( "\u0022" , LVSTRING ) - 1) ) ; 
                                                        __context__.SourceCodeLine = 735;
                                                        VOIP_CALLINCOMINGNUM [ LVVOIPNUM]  .UpdateValue ( Functions.Remove ( FVOIPNUM, LVSTRING )  ) ; 
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 738;
                                                    VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 742;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 744;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 745;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 746;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Choices\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 748;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Text\u005C\u0022:\u005C\u0022" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 750;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Text\u005C\u0022:\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 751;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 752;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 753;
                                                AUDIODEVICE . STATUSVOIPRECENTCALL [ LVVOIPNUM ]  .UpdateValue ( LVSTRING  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 758;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 760;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 761;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022load." , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 763;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022load." , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 764;
                                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 765;
                                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                            __context__.SourceCodeLine = 766;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Color\u0022:\u0022@" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 768;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Color\u0022:\u0022@" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 769;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 770;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 771;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "7F7F"))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 772;
                                                    GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVINSTANCEINDEX ) ) ; 
                                                    }
                                                
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 402;
            } 
        
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVSTRING;
        CrestronString LVTAG;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTAG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 784;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , AUDIODEVICE.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 786;
            AUDIODEVICE . STATUSPARSING = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 787;
            LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , AUDIODEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 788;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 789;
            SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 790;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
                {
                __context__.SourceCodeLine = 791;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
                }
            
            __context__.SourceCodeLine = 792;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Changes\u0022:[{" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 794;
                LVTRASH  .UpdateValue ( Functions.Remove ( "Changes\u0022:[{" , LVRX )  ) ; 
                __context__.SourceCodeLine = 795;
                while ( Functions.TestForTrue  ( ( Functions.Find( "Component\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 797;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Component\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 798;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 799;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 800;
                    if ( Functions.TestForTrue  ( ( Functions.Find( ",{\u0022Component\u0022:\u0022" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 802;
                        LVSTRING  .UpdateValue ( Functions.Remove ( ",{\u0022Component\u0022:\u0022" , LVRX )  ) ; 
                        __context__.SourceCodeLine = 803;
                        LVRX  .UpdateValue ( ",{\u0022Component\u0022:\u0022" + LVRX  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 806;
                        LVSTRING  .UpdateValue ( LVRX  ) ; 
                        }
                    
                    __context__.SourceCodeLine = 807;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVSTRING) ; 
                    __context__.SourceCodeLine = 795;
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 810;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 812;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 813;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 814;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 815;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVRX) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 784;
            } 
        
        __context__.SourceCodeLine = 818;
        AUDIODEVICE . STATUSPARSING = (ushort) ( 0 ) ; 
        
        }
        
    object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 827;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 828;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 829;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            __context__.SourceCodeLine = 830;
            AUDIODEVICE . RECONNECTING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 831;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.LOGINPASSWORD ) > 3 ))  ) ) 
                { 
                __context__.SourceCodeLine = 833;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022:\u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022:\u0022Logon\u0022, \u0022params\u0022:{ \u0022User\u0022:\u0022" + AUDIODEVICE . LOGINNAME + "\u0022, "  ) ; 
                __context__.SourceCodeLine = 834;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022Password\u0022:\u0022" + AUDIODEVICE . LOGINPASSWORD + "\u0022 } }"  ) ; 
                __context__.SourceCodeLine = 835;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            __context__.SourceCodeLine = 837;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_26__" , 200 , __SPLS_TMPVAR__WAITLABEL_26___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_26___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 838;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 843;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 844;
        AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 845;
        AUDIODEVICE . STATUSPARSING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 846;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 847;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 852;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 853;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 855;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 856;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 860;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 861;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 862;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 868;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 869;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 870;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ) ) && Functions.TestForTrue ( Functions.Not( AUDIODEVICE.STATUSPARSING ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 871;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 875;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 877;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 878;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 882;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 883;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 888;
        SETQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLL) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 892;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 893;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 897;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 901;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 905;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 909;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 913;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 919;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 920;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 921;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 923;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 924;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 925;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 927;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 928;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 930;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 931;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 934;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 936;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 938;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 945;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 946;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 948;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 949;
            CreateWait ( "VOLUP" , 50 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 946;
            } 
        
        __context__.SourceCodeLine = 969;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 952;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 954;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 955;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 956;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 957;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 958;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 959;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 960;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 961;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 962;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 963;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 964;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 965;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 973;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 974;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 976;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 977;
            CreateWait ( "VOLDN" , 10 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 974;
            } 
        
        __context__.SourceCodeLine = 997;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 980;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 982;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 983;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 984;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 985;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 986;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 987;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 988;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 989;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 990;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 991;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 992;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 993;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1002;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1003;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1005;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1006;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1007;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1009;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1011;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1012;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1013;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1015;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1019;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1021;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1022;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1023;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                        __context__.SourceCodeLine = 1024;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1026;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1030;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1031;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1036;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1037;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1039;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1040;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
            __context__.SourceCodeLine = 1041;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1043;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1045;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1046;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1047;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1049;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1052;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1054;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1055;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1056;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                        __context__.SourceCodeLine = 1057;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1059;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1062;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1063;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1068;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1069;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1071;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1073;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1074;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1075;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1077;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1079;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1080;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1081;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1083;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1086;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1088;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1089;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1090;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                            __context__.SourceCodeLine = 1091;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1093;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1096;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1097;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1101;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1103;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1104;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1105;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1107;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1109;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1110;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1111;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1113;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1116;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1118;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1119;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1120;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                            __context__.SourceCodeLine = 1121;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1123;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1126;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1127;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVSTRINGVOL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1134;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1135;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1136;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1138;
            MakeString ( LVSTRINGVOL , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1139;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1141;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1142;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1143;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1145;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1147;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1148;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1149;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 1151;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1152;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        CrestronString LVWALLCONFIG;
        CrestronString LVNAME;
        CrestronString LVSTRINGWALL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVWALLCONFIG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVSTRINGWALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1159;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1160;
        LVNAME  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
        __context__.SourceCodeLine = 1161;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1163;
            if ( Functions.TestForTrue  ( ( Functions.Not( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ))  ) ) 
                { 
                __context__.SourceCodeLine = 1165;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1166;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 0 },"  ) ; 
                __context__.SourceCodeLine = 1167;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022String\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1168;
                LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1169;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1170;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1175;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)64; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 1177;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1179;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1181;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1183;
                                LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1175;
                    } 
                
                __context__.SourceCodeLine = 1188;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)64; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 1190;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1192;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1194;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1196;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ]  ) ; 
                                __context__.SourceCodeLine = 1197;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                                __context__.SourceCodeLine = 1198;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                                __context__.SourceCodeLine = 1199;
                                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ) + "\u0022 } ] } }"  ) ; 
                                __context__.SourceCodeLine = 1200;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1201;
                                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1188;
                    } 
                
                __context__.SourceCodeLine = 1206;
                LVWALLCONFIG  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 1207;
                AUDIODEVICE . STATUSGROUP [ AUDIODEVICE.LASTINDEX] = (ushort) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
                __context__.SourceCodeLine = 1208;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)64; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 1210;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1212;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1214;
                            LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1208;
                    } 
                
                __context__.SourceCodeLine = 1218;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1219;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                __context__.SourceCodeLine = 1220;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1221;
                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "" + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1222;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1223;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1227;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1229;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1230;
                if ( Functions.TestForTrue  ( ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1232;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: "  ) ; 
                    __context__.SourceCodeLine = 1233;
                    LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + " }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1236;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    }
                
                __context__.SourceCodeLine = 1237;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1238;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1239;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1241;
                if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1243;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1244;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1245;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    __context__.SourceCodeLine = 1246;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1247;
                    POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1249;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1257;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1268;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1269;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1274;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1275;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1276;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1277;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1278;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1280;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1281;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1282;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1283;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1288;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1289;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1290;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1291;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1292;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1293;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1298;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1299;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1300;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1301;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1302;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1303;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1308;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1309;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1310;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1311;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1312;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1314;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1315;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1316;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1317;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1322;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1323;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1324;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1325;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1326;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1327;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1332;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1333;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1334;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1335;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1336;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1337;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1342;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1343;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1344;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1345;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1346;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1347;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1352;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1353;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1354;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1355;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1356;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1357;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        __context__.SourceCodeLine = 1358;
        VOIP_CALLINCOMING_FB [ AUDIODEVICE.LASTINDEX]  .Value = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1363;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1364;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1365;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022hook.flash\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1366;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1367;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1368;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1376;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1377;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.STATUSVOIPRECENTCALL[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1379;
            VOIP_DIALSTRING [ AUDIODEVICE.LASTINDEX]  .UpdateValue ( AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1380;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1381;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1382;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1383;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1384;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1385;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1386;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1387;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1388;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1394;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1398;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1402;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1406;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1410;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1414;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1418;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1422;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1426;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1430;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1434;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1438;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1446;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1447;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1449;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1450;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1451;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1452;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1453;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1454;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1455;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1456;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1457;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1463;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1464;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1465;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1466;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1467;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1468;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1472;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1473;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1484;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1485;
        AUDIODEVICE . IPPORT = (ushort) ( 1710 ) ; 
        __context__.SourceCodeLine = 1486;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1487;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1488;
        AUDIODEVICE . ETX  .UpdateValue ( "\u0000"  ) ; 
        __context__.SourceCodeLine = 1489;
        AUDIODEVICE . COMMANDPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022StatusGet\u0022, \u0022params\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1490;
        AUDIODEVICE . COMMANDAUTOPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AutoPoll\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022,\u0022Rate\u0022: 5 } }"  ) ; 
        __context__.SourceCodeLine = 1491;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1493;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 20 ) ) ) ; 
            __context__.SourceCodeLine = 1494;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 1495;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1491;
            } 
        
        __context__.SourceCodeLine = 1497;
        AUDIODEVICE . VOLUMEMIN [ 1] = (short) ( Functions.ToInteger( -( 60 ) ) ) ; 
        __context__.SourceCodeLine = 1498;
        AUDIODEVICE . VOLUMEMAX [ 1] = (short) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 65534, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 1023, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_25___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_25___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_26___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_26___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_QSYS_V0_4_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_25___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_26___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint INSTANCETAGS__AnalogSerialInput__ = 200;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 264;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  RECONNECTING = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  STATUSPARSING = 0;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [] STATUSWALL;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSGROUP;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(9, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(10, false, false)]
    public ushort  [] STATUSVOIPCALLSTATE;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  [] STATUSVOIPRECENTCALL;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  COMMANDAUTOPOLL;
    
    [SplusStructAttribute(15, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(16, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(17, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(18, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(20, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(21, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(22, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(25, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(26, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(27, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(28, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(29, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(30, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(31, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(32, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(33, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(34, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(35, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(36, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSWALL  = new ushort[ 65 ];
        STATUSGROUP  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPCALLSTATE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMMANDAUTOPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        STATUSVOIPRECENTCALL  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            STATUSVOIPRECENTCALL [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
