namespace Clock_Audio_CDT100;
        // class declarations
         class PortUtil;
         class ClockAudioCDT100;
         class DeviceTypes;
         class ClockAudioCDT100State;
     class PortUtil 
    {
        // class delegates

        // class events

        // class functions
        static SIGNED_LONG_INTEGER_FUNCTION GetNextLocalPort ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class ClockAudioCDT100 
    {
        // class delegates
        delegate FUNCTION StringCallback ( SIMPLSHARPSTRING msg );
        delegate FUNCTION StateFBCallback ( ClockAudioCDT100State state );
        delegate FUNCTION PresetSavedCallback ( );
        delegate FUNCTION InvalidAdapterCallback ( INTEGER state );

        // class events

        // class functions
        FUNCTION DoUpdateFB ();
        FUNCTION Print ( STRING data );
        FUNCTION Reinit ( INTEGER deviceType , STRING remoteAddress , INTEGER remotePort , INTEGER adapterType );
        FUNCTION SetPolling ( INTEGER state );
        FUNCTION SetPhantomOnAll ();
        FUNCTION SetPhantomOffAll ();
        FUNCTION SetPhantomOn ( INTEGER channel );
        FUNCTION SetPhantomOff ( INTEGER channel );
        FUNCTION SetArmCUp ();
        FUNCTION SetArmCDown ();
        FUNCTION SetLedRedOnAll ();
        FUNCTION SetLedRedOffAll ();
        FUNCTION SetLedGreenOnAll ();
        FUNCTION SetLedGreenOffAll ();
        FUNCTION SetLedBlueOnAll ();
        FUNCTION SetLedBlueOffAll ();
        FUNCTION SetLedRedOn ( INTEGER channel );
        FUNCTION SetLedRedOff ( INTEGER channel );
        FUNCTION SetLedGreenOn ( INTEGER channel );
        FUNCTION SetLedGreenOff ( INTEGER channel );
        FUNCTION SetLedBlueOn ( INTEGER channel );
        FUNCTION SetLedBlueOff ( INTEGER channel );
        FUNCTION SetLedRedBrightAll ( INTEGER level );
        FUNCTION SetLedRedBright ( INTEGER channel , INTEGER level );
        FUNCTION SetLedGreenBrightAll ( INTEGER level );
        FUNCTION SetLedGreenBright ( INTEGER channel , INTEGER level );
        FUNCTION SetLedBlueBrightAll ( INTEGER level );
        FUNCTION SetLedBlueBright ( INTEGER channel , INTEGER level );
        FUNCTION PresetSave ();
        FUNCTION PresetLoad ();
        FUNCTION EnQueue ( STRING msg );
        FUNCTION SetLocalAddress ( STRING address );
        STRING_FUNCTION GetLocalAddress ();
        FUNCTION SetLocalPort ( INTEGER port );
        SIGNED_LONG_INTEGER_FUNCTION GetLocalPort ();
        FUNCTION SetRemoteAddress ( STRING address );
        STRING_FUNCTION GetRemoteAddress ();
        FUNCTION SetRemotePort ( INTEGER port );
        INTEGER_FUNCTION GetRemotePort ();
        FUNCTION SetAdapterType ( INTEGER type );
        SIGNED_LONG_INTEGER_FUNCTION GetAdapterType ();
        FUNCTION StartAsync ();
        FUNCTION SetMode ( DeviceTypes device );
        INTEGER_FUNCTION GetHeartbeatTimeout ();
        INTEGER_FUNCTION GetResponseTimeout ();
        FUNCTION SetFailedResponseCount ( INTEGER count );
        INTEGER_FUNCTION GetFailedResponseCount ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static INTEGER MAX_CHANNELS;
        static INTEGER MAX_IP_ADDRESS;
        static INTEGER MAX_QUEUE;
        static INTEGER MAX_BUFFER;
        static INTEGER MAX_FAILED_RSP;
        static INTEGER MAX_HEARTBEAT_TIME;
        static INTEGER MAX_RESPONSE_TIME;
        static INTEGER POLL_TIME;
        static INTEGER STATE_OFF;
        static INTEGER STATE_ON;
        static INTEGER MODE_MKI;
        static INTEGER MODE_MKII;
        static STRING DEV_CMD_DELIM[];
        static STRING DEV_CMD_SEPERATOR[];
        static STRING DEV_CMD_SET_PHANTOM[];
        static STRING DEV_CMD_QUERY[];
        static STRING DEV_CMD_SET_CH32[];
        static STRING DEV_CMD_GET_CH32[];
        static STRING DEV_CMD_SET_CH32_B[];
        static STRING DEV_CMD_GET_CH32_B[];
        static STRING DEV_CMD_SET_ARM_C[];
        static STRING DEV_CMD_GET_ARM_C[];
        static STRING DEV_CMD_VERSION[];
        static STRING DEV_CMD_ASYNC[];
        static STRING DEV_CMD_INPUT_STATUS[];
        static STRING DEV_CMD_LED_RED[];
        static STRING DEV_CMD_LED_GREEN[];
        static STRING DEV_CMD_LED_BLUE[];
        static STRING DEV_CMD_INPUT_CH32[];
        static STRING DEV_CMD_INPUT_TS_1[];
        static STRING DEV_CMD_INPUT_TS_2[];
        static STRING DEV_CMD_INPUT_TS_3[];
        static STRING DEV_CMD_ID[];
        static STRING DEV_CMD_ADDRESS[];
        static STRING DEV_CMD_PRESET_SAVE[];
        static STRING DEV_CMD_PRESET_LOAD[];
        static STRING DEV_CMD_SET_MODE[];
        static STRING DEV_CMD_GET_MODE[];
        static STRING DEV_CMD_SET_TS[];
        static STRING DEV_CMD_GET_TS[];
        static STRING DEV_CMD_SET_RGB[];
        static STRING DEV_CMD_GET_RGB[];
        static STRING DEV_CMD_SET_TSB[];
        static STRING DEV_CMD_GET_TSB[];
        static STRING DEV_RSP_ACK[];
        static STRING DEV_RSP_NACK[];
        static INTEGER MIN_BRIGHTNESS_MKI;
        static INTEGER MAX_BRIGHTNESS_MKI;
        static INTEGER MULTIPLIER_MKI;
        static INTEGER MIN_BRIGHTNESS_MKII;
        static INTEGER MAX_BRIGHTNESS_MKII;

        // class properties
        DelegateProperty StringCallback SendTrace;
        DelegateProperty StateFBCallback UpdateFB;
        DelegateProperty PresetSavedCallback PresetSaved;
        DelegateProperty InvalidAdapterCallback InvalidAdapter;
        ClockAudioCDT100State myState;
    };

    static class DeviceTypes // enum
    {
        static SIGNED_LONG_INTEGER CDT100mki;
        static SIGNED_LONG_INTEGER CDT100mkii;
        static SIGNED_LONG_INTEGER C303D;
        static SIGNED_LONG_INTEGER Unknown;
    };

     class ClockAudioCDT100State 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION ResetAllStates ();
        FUNCTION GetInitialized ();
        FUNCTION InitAllStates ();
        INTEGER_FUNCTION GetIsInitialized ();
        FUNCTION SetLedRedStateOn ( INTEGER channel );
        FUNCTION SetLedRedStateOff ( INTEGER channel );
        FUNCTION SetLedRedStateOnAll ();
        FUNCTION SetLedRedStateOffAll ();
        INTEGER_FUNCTION GetLedRedState ( INTEGER channel );
        FUNCTION SetLedRedStateBright ( INTEGER channel , INTEGER level );
        FUNCTION SetLedRedStateBrightAll ( INTEGER level );
        INTEGER_FUNCTION GetLedRedStateBright ( INTEGER channel );
        FUNCTION SetLedGreenStateOn ( INTEGER channel );
        FUNCTION SetLedGreenStateOff ( INTEGER channel );
        FUNCTION SetLedGreenStateOnAll ();
        FUNCTION SetLedGreenStateOffAll ();
        INTEGER_FUNCTION GetLedGreenState ( INTEGER channel );
        FUNCTION SetLedGreenStateBright ( INTEGER channel , INTEGER level );
        FUNCTION SetLedGreenStateBrightAll ( INTEGER level );
        INTEGER_FUNCTION GetLedGreenStateBright ( INTEGER channel );
        FUNCTION SetLedBlueStateOn ( INTEGER channel );
        FUNCTION SetLedBlueStateOff ( INTEGER channel );
        FUNCTION SetLedBlueStateOnAll ();
        FUNCTION SetLedBlueStateOffAll ();
        INTEGER_FUNCTION GetLedBlueState ( INTEGER channel );
        FUNCTION SetLedBlueStateBright ( INTEGER channel , INTEGER level );
        FUNCTION SetLedBlueStateBrightAll ( INTEGER level );
        INTEGER_FUNCTION GetLedBlueStateBright ( INTEGER channel );
        FUNCTION SetPhantomStateOn ( INTEGER channel );
        FUNCTION SetPhantomStateOff ( INTEGER channel );
        FUNCTION SetPhantomStateOnAll ();
        FUNCTION SetPhantomStateOffAll ();
        INTEGER_FUNCTION GetPhantomState ( INTEGER channel );
        FUNCTION SetArmCStateUp ();
        FUNCTION SetArmCStateDown ();
        INTEGER_FUNCTION GetArmCState ();
        FUNCTION SetInputCH32StateOn ( INTEGER channel );
        FUNCTION SetInputCH32StateOff ( INTEGER channel );
        FUNCTION SetInputCH32StateOnAll ();
        FUNCTION SetInputCH32StateOffAll ();
        INTEGER_FUNCTION GetInputCH32State ( INTEGER channel );
        FUNCTION SetInputTSStateOn ( INTEGER channel , INTEGER sw );
        FUNCTION SetInputTSStateOff ( INTEGER channel , INTEGER sw );
        FUNCTION SetInputTSStateOnAll ( INTEGER sw );
        FUNCTION SetInputTSStateOffAll ( INTEGER sw );
        INTEGER_FUNCTION GetInputTSState ( INTEGER channel , INTEGER sw );
        FUNCTION SetSwitchAddress ( STRING address );
        STRING_FUNCTION GetSwitchAddress ();
        FUNCTION SetIPAddress ( STRING address );
        STRING_FUNCTION GetIPAddress ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

