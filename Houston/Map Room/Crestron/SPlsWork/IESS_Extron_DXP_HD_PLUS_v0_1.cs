using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_EXTRON_DXP_HD_PLUS_V0_1
{
    public class UserModuleClass_IESS_EXTRON_DXP_HD_PLUS_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.AnalogInput RECALL_PRESET;
        Crestron.Logos.SplusObjects.StringInput RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> SOURCE_TO_OUTPUT;
        Crestron.Logos.SplusObjects.StringOutput TX__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> STATUS_OUTPUT;
        SSWITCHER SWITCHERDEVICE;
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            
            __context__.SourceCodeLine = 78;
            if ( Functions.TestForTrue  ( ( SWITCHERDEVICE.DEBUG)  ) ) 
                {
                __context__.SourceCodeLine = 79;
                Trace( "DEBUG: {0}", LVSTRING ) ; 
                }
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            
            __context__.SourceCodeLine = 83;
            SWITCHERDEVICE . TXQUEUE  .UpdateValue ( SWITCHERDEVICE . TXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 84;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , SWITCHERDEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 86;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_5__" , 10 , __SPLS_TMPVAR__WAITLABEL_5___Callback ) ;
                __context__.SourceCodeLine = 84;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_5___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            __context__.SourceCodeLine = 89;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , SWITCHERDEVICE . TXQUEUE )  ) ; 
            __context__.SourceCodeLine = 90;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 92;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 93;
                LVTEMP  .UpdateValue ( LVTEMP + SWITCHERDEVICE . ETX  ) ; 
                __context__.SourceCodeLine = 94;
                TX__DOLLAR__  .UpdateValue ( LVTEMP  ) ; 
                __context__.SourceCodeLine = 95;
                SETDEBUG (  __context__ , LVTEMP) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void ROUTEVIDEO (  SplusExecutionContext __context__, ushort LVINPUT , ushort LVOUTPUT ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        __context__.SourceCodeLine = 103;
        MakeString ( LVSTRING , "{0:d}*{1:d}!", (short)LVINPUT, (short)LVOUTPUT) ; 
        __context__.SourceCodeLine = 104;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        }
        
    object DEBUG_OnChange_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 111;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                {
                __context__.SourceCodeLine = 112;
                SWITCHERDEVICE . DEBUG = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 114;
                SWITCHERDEVICE . DEBUG = (ushort) ( 0 ) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object POLL_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 118;
        SETQUEUE (  __context__ , "S") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOURCE_TO_OUTPUT_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 123;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 124;
        ROUTEVIDEO (  __context__ , (ushort)( SOURCE_TO_OUTPUT[ LVINDEX ] .UshortValue ), (ushort)( LVINDEX )) ; 
        __context__.SourceCodeLine = 125;
        STATUS_OUTPUT [ LVINDEX]  .Value = (ushort) ( SOURCE_TO_OUTPUT[ LVINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RECALL_PRESET_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        __context__.SourceCodeLine = 130;
        MakeString ( LVSTRING , "{0:d}.", (short)RECALL_PRESET  .UshortValue) ; 
        __context__.SourceCodeLine = 131;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 207;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 214;
        SWITCHERDEVICE . ETX  .UpdateValue ( "\u000D"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    SWITCHERDEVICE  = new SSWITCHER( this, true );
    SWITCHERDEVICE .PopulateCustomAttributeList( false );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    RECALL_PRESET = new Crestron.Logos.SplusObjects.AnalogInput( RECALL_PRESET__AnalogSerialInput__, this );
    m_AnalogInputList.Add( RECALL_PRESET__AnalogSerialInput__, RECALL_PRESET );
    
    SOURCE_TO_OUTPUT = new InOutArray<AnalogInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SOURCE_TO_OUTPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( SOURCE_TO_OUTPUT__AnalogSerialInput__ + i, SOURCE_TO_OUTPUT__AnalogSerialInput__, this );
        m_AnalogInputList.Add( SOURCE_TO_OUTPUT__AnalogSerialInput__ + i, SOURCE_TO_OUTPUT[i+1] );
    }
    
    STATUS_OUTPUT = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        STATUS_OUTPUT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_OUTPUT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( STATUS_OUTPUT__AnalogSerialOutput__ + i, STATUS_OUTPUT[i+1] );
    }
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( RX__DOLLAR____AnalogSerialInput__, 255, this );
    m_StringInputList.Add( RX__DOLLAR____AnalogSerialInput__, RX__DOLLAR__ );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__DOLLAR____AnalogSerialOutput__, TX__DOLLAR__ );
    
    __SPLS_TMPVAR__WAITLABEL_5___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_5___CallbackFn );
    
    DEBUG.OnDigitalChange.Add( new InputChangeHandlerWrapper( DEBUG_OnChange_0, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_1, false ) );
    for( uint i = 0; i < 8; i++ )
        SOURCE_TO_OUTPUT[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( SOURCE_TO_OUTPUT_OnChange_2, false ) );
        
    RECALL_PRESET.OnAnalogChange.Add( new InputChangeHandlerWrapper( RECALL_PRESET_OnChange_3, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_4, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_EXTRON_DXP_HD_PLUS_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_5___Callback;


const uint DEBUG__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint RECALL_PRESET__AnalogSerialInput__ = 0;
const uint RX__DOLLAR____AnalogSerialInput__ = 1;
const uint MANUALCMD__AnalogSerialInput__ = 2;
const uint SOURCE_TO_OUTPUT__AnalogSerialInput__ = 3;
const uint TX__DOLLAR____AnalogSerialOutput__ = 0;
const uint STATUS_OUTPUT__AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SSWITCHER : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  DEBUG = 0;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  ETX;
    
    
    public SSWITCHER( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        
        
    }
    
}

}
