using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_PASSWORD_V0_2
{
    public class UserModuleClass_IESS_PASSWORD_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput START;
        Crestron.Logos.SplusObjects.DigitalInput PASSWORD_ENTER;
        Crestron.Logos.SplusObjects.DigitalInput PASSWORD_SAVE;
        Crestron.Logos.SplusObjects.StringInput FILENAME;
        Crestron.Logos.SplusObjects.StringInput PASSWORD_ENTRY;
        Crestron.Logos.SplusObjects.StringInput NEW_PASSWORD;
        Crestron.Logos.SplusObjects.DigitalOutput PASSWORD_VALID;
        Crestron.Logos.SplusObjects.DigitalOutput PASSWORD_INVALID;
        Crestron.Logos.SplusObjects.DigitalOutput PASSWORD_CHANGE_SUCCESS;
        Crestron.Logos.SplusObjects.StringOutput PASSWORD_STATUS;
        Crestron.Logos.SplusObjects.StringOutput PASSWORD_CURRENT;
        Crestron.Logos.SplusObjects.StringOutput PASSWORD_STRING;
        CrestronString GVADMINPASSWORD;
        CrestronString GVFILEPASSWORD;
        CrestronString GVENTRYPASSWORD;
        private void FILEOPENCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVREADFILE;
            CrestronString LVREADLINE;
            CrestronString LVFILENAME;
            LVREADFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
            LVREADLINE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            
            __context__.SourceCodeLine = 73;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 75;
                LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
                __context__.SourceCodeLine = 76;
                StartFileOperations ( ) ; 
                __context__.SourceCodeLine = 77;
                LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 0) ) ) ; 
                __context__.SourceCodeLine = 78;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 80;
                    LVREAD = (ushort) ( FileRead( (short)( LVHANDLE ) , LVREADFILE , (ushort)( 127 ) ) ) ; 
                    __context__.SourceCodeLine = 81;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , LVREADFILE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 83;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , LVREADFILE )  ) ; 
                        __context__.SourceCodeLine = 84;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( (Functions.Length( LVREADLINE ) - 2), LVREADLINE )  ) ; 
                        __context__.SourceCodeLine = 85;
                        Trace( "CONFIG READ: {0}", LVREADLINE ) ; 
                        __context__.SourceCodeLine = 86;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( LVREADLINE ) >= 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( LVREADLINE ) <= 32 ) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 87;
                            GVFILEPASSWORD  .UpdateValue ( LVREADLINE  ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 89;
                            GVFILEPASSWORD  .UpdateValue ( GVADMINPASSWORD  ) ; 
                            }
                        
                        __context__.SourceCodeLine = 90;
                        PASSWORD_CURRENT  .UpdateValue ( GVFILEPASSWORD  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 92;
                    LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 93;
                    EndFileOperations ( ) ; 
                    } 
                
                } 
            
            
            }
            
        private void WRITEDATATOCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVFILENAME;
            CrestronString LVWRITEFILE;
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVWRITEFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
            
            
            __context__.SourceCodeLine = 102;
            LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
            __context__.SourceCodeLine = 103;
            LVWRITEFILE  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 104;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 105;
            LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 1) ) ) ; 
            __context__.SourceCodeLine = 106;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 108;
                LVWRITEFILE  .UpdateValue ( NEW_PASSWORD + "\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 109;
                GVFILEPASSWORD  .UpdateValue ( NEW_PASSWORD  ) ; 
                __context__.SourceCodeLine = 110;
                PASSWORD_CURRENT  .UpdateValue ( NEW_PASSWORD  ) ; 
                __context__.SourceCodeLine = 111;
                NEW_PASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 112;
                GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 113;
                PASSWORD_STRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 114;
                PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 115;
                PASSWORD_STATUS  .UpdateValue ( "Password Change Successful"  ) ; 
                __context__.SourceCodeLine = 116;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_0__" , 100 , __SPLS_TMPVAR__WAITLABEL_0___Callback ) ;
                __context__.SourceCodeLine = 121;
                FileWrite (  (short) ( LVHANDLE ) , LVWRITEFILE ,  (ushort) ( Functions.Length( LVWRITEFILE ) ) ) ; 
                __context__.SourceCodeLine = 122;
                LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                __context__.SourceCodeLine = 123;
                EndFileOperations ( ) ; 
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_0___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 118;
            PASSWORD_STATUS  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 119;
            Functions.Pulse ( 20, PASSWORD_CHANGE_SUCCESS ) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    object START_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 131;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 132;
                FILEOPENCONFIG (  __context__  ) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object PASSWORD_ENTRY_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVPASSWORD;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        LVPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 138;
        LVSTRING  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 139;
        GVENTRYPASSWORD  .UpdateValue ( PASSWORD_ENTRY  ) ; 
        __context__.SourceCodeLine = 140;
        Trace( "Password Entry: {0}", GVENTRYPASSWORD ) ; 
        __context__.SourceCodeLine = 141;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GVENTRYPASSWORD ) > 1 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GVENTRYPASSWORD ) <= 32 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 143;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)(Functions.Length( GVENTRYPASSWORD ) - 1); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 145;
                LVSTRING  .UpdateValue ( LVSTRING + "*"  ) ; 
                __context__.SourceCodeLine = 143;
                } 
            
            __context__.SourceCodeLine = 147;
            LVSTRING  .UpdateValue ( LVSTRING + Functions.Right ( GVENTRYPASSWORD ,  (int) ( 1 ) )  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 149;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GVENTRYPASSWORD ) > 32 ))  ) ) 
                { 
                __context__.SourceCodeLine = 151;
                PASSWORD_STATUS  .UpdateValue ( "Password too large (4-32 characters)"  ) ; 
                __context__.SourceCodeLine = 152;
                LVSTRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 153;
                GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 154;
                PASSWORD_STRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 155;
                PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 158;
                LVSTRING  .UpdateValue ( PASSWORD_ENTRY  ) ; 
                }
            
            }
        
        __context__.SourceCodeLine = 159;
        PASSWORD_STRING  .UpdateValue ( LVSTRING  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PASSWORD_ENTER_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 163;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GVENTRYPASSWORD == GVFILEPASSWORD) ) || Functions.TestForTrue ( Functions.BoolToInt (GVENTRYPASSWORD == GVADMINPASSWORD) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 165;
            Functions.Pulse ( 20, PASSWORD_VALID ) ; 
            __context__.SourceCodeLine = 166;
            PASSWORD_STATUS  .UpdateValue ( "Valid Password"  ) ; 
            __context__.SourceCodeLine = 167;
            GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 168;
            PASSWORD_STRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 169;
            PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 173;
            Functions.Pulse ( 20, PASSWORD_INVALID ) ; 
            __context__.SourceCodeLine = 174;
            PASSWORD_STATUS  .UpdateValue ( "In-valid Password"  ) ; 
            __context__.SourceCodeLine = 175;
            GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 176;
            PASSWORD_STRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 177;
            PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
            } 
        
        __context__.SourceCodeLine = 179;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_1__" , 300 , __SPLS_TMPVAR__WAITLABEL_1___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_1___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 180;
            PASSWORD_STATUS  .UpdateValue ( ""  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object PASSWORD_SAVE_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 184;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( NEW_PASSWORD ) >= 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( NEW_PASSWORD ) <= 32 ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 185;
            WRITEDATATOCONFIG (  __context__  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 186;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( NEW_PASSWORD ) < 4 ))  ) ) 
                { 
                __context__.SourceCodeLine = 188;
                PASSWORD_STATUS  .UpdateValue ( "Password Change Un-successful, too small (4-32 characters)"  ) ; 
                __context__.SourceCodeLine = 189;
                NEW_PASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 190;
                GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 191;
                PASSWORD_STRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 192;
                PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 196;
                PASSWORD_STATUS  .UpdateValue ( "Password Change Un-successful, too large (4-32 characters)"  ) ; 
                __context__.SourceCodeLine = 197;
                NEW_PASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 198;
                GVENTRYPASSWORD  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 199;
                PASSWORD_STRING  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 200;
                PASSWORD_ENTRY  .UpdateValue ( ""  ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 209;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GVFILEPASSWORD ) < 2 ))  ) ) 
            {
            __context__.SourceCodeLine = 210;
            GVFILEPASSWORD  .UpdateValue ( "1111"  ) ; 
            }
        
        __context__.SourceCodeLine = 211;
        GVADMINPASSWORD  .UpdateValue ( "15200"  ) ; 
        __context__.SourceCodeLine = 212;
        PASSWORD_STATUS  .UpdateValue ( ""  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    GVADMINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
    GVFILEPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
    GVENTRYPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
    
    START = new Crestron.Logos.SplusObjects.DigitalInput( START__DigitalInput__, this );
    m_DigitalInputList.Add( START__DigitalInput__, START );
    
    PASSWORD_ENTER = new Crestron.Logos.SplusObjects.DigitalInput( PASSWORD_ENTER__DigitalInput__, this );
    m_DigitalInputList.Add( PASSWORD_ENTER__DigitalInput__, PASSWORD_ENTER );
    
    PASSWORD_SAVE = new Crestron.Logos.SplusObjects.DigitalInput( PASSWORD_SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( PASSWORD_SAVE__DigitalInput__, PASSWORD_SAVE );
    
    PASSWORD_VALID = new Crestron.Logos.SplusObjects.DigitalOutput( PASSWORD_VALID__DigitalOutput__, this );
    m_DigitalOutputList.Add( PASSWORD_VALID__DigitalOutput__, PASSWORD_VALID );
    
    PASSWORD_INVALID = new Crestron.Logos.SplusObjects.DigitalOutput( PASSWORD_INVALID__DigitalOutput__, this );
    m_DigitalOutputList.Add( PASSWORD_INVALID__DigitalOutput__, PASSWORD_INVALID );
    
    PASSWORD_CHANGE_SUCCESS = new Crestron.Logos.SplusObjects.DigitalOutput( PASSWORD_CHANGE_SUCCESS__DigitalOutput__, this );
    m_DigitalOutputList.Add( PASSWORD_CHANGE_SUCCESS__DigitalOutput__, PASSWORD_CHANGE_SUCCESS );
    
    FILENAME = new Crestron.Logos.SplusObjects.StringInput( FILENAME__AnalogSerialInput__, 63, this );
    m_StringInputList.Add( FILENAME__AnalogSerialInput__, FILENAME );
    
    PASSWORD_ENTRY = new Crestron.Logos.SplusObjects.StringInput( PASSWORD_ENTRY__AnalogSerialInput__, 63, this );
    m_StringInputList.Add( PASSWORD_ENTRY__AnalogSerialInput__, PASSWORD_ENTRY );
    
    NEW_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( NEW_PASSWORD__AnalogSerialInput__, 63, this );
    m_StringInputList.Add( NEW_PASSWORD__AnalogSerialInput__, NEW_PASSWORD );
    
    PASSWORD_STATUS = new Crestron.Logos.SplusObjects.StringOutput( PASSWORD_STATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PASSWORD_STATUS__AnalogSerialOutput__, PASSWORD_STATUS );
    
    PASSWORD_CURRENT = new Crestron.Logos.SplusObjects.StringOutput( PASSWORD_CURRENT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PASSWORD_CURRENT__AnalogSerialOutput__, PASSWORD_CURRENT );
    
    PASSWORD_STRING = new Crestron.Logos.SplusObjects.StringOutput( PASSWORD_STRING__AnalogSerialOutput__, this );
    m_StringOutputList.Add( PASSWORD_STRING__AnalogSerialOutput__, PASSWORD_STRING );
    
    __SPLS_TMPVAR__WAITLABEL_0___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_0___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_1___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_1___CallbackFn );
    
    START.OnDigitalPush.Add( new InputChangeHandlerWrapper( START_OnPush_0, false ) );
    PASSWORD_ENTRY.OnSerialChange.Add( new InputChangeHandlerWrapper( PASSWORD_ENTRY_OnChange_1, false ) );
    PASSWORD_ENTER.OnDigitalPush.Add( new InputChangeHandlerWrapper( PASSWORD_ENTER_OnPush_2, false ) );
    PASSWORD_SAVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PASSWORD_SAVE_OnPush_3, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_PASSWORD_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_0___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_1___Callback;


const uint START__DigitalInput__ = 0;
const uint PASSWORD_ENTER__DigitalInput__ = 1;
const uint PASSWORD_SAVE__DigitalInput__ = 2;
const uint FILENAME__AnalogSerialInput__ = 0;
const uint PASSWORD_ENTRY__AnalogSerialInput__ = 1;
const uint NEW_PASSWORD__AnalogSerialInput__ = 2;
const uint PASSWORD_VALID__DigitalOutput__ = 0;
const uint PASSWORD_INVALID__DigitalOutput__ = 1;
const uint PASSWORD_CHANGE_SUCCESS__DigitalOutput__ = 2;
const uint PASSWORD_STATUS__AnalogSerialOutput__ = 0;
const uint PASSWORD_CURRENT__AnalogSerialOutput__ = 1;
const uint PASSWORD_STRING__AnalogSerialOutput__ = 2;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
