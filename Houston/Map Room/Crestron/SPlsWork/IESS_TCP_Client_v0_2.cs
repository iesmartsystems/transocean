using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_TCP_CLIENT_V0_2
{
    public class UserModuleClass_IESS_TCP_CLIENT_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput TX__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput RX__DOLLAR__;
        SplusTcpClient TCPCLIENT;
        STCPCLIENT GLBL_TCPCLIENT;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 82;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 84;
                if ( Functions.TestForTrue  ( ( GLBL_TCPCLIENT.STATUSCONNECTED)  ) ) 
                    {
                    __context__.SourceCodeLine = 85;
                    LVSTATUS = (short) ( Functions.SocketDisconnectClient( TCPCLIENT ) ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 87;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 89;
                    if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_TCPCLIENT.STATUSCONNECTED ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 91;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GLBL_TCPCLIENT.IPADDRESS ) > 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( GLBL_TCPCLIENT.IPPORT > 0 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 93;
                            if ( Functions.TestForTrue  ( ( GLBL_TCPCLIENT.STATUSCONNECTREQUEST)  ) ) 
                                {
                                __context__.SourceCodeLine = 94;
                                LVSTATUS = (short) ( Functions.SocketConnectClient( TCPCLIENT , GLBL_TCPCLIENT.IPADDRESS , (ushort)( GLBL_TCPCLIENT.IPPORT ) , (ushort)( 1 ) ) ) ; 
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                }
            
            
            }
            
        object TCPCLIENT_OnSocketConnect_0 ( Object __Info__ )
        
            { 
            SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
                
                __context__.SourceCodeLine = 105;
                GLBL_TCPCLIENT . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 106;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 107;
                CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SocketInfo__ ); }
            return this;
            
        }
        
    object TCPCLIENT_OnSocketDisconnect_1 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            
            __context__.SourceCodeLine = 112;
            GLBL_TCPCLIENT . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 113;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 114;
            CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
object TCPCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 118;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 119;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 121;
            GLBL_TCPCLIENT . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 122;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 126;
            GLBL_TCPCLIENT . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 127;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 133;
        RX__DOLLAR__  .UpdateValue ( TCPCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 134;
        Functions.ClearBuffer ( TCPCLIENT .  SocketRxBuf ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 138;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 140;
            GLBL_TCPCLIENT . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 141;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 145;
            GLBL_TCPCLIENT . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 146;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 151;
        GLBL_TCPCLIENT . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 156;
        GLBL_TCPCLIENT . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TX__DOLLAR___OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 161;
        if ( Functions.TestForTrue  ( ( GLBL_TCPCLIENT.STATUSCONNECTED)  ) ) 
            {
            __context__.SourceCodeLine = 162;
            Functions.SocketSend ( TCPCLIENT , TX__DOLLAR__ ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    TCPCLIENT  = new SplusTcpClient ( 2047, this );
    GLBL_TCPCLIENT  = new STCPCLIENT( this, true );
    GLBL_TCPCLIENT .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( TX__DOLLAR____AnalogSerialInput__, 511, this );
    m_StringInputList.Add( TX__DOLLAR____AnalogSerialInput__, TX__DOLLAR__ );
    
    RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( RX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( RX__DOLLAR____AnalogSerialOutput__, RX__DOLLAR__ );
    
    
    TCPCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketConnect_0, false ) );
    TCPCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketDisconnect_1, false ) );
    TCPCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketStatus_2, false ) );
    TCPCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_5, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_6, false ) );
    TX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( TX__DOLLAR___OnChange_7, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_TCP_CLIENT_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint CONNECT__DigitalInput__ = 0;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint TX__DOLLAR____AnalogSerialInput__ = 2;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint RX__DOLLAR____AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class STCPCLIENT : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  IPPORT = 0;
    
    
    public STCPCLIENT( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        
        
    }
    
}

}
