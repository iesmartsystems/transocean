/*
Dealer Name: i.e.SmartSytems
System Name: VISCA
System Number:
Programmer: Matthew Laletas
Comments: Module for VISCA
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
//#ENABLE_TRACE
#DEFINE_CONSTANT STRING_SIZE 15   
#DEFINE_CONSTANT NUM_PRESET 4  
#HELP "VISCA v0.1"
#HELP ""
/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT Pan_Left, Pan_Right, _SKIP_;
DIGITAL_INPUT Tilt_Up, Tilt_Down, _SKIP_;
DIGITAL_INPUT Zoom_In, Zoom_Out, _SKIP_;
DIGITAL_INPUT Power_On, Power_Off, _SKIP_;
DIGITAL_INPUT Preset[NUM_PRESET], _SKIP_;
STRING_INPUT Device_RX$[100];
STRING_INPUT Camera_ID$[10];
STRING_OUTPUT Device_TX$, _SKIP_;
DIGITAL_OUTPUT Preset_Selected[NUM_PRESET];
/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
STRUCTURE sSource
{
	STRING CommandMove[STRING_SIZE];		
	STRING CommandZoom[STRING_SIZE];
	STRING CommandPreset[STRING_SIZE];
	STRING CommandPresetSave[STRING_SIZE];	
	STRING PowerOn[STRING_SIZE];
	STRING PowerOff[STRING_SIZE];			
	STRING STX[1];
	STRING ETX[1];
};
sSource SourceDEV;

STRING GLBL_String[STRING_SIZE];
INTEGER GLBL_Index;
/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/     
FUNCTION BuildString( STRING lvIncoming )
{
	Device_TX$ = SourceDEV.STX + lvIncoming + SourceDEV.ETX;
}
/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
PUSH Pan_Left
{
	INTEGER lvCounter;
	STRING lvString[100];	
	lvString = SourceDEV.CommandMove + "\x01\x03";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Pan_Left
{
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x03";
	BuildString( lvString );
}

PUSH Pan_Right
{
	INTEGER lvCounter;
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x02\x03";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Pan_Right
{
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x03";
	BuildString( lvString );
}

PUSH Tilt_Up
{
	INTEGER lvCounter;
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x01";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Tilt_Up
{
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x03";
	BuildString( lvString );
}

PUSH Tilt_Down
{
	INTEGER lvCounter;
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x02";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Tilt_Down
{
	STRING lvString[100];
	lvString = SourceDEV.CommandMove + "\x03\x03";
	BuildString( lvString );
}

PUSH Zoom_In
{
	INTEGER lvCounter;
	STRING lvString[100];
	lvString = SourceDEV.CommandZoom + "\x24";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Zoom_In
{
	STRING lvString[100];
	lvString = SourceDEV.CommandZoom + "\x00";
	BuildString( lvString );
}

PUSH Zoom_Out
{
	INTEGER lvCounter;
	STRING lvString[100];	
	lvString = SourceDEV.CommandZoom + "\x34";
	BuildString( lvString );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
}
RELEASE Zoom_Out
{
	STRING lvString[100];
	lvString = SourceDEV.CommandZoom + "\x00";
	BuildString( lvString );
}

PUSH Preset
{			
	GLBL_Index = GETLASTMODIFIEDARRAYINDEX();
	WAIT( 200, Presets )										//Hold 2 secs
	{
		IF(	Preset[GLBL_Index] )									//Still Pressed
		{
			GLBL_String = SourceDEV.CommandPresetSave + CHR( GLBL_Index - 1);
			BuildString( GLBL_String );			
		}
		CANCELWAIT( Presets );
	}
}
RELEASE Preset
{
	INTEGER lvIndex, lvCounter;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	GLBL_String = SourceDEV.CommandPreset + CHR( lvIndex - 1 );
	BuildString( GLBL_String );
	FOR( lvCounter = 1 TO NUM_PRESET )
		Preset_Selected[lvCounter] = 0;
	Preset_Selected[lvIndex] = 1;
}
CHANGE Device_RX$
{
}
CHANGE Camera_ID$
{
	SourceDEV.STX = Camera_ID$;
}
PUSH Power_On
{
	BuildString( SourceDEV.PowerOn );
}
PUSH Power_Off
{
	BuildString( SourceDEV.PowerOff );
} 	
/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main()
{	
	SourceDEV.STX 						= "\x81";
	SourceDEV.ETX 						= "\xFF";
	SourceDEV.CommandMove				= "\x01\x06\x01\x14\x08";
	SourceDEV.CommandZoom				= "\x01\x04\x07";
	SourceDEV.CommandPreset				= "\x01\x04\x3F\x02";
	SourceDEV.CommandPresetSave			= "\x01\x04\x3F\x01";
	SourceDEV.PowerOn					= "\x01\x04\x00\x02";
	SourceDEV.PowerOff					= "\x01\x04\x00\x03";

}


