#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Display_Routing_v0_6.h"

FUNCTION_MAIN( S2_IESS_Display_Routing_v0_6 );
EVENT_HANDLER( S2_IESS_Display_Routing_v0_6 );
DEFINE_ENTRYPOINT( S2_IESS_Display_Routing_v0_6 );


static void S2_IESS_Display_Routing_v0_6__ROUTEALLDISPLAYS ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 66 );
    __FN_FOREND_VAL__1 = Globals->S2_IESS_Display_Routing_v0_6.__GLBL_NUMBERDISPLAYS; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 68 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOSWITCH ,__LVCOUNTER, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 69 );
        if ( Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 70 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 72 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,__LVCOUNTER, 99) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 66 );
        } 
    
    
    S2_IESS_Display_Routing_v0_6_Exit__ROUTEALLDISPLAYS:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00000 /*Inputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 80 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT = GetLocalLastModifiedArrayIndex ( S2_IESS_Display_Routing_v0_6 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 81 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), __S2_IESS_Display_Routing_v0_6_LASTSELECTEDINPUT_ANALOG_OUTPUT, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 82 );
    if ( (Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE == 1)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 83 );
        S2_IESS_Display_Routing_v0_6__ROUTEALLDISPLAYS ( ) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 84 );
        if ( (Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE == 3)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 86 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 87 );
            if ( Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 88 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 1) ; 
                }
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 90 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 99) ; 
                }
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 91 );
            SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_6, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
            } 
        
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 93 );
    CREATE_WAIT( S2_IESS_Display_Routing_v0_6, 400, SOURCEHOLD );
    
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_WAITEVENT( S2_IESS_Display_Routing_v0_6, SOURCEHOLD )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 95 );
    if ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__INPUTS , Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 96 );
        S2_IESS_Display_Routing_v0_6__ROUTEALLDISPLAYS ( ) ; 
        }
    
    

S2_IESS_Display_Routing_v0_6_Exit__SOURCEHOLD:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00001 /*Inputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 101 );
    CancelWait ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), "SOURCEHOLD" ) ; 
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00002 /*NumberDisplays*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 105 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_NUMBERDISPLAYS = GetAnalogInput( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), __S2_IESS_Display_Routing_v0_6_NUMBERDISPLAYS_ANALOG_INPUT ); 
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_2:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00003 /*RoutingMode*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 109 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE = GetAnalogInput( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), __S2_IESS_Display_Routing_v0_6_ROUTINGMODE_ANALOG_INPUT ); 
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_3:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00004 /*Outputs*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 113 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT = GetLocalLastModifiedArrayIndex ( S2_IESS_Display_Routing_v0_6 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 114 );
    if ( (Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE == 2)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 116 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 117 );
        if ( Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 118 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 120 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 99) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 122 );
        SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_6, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
        } 
    
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_4:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00005 /*PowerOff*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 127 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 128 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), __S2_IESS_Display_Routing_v0_6_LASTSELECTEDINPUT_ANALOG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 129 );
    if ( (Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE == 1)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 130 );
        S2_IESS_Display_Routing_v0_6__ROUTEALLDISPLAYS ( ) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 131 );
        if ( (Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE == 3)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 133 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOSWITCH ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 134 );
            if ( Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 135 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 1) ; 
                }
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 137 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), &Globals->S2_IESS_Display_Routing_v0_6.__VIDEOCONTROL ,Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT, 99) ; 
                }
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 138 );
            SET_GLOBAL_INTARRAY_VALUE( S2_IESS_Display_Routing_v0_6, __GLBL_OUTPUTS, 0, Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTOUTPUT , Globals->S2_IESS_Display_Routing_v0_6.__GLBL_LASTINPUT) ; 
            } 
        
        }
    
    
    S2_IESS_Display_Routing_v0_6_Exit__Event_5:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/

/********************************************************************************
  Destructor
********************************************************************************/

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Display_Routing_v0_6_POWEROFF_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00005 /*PowerOff*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
                
            }
            break;
            
        case __S2_IESS_Display_Routing_v0_6_INPUTS_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00000 /*Inputs*/, 0 );
                
            }
            else /*Release*/
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00001 /*Inputs*/, 0 );
                
            }
            break;
            
        case __S2_IESS_Display_Routing_v0_6_OUTPUTS_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00004 /*Outputs*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Display_Routing_v0_6_NUMBERDISPLAYS_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00002 /*NumberDisplays*/, 0 );
            break;
            
        case __S2_IESS_Display_Routing_v0_6_ROUTINGMODE_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Display_Routing_v0_6, 00003 /*RoutingMode*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Display_Routing_v0_6 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Display_Routing_v0_6 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Display_Routing_v0_6 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Display_Routing_v0_6 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_IESS_Display_Routing_v0_6, __INPUTS ) ;
    CHECK_INPUT_ARRAY ( S2_IESS_Display_Routing_v0_6, __OUTPUTS ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Display_Routing_v0_6 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Display_Routing_v0_6 )
{
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_Display_Routing_v0_6 );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_6, __INPUTS, IO_TYPE_DIGITAL_INPUT, __S2_IESS_Display_Routing_v0_6_INPUTS_DIG_INPUT, __S2_IESS_Display_Routing_v0_6_INPUTS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_6, __OUTPUTS, IO_TYPE_DIGITAL_INPUT, __S2_IESS_Display_Routing_v0_6_OUTPUTS_DIG_INPUT, __S2_IESS_Display_Routing_v0_6_OUTPUTS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_6, __VIDEOSWITCH, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_6_VIDEOSWITCH_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_6_VIDEOSWITCH_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Display_Routing_v0_6, __VIDEOCONTROL, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_6_VIDEOCONTROL_ANALOG_OUTPUT, __S2_IESS_Display_Routing_v0_6_VIDEOCONTROL_ARRAY_LENGTH );
    
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Display_Routing_v0_6, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    INITIALIZE_GLOBAL_INTARRAY ( S2_IESS_Display_Routing_v0_6, __GLBL_OUTPUTS, 0, 16 );
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Display_Routing_v0_6 ), 146 );
    Globals->S2_IESS_Display_Routing_v0_6.__GLBL_ROUTINGMODE = 2; 
    
    S2_IESS_Display_Routing_v0_6_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
