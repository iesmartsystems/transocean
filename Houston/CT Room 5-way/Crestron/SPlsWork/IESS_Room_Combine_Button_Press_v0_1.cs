using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ROOM_COMBINE_BUTTON_PRESS_V0_1
{
    public class UserModuleClass_IESS_ROOM_COMBINE_BUTTON_PRESS_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> BUTTON_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> PARTITION_CLOSED;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> PARTITION_OPEN;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUPSTATUS;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> BUTTON_1_OUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_OUT;
        ushort [] GVGROUP;
        object BUTTON_1_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINDEX = 0;
                ushort LVCOUNTER = 0;
                
                
                __context__.SourceCodeLine = 76;
                LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 77;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)5; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 79;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GROUPSTATUS[ LVINDEX ] .UshortValue == GROUPSTATUS[ LVCOUNTER ] .UshortValue))  ) ) 
                        {
                        __context__.SourceCodeLine = 80;
                        Functions.Pulse ( 20, BUTTON_1_OUT [ LVCOUNTER] ) ; 
                        }
                    
                    __context__.SourceCodeLine = 77;
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object PARTITION_CLOSED_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVINDEX = 0;
            
            
            __context__.SourceCodeLine = 87;
            LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 88;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 90;
                GROUP_OUT [ 1]  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 91;
                GVGROUP [ 1] = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 92;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_34__" , 100 , __SPLS_TMPVAR__WAITLABEL_34___Callback ) ;
                } 
            
            else 
                {
                __context__.SourceCodeLine = 122;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 2))  ) ) 
                    { 
                    __context__.SourceCodeLine = 124;
                    GROUP_OUT [ 3]  .Value = (ushort) ( 3 ) ; 
                    __context__.SourceCodeLine = 125;
                    GVGROUP [ 3] = (ushort) ( 3 ) ; 
                    __context__.SourceCodeLine = 126;
                    if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 3 ] .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 128;
                        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_38__" , 100 , __SPLS_TMPVAR__WAITLABEL_38___Callback ) ;
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 143;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 3))  ) ) 
                        { 
                        __context__.SourceCodeLine = 145;
                        GROUP_OUT [ 4]  .Value = (ushort) ( 4 ) ; 
                        __context__.SourceCodeLine = 146;
                        GVGROUP [ 4] = (ushort) ( 4 ) ; 
                        __context__.SourceCodeLine = 147;
                        if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 149;
                            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_40__" , 100 , __SPLS_TMPVAR__WAITLABEL_40___Callback ) ;
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 156;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 4))  ) ) 
                            { 
                            __context__.SourceCodeLine = 158;
                            GROUP_OUT [ 5]  .Value = (ushort) ( 5 ) ; 
                            __context__.SourceCodeLine = 159;
                            GROUP_OUT [ 5]  .Value = (ushort) ( 5 ) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_34___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 94;
            GROUP_OUT [ 2]  .Value = (ushort) ( 2 ) ; 
            __context__.SourceCodeLine = 95;
            GVGROUP [ 2] = (ushort) ( 2 ) ; 
            __context__.SourceCodeLine = 96;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 2 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 98;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_35__" , 100 , __SPLS_TMPVAR__WAITLABEL_35___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_35___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 100;
            GROUP_OUT [ 3]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 101;
            GVGROUP [ 3] = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 102;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 3 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 104;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_36__" , 100 , __SPLS_TMPVAR__WAITLABEL_36___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_36___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 106;
            GROUP_OUT [ 4]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 107;
            GVGROUP [ 4] = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 108;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 110;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_37__" , 100 , __SPLS_TMPVAR__WAITLABEL_37___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_37___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 112;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 113;
            GVGROUP [ 5] = (ushort) ( GVGROUP[ 2 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_38___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 130;
            GROUP_OUT [ 4]  .Value = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 131;
            GVGROUP [ 4] = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 132;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 134;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_39__" , 100 , __SPLS_TMPVAR__WAITLABEL_39___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_39___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 136;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 137;
            GVGROUP [ 5] = (ushort) ( GVGROUP[ 3 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_40___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 151;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 4 ] ) ; 
            __context__.SourceCodeLine = 152;
            GVGROUP [ 5] = (ushort) ( GVGROUP[ 4 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object PARTITION_OPEN_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 165;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 166;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 1))  ) ) 
            { 
            __context__.SourceCodeLine = 168;
            GROUP_OUT [ 1]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 169;
            GVGROUP [ 1] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 170;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_41__" , 100 , __SPLS_TMPVAR__WAITLABEL_41___Callback ) ;
            } 
        
        else 
            {
            __context__.SourceCodeLine = 200;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 202;
                if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 1 ] .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 204;
                    GROUP_OUT [ 2]  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 205;
                    GVGROUP [ 2] = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 209;
                    GROUP_OUT [ 2]  .Value = (ushort) ( 2 ) ; 
                    __context__.SourceCodeLine = 210;
                    GVGROUP [ 2] = (ushort) ( 2 ) ; 
                    } 
                
                __context__.SourceCodeLine = 212;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_45__" , 100 , __SPLS_TMPVAR__WAITLABEL_45___Callback ) ;
                } 
            
            else 
                {
                __context__.SourceCodeLine = 234;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 236;
                    if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 2 ] .Value)  ) ) 
                        { 
                        __context__.SourceCodeLine = 238;
                        GROUP_OUT [ 3]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
                        __context__.SourceCodeLine = 239;
                        GVGROUP [ 3] = (ushort) ( GVGROUP[ 2 ] ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 243;
                        GROUP_OUT [ 3]  .Value = (ushort) ( 3 ) ; 
                        __context__.SourceCodeLine = 244;
                        GVGROUP [ 3] = (ushort) ( 3 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 246;
                    CreateWait ( "__SPLS_TMPVAR__WAITLABEL_48__" , 100 , __SPLS_TMPVAR__WAITLABEL_48___Callback ) ;
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 260;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 4))  ) ) 
                        { 
                        __context__.SourceCodeLine = 262;
                        if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 3 ] .Value)  ) ) 
                            { 
                            __context__.SourceCodeLine = 264;
                            GROUP_OUT [ 4]  .Value = (ushort) ( GVGROUP[ 3 ] ) ; 
                            __context__.SourceCodeLine = 265;
                            GVGROUP [ 4] = (ushort) ( GVGROUP[ 3 ] ) ; 
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 269;
                            GROUP_OUT [ 4]  .Value = (ushort) ( 4 ) ; 
                            __context__.SourceCodeLine = 270;
                            GVGROUP [ 4] = (ushort) ( 4 ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 272;
                        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_50__" , 100 , __SPLS_TMPVAR__WAITLABEL_50___Callback ) ;
                        } 
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_41___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 172;
            GROUP_OUT [ 2]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 173;
            GVGROUP [ 2] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 174;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 2 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 176;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_42__" , 100 , __SPLS_TMPVAR__WAITLABEL_42___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_42___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 178;
            GROUP_OUT [ 3]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 179;
            GVGROUP [ 3] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 180;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 3 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 182;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_43__" , 100 , __SPLS_TMPVAR__WAITLABEL_43___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_43___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 184;
            GROUP_OUT [ 4]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 185;
            GVGROUP [ 4] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 186;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 188;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_44__" , 100 , __SPLS_TMPVAR__WAITLABEL_44___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_44___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 190;
            GROUP_OUT [ 5]  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 191;
            GVGROUP [ 5] = (ushort) ( 1 ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_45___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 214;
            GROUP_OUT [ 3]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 215;
            GVGROUP [ 3] = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 216;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 3 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 218;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_46__" , 100 , __SPLS_TMPVAR__WAITLABEL_46___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_46___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 220;
            GROUP_OUT [ 4]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 221;
            GVGROUP [ 4] = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 222;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 224;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_47__" , 100 , __SPLS_TMPVAR__WAITLABEL_47___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_47___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 226;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 2 ] ) ; 
            __context__.SourceCodeLine = 227;
            GVGROUP [ 5] = (ushort) ( GVGROUP[ 2 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_48___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 248;
            GROUP_OUT [ 4]  .Value = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 249;
            GVGROUP [ 4] = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 250;
            if ( Functions.TestForTrue  ( ( PARTITION_OPEN[ 4 ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 252;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_49__" , 100 , __SPLS_TMPVAR__WAITLABEL_49___Callback ) ;
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_49___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 254;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 3 ] ) ; 
            __context__.SourceCodeLine = 255;
            GVGROUP [ 5] = (ushort) ( GVGROUP[ 3 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_50___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 274;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 4 ] ) ; 
            __context__.SourceCodeLine = 275;
            GROUP_OUT [ 5]  .Value = (ushort) ( GVGROUP[ 4 ] ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}


public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVGROUP  = new ushort[ 6 ];
    
    BUTTON_1 = new InOutArray<DigitalInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        BUTTON_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( BUTTON_1__DigitalInput__ + i, BUTTON_1__DigitalInput__, this );
        m_DigitalInputList.Add( BUTTON_1__DigitalInput__ + i, BUTTON_1[i+1] );
    }
    
    PARTITION_CLOSED = new InOutArray<DigitalInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        PARTITION_CLOSED[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION_CLOSED__DigitalInput__ + i, PARTITION_CLOSED__DigitalInput__, this );
        m_DigitalInputList.Add( PARTITION_CLOSED__DigitalInput__ + i, PARTITION_CLOSED[i+1] );
    }
    
    PARTITION_OPEN = new InOutArray<DigitalInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        PARTITION_OPEN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( PARTITION_OPEN__DigitalInput__ + i, PARTITION_OPEN__DigitalInput__, this );
        m_DigitalInputList.Add( PARTITION_OPEN__DigitalInput__ + i, PARTITION_OPEN[i+1] );
    }
    
    BUTTON_1_OUT = new InOutArray<DigitalOutput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        BUTTON_1_OUT[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( BUTTON_1_OUT__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( BUTTON_1_OUT__DigitalOutput__ + i, BUTTON_1_OUT[i+1] );
    }
    
    GROUPSTATUS = new InOutArray<AnalogInput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        GROUPSTATUS[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUPSTATUS__AnalogSerialInput__ + i, GROUPSTATUS__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUPSTATUS__AnalogSerialInput__ + i, GROUPSTATUS[i+1] );
    }
    
    GROUP_OUT = new InOutArray<AnalogOutput>( 5, this );
    for( uint i = 0; i < 5; i++ )
    {
        GROUP_OUT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_OUT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_OUT__AnalogSerialOutput__ + i, GROUP_OUT[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_34___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_34___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_35___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_35___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_36___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_36___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_37___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_37___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_38___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_38___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_39___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_39___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_40___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_40___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_41___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_41___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_42___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_42___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_43___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_43___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_44___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_44___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_45___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_45___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_46___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_46___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_47___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_47___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_48___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_48___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_49___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_49___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_50___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_50___CallbackFn );
    
    for( uint i = 0; i < 5; i++ )
        BUTTON_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( BUTTON_1_OnPush_0, false ) );
        
    for( uint i = 0; i < 4; i++ )
        PARTITION_CLOSED[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION_CLOSED_OnPush_1, true ) );
        
    for( uint i = 0; i < 4; i++ )
        PARTITION_OPEN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( PARTITION_OPEN_OnPush_2, true ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_ROOM_COMBINE_BUTTON_PRESS_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_34___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_35___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_36___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_37___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_38___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_39___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_40___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_41___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_42___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_43___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_44___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_45___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_46___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_47___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_48___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_49___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_50___Callback;


const uint BUTTON_1__DigitalInput__ = 0;
const uint PARTITION_CLOSED__DigitalInput__ = 5;
const uint PARTITION_OPEN__DigitalInput__ = 9;
const uint GROUPSTATUS__AnalogSerialInput__ = 0;
const uint BUTTON_1_OUT__DigitalOutput__ = 0;
const uint GROUP_OUT__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
