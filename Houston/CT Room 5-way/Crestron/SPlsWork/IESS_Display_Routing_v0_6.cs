using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DISPLAY_ROUTING_V0_6
{
    public class UserModuleClass_IESS_DISPLAY_ROUTING_V0_6 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> INPUTS;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> OUTPUTS;
        Crestron.Logos.SplusObjects.AnalogInput NUMBERDISPLAYS;
        Crestron.Logos.SplusObjects.AnalogInput ROUTINGMODE;
        Crestron.Logos.SplusObjects.AnalogOutput LASTSELECTEDINPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VIDEOSWITCH;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VIDEOCONTROL;
        ushort GLBL_LASTINPUT = 0;
        ushort GLBL_LASTOUTPUT = 0;
        ushort [] GLBL_OUTPUTS;
        ushort GLBL_COUNTDOWN = 0;
        ushort GLBL_NUMBERDISPLAYS = 0;
        ushort GLBL_ROUTINGMODE = 0;
        private void ROUTEALLDISPLAYS (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 66;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)GLBL_NUMBERDISPLAYS; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 68;
                VIDEOSWITCH [ LVCOUNTER]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                __context__.SourceCodeLine = 69;
                if ( Functions.TestForTrue  ( ( GLBL_LASTINPUT)  ) ) 
                    {
                    __context__.SourceCodeLine = 70;
                    VIDEOCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 72;
                    VIDEOCONTROL [ LVCOUNTER]  .Value = (ushort) ( 99 ) ; 
                    }
                
                __context__.SourceCodeLine = 66;
                } 
            
            
            }
            
        object INPUTS_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 80;
                GLBL_LASTINPUT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 81;
                LASTSELECTEDINPUT  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                __context__.SourceCodeLine = 82;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 83;
                    ROUTEALLDISPLAYS (  __context__  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 84;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 3))  ) ) 
                        { 
                        __context__.SourceCodeLine = 86;
                        VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                        __context__.SourceCodeLine = 87;
                        if ( Functions.TestForTrue  ( ( GLBL_LASTINPUT)  ) ) 
                            {
                            __context__.SourceCodeLine = 88;
                            VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 1 ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 90;
                            VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 99 ) ; 
                            }
                        
                        __context__.SourceCodeLine = 91;
                        GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 93;
                CreateWait ( "SOURCEHOLD" , 400 , SOURCEHOLD_Callback ) ;
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    public void SOURCEHOLD_CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 95;
            if ( Functions.TestForTrue  ( ( INPUTS[ GLBL_LASTINPUT ] .Value)  ) ) 
                {
                __context__.SourceCodeLine = 96;
                ROUTEALLDISPLAYS (  __context__  ) ; 
                }
            
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
object INPUTS_OnRelease_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 101;
        CancelWait ( "SOURCEHOLD" ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object NUMBERDISPLAYS_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 105;
        GLBL_NUMBERDISPLAYS = (ushort) ( NUMBERDISPLAYS  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ROUTINGMODE_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 109;
        GLBL_ROUTINGMODE = (ushort) ( ROUTINGMODE  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object OUTPUTS_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 113;
        GLBL_LASTOUTPUT = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 114;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 116;
            VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
            __context__.SourceCodeLine = 117;
            if ( Functions.TestForTrue  ( ( GLBL_LASTINPUT)  ) ) 
                {
                __context__.SourceCodeLine = 118;
                VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 120;
                VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 99 ) ; 
                }
            
            __context__.SourceCodeLine = 122;
            GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWEROFF_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 127;
        GLBL_LASTINPUT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 128;
        LASTSELECTEDINPUT  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 129;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 1))  ) ) 
            {
            __context__.SourceCodeLine = 130;
            ROUTEALLDISPLAYS (  __context__  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 131;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GLBL_ROUTINGMODE == 3))  ) ) 
                { 
                __context__.SourceCodeLine = 133;
                VIDEOSWITCH [ GLBL_LASTOUTPUT]  .Value = (ushort) ( GLBL_LASTINPUT ) ; 
                __context__.SourceCodeLine = 134;
                if ( Functions.TestForTrue  ( ( GLBL_LASTINPUT)  ) ) 
                    {
                    __context__.SourceCodeLine = 135;
                    VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 137;
                    VIDEOCONTROL [ GLBL_LASTOUTPUT]  .Value = (ushort) ( 99 ) ; 
                    }
                
                __context__.SourceCodeLine = 138;
                GLBL_OUTPUTS [ GLBL_LASTOUTPUT] = (ushort) ( GLBL_LASTINPUT ) ; 
                } 
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 146;
        GLBL_ROUTINGMODE = (ushort) ( 2 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_OUTPUTS  = new ushort[ 17 ];
    
    POWEROFF = new Crestron.Logos.SplusObjects.DigitalInput( POWEROFF__DigitalInput__, this );
    m_DigitalInputList.Add( POWEROFF__DigitalInput__, POWEROFF );
    
    INPUTS = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        INPUTS[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( INPUTS__DigitalInput__ + i, INPUTS__DigitalInput__, this );
        m_DigitalInputList.Add( INPUTS__DigitalInput__ + i, INPUTS[i+1] );
    }
    
    OUTPUTS = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        OUTPUTS[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( OUTPUTS__DigitalInput__ + i, OUTPUTS__DigitalInput__, this );
        m_DigitalInputList.Add( OUTPUTS__DigitalInput__ + i, OUTPUTS[i+1] );
    }
    
    NUMBERDISPLAYS = new Crestron.Logos.SplusObjects.AnalogInput( NUMBERDISPLAYS__AnalogSerialInput__, this );
    m_AnalogInputList.Add( NUMBERDISPLAYS__AnalogSerialInput__, NUMBERDISPLAYS );
    
    ROUTINGMODE = new Crestron.Logos.SplusObjects.AnalogInput( ROUTINGMODE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( ROUTINGMODE__AnalogSerialInput__, ROUTINGMODE );
    
    LASTSELECTEDINPUT = new Crestron.Logos.SplusObjects.AnalogOutput( LASTSELECTEDINPUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( LASTSELECTEDINPUT__AnalogSerialOutput__, LASTSELECTEDINPUT );
    
    VIDEOSWITCH = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        VIDEOSWITCH[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEOSWITCH__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VIDEOSWITCH__AnalogSerialOutput__ + i, VIDEOSWITCH[i+1] );
    }
    
    VIDEOCONTROL = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        VIDEOCONTROL[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VIDEOCONTROL__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VIDEOCONTROL__AnalogSerialOutput__ + i, VIDEOCONTROL[i+1] );
    }
    
    SOURCEHOLD_Callback = new WaitFunction( SOURCEHOLD_CallbackFn );
    
    for( uint i = 0; i < 16; i++ )
        INPUTS[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( INPUTS_OnPush_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        INPUTS[i+1].OnDigitalRelease.Add( new InputChangeHandlerWrapper( INPUTS_OnRelease_1, false ) );
        
    NUMBERDISPLAYS.OnAnalogChange.Add( new InputChangeHandlerWrapper( NUMBERDISPLAYS_OnChange_2, false ) );
    ROUTINGMODE.OnAnalogChange.Add( new InputChangeHandlerWrapper( ROUTINGMODE_OnChange_3, false ) );
    for( uint i = 0; i < 16; i++ )
        OUTPUTS[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( OUTPUTS_OnPush_4, false ) );
        
    POWEROFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWEROFF_OnPush_5, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DISPLAY_ROUTING_V0_6 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction SOURCEHOLD_Callback;


const uint POWEROFF__DigitalInput__ = 0;
const uint INPUTS__DigitalInput__ = 1;
const uint OUTPUTS__DigitalInput__ = 17;
const uint NUMBERDISPLAYS__AnalogSerialInput__ = 0;
const uint ROUTINGMODE__AnalogSerialInput__ = 1;
const uint LASTSELECTEDINPUT__AnalogSerialOutput__ = 0;
const uint VIDEOSWITCH__AnalogSerialOutput__ = 1;
const uint VIDEOCONTROL__AnalogSerialOutput__ = 17;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
