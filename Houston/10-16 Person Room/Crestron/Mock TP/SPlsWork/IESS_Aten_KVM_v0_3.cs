using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ATEN_KVM_V0_3
{
    public class UserModuleClass_IESS_ATEN_KVM_V0_3 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.AnalogInput KVM_INPUT;
        Crestron.Logos.SplusObjects.AnalogInput KVM_MODE;
        Crestron.Logos.SplusObjects.StringInput KVM_RX__DOLLAR__;
        Crestron.Logos.SplusObjects.DigitalOutput STATUS_CONNECT;
        Crestron.Logos.SplusObjects.AnalogOutput STATUS_INPUT;
        Crestron.Logos.SplusObjects.StringOutput KVM_TX__DOLLAR__;
        SKVM KVM_COMMAND;
        SLOCALKVM KVM_RX_COMMAND;
        CrestronString SOCKETBUFF;
        ushort GLBL_KVM_STATUS = 0;
        private void SENDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
            { 
            
            __context__.SourceCodeLine = 128;
            KVM_TX__DOLLAR__  .UpdateValue ( LVINCOMING + KVM_COMMAND . CR  ) ; 
            
            }
            
        private void STARTHEARTBEAT (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 132;
            CreateWait ( "HEARTBEAT" , KVM_COMMAND.TIMER , HEARTBEAT_Callback ) ;
            
            }
            
        public void HEARTBEAT_CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 134;
            if ( Functions.TestForTrue  ( ( GLBL_KVM_STATUS)  ) ) 
                { 
                __context__.SourceCodeLine = 136;
                SENDSTRING (  __context__ , KVM_COMMAND.HEARTBEAT) ; 
                __context__.SourceCodeLine = 137;
                STARTHEARTBEAT (  __context__  ) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void RESETUSB (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 143;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_23__" , 300 , __SPLS_TMPVAR__WAITLABEL_23___Callback ) ;
        
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_23___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            {
            __context__.SourceCodeLine = 144;
            SENDSTRING (  __context__ , KVM_COMMAND.RESETUSB) ; 
            }
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
object CONNECT_OnPush_0 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 152;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            {
            __context__.SourceCodeLine = 153;
            SENDSTRING (  __context__ , KVM_COMMAND.OPEN) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KVM_INPUT_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        
        __context__.SourceCodeLine = 159;
        LVINDEX = (ushort) ( KVM_INPUT  .UshortValue ) ; 
        __context__.SourceCodeLine = 160;
        MakeString ( LVSTRING , "sw i0{0:d}", (ushort)LVINDEX) ; 
        __context__.SourceCodeLine = 161;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KVM_MODE_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        
        __context__.SourceCodeLine = 167;
        LVINDEX = (ushort) ( KVM_MODE  .UshortValue ) ; 
        __context__.SourceCodeLine = 168;
        
            {
            int __SPLS_TMPVAR__SWTCH_1__ = ((int)KVM_MODE  .UshortValue);
            
                { 
                if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                    {
                    __context__.SourceCodeLine = 170;
                    MakeString ( LVSTRING , "display sst") ; 
                    }
                
                else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                    {
                    __context__.SourceCodeLine = 171;
                    MakeString ( LVSTRING , "display mst") ; 
                    }
                
                } 
                
            }
            
        
        __context__.SourceCodeLine = 173;
        SENDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KVM_RX__DOLLAR___OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVRX;
        CrestronString LVTRASH;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        ushort LVCOUNTER = 0;
        ushort LVINDEX = 0;
        ushort LVLEVEL = 0;
        
        
        __context__.SourceCodeLine = 179;
        KVM_RX_COMMAND . CRXQUEUE  .UpdateValue ( KVM_RX_COMMAND . CRXQUEUE + KVM_RX__DOLLAR__  ) ; 
        __context__.SourceCodeLine = 180;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D" , KVM_RX_COMMAND.CRXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 182;
            LVRX  .UpdateValue ( Functions.Remove ( "\u000D" , KVM_RX_COMMAND . CRXQUEUE )  ) ; 
            __context__.SourceCodeLine = 183;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 184;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Welcome" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 186;
                GLBL_KVM_STATUS = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 187;
                STATUS_CONNECT  .Value = (ushort) ( GLBL_KVM_STATUS ) ; 
                __context__.SourceCodeLine = 188;
                STARTHEARTBEAT (  __context__  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 190;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Please Open RS232 Function!" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 192;
                    GLBL_KVM_STATUS = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 193;
                    STATUS_CONNECT  .Value = (ushort) ( GLBL_KVM_STATUS ) ; 
                    __context__.SourceCodeLine = 194;
                    SENDSTRING (  __context__ , KVM_COMMAND.OPEN) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 196;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Command OK" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 198;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "sw i01" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 200;
                            STATUS_INPUT  .Value = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 201;
                            RESETUSB (  __context__  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 203;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "sw i02" , LVRX ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 205;
                                STATUS_INPUT  .Value = (ushort) ( 2 ) ; 
                                __context__.SourceCodeLine = 206;
                                RESETUSB (  __context__  ) ; 
                                } 
                            
                            }
                        
                        } 
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 180;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 219;
        KVM_COMMAND . OPEN  .UpdateValue ( "open"  ) ; 
        __context__.SourceCodeLine = 220;
        KVM_COMMAND . CR  .UpdateValue ( "\u000D\u000A"  ) ; 
        __context__.SourceCodeLine = 221;
        KVM_COMMAND . HEARTBEAT  .UpdateValue ( "status on"  ) ; 
        __context__.SourceCodeLine = 222;
        KVM_COMMAND . RESETUSB  .UpdateValue ( "usbreset on"  ) ; 
        __context__.SourceCodeLine = 223;
        KVM_COMMAND . TIMER = (ushort) ( 6000 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    SOCKETBUFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    KVM_COMMAND  = new SKVM( this, true );
    KVM_COMMAND .PopulateCustomAttributeList( false );
    KVM_RX_COMMAND  = new SLOCALKVM( this, true );
    KVM_RX_COMMAND .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    STATUS_CONNECT = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_CONNECT__DigitalOutput__, this );
    m_DigitalOutputList.Add( STATUS_CONNECT__DigitalOutput__, STATUS_CONNECT );
    
    KVM_INPUT = new Crestron.Logos.SplusObjects.AnalogInput( KVM_INPUT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( KVM_INPUT__AnalogSerialInput__, KVM_INPUT );
    
    KVM_MODE = new Crestron.Logos.SplusObjects.AnalogInput( KVM_MODE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( KVM_MODE__AnalogSerialInput__, KVM_MODE );
    
    STATUS_INPUT = new Crestron.Logos.SplusObjects.AnalogOutput( STATUS_INPUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( STATUS_INPUT__AnalogSerialOutput__, STATUS_INPUT );
    
    KVM_RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( KVM_RX__DOLLAR____AnalogSerialInput__, 255, this );
    m_StringInputList.Add( KVM_RX__DOLLAR____AnalogSerialInput__, KVM_RX__DOLLAR__ );
    
    KVM_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( KVM_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( KVM_TX__DOLLAR____AnalogSerialOutput__, KVM_TX__DOLLAR__ );
    
    HEARTBEAT_Callback = new WaitFunction( HEARTBEAT_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_23___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_23___CallbackFn );
    
    CONNECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( CONNECT_OnPush_0, false ) );
    KVM_INPUT.OnAnalogChange.Add( new InputChangeHandlerWrapper( KVM_INPUT_OnChange_1, false ) );
    KVM_MODE.OnAnalogChange.Add( new InputChangeHandlerWrapper( KVM_MODE_OnChange_2, false ) );
    KVM_RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( KVM_RX__DOLLAR___OnChange_3, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_ATEN_KVM_V0_3 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction HEARTBEAT_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_23___Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint KVM_INPUT__AnalogSerialInput__ = 0;
const uint KVM_MODE__AnalogSerialInput__ = 1;
const uint KVM_RX__DOLLAR____AnalogSerialInput__ = 2;
const uint STATUS_CONNECT__DigitalOutput__ = 0;
const uint STATUS_INPUT__AnalogSerialOutput__ = 0;
const uint KVM_TX__DOLLAR____AnalogSerialOutput__ = 1;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SKVM : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  HEARTBEAT;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  TIMER = 0;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  OPEN;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  CR;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  RESETUSB;
    
    
    public SKVM( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        HEARTBEAT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        OPEN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        CR  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 4, Owner );
        RESETUSB  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SLOCALKVM : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  CRXQUEUE;
    
    
    public SLOCALKVM( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        CRXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1000, Owner );
        
        
    }
    
}

}
