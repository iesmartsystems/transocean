using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_READ_CONFIG_V1_2
{
    public class UserModuleClass_IESS_READ_CONFIG_V1_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput SYSTEMSTART;
        Crestron.Logos.SplusObjects.DigitalInput SAVECONFIG;
        Crestron.Logos.SplusObjects.StringInput FILENAME;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMNAME;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMPHONENUMBER;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMNOTOUCHOFFENABLE;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMNOTOUCHONENABLE;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMNOTOUCHPOWEROFF;
        Crestron.Logos.SplusObjects.StringInput WRITE_ROOMROUTINGMODE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYID;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYTYPE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYPROTOCOL;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYPOWERONTIME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYPOWEROFFTIME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYBAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYPARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYOUTPUTNUM;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYSCREENUP;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_DISPLAYSCREENDOWN;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERBAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SWITCHERPARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOOBJECTID;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOINSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOBAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_AUDIOPARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECNAMECONTENT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECBAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECPARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECINPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECOUTPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_CODECICON;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCENAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCELOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCELOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEBAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEPARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEINPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEICON;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_SOURCEUSESNOTOUCH;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_ENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_IP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_LOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_LOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_IP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_BAUD;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_ENV_PARITY;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_MISCENABLED;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_MISCSTRINGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> WRITE_MISCANALOGS;
        Crestron.Logos.SplusObjects.DigitalOutput READCOMPLETE_FB;
        Crestron.Logos.SplusObjects.DigitalOutput WRITECOMPLETE_FB;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMNOTOUCHOFFENABLE;
        Crestron.Logos.SplusObjects.DigitalOutput ROOMNOTOUCHONENABLE;
        Crestron.Logos.SplusObjects.StringOutput ROOMNAME;
        Crestron.Logos.SplusObjects.StringOutput ROOMPHONENUMBER;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMNOTOUCHPOWEROFF;
        Crestron.Logos.SplusObjects.AnalogOutput DISPLAYNUMBER;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMROUTINGMODE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DISPLAYNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DISPLAYID;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DISPLAYIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DISPLAYLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> DISPLAYLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYTYPE;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYPROTOCOL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYPOWERONTIME;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYPOWEROFFTIME;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYBAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYPARITY;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYOUTPUTNUM;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYSCREENUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> DISPLAYSCREENDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> DISPLAYIPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> DISPLAYUSED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SWITCHERIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SWITCHERLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SWITCHERLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SWITCHERIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SWITCHERBAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SWITCHERPARITY;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SWITCHERIPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SWITCHERUSED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIOIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIOLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIOLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIOOBJECTID;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIOINSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> AUDIOIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> AUDIOBAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> AUDIOPARITY;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> AUDIOIPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> AUDIOUSED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CODECNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CODECNAMECONTENT;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CODECIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CODECLOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> CODECLOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECBAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECPARITY;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECINPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECOUTPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CODECICON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> CODECIPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> CODECUSED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCENAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCEIP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCELOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> SOURCELOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEIP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEBAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEPARITY;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEINPUTVIDEO;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SOURCEICON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SOURCEIPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SOURCEUSESNOTOUCH;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SOURCEUSED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> ENV_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> ENV_IP_ADDRESS;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> ENV_LOGINNAME;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> ENV_LOGINPASSWORD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ENV_IP_PORT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ENV_BAUD;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ENV_PARITY;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> ENV_IPCONTROL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> ENV_USED;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> MISCSTRINGS;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> MISCANALOGS;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> MISCUSED;
        ushort GLBL_STARTREAD = 0;
        ushort GLBL_MAXDISPLAYS = 0;
        ushort GVROOMNOTOUCHOFF = 0;
        ushort GVROOMNOTOUCHON = 0;
        SDEVICE [] GVDISPLAY;
        SDEVICE [] GVSWITCHER;
        SDEVICE [] GVAUDIO;
        SDEVICE [] GVCODEC;
        SDEVICE [] GVSOURCE;
        SDEVICE [] GVENV;
        SDEVICE [] GVMISC;
        private void STOPSYSTEM (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 149;
            GLBL_MAXDISPLAYS = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 150;
            GLBL_STARTREAD = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 151;
            READCOMPLETE_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 152;
            ROOMNAME  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 153;
            ROOMPHONENUMBER  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 154;
            ROOMNOTOUCHOFFENABLE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 155;
            ROOMNOTOUCHONENABLE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 156;
            ROOMNOTOUCHPOWEROFF  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 157;
            ROOMROUTINGMODE  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 158;
            DISPLAYNUMBER  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 159;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)8; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 161;
                GVDISPLAY [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 162;
                GVDISPLAY [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 163;
                DISPLAYIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 164;
                DISPLAYUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 165;
                DISPLAYNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 166;
                DISPLAYID [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 167;
                DISPLAYIP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 168;
                DISPLAYLOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 169;
                DISPLAYLOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 170;
                DISPLAYTYPE [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 171;
                DISPLAYPROTOCOL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 172;
                DISPLAYPOWERONTIME [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 173;
                DISPLAYPOWEROFFTIME [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 174;
                DISPLAYIP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 175;
                DISPLAYBAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 176;
                DISPLAYPARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 177;
                DISPLAYOUTPUTNUM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 178;
                DISPLAYSCREENUP [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 179;
                DISPLAYSCREENDOWN [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 159;
                } 
            
            __context__.SourceCodeLine = 181;
            ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)8; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                __context__.SourceCodeLine = 183;
                GVSWITCHER [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 184;
                GVSWITCHER [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 185;
                SWITCHERIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 186;
                SWITCHERUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 187;
                SWITCHERIP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 188;
                SWITCHERLOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 189;
                SWITCHERLOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 190;
                SWITCHERBAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 191;
                SWITCHERPARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 192;
                SWITCHERIP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 181;
                } 
            
            __context__.SourceCodeLine = 194;
            ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__3 = (ushort)4; 
            int __FN_FORSTEP_VAL__3 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                { 
                __context__.SourceCodeLine = 196;
                GVAUDIO [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 197;
                GVAUDIO [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 198;
                AUDIOIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 199;
                AUDIOUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 200;
                AUDIOIP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 201;
                AUDIOLOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 202;
                AUDIOLOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 203;
                AUDIOOBJECTID [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 204;
                AUDIOBAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 205;
                AUDIOPARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 206;
                AUDIOIP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 194;
                } 
            
            __context__.SourceCodeLine = 208;
            ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__4 = (ushort)64; 
            int __FN_FORSTEP_VAL__4 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__4) && (LVCOUNTER  <= __FN_FOREND_VAL__4) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__4) && (LVCOUNTER  >= __FN_FOREND_VAL__4) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__4) 
                { 
                __context__.SourceCodeLine = 210;
                AUDIOINSTANCETAGS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 208;
                } 
            
            __context__.SourceCodeLine = 212;
            ushort __FN_FORSTART_VAL__5 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__5 = (ushort)4; 
            int __FN_FORSTEP_VAL__5 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__5) && (LVCOUNTER  <= __FN_FOREND_VAL__5) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__5) && (LVCOUNTER  >= __FN_FOREND_VAL__5) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__5) 
                { 
                __context__.SourceCodeLine = 214;
                GVCODEC [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 215;
                GVCODEC [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 216;
                CODECIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 217;
                CODECUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 218;
                CODECNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 219;
                CODECNAMECONTENT [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 220;
                CODECIP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 221;
                CODECLOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 222;
                CODECLOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 223;
                CODECBAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 224;
                CODECPARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 225;
                CODECIP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 226;
                CODECINPUTVIDEO [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 227;
                CODECOUTPUTVIDEO [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 228;
                CODECICON [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 212;
                } 
            
            __context__.SourceCodeLine = 230;
            ushort __FN_FORSTART_VAL__6 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__6 = (ushort)16; 
            int __FN_FORSTEP_VAL__6 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__6; (__FN_FORSTEP_VAL__6 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__6) && (LVCOUNTER  <= __FN_FOREND_VAL__6) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__6) && (LVCOUNTER  >= __FN_FOREND_VAL__6) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__6) 
                { 
                __context__.SourceCodeLine = 232;
                GVSOURCE [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 233;
                GVSOURCE [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 234;
                SOURCEIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 235;
                SOURCEUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 236;
                SOURCEUSESNOTOUCH [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 237;
                SOURCENAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 238;
                SOURCEIP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 239;
                SOURCELOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 240;
                SOURCELOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 241;
                SOURCEBAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 242;
                SOURCEPARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 243;
                SOURCEIP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 244;
                SOURCEINPUTVIDEO [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 245;
                SOURCEICON [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 230;
                } 
            
            __context__.SourceCodeLine = 247;
            ushort __FN_FORSTART_VAL__7 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__7 = (ushort)4; 
            int __FN_FORSTEP_VAL__7 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__7; (__FN_FORSTEP_VAL__7 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__7) && (LVCOUNTER  <= __FN_FOREND_VAL__7) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__7) && (LVCOUNTER  >= __FN_FOREND_VAL__7) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__7) 
                { 
                __context__.SourceCodeLine = 249;
                GVENV [ LVCOUNTER] . USINGIP = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 250;
                GVENV [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 251;
                ENV_IPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 252;
                ENV_USED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 253;
                ENV_NAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 254;
                ENV_IP_ADDRESS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 255;
                ENV_LOGINNAME [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 256;
                ENV_LOGINPASSWORD [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 257;
                ENV_BAUD [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 258;
                ENV_PARITY [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 259;
                ENV_IP_PORT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 247;
                } 
            
            __context__.SourceCodeLine = 261;
            ushort __FN_FORSTART_VAL__8 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__8 = (ushort)16; 
            int __FN_FORSTEP_VAL__8 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__8; (__FN_FORSTEP_VAL__8 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__8) && (LVCOUNTER  <= __FN_FOREND_VAL__8) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__8) && (LVCOUNTER  >= __FN_FOREND_VAL__8) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__8) 
                { 
                __context__.SourceCodeLine = 263;
                GVMISC [ LVCOUNTER] . USED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 264;
                MISCUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 265;
                MISCSTRINGS [ LVCOUNTER]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 266;
                MISCANALOGS [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 261;
                } 
            
            
            }
            
        private void SHOWDIGITALS (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 272;
            if ( Functions.TestForTrue  ( ( GVROOMNOTOUCHOFF)  ) ) 
                {
                __context__.SourceCodeLine = 273;
                ROOMNOTOUCHOFFENABLE  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 275;
                ROOMNOTOUCHOFFENABLE  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 276;
            if ( Functions.TestForTrue  ( ( GVROOMNOTOUCHON)  ) ) 
                {
                __context__.SourceCodeLine = 277;
                ROOMNOTOUCHONENABLE  .Value = (ushort) ( 1 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 279;
                ROOMNOTOUCHONENABLE  .Value = (ushort) ( 0 ) ; 
                }
            
            __context__.SourceCodeLine = 281;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)8; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 283;
                if ( Functions.TestForTrue  ( ( GVDISPLAY[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 284;
                    DISPLAYIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 286;
                    DISPLAYIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 287;
                if ( Functions.TestForTrue  ( ( GVDISPLAY[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 288;
                    DISPLAYUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 290;
                    DISPLAYUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 281;
                } 
            
            __context__.SourceCodeLine = 292;
            ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)8; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                __context__.SourceCodeLine = 294;
                if ( Functions.TestForTrue  ( ( GVSWITCHER[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 295;
                    SWITCHERIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 297;
                    SWITCHERIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 298;
                if ( Functions.TestForTrue  ( ( GVSWITCHER[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 299;
                    SWITCHERUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 301;
                    SWITCHERUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 292;
                } 
            
            __context__.SourceCodeLine = 304;
            ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__3 = (ushort)4; 
            int __FN_FORSTEP_VAL__3 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                { 
                __context__.SourceCodeLine = 306;
                if ( Functions.TestForTrue  ( ( GVAUDIO[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 307;
                    AUDIOIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 309;
                    AUDIOIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 310;
                if ( Functions.TestForTrue  ( ( GVAUDIO[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 311;
                    AUDIOUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 313;
                    AUDIOUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 304;
                } 
            
            __context__.SourceCodeLine = 315;
            ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__4 = (ushort)4; 
            int __FN_FORSTEP_VAL__4 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__4) && (LVCOUNTER  <= __FN_FOREND_VAL__4) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__4) && (LVCOUNTER  >= __FN_FOREND_VAL__4) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__4) 
                { 
                __context__.SourceCodeLine = 317;
                if ( Functions.TestForTrue  ( ( GVCODEC[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 318;
                    CODECIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 320;
                    CODECIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 321;
                if ( Functions.TestForTrue  ( ( GVCODEC[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 322;
                    CODECUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 324;
                    CODECUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 315;
                } 
            
            __context__.SourceCodeLine = 326;
            ushort __FN_FORSTART_VAL__5 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__5 = (ushort)16; 
            int __FN_FORSTEP_VAL__5 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__5) && (LVCOUNTER  <= __FN_FOREND_VAL__5) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__5) && (LVCOUNTER  >= __FN_FOREND_VAL__5) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__5) 
                { 
                __context__.SourceCodeLine = 328;
                if ( Functions.TestForTrue  ( ( GVSOURCE[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 329;
                    SOURCEIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 331;
                    SOURCEIPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 332;
                if ( Functions.TestForTrue  ( ( GVSOURCE[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 333;
                    SOURCEUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 335;
                    SOURCEUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 326;
                } 
            
            __context__.SourceCodeLine = 338;
            ushort __FN_FORSTART_VAL__6 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__6 = (ushort)4; 
            int __FN_FORSTEP_VAL__6 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__6; (__FN_FORSTEP_VAL__6 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__6) && (LVCOUNTER  <= __FN_FOREND_VAL__6) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__6) && (LVCOUNTER  >= __FN_FOREND_VAL__6) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__6) 
                { 
                __context__.SourceCodeLine = 340;
                if ( Functions.TestForTrue  ( ( GVENV[ LVCOUNTER ].USINGIP)  ) ) 
                    {
                    __context__.SourceCodeLine = 341;
                    ENV_IPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 343;
                    ENV_IPCONTROL [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 344;
                if ( Functions.TestForTrue  ( ( GVENV[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 345;
                    ENV_USED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 347;
                    ENV_USED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 338;
                } 
            
            __context__.SourceCodeLine = 349;
            ushort __FN_FORSTART_VAL__7 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__7 = (ushort)16; 
            int __FN_FORSTEP_VAL__7 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__7; (__FN_FORSTEP_VAL__7 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__7) && (LVCOUNTER  <= __FN_FOREND_VAL__7) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__7) && (LVCOUNTER  >= __FN_FOREND_VAL__7) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__7) 
                { 
                __context__.SourceCodeLine = 351;
                if ( Functions.TestForTrue  ( ( GVMISC[ LVCOUNTER ].USED)  ) ) 
                    {
                    __context__.SourceCodeLine = 352;
                    MISCUSED [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 354;
                    MISCUSED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 349;
                } 
            
            
            }
            
        private CrestronString PARSEASCIIHEX (  SplusExecutionContext __context__, CrestronString LVASCII ) 
            { 
            CrestronString LVDATAHEX;
            LVDATAHEX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            CrestronString LVTRASH;
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            
            __context__.SourceCodeLine = 362;
            LVDATAHEX  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 363;
            while ( Functions.TestForTrue  ( ( Functions.Find( "$" , LVASCII ))  ) ) 
                { 
                __context__.SourceCodeLine = 365;
                if ( Functions.TestForTrue  ( ( Functions.Find( ",$" , LVASCII ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 367;
                    LVTEMP  .UpdateValue ( Functions.Remove ( ",$" , LVASCII )  ) ; 
                    __context__.SourceCodeLine = 368;
                    LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                    __context__.SourceCodeLine = 369;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "$" , LVTEMP ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 371;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "$" , LVTEMP )  ) ; 
                        __context__.SourceCodeLine = 372;
                        LVDATAHEX  .UpdateValue ( LVDATAHEX + Functions.Chr (  (int) ( Functions.HextoI( LVTEMP ) ) )  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 375;
                        LVDATAHEX  .UpdateValue ( LVDATAHEX + Functions.Chr (  (int) ( Functions.HextoI( LVTEMP ) ) )  ) ; 
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 363;
                } 
            
            __context__.SourceCodeLine = 378;
            LVTEMP  .UpdateValue ( Functions.Remove ( Functions.Left ( LVASCII ,  (int) ( 2 ) ) , LVASCII )  ) ; 
            __context__.SourceCodeLine = 379;
            LVDATAHEX  .UpdateValue ( LVDATAHEX + Functions.Chr (  (int) ( Functions.HextoI( LVTEMP ) ) )  ) ; 
            __context__.SourceCodeLine = 380;
            LVDATAHEX  .UpdateValue ( LVDATAHEX + LVASCII  ) ; 
            __context__.SourceCodeLine = 381;
            return ( LVDATAHEX ) ; 
            
            }
            
        private void PARSEDATAFROMCONFIG (  SplusExecutionContext __context__, CrestronString LVDATA ) 
            { 
            CrestronString LVDEVICE;
            CrestronString LVINDEXTEMP;
            CrestronString LVTRASH;
            LVDEVICE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            LVINDEXTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINSTANCEINDEX = 0;
            
            
            __context__.SourceCodeLine = 388;
            if ( Functions.TestForTrue  ( ( Functions.Find( "//" , LVDATA ))  ) ) 
                { 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 395;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Room: " , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 397;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Room: " , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 398;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RoomName-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 400;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "RoomName-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 401;
                        ROOMNAME  .UpdateValue ( LVDATA  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 404;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "NoTouchOffEnable-" , LVDATA ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 406;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "NoTouchOffEnable-" , LVDATA )  ) ; 
                            __context__.SourceCodeLine = 407;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Atoi( LVDATA ) == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Atoi( LVDATA ) == 1) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 409;
                                GVROOMNOTOUCHOFF = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                __context__.SourceCodeLine = 410;
                                ROOMNOTOUCHOFFENABLE  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 414;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "NoTouchOnEnable-" , LVDATA ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 416;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "NoTouchOnEnable-" , LVDATA )  ) ; 
                                __context__.SourceCodeLine = 417;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (Functions.Atoi( LVDATA ) == 0) ) || Functions.TestForTrue ( Functions.BoolToInt (Functions.Atoi( LVDATA ) == 1) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 419;
                                    GVROOMNOTOUCHON = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                    __context__.SourceCodeLine = 420;
                                    ROOMNOTOUCHONENABLE  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 424;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "NoTouchPowerOff-" , LVDATA ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 426;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "NoTouchPowerOff-" , LVDATA )  ) ; 
                                    __context__.SourceCodeLine = 427;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVDATA ) > 0 ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 428;
                                        ROOMNOTOUCHPOWEROFF  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 431;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "PhoneNumber-" , LVDATA ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 433;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "PhoneNumber-" , LVDATA )  ) ; 
                                        __context__.SourceCodeLine = 434;
                                        ROOMPHONENUMBER  .UpdateValue ( LVDATA  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 436;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "RoutingMode-" , LVDATA ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 438;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "RoutingMode-" , LVDATA )  ) ; 
                                            __context__.SourceCodeLine = 439;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVDATA ) > 0 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 440;
                                                ROOMROUTINGMODE  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 445;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Display " , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 447;
                        LVDEVICE  .UpdateValue ( "Display"  ) ; 
                        __context__.SourceCodeLine = 448;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "Display " , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 449;
                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 450;
                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                        __context__.SourceCodeLine = 451;
                        LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                        __context__.SourceCodeLine = 452;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVINDEX > GLBL_MAXDISPLAYS ))  ) ) 
                            {
                            __context__.SourceCodeLine = 453;
                            GLBL_MAXDISPLAYS = (ushort) ( LVINDEX ) ; 
                            }
                        
                        __context__.SourceCodeLine = 454;
                        if ( Functions.TestForTrue  ( ( Functions.Not( GVDISPLAY[ LVINDEX ].USED ))  ) ) 
                            {
                            __context__.SourceCodeLine = 455;
                            GVDISPLAY [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 458;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "Switcher " , LVDATA ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 460;
                            LVDEVICE  .UpdateValue ( "Switcher"  ) ; 
                            __context__.SourceCodeLine = 461;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "Switcher " , LVDATA )  ) ; 
                            __context__.SourceCodeLine = 462;
                            LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                            __context__.SourceCodeLine = 463;
                            LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                            __context__.SourceCodeLine = 464;
                            LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                            __context__.SourceCodeLine = 465;
                            if ( Functions.TestForTrue  ( ( Functions.Not( GVSWITCHER[ LVINDEX ].USED ))  ) ) 
                                {
                                __context__.SourceCodeLine = 466;
                                GVSWITCHER [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 469;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "Audio " , LVDATA ) ) && Functions.TestForTrue ( Functions.Not( Functions.Find( "InputAudio" , LVDATA ) ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 471;
                                LVDEVICE  .UpdateValue ( "Audio"  ) ; 
                                __context__.SourceCodeLine = 472;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "Audio " , LVDATA )  ) ; 
                                __context__.SourceCodeLine = 473;
                                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                                __context__.SourceCodeLine = 474;
                                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                                __context__.SourceCodeLine = 475;
                                LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                                __context__.SourceCodeLine = 476;
                                if ( Functions.TestForTrue  ( ( Functions.Not( GVAUDIO[ LVINDEX ].USED ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 477;
                                    GVAUDIO [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 480;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "Codec " , LVDATA ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 482;
                                    LVDEVICE  .UpdateValue ( "Codec"  ) ; 
                                    __context__.SourceCodeLine = 483;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "Codec " , LVDATA )  ) ; 
                                    __context__.SourceCodeLine = 484;
                                    LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                                    __context__.SourceCodeLine = 485;
                                    LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                                    __context__.SourceCodeLine = 486;
                                    LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                                    __context__.SourceCodeLine = 487;
                                    if ( Functions.TestForTrue  ( ( Functions.Not( GVCODEC[ LVINDEX ].USED ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 488;
                                        GVCODEC [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 491;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "Source " , LVDATA ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 493;
                                        LVDEVICE  .UpdateValue ( "Source"  ) ; 
                                        __context__.SourceCodeLine = 494;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "Source " , LVDATA )  ) ; 
                                        __context__.SourceCodeLine = 495;
                                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                                        __context__.SourceCodeLine = 496;
                                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                                        __context__.SourceCodeLine = 497;
                                        LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                                        __context__.SourceCodeLine = 498;
                                        if ( Functions.TestForTrue  ( ( Functions.Not( GVSOURCE[ LVINDEX ].USED ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 499;
                                            GVSOURCE [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 502;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "ENV " , LVDATA ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 504;
                                            LVDEVICE  .UpdateValue ( "ENV"  ) ; 
                                            __context__.SourceCodeLine = 505;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "ENV " , LVDATA )  ) ; 
                                            __context__.SourceCodeLine = 506;
                                            LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                                            __context__.SourceCodeLine = 507;
                                            LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                                            __context__.SourceCodeLine = 508;
                                            LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                                            __context__.SourceCodeLine = 509;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( GVENV[ LVINDEX ].USED ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 510;
                                                GVENV [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 513;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Misc " , LVDATA ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 515;
                                                LVDEVICE  .UpdateValue ( "Misc"  ) ; 
                                                __context__.SourceCodeLine = 516;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Misc " , LVDATA )  ) ; 
                                                __context__.SourceCodeLine = 517;
                                                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                                                __context__.SourceCodeLine = 518;
                                                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                                                __context__.SourceCodeLine = 519;
                                                LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                                                __context__.SourceCodeLine = 520;
                                                if ( Functions.TestForTrue  ( ( Functions.Not( GVMISC[ LVINDEX ].USED ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 521;
                                                    GVMISC [ LVINDEX] . USED = (ushort) ( 1 ) ; 
                                                    }
                                                
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 525;
                if ( Functions.TestForTrue  ( ( Functions.Find( "ControlDeviceType-" , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 527;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "ControlDeviceType-" , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 528;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 529;
                        DISPLAYTYPE [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 532;
                if ( Functions.TestForTrue  ( ( Functions.Find( "ControlBaud-" , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 534;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "ControlBaud-" , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 535;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVDATA ) > 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 537;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 539;
                            DISPLAYBAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                            __context__.SourceCodeLine = 540;
                            GVDISPLAY [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 542;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 544;
                                SWITCHERBAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                __context__.SourceCodeLine = 545;
                                GVSWITCHER [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 547;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 549;
                                    AUDIOBAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                    __context__.SourceCodeLine = 550;
                                    GVAUDIO [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 552;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 554;
                                        CODECBAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                        __context__.SourceCodeLine = 555;
                                        GVCODEC [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 557;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 559;
                                            SOURCEBAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                            __context__.SourceCodeLine = 560;
                                            GVSOURCE [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 562;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 564;
                                                ENV_BAUD [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                __context__.SourceCodeLine = 565;
                                                GVENV [ LVINDEX] . USINGIP = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 570;
                if ( Functions.TestForTrue  ( ( Functions.Find( "ControlParity-" , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 572;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "ControlParity-" , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 573;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 574;
                        DISPLAYPARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 575;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 576;
                            SWITCHERPARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 577;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 578;
                                AUDIOPARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 579;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 580;
                                    CODECPARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 581;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 582;
                                        SOURCEPARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                        }
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 583;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 584;
                                            ENV_PARITY [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 587;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "ControlID-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 589;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "ControlID-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 590;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                            {
                            __context__.SourceCodeLine = 591;
                            DISPLAYID [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 594;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "ControlProtocol-" , LVDATA ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 596;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "ControlProtocol-" , LVDATA )  ) ; 
                            __context__.SourceCodeLine = 597;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                {
                                __context__.SourceCodeLine = 598;
                                DISPLAYPROTOCOL [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 601;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "PowerOnTime-" , LVDATA ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 603;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "PowerOnTime-" , LVDATA )  ) ; 
                                __context__.SourceCodeLine = 604;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 605;
                                    DISPLAYPOWERONTIME [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 608;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "PowerOffTime-" , LVDATA ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 610;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "PowerOffTime-" , LVDATA )  ) ; 
                                    __context__.SourceCodeLine = 611;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 612;
                                        DISPLAYPOWEROFFTIME [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 615;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "ControlIP-" , LVDATA ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 617;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "ControlIP-" , LVDATA )  ) ; 
                                        __context__.SourceCodeLine = 618;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVDATA ) > 6 ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 620;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "$" , LVDATA ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 621;
                                                LVDATA  .UpdateValue ( PARSEASCIIHEX (  __context__ , LVDATA)  ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 622;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 624;
                                                DISPLAYIP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                __context__.SourceCodeLine = 625;
                                                GVDISPLAY [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 627;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 629;
                                                    SWITCHERIP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                    __context__.SourceCodeLine = 630;
                                                    GVSWITCHER [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 632;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 634;
                                                        AUDIOIP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                        __context__.SourceCodeLine = 635;
                                                        GVAUDIO [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 637;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 639;
                                                            CODECIP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                            __context__.SourceCodeLine = 640;
                                                            GVCODEC [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 642;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 644;
                                                                SOURCEIP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                __context__.SourceCodeLine = 645;
                                                                GVSOURCE [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 647;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 649;
                                                                    ENV_IP_ADDRESS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                    __context__.SourceCodeLine = 650;
                                                                    GVENV [ LVINDEX] . USINGIP = (ushort) ( 1 ) ; 
                                                                    } 
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 655;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "ControlIPPort-" , LVDATA ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 657;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "ControlIPPort-" , LVDATA )  ) ; 
                                            __context__.SourceCodeLine = 658;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVDATA ) > 0 ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 660;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 661;
                                                    DISPLAYIP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 662;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 663;
                                                        SWITCHERIP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 664;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 665;
                                                            AUDIOIP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 666;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 667;
                                                                CODECIP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 668;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 669;
                                                                    SOURCEIP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                    }
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 670;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 671;
                                                                        ENV_IP_PORT [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 675;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ControlLogin-" , LVDATA ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 677;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "ControlLogin-" , LVDATA )  ) ; 
                                                __context__.SourceCodeLine = 678;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 679;
                                                    DISPLAYLOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 680;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 681;
                                                        SWITCHERLOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 682;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 683;
                                                            AUDIOLOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 684;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 685;
                                                                CODECLOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 686;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 687;
                                                                    SOURCELOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                    }
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 688;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 689;
                                                                        ENV_LOGINNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 692;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "ControlPassword-" , LVDATA ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 694;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "ControlPassword-" , LVDATA )  ) ; 
                                                    __context__.SourceCodeLine = 695;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 696;
                                                        DISPLAYLOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 697;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Switcher") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 698;
                                                            SWITCHERLOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 699;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 700;
                                                                AUDIOLOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 701;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 702;
                                                                    CODECLOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                    }
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 703;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 704;
                                                                        SOURCELOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                        }
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 705;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                            {
                                                                            __context__.SourceCodeLine = 706;
                                                                            ENV_LOGINPASSWORD [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 709;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "ObjectID-" , LVDATA ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 711;
                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "ObjectID-" , LVDATA )  ) ; 
                                                        __context__.SourceCodeLine = 712;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 714;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "$" , LVDATA ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 715;
                                                                AUDIOOBJECTID [ LVINDEX]  .UpdateValue ( PARSEASCIIHEX (  __context__ , LVDATA)  ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 717;
                                                                AUDIOOBJECTID [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                }
                                                            
                                                            } 
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 721;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "InstanceTags " , LVDATA ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 723;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "InstanceTags " , LVDATA )  ) ; 
                                                            __context__.SourceCodeLine = 724;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "-" , LVDATA )  ) ; 
                                                            __context__.SourceCodeLine = 725;
                                                            LVTRASH  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTRASH ) - 1), LVTRASH )  ) ; 
                                                            __context__.SourceCodeLine = 726;
                                                            LVINSTANCEINDEX = (ushort) ( Functions.Atoi( LVTRASH ) ) ; 
                                                            __context__.SourceCodeLine = 727;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Audio") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 729;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVINSTANCEINDEX > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINSTANCEINDEX <= 64 ) )) ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 731;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "$" , LVDATA ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 732;
                                                                        AUDIOINSTANCETAGS [ LVINSTANCEINDEX]  .UpdateValue ( PARSEASCIIHEX (  __context__ , LVDATA)  ) ; 
                                                                        }
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 734;
                                                                        AUDIOINSTANCETAGS [ LVINSTANCEINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 739;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "InputVideo-" , LVDATA ))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 741;
                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "InputVideo-" , LVDATA )  ) ; 
                                                                __context__.SourceCodeLine = 742;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                    {
                                                                    __context__.SourceCodeLine = 743;
                                                                    CODECINPUTVIDEO [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                    }
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 744;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 745;
                                                                        SOURCEINPUTVIDEO [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 748;
                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "IconType-" , LVDATA ))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 750;
                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "IconType-" , LVDATA )  ) ; 
                                                                    __context__.SourceCodeLine = 751;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                        {
                                                                        __context__.SourceCodeLine = 752;
                                                                        CODECICON [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                        }
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 753;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                            {
                                                                            __context__.SourceCodeLine = 754;
                                                                            SOURCEICON [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 757;
                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "Name-" , LVDATA ))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 759;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "ContentName-" , LVDATA ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 761;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "ContentName-" , LVDATA )  ) ; 
                                                                            __context__.SourceCodeLine = 762;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                                {
                                                                                __context__.SourceCodeLine = 763;
                                                                                CODECNAMECONTENT [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                }
                                                                            
                                                                            } 
                                                                        
                                                                        else 
                                                                            { 
                                                                            __context__.SourceCodeLine = 767;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "Name-" , LVDATA )  ) ; 
                                                                            __context__.SourceCodeLine = 768;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVDATA ) > 0 ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 770;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 771;
                                                                                    CODECNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                    }
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 772;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 773;
                                                                                        DISPLAYNAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                        }
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 774;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 775;
                                                                                            SOURCENAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                            }
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 776;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "ENV") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                                                {
                                                                                                __context__.SourceCodeLine = 777;
                                                                                                ENV_NAME [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                                }
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                } 
                                                                            
                                                                            } 
                                                                        
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 782;
                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "OutputVideo-" , LVDATA ))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 784;
                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "OutputVideo-" , LVDATA )  ) ; 
                                                                            __context__.SourceCodeLine = 785;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                                                {
                                                                                __context__.SourceCodeLine = 786;
                                                                                DISPLAYOUTPUTNUM [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                }
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 787;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Codec") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 4 ) )) ))  ) ) 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 788;
                                                                                    CODECOUTPUTVIDEO [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 791;
                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "ScreenUp-" , LVDATA ))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 793;
                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "ScreenUp-" , LVDATA )  ) ; 
                                                                                __context__.SourceCodeLine = 794;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 795;
                                                                                    DISPLAYSCREENUP [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                    }
                                                                                
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 798;
                                                                                if ( Functions.TestForTrue  ( ( Functions.Find( "ScreenDown-" , LVDATA ))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 800;
                                                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "ScreenDown-" , LVDATA )  ) ; 
                                                                                    __context__.SourceCodeLine = 801;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Display") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 8 ) )) ))  ) ) 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 802;
                                                                                        DISPLAYSCREENDOWN [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                        }
                                                                                    
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 805;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "Strings-" , LVDATA ))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 807;
                                                                                        LVTRASH  .UpdateValue ( Functions.Remove ( "Strings-" , LVDATA )  ) ; 
                                                                                        __context__.SourceCodeLine = 808;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Misc") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 809;
                                                                                            MISCSTRINGS [ LVINDEX]  .UpdateValue ( LVDATA  ) ; 
                                                                                            }
                                                                                        
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 811;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Analogs-" , LVDATA ))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 813;
                                                                                            LVTRASH  .UpdateValue ( Functions.Remove ( "Analogs-" , LVDATA )  ) ; 
                                                                                            __context__.SourceCodeLine = 814;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Misc") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                                                {
                                                                                                __context__.SourceCodeLine = 815;
                                                                                                MISCANALOGS [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                                }
                                                                                            
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 817;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "UsesNoTouch-" , LVDATA ))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 819;
                                                                                                LVTRASH  .UpdateValue ( Functions.Remove ( "UsesNoTouch-" , LVDATA )  ) ; 
                                                                                                __context__.SourceCodeLine = 820;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDATA == "1") ) || Functions.TestForTrue ( Functions.BoolToInt (LVDATA == "0") )) ))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 822;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVDEVICE == "Source") ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX > 0 ) )) ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVINDEX <= 16 ) )) ))  ) ) 
                                                                                                        {
                                                                                                        __context__.SourceCodeLine = 823;
                                                                                                        SOURCEUSESNOTOUCH [ LVINDEX]  .Value = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                                                                                                        }
                                                                                                    
                                                                                                    } 
                                                                                                
                                                                                                } 
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            
            }
            
        private void FILEOPENCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVREADFILE;
            CrestronString LVREADLINE;
            CrestronString LVFILENAME;
            CrestronString LVTRASH;
            LVREADFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
            LVREADLINE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
            
            
            __context__.SourceCodeLine = 833;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 835;
                LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
                __context__.SourceCodeLine = 836;
                StartFileOperations ( ) ; 
                __context__.SourceCodeLine = 837;
                __context__.SourceCodeLine = 840;
                LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 0) ) ) ; 
                
                __context__.SourceCodeLine = 842;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 844;
                    LVREAD = (ushort) ( FileRead( (short)( LVHANDLE ) , LVREADFILE , (ushort)( 16383 ) ) ) ; 
                    __context__.SourceCodeLine = 845;
                    while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , LVREADFILE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 847;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , LVREADFILE )  ) ; 
                        __context__.SourceCodeLine = 848;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( (Functions.Length( LVREADLINE ) - 2), LVREADLINE )  ) ; 
                        __context__.SourceCodeLine = 849;
                        Trace( "CONFIG READ: {0}", LVREADLINE ) ; 
                        __context__.SourceCodeLine = 850;
                        PARSEDATAFROMCONFIG (  __context__ , LVREADLINE) ; 
                        __context__.SourceCodeLine = 845;
                        } 
                    
                    __context__.SourceCodeLine = 853;
                    LVTRASH  .UpdateValue ( Functions.Remove ( LVREADLINE , LVREADFILE )  ) ; 
                    __context__.SourceCodeLine = 854;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVREADFILE ) > 2 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 856;
                        Trace( "CONFIG READ: {0}", LVREADFILE ) ; 
                        __context__.SourceCodeLine = 857;
                        PARSEDATAFROMCONFIG (  __context__ , LVREADFILE) ; 
                        } 
                    
                    __context__.SourceCodeLine = 860;
                    DISPLAYNUMBER  .Value = (ushort) ( GLBL_MAXDISPLAYS ) ; 
                    __context__.SourceCodeLine = 861;
                    READCOMPLETE_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 862;
                    SHOWDIGITALS (  __context__  ) ; 
                    __context__.SourceCodeLine = 863;
                    LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                    __context__.SourceCodeLine = 864;
                    EndFileOperations ( ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 867;
                    READCOMPLETE_FB  .Value = (ushort) ( 0 ) ; 
                    }
                
                } 
            
            
            }
            
        private void WRITEDATATOCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVCOUNTERTAGS = 0;
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVFILENAME;
            CrestronString LVWRITEFILE;
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVWRITEFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
            
            
            __context__.SourceCodeLine = 875;
            LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
            __context__.SourceCodeLine = 876;
            LVWRITEFILE  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 877;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 878;
            LVHANDLE = (short) ( FileDelete( LVFILENAME ) ) ; 
            __context__.SourceCodeLine = 879;
            LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 1) ) ) ; 
            __context__.SourceCodeLine = 880;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 882;
                LVWRITEFILE  .UpdateValue ( "//ROOM SETUP\u000D\u000A" + "Room: RoomName-" + WRITE_ROOMNAME + "\u000D\u000ARoom: PhoneNumber-" + WRITE_ROOMPHONENUMBER + "\u000D\u000A" + "Room: NoTouchPowerOff-" + WRITE_ROOMNOTOUCHPOWEROFF + "\u000D\u000A" + "Room: NoTouchOffEnable-" + WRITE_ROOMNOTOUCHOFFENABLE + "\u000D\u000A" + "Room: NoTouchOnEnable-" + WRITE_ROOMNOTOUCHONENABLE + "\u000D\u000ARoom: RoutingMode-" + WRITE_ROOMROUTINGMODE  ) ; 
                __context__.SourceCodeLine = 886;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "\u000D\u000A//DISPLAY(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 887;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)8; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 889;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_DISPLAYENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 891;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Display " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "Display " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": SourceName-" + WRITE_DISPLAYNAME [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlID-" + WRITE_DISPLAYID [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_DISPLAYIP_ADDRESS [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_DISPLAYLOGINNAME [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_DISPLAYLOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlDeviceType-" + WRITE_DISPLAYTYPE [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlProtocol-" + WRITE_DISPLAYPROTOCOL [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": PowerOnTime-" + WRITE_DISPLAYPOWERONTIME [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": PowerOffTime-" + WRITE_DISPLAYPOWEROFFTIME [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_DISPLAYIP_PORT [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_DISPLAYBAUD [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_DISPLAYPARITY [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": OutputVideo-" + WRITE_DISPLAYOUTPUTNUM [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ScreenUp-" + WRITE_DISPLAYSCREENUP [ LVCOUNTER ] + "\u000D\u000ADisplay " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ScreenDown-" + WRITE_DISPLAYSCREENDOWN [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 887;
                    } 
                
                __context__.SourceCodeLine = 904;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//SWITCHER(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 905;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)8; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 907;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_SWITCHERENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 909;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Switcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "Switcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_SWITCHERIP_ADDRESS [ LVCOUNTER ] + "\u000D\u000ASwitcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_SWITCHERLOGINNAME [ LVCOUNTER ] + "\u000D\u000ASwitcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_SWITCHERLOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000ASwitcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_SWITCHERIP_PORT [ LVCOUNTER ] + "\u000D\u000ASwitcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_SWITCHERBAUD [ LVCOUNTER ] + "\u000D\u000ASwitcher " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_SWITCHERPARITY [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 905;
                    } 
                
                __context__.SourceCodeLine = 916;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//AUDIO(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 917;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)4; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 919;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_AUDIOENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 921;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Audio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "Audio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_AUDIOIP_ADDRESS [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_AUDIOLOGINNAME [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_AUDIOLOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_AUDIOIP_PORT [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ObjectID-" + WRITE_AUDIOOBJECTID [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_AUDIOBAUD [ LVCOUNTER ] + "\u000D\u000AAudio " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_AUDIOPARITY [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        __context__.SourceCodeLine = 926;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 928;
                            ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                            ushort __FN_FOREND_VAL__4 = (ushort)64; 
                            int __FN_FORSTEP_VAL__4 = (int)1; 
                            for ( LVCOUNTERTAGS  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVCOUNTERTAGS  >= __FN_FORSTART_VAL__4) && (LVCOUNTERTAGS  <= __FN_FOREND_VAL__4) ) : ( (LVCOUNTERTAGS  <= __FN_FORSTART_VAL__4) && (LVCOUNTERTAGS  >= __FN_FOREND_VAL__4) ) ; LVCOUNTERTAGS  += (ushort)__FN_FORSTEP_VAL__4) 
                                { 
                                __context__.SourceCodeLine = 930;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( WRITE_AUDIOINSTANCETAGS[ LVCOUNTERTAGS ] ) > 4 ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 931;
                                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Audio 1: InstanceTags " + Functions.ItoA (  (int) ( LVCOUNTERTAGS ) ) + "-" + WRITE_AUDIOINSTANCETAGS [ LVCOUNTERTAGS ] + "\u000D\u000A"  ) ; 
                                    }
                                
                                __context__.SourceCodeLine = 928;
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 917;
                    } 
                
                __context__.SourceCodeLine = 936;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//CODEC(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 937;
                ushort __FN_FORSTART_VAL__5 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__5 = (ushort)4; 
                int __FN_FORSTEP_VAL__5 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__5; (__FN_FORSTEP_VAL__5 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__5) && (LVCOUNTER  <= __FN_FOREND_VAL__5) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__5) && (LVCOUNTER  >= __FN_FOREND_VAL__5) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__5) 
                    { 
                    __context__.SourceCodeLine = 939;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_CODECENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 941;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Codec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "Codec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": SourceName-" + WRITE_CODECNAME [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ContentName-" + WRITE_CODECNAMECONTENT [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_CODECIP_ADDRESS [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_CODECLOGINNAME [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_CODECLOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_CODECIP_PORT [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_CODECBAUD [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_CODECPARITY [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": InputVideo-" + WRITE_CODECINPUTVIDEO [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": OutputVideo-" + WRITE_CODECOUTPUTVIDEO [ LVCOUNTER ] + "\u000D\u000ACodec " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": IconType-" + WRITE_CODECICON [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 937;
                    } 
                
                __context__.SourceCodeLine = 951;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//SOURCE(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 952;
                ushort __FN_FORSTART_VAL__6 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__6 = (ushort)16; 
                int __FN_FORSTEP_VAL__6 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__6; (__FN_FORSTEP_VAL__6 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__6) && (LVCOUNTER  <= __FN_FOREND_VAL__6) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__6) && (LVCOUNTER  >= __FN_FOREND_VAL__6) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__6) 
                    { 
                    __context__.SourceCodeLine = 954;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_SOURCEENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 956;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Source " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "Source " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": SourceName-" + WRITE_SOURCENAME [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_SOURCEIP_ADDRESS [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_SOURCELOGINNAME [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_SOURCELOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_SOURCEIP_PORT [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_SOURCEBAUD [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_SOURCEPARITY [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": InputVideo-" + WRITE_SOURCEINPUTVIDEO [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": IconType-" + WRITE_SOURCEICON [ LVCOUNTER ] + "\u000D\u000ASource " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": UsesNoTouch-" + WRITE_SOURCEUSESNOTOUCH [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 952;
                    } 
                
                __context__.SourceCodeLine = 965;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//ENVIRONMENT(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 966;
                ushort __FN_FORSTART_VAL__7 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__7 = (ushort)4; 
                int __FN_FORSTEP_VAL__7 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__7; (__FN_FORSTEP_VAL__7 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__7) && (LVCOUNTER  <= __FN_FOREND_VAL__7) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__7) && (LVCOUNTER  >= __FN_FOREND_VAL__7) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__7) 
                    { 
                    __context__.SourceCodeLine = 968;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_ENV_ENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 970;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//ENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u000D\u000A" + "ENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": SourceName-" + WRITE_ENV_NAME [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIP-" + WRITE_ENV_IP_ADDRESS [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlLogin-" + WRITE_ENV_LOGINNAME [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlPassword-" + WRITE_ENV_LOGINPASSWORD [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlIPPort-" + WRITE_ENV_IP_PORT [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlBaud-" + WRITE_ENV_BAUD [ LVCOUNTER ] + "\u000D\u000AENV " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": ControlParity-" + WRITE_ENV_PARITY [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 966;
                    } 
                
                __context__.SourceCodeLine = 977;
                LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//MISC(S)_SETUP\u000D\u000A"  ) ; 
                __context__.SourceCodeLine = 978;
                ushort __FN_FORSTART_VAL__8 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__8 = (ushort)16; 
                int __FN_FORSTEP_VAL__8 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__8; (__FN_FORSTEP_VAL__8 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__8) && (LVCOUNTER  <= __FN_FOREND_VAL__8) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__8) && (LVCOUNTER  >= __FN_FOREND_VAL__8) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__8) 
                    { 
                    __context__.SourceCodeLine = 980;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Atoi( WRITE_MISCENABLED[ LVCOUNTER ] ) == 1))  ) ) 
                        { 
                        __context__.SourceCodeLine = 982;
                        LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Misc " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": Strings-" + WRITE_MISCSTRINGS [ LVCOUNTER ] + "\u000D\u000AMisc " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ": Analogs-" + WRITE_MISCANALOGS [ LVCOUNTER ] + "\u000D\u000A"  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 978;
                    } 
                
                __context__.SourceCodeLine = 987;
                FileWrite (  (short) ( LVHANDLE ) , LVWRITEFILE ,  (ushort) ( Functions.Length( LVWRITEFILE ) ) ) ; 
                __context__.SourceCodeLine = 988;
                LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                __context__.SourceCodeLine = 989;
                EndFileOperations ( ) ; 
                __context__.SourceCodeLine = 990;
                STOPSYSTEM (  __context__  ) ; 
                __context__.SourceCodeLine = 991;
                Functions.Pulse ( 500, WRITECOMPLETE_FB ) ; 
                __context__.SourceCodeLine = 992;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_12__" , 100 , __SPLS_TMPVAR__WAITLABEL_12___Callback ) ;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_12___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 993;
            FILEOPENCONFIG (  __context__  ) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    object SYSTEMSTART_OnChange_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 1001;
            if ( Functions.TestForTrue  ( ( SYSTEMSTART  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 1003;
                GLBL_STARTREAD = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 1004;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1005;
                    FILEOPENCONFIG (  __context__  ) ; 
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1009;
                STOPSYSTEM (  __context__  ) ; 
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object SAVECONFIG_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1014;
        WRITEDATATOCONFIG (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FILENAME_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1018;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1020;
            if ( Functions.TestForTrue  ( ( GLBL_STARTREAD)  ) ) 
                {
                __context__.SourceCodeLine = 1021;
                FILEOPENCONFIG (  __context__  ) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1027;
        GLBL_MAXDISPLAYS = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    GVDISPLAY  = new SDEVICE[ 9 ];
    for( uint i = 0; i < 9; i++ )
    {
        GVDISPLAY [i] = new SDEVICE( this, true );
        GVDISPLAY [i].PopulateCustomAttributeList( false );
        
    }
    GVSWITCHER  = new SDEVICE[ 9 ];
    for( uint i = 0; i < 9; i++ )
    {
        GVSWITCHER [i] = new SDEVICE( this, true );
        GVSWITCHER [i].PopulateCustomAttributeList( false );
        
    }
    GVAUDIO  = new SDEVICE[ 5 ];
    for( uint i = 0; i < 5; i++ )
    {
        GVAUDIO [i] = new SDEVICE( this, true );
        GVAUDIO [i].PopulateCustomAttributeList( false );
        
    }
    GVCODEC  = new SDEVICE[ 5 ];
    for( uint i = 0; i < 5; i++ )
    {
        GVCODEC [i] = new SDEVICE( this, true );
        GVCODEC [i].PopulateCustomAttributeList( false );
        
    }
    GVSOURCE  = new SDEVICE[ 17 ];
    for( uint i = 0; i < 17; i++ )
    {
        GVSOURCE [i] = new SDEVICE( this, true );
        GVSOURCE [i].PopulateCustomAttributeList( false );
        
    }
    GVENV  = new SDEVICE[ 5 ];
    for( uint i = 0; i < 5; i++ )
    {
        GVENV [i] = new SDEVICE( this, true );
        GVENV [i].PopulateCustomAttributeList( false );
        
    }
    GVMISC  = new SDEVICE[ 17 ];
    for( uint i = 0; i < 17; i++ )
    {
        GVMISC [i] = new SDEVICE( this, true );
        GVMISC [i].PopulateCustomAttributeList( false );
        
    }
    
    SYSTEMSTART = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEMSTART__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEMSTART__DigitalInput__, SYSTEMSTART );
    
    SAVECONFIG = new Crestron.Logos.SplusObjects.DigitalInput( SAVECONFIG__DigitalInput__, this );
    m_DigitalInputList.Add( SAVECONFIG__DigitalInput__, SAVECONFIG );
    
    READCOMPLETE_FB = new Crestron.Logos.SplusObjects.DigitalOutput( READCOMPLETE_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( READCOMPLETE_FB__DigitalOutput__, READCOMPLETE_FB );
    
    WRITECOMPLETE_FB = new Crestron.Logos.SplusObjects.DigitalOutput( WRITECOMPLETE_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( WRITECOMPLETE_FB__DigitalOutput__, WRITECOMPLETE_FB );
    
    ROOMNOTOUCHOFFENABLE = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMNOTOUCHOFFENABLE__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMNOTOUCHOFFENABLE__DigitalOutput__, ROOMNOTOUCHOFFENABLE );
    
    ROOMNOTOUCHONENABLE = new Crestron.Logos.SplusObjects.DigitalOutput( ROOMNOTOUCHONENABLE__DigitalOutput__, this );
    m_DigitalOutputList.Add( ROOMNOTOUCHONENABLE__DigitalOutput__, ROOMNOTOUCHONENABLE );
    
    DISPLAYIPCONTROL = new InOutArray<DigitalOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYIPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAYIPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( DISPLAYIPCONTROL__DigitalOutput__ + i, DISPLAYIPCONTROL[i+1] );
    }
    
    DISPLAYUSED = new InOutArray<DigitalOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( DISPLAYUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( DISPLAYUSED__DigitalOutput__ + i, DISPLAYUSED[i+1] );
    }
    
    SWITCHERIPCONTROL = new InOutArray<DigitalOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERIPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SWITCHERIPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SWITCHERIPCONTROL__DigitalOutput__ + i, SWITCHERIPCONTROL[i+1] );
    }
    
    SWITCHERUSED = new InOutArray<DigitalOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SWITCHERUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SWITCHERUSED__DigitalOutput__ + i, SWITCHERUSED[i+1] );
    }
    
    AUDIOIPCONTROL = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOIPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( AUDIOIPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( AUDIOIPCONTROL__DigitalOutput__ + i, AUDIOIPCONTROL[i+1] );
    }
    
    AUDIOUSED = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( AUDIOUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( AUDIOUSED__DigitalOutput__ + i, AUDIOUSED[i+1] );
    }
    
    CODECIPCONTROL = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECIPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( CODECIPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( CODECIPCONTROL__DigitalOutput__ + i, CODECIPCONTROL[i+1] );
    }
    
    CODECUSED = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( CODECUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( CODECUSED__DigitalOutput__ + i, CODECUSED[i+1] );
    }
    
    SOURCEIPCONTROL = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEIPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SOURCEIPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SOURCEIPCONTROL__DigitalOutput__ + i, SOURCEIPCONTROL[i+1] );
    }
    
    SOURCEUSESNOTOUCH = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEUSESNOTOUCH[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SOURCEUSESNOTOUCH__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SOURCEUSESNOTOUCH__DigitalOutput__ + i, SOURCEUSESNOTOUCH[i+1] );
    }
    
    SOURCEUSED = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SOURCEUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SOURCEUSED__DigitalOutput__ + i, SOURCEUSED[i+1] );
    }
    
    ENV_IPCONTROL = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_IPCONTROL[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( ENV_IPCONTROL__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( ENV_IPCONTROL__DigitalOutput__ + i, ENV_IPCONTROL[i+1] );
    }
    
    ENV_USED = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_USED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( ENV_USED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( ENV_USED__DigitalOutput__ + i, ENV_USED[i+1] );
    }
    
    MISCUSED = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        MISCUSED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( MISCUSED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( MISCUSED__DigitalOutput__ + i, MISCUSED[i+1] );
    }
    
    ROOMNOTOUCHPOWEROFF = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMNOTOUCHPOWEROFF__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMNOTOUCHPOWEROFF__AnalogSerialOutput__, ROOMNOTOUCHPOWEROFF );
    
    DISPLAYNUMBER = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYNUMBER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( DISPLAYNUMBER__AnalogSerialOutput__, DISPLAYNUMBER );
    
    ROOMROUTINGMODE = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMROUTINGMODE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMROUTINGMODE__AnalogSerialOutput__, ROOMROUTINGMODE );
    
    DISPLAYTYPE = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYTYPE[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYTYPE__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYTYPE__AnalogSerialOutput__ + i, DISPLAYTYPE[i+1] );
    }
    
    DISPLAYPROTOCOL = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYPROTOCOL[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYPROTOCOL__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYPROTOCOL__AnalogSerialOutput__ + i, DISPLAYPROTOCOL[i+1] );
    }
    
    DISPLAYPOWERONTIME = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYPOWERONTIME[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYPOWERONTIME__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYPOWERONTIME__AnalogSerialOutput__ + i, DISPLAYPOWERONTIME[i+1] );
    }
    
    DISPLAYPOWEROFFTIME = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYPOWEROFFTIME[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYPOWEROFFTIME__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYPOWEROFFTIME__AnalogSerialOutput__ + i, DISPLAYPOWEROFFTIME[i+1] );
    }
    
    DISPLAYIP_PORT = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYIP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYIP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYIP_PORT__AnalogSerialOutput__ + i, DISPLAYIP_PORT[i+1] );
    }
    
    DISPLAYBAUD = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYBAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYBAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYBAUD__AnalogSerialOutput__ + i, DISPLAYBAUD[i+1] );
    }
    
    DISPLAYPARITY = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYPARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYPARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYPARITY__AnalogSerialOutput__ + i, DISPLAYPARITY[i+1] );
    }
    
    DISPLAYOUTPUTNUM = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYOUTPUTNUM[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYOUTPUTNUM__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYOUTPUTNUM__AnalogSerialOutput__ + i, DISPLAYOUTPUTNUM[i+1] );
    }
    
    DISPLAYSCREENUP = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYSCREENUP[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYSCREENUP__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYSCREENUP__AnalogSerialOutput__ + i, DISPLAYSCREENUP[i+1] );
    }
    
    DISPLAYSCREENDOWN = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYSCREENDOWN[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( DISPLAYSCREENDOWN__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( DISPLAYSCREENDOWN__AnalogSerialOutput__ + i, DISPLAYSCREENDOWN[i+1] );
    }
    
    SWITCHERIP_PORT = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERIP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SWITCHERIP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SWITCHERIP_PORT__AnalogSerialOutput__ + i, SWITCHERIP_PORT[i+1] );
    }
    
    SWITCHERBAUD = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERBAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SWITCHERBAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SWITCHERBAUD__AnalogSerialOutput__ + i, SWITCHERBAUD[i+1] );
    }
    
    SWITCHERPARITY = new InOutArray<AnalogOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERPARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SWITCHERPARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SWITCHERPARITY__AnalogSerialOutput__ + i, SWITCHERPARITY[i+1] );
    }
    
    AUDIOIP_PORT = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOIP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( AUDIOIP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( AUDIOIP_PORT__AnalogSerialOutput__ + i, AUDIOIP_PORT[i+1] );
    }
    
    AUDIOBAUD = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOBAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( AUDIOBAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( AUDIOBAUD__AnalogSerialOutput__ + i, AUDIOBAUD[i+1] );
    }
    
    AUDIOPARITY = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOPARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( AUDIOPARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( AUDIOPARITY__AnalogSerialOutput__ + i, AUDIOPARITY[i+1] );
    }
    
    CODECIP_PORT = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECIP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECIP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECIP_PORT__AnalogSerialOutput__ + i, CODECIP_PORT[i+1] );
    }
    
    CODECBAUD = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECBAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECBAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECBAUD__AnalogSerialOutput__ + i, CODECBAUD[i+1] );
    }
    
    CODECPARITY = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECPARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECPARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECPARITY__AnalogSerialOutput__ + i, CODECPARITY[i+1] );
    }
    
    CODECINPUTVIDEO = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECINPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECINPUTVIDEO__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECINPUTVIDEO__AnalogSerialOutput__ + i, CODECINPUTVIDEO[i+1] );
    }
    
    CODECOUTPUTVIDEO = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECOUTPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECOUTPUTVIDEO__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECOUTPUTVIDEO__AnalogSerialOutput__ + i, CODECOUTPUTVIDEO[i+1] );
    }
    
    CODECICON = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECICON[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CODECICON__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CODECICON__AnalogSerialOutput__ + i, CODECICON[i+1] );
    }
    
    SOURCEIP_PORT = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEIP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEIP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEIP_PORT__AnalogSerialOutput__ + i, SOURCEIP_PORT[i+1] );
    }
    
    SOURCEBAUD = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEBAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEBAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEBAUD__AnalogSerialOutput__ + i, SOURCEBAUD[i+1] );
    }
    
    SOURCEPARITY = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEPARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEPARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEPARITY__AnalogSerialOutput__ + i, SOURCEPARITY[i+1] );
    }
    
    SOURCEINPUTVIDEO = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEINPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEINPUTVIDEO__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEINPUTVIDEO__AnalogSerialOutput__ + i, SOURCEINPUTVIDEO[i+1] );
    }
    
    SOURCEICON = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEICON[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SOURCEICON__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SOURCEICON__AnalogSerialOutput__ + i, SOURCEICON[i+1] );
    }
    
    ENV_IP_PORT = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_IP_PORT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ENV_IP_PORT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ENV_IP_PORT__AnalogSerialOutput__ + i, ENV_IP_PORT[i+1] );
    }
    
    ENV_BAUD = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_BAUD[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ENV_BAUD__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ENV_BAUD__AnalogSerialOutput__ + i, ENV_BAUD[i+1] );
    }
    
    ENV_PARITY = new InOutArray<AnalogOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_PARITY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ENV_PARITY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ENV_PARITY__AnalogSerialOutput__ + i, ENV_PARITY[i+1] );
    }
    
    MISCANALOGS = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        MISCANALOGS[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( MISCANALOGS__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( MISCANALOGS__AnalogSerialOutput__ + i, MISCANALOGS[i+1] );
    }
    
    FILENAME = new Crestron.Logos.SplusObjects.StringInput( FILENAME__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( FILENAME__AnalogSerialInput__, FILENAME );
    
    WRITE_ROOMNAME = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMNAME__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMNAME__AnalogSerialInput__, WRITE_ROOMNAME );
    
    WRITE_ROOMPHONENUMBER = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMPHONENUMBER__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMPHONENUMBER__AnalogSerialInput__, WRITE_ROOMPHONENUMBER );
    
    WRITE_ROOMNOTOUCHOFFENABLE = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMNOTOUCHOFFENABLE__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMNOTOUCHOFFENABLE__AnalogSerialInput__, WRITE_ROOMNOTOUCHOFFENABLE );
    
    WRITE_ROOMNOTOUCHONENABLE = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMNOTOUCHONENABLE__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMNOTOUCHONENABLE__AnalogSerialInput__, WRITE_ROOMNOTOUCHONENABLE );
    
    WRITE_ROOMNOTOUCHPOWEROFF = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMNOTOUCHPOWEROFF__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMNOTOUCHPOWEROFF__AnalogSerialInput__, WRITE_ROOMNOTOUCHPOWEROFF );
    
    WRITE_ROOMROUTINGMODE = new Crestron.Logos.SplusObjects.StringInput( WRITE_ROOMROUTINGMODE__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( WRITE_ROOMROUTINGMODE__AnalogSerialInput__, WRITE_ROOMROUTINGMODE );
    
    WRITE_DISPLAYENABLED = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYENABLED__AnalogSerialInput__ + i, WRITE_DISPLAYENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYENABLED__AnalogSerialInput__ + i, WRITE_DISPLAYENABLED[i+1] );
    }
    
    WRITE_DISPLAYNAME = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYNAME__AnalogSerialInput__ + i, WRITE_DISPLAYNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYNAME__AnalogSerialInput__ + i, WRITE_DISPLAYNAME[i+1] );
    }
    
    WRITE_DISPLAYID = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYID[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYID__AnalogSerialInput__ + i, WRITE_DISPLAYID__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYID__AnalogSerialInput__ + i, WRITE_DISPLAYID[i+1] );
    }
    
    WRITE_DISPLAYIP_ADDRESS = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYIP_ADDRESS__AnalogSerialInput__ + i, WRITE_DISPLAYIP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYIP_ADDRESS__AnalogSerialInput__ + i, WRITE_DISPLAYIP_ADDRESS[i+1] );
    }
    
    WRITE_DISPLAYLOGINNAME = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYLOGINNAME__AnalogSerialInput__ + i, WRITE_DISPLAYLOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYLOGINNAME__AnalogSerialInput__ + i, WRITE_DISPLAYLOGINNAME[i+1] );
    }
    
    WRITE_DISPLAYLOGINPASSWORD = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_DISPLAYLOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_DISPLAYLOGINPASSWORD[i+1] );
    }
    
    WRITE_DISPLAYTYPE = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYTYPE[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYTYPE__AnalogSerialInput__ + i, WRITE_DISPLAYTYPE__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYTYPE__AnalogSerialInput__ + i, WRITE_DISPLAYTYPE[i+1] );
    }
    
    WRITE_DISPLAYPROTOCOL = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYPROTOCOL[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYPROTOCOL__AnalogSerialInput__ + i, WRITE_DISPLAYPROTOCOL__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYPROTOCOL__AnalogSerialInput__ + i, WRITE_DISPLAYPROTOCOL[i+1] );
    }
    
    WRITE_DISPLAYPOWERONTIME = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYPOWERONTIME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYPOWERONTIME__AnalogSerialInput__ + i, WRITE_DISPLAYPOWERONTIME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYPOWERONTIME__AnalogSerialInput__ + i, WRITE_DISPLAYPOWERONTIME[i+1] );
    }
    
    WRITE_DISPLAYPOWEROFFTIME = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYPOWEROFFTIME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYPOWEROFFTIME__AnalogSerialInput__ + i, WRITE_DISPLAYPOWEROFFTIME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYPOWEROFFTIME__AnalogSerialInput__ + i, WRITE_DISPLAYPOWEROFFTIME[i+1] );
    }
    
    WRITE_DISPLAYIP_PORT = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYIP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYIP_PORT__AnalogSerialInput__ + i, WRITE_DISPLAYIP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYIP_PORT__AnalogSerialInput__ + i, WRITE_DISPLAYIP_PORT[i+1] );
    }
    
    WRITE_DISPLAYBAUD = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYBAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYBAUD__AnalogSerialInput__ + i, WRITE_DISPLAYBAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYBAUD__AnalogSerialInput__ + i, WRITE_DISPLAYBAUD[i+1] );
    }
    
    WRITE_DISPLAYPARITY = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYPARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYPARITY__AnalogSerialInput__ + i, WRITE_DISPLAYPARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYPARITY__AnalogSerialInput__ + i, WRITE_DISPLAYPARITY[i+1] );
    }
    
    WRITE_DISPLAYOUTPUTNUM = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYOUTPUTNUM[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYOUTPUTNUM__AnalogSerialInput__ + i, WRITE_DISPLAYOUTPUTNUM__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYOUTPUTNUM__AnalogSerialInput__ + i, WRITE_DISPLAYOUTPUTNUM[i+1] );
    }
    
    WRITE_DISPLAYSCREENUP = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYSCREENUP[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYSCREENUP__AnalogSerialInput__ + i, WRITE_DISPLAYSCREENUP__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYSCREENUP__AnalogSerialInput__ + i, WRITE_DISPLAYSCREENUP[i+1] );
    }
    
    WRITE_DISPLAYSCREENDOWN = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_DISPLAYSCREENDOWN[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_DISPLAYSCREENDOWN__AnalogSerialInput__ + i, WRITE_DISPLAYSCREENDOWN__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_DISPLAYSCREENDOWN__AnalogSerialInput__ + i, WRITE_DISPLAYSCREENDOWN[i+1] );
    }
    
    WRITE_SWITCHERENABLED = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERENABLED__AnalogSerialInput__ + i, WRITE_SWITCHERENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERENABLED__AnalogSerialInput__ + i, WRITE_SWITCHERENABLED[i+1] );
    }
    
    WRITE_SWITCHERIP_ADDRESS = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERIP_ADDRESS__AnalogSerialInput__ + i, WRITE_SWITCHERIP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERIP_ADDRESS__AnalogSerialInput__ + i, WRITE_SWITCHERIP_ADDRESS[i+1] );
    }
    
    WRITE_SWITCHERLOGINNAME = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERLOGINNAME__AnalogSerialInput__ + i, WRITE_SWITCHERLOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERLOGINNAME__AnalogSerialInput__ + i, WRITE_SWITCHERLOGINNAME[i+1] );
    }
    
    WRITE_SWITCHERLOGINPASSWORD = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_SWITCHERLOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_SWITCHERLOGINPASSWORD[i+1] );
    }
    
    WRITE_SWITCHERIP_PORT = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERIP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERIP_PORT__AnalogSerialInput__ + i, WRITE_SWITCHERIP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERIP_PORT__AnalogSerialInput__ + i, WRITE_SWITCHERIP_PORT[i+1] );
    }
    
    WRITE_SWITCHERBAUD = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERBAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERBAUD__AnalogSerialInput__ + i, WRITE_SWITCHERBAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERBAUD__AnalogSerialInput__ + i, WRITE_SWITCHERBAUD[i+1] );
    }
    
    WRITE_SWITCHERPARITY = new InOutArray<StringInput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        WRITE_SWITCHERPARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SWITCHERPARITY__AnalogSerialInput__ + i, WRITE_SWITCHERPARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SWITCHERPARITY__AnalogSerialInput__ + i, WRITE_SWITCHERPARITY[i+1] );
    }
    
    WRITE_AUDIOENABLED = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOENABLED__AnalogSerialInput__ + i, WRITE_AUDIOENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOENABLED__AnalogSerialInput__ + i, WRITE_AUDIOENABLED[i+1] );
    }
    
    WRITE_AUDIOIP_ADDRESS = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOIP_ADDRESS__AnalogSerialInput__ + i, WRITE_AUDIOIP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOIP_ADDRESS__AnalogSerialInput__ + i, WRITE_AUDIOIP_ADDRESS[i+1] );
    }
    
    WRITE_AUDIOLOGINNAME = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOLOGINNAME__AnalogSerialInput__ + i, WRITE_AUDIOLOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOLOGINNAME__AnalogSerialInput__ + i, WRITE_AUDIOLOGINNAME[i+1] );
    }
    
    WRITE_AUDIOLOGINPASSWORD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_AUDIOLOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_AUDIOLOGINPASSWORD[i+1] );
    }
    
    WRITE_AUDIOOBJECTID = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOOBJECTID[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOOBJECTID__AnalogSerialInput__ + i, WRITE_AUDIOOBJECTID__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOOBJECTID__AnalogSerialInput__ + i, WRITE_AUDIOOBJECTID[i+1] );
    }
    
    WRITE_AUDIOINSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        WRITE_AUDIOINSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOINSTANCETAGS__AnalogSerialInput__ + i, WRITE_AUDIOINSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOINSTANCETAGS__AnalogSerialInput__ + i, WRITE_AUDIOINSTANCETAGS[i+1] );
    }
    
    WRITE_AUDIOIP_PORT = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOIP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOIP_PORT__AnalogSerialInput__ + i, WRITE_AUDIOIP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOIP_PORT__AnalogSerialInput__ + i, WRITE_AUDIOIP_PORT[i+1] );
    }
    
    WRITE_AUDIOBAUD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOBAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOBAUD__AnalogSerialInput__ + i, WRITE_AUDIOBAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOBAUD__AnalogSerialInput__ + i, WRITE_AUDIOBAUD[i+1] );
    }
    
    WRITE_AUDIOPARITY = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_AUDIOPARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_AUDIOPARITY__AnalogSerialInput__ + i, WRITE_AUDIOPARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_AUDIOPARITY__AnalogSerialInput__ + i, WRITE_AUDIOPARITY[i+1] );
    }
    
    WRITE_CODECENABLED = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECENABLED__AnalogSerialInput__ + i, WRITE_CODECENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECENABLED__AnalogSerialInput__ + i, WRITE_CODECENABLED[i+1] );
    }
    
    WRITE_CODECNAME = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECNAME__AnalogSerialInput__ + i, WRITE_CODECNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECNAME__AnalogSerialInput__ + i, WRITE_CODECNAME[i+1] );
    }
    
    WRITE_CODECNAMECONTENT = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECNAMECONTENT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECNAMECONTENT__AnalogSerialInput__ + i, WRITE_CODECNAMECONTENT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECNAMECONTENT__AnalogSerialInput__ + i, WRITE_CODECNAMECONTENT[i+1] );
    }
    
    WRITE_CODECIP_ADDRESS = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECIP_ADDRESS__AnalogSerialInput__ + i, WRITE_CODECIP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECIP_ADDRESS__AnalogSerialInput__ + i, WRITE_CODECIP_ADDRESS[i+1] );
    }
    
    WRITE_CODECLOGINNAME = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECLOGINNAME__AnalogSerialInput__ + i, WRITE_CODECLOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECLOGINNAME__AnalogSerialInput__ + i, WRITE_CODECLOGINNAME[i+1] );
    }
    
    WRITE_CODECLOGINPASSWORD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_CODECLOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECLOGINPASSWORD__AnalogSerialInput__ + i, WRITE_CODECLOGINPASSWORD[i+1] );
    }
    
    WRITE_CODECIP_PORT = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECIP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECIP_PORT__AnalogSerialInput__ + i, WRITE_CODECIP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECIP_PORT__AnalogSerialInput__ + i, WRITE_CODECIP_PORT[i+1] );
    }
    
    WRITE_CODECBAUD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECBAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECBAUD__AnalogSerialInput__ + i, WRITE_CODECBAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECBAUD__AnalogSerialInput__ + i, WRITE_CODECBAUD[i+1] );
    }
    
    WRITE_CODECPARITY = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECPARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECPARITY__AnalogSerialInput__ + i, WRITE_CODECPARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECPARITY__AnalogSerialInput__ + i, WRITE_CODECPARITY[i+1] );
    }
    
    WRITE_CODECINPUTVIDEO = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECINPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECINPUTVIDEO__AnalogSerialInput__ + i, WRITE_CODECINPUTVIDEO__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECINPUTVIDEO__AnalogSerialInput__ + i, WRITE_CODECINPUTVIDEO[i+1] );
    }
    
    WRITE_CODECOUTPUTVIDEO = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECOUTPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECOUTPUTVIDEO__AnalogSerialInput__ + i, WRITE_CODECOUTPUTVIDEO__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECOUTPUTVIDEO__AnalogSerialInput__ + i, WRITE_CODECOUTPUTVIDEO[i+1] );
    }
    
    WRITE_CODECICON = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_CODECICON[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_CODECICON__AnalogSerialInput__ + i, WRITE_CODECICON__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_CODECICON__AnalogSerialInput__ + i, WRITE_CODECICON[i+1] );
    }
    
    WRITE_SOURCEENABLED = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEENABLED__AnalogSerialInput__ + i, WRITE_SOURCEENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEENABLED__AnalogSerialInput__ + i, WRITE_SOURCEENABLED[i+1] );
    }
    
    WRITE_SOURCENAME = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCENAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCENAME__AnalogSerialInput__ + i, WRITE_SOURCENAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCENAME__AnalogSerialInput__ + i, WRITE_SOURCENAME[i+1] );
    }
    
    WRITE_SOURCEIP_ADDRESS = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEIP_ADDRESS__AnalogSerialInput__ + i, WRITE_SOURCEIP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEIP_ADDRESS__AnalogSerialInput__ + i, WRITE_SOURCEIP_ADDRESS[i+1] );
    }
    
    WRITE_SOURCELOGINNAME = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCELOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCELOGINNAME__AnalogSerialInput__ + i, WRITE_SOURCELOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCELOGINNAME__AnalogSerialInput__ + i, WRITE_SOURCELOGINNAME[i+1] );
    }
    
    WRITE_SOURCELOGINPASSWORD = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCELOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCELOGINPASSWORD__AnalogSerialInput__ + i, WRITE_SOURCELOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCELOGINPASSWORD__AnalogSerialInput__ + i, WRITE_SOURCELOGINPASSWORD[i+1] );
    }
    
    WRITE_SOURCEIP_PORT = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEIP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEIP_PORT__AnalogSerialInput__ + i, WRITE_SOURCEIP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEIP_PORT__AnalogSerialInput__ + i, WRITE_SOURCEIP_PORT[i+1] );
    }
    
    WRITE_SOURCEBAUD = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEBAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEBAUD__AnalogSerialInput__ + i, WRITE_SOURCEBAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEBAUD__AnalogSerialInput__ + i, WRITE_SOURCEBAUD[i+1] );
    }
    
    WRITE_SOURCEPARITY = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEPARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEPARITY__AnalogSerialInput__ + i, WRITE_SOURCEPARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEPARITY__AnalogSerialInput__ + i, WRITE_SOURCEPARITY[i+1] );
    }
    
    WRITE_SOURCEINPUTVIDEO = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEINPUTVIDEO[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEINPUTVIDEO__AnalogSerialInput__ + i, WRITE_SOURCEINPUTVIDEO__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEINPUTVIDEO__AnalogSerialInput__ + i, WRITE_SOURCEINPUTVIDEO[i+1] );
    }
    
    WRITE_SOURCEICON = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEICON[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEICON__AnalogSerialInput__ + i, WRITE_SOURCEICON__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEICON__AnalogSerialInput__ + i, WRITE_SOURCEICON[i+1] );
    }
    
    WRITE_SOURCEUSESNOTOUCH = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_SOURCEUSESNOTOUCH[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_SOURCEUSESNOTOUCH__AnalogSerialInput__ + i, WRITE_SOURCEUSESNOTOUCH__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_SOURCEUSESNOTOUCH__AnalogSerialInput__ + i, WRITE_SOURCEUSESNOTOUCH[i+1] );
    }
    
    WRITE_ENV_ENABLED = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_ENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_ENABLED__AnalogSerialInput__ + i, WRITE_ENV_ENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_ENABLED__AnalogSerialInput__ + i, WRITE_ENV_ENABLED[i+1] );
    }
    
    WRITE_ENV_NAME = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_NAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_NAME__AnalogSerialInput__ + i, WRITE_ENV_NAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_NAME__AnalogSerialInput__ + i, WRITE_ENV_NAME[i+1] );
    }
    
    WRITE_ENV_IP_ADDRESS = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_IP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_IP_ADDRESS__AnalogSerialInput__ + i, WRITE_ENV_IP_ADDRESS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_IP_ADDRESS__AnalogSerialInput__ + i, WRITE_ENV_IP_ADDRESS[i+1] );
    }
    
    WRITE_ENV_LOGINNAME = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_LOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_LOGINNAME__AnalogSerialInput__ + i, WRITE_ENV_LOGINNAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_LOGINNAME__AnalogSerialInput__ + i, WRITE_ENV_LOGINNAME[i+1] );
    }
    
    WRITE_ENV_LOGINPASSWORD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_LOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_LOGINPASSWORD__AnalogSerialInput__ + i, WRITE_ENV_LOGINPASSWORD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_LOGINPASSWORD__AnalogSerialInput__ + i, WRITE_ENV_LOGINPASSWORD[i+1] );
    }
    
    WRITE_ENV_IP_PORT = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_IP_PORT[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_IP_PORT__AnalogSerialInput__ + i, WRITE_ENV_IP_PORT__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_IP_PORT__AnalogSerialInput__ + i, WRITE_ENV_IP_PORT[i+1] );
    }
    
    WRITE_ENV_BAUD = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_BAUD[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_BAUD__AnalogSerialInput__ + i, WRITE_ENV_BAUD__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_BAUD__AnalogSerialInput__ + i, WRITE_ENV_BAUD[i+1] );
    }
    
    WRITE_ENV_PARITY = new InOutArray<StringInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        WRITE_ENV_PARITY[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_ENV_PARITY__AnalogSerialInput__ + i, WRITE_ENV_PARITY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_ENV_PARITY__AnalogSerialInput__ + i, WRITE_ENV_PARITY[i+1] );
    }
    
    WRITE_MISCENABLED = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_MISCENABLED[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_MISCENABLED__AnalogSerialInput__ + i, WRITE_MISCENABLED__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_MISCENABLED__AnalogSerialInput__ + i, WRITE_MISCENABLED[i+1] );
    }
    
    WRITE_MISCSTRINGS = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_MISCSTRINGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_MISCSTRINGS__AnalogSerialInput__ + i, WRITE_MISCSTRINGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_MISCSTRINGS__AnalogSerialInput__ + i, WRITE_MISCSTRINGS[i+1] );
    }
    
    WRITE_MISCANALOGS = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        WRITE_MISCANALOGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( WRITE_MISCANALOGS__AnalogSerialInput__ + i, WRITE_MISCANALOGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( WRITE_MISCANALOGS__AnalogSerialInput__ + i, WRITE_MISCANALOGS[i+1] );
    }
    
    ROOMNAME = new Crestron.Logos.SplusObjects.StringOutput( ROOMNAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMNAME__AnalogSerialOutput__, ROOMNAME );
    
    ROOMPHONENUMBER = new Crestron.Logos.SplusObjects.StringOutput( ROOMPHONENUMBER__AnalogSerialOutput__, this );
    m_StringOutputList.Add( ROOMPHONENUMBER__AnalogSerialOutput__, ROOMPHONENUMBER );
    
    DISPLAYNAME = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( DISPLAYNAME__AnalogSerialOutput__ + i, DISPLAYNAME[i+1] );
    }
    
    DISPLAYID = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYID[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYID__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( DISPLAYID__AnalogSerialOutput__ + i, DISPLAYID[i+1] );
    }
    
    DISPLAYIP_ADDRESS = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYIP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( DISPLAYIP_ADDRESS__AnalogSerialOutput__ + i, DISPLAYIP_ADDRESS[i+1] );
    }
    
    DISPLAYLOGINNAME = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYLOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( DISPLAYLOGINNAME__AnalogSerialOutput__ + i, DISPLAYLOGINNAME[i+1] );
    }
    
    DISPLAYLOGINPASSWORD = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        DISPLAYLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( DISPLAYLOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( DISPLAYLOGINPASSWORD__AnalogSerialOutput__ + i, DISPLAYLOGINPASSWORD[i+1] );
    }
    
    SWITCHERIP_ADDRESS = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SWITCHERIP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SWITCHERIP_ADDRESS__AnalogSerialOutput__ + i, SWITCHERIP_ADDRESS[i+1] );
    }
    
    SWITCHERLOGINNAME = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SWITCHERLOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SWITCHERLOGINNAME__AnalogSerialOutput__ + i, SWITCHERLOGINNAME[i+1] );
    }
    
    SWITCHERLOGINPASSWORD = new InOutArray<StringOutput>( 8, this );
    for( uint i = 0; i < 8; i++ )
    {
        SWITCHERLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SWITCHERLOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SWITCHERLOGINPASSWORD__AnalogSerialOutput__ + i, SWITCHERLOGINPASSWORD[i+1] );
    }
    
    AUDIOIP_ADDRESS = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIOIP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIOIP_ADDRESS__AnalogSerialOutput__ + i, AUDIOIP_ADDRESS[i+1] );
    }
    
    AUDIOLOGINNAME = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIOLOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIOLOGINNAME__AnalogSerialOutput__ + i, AUDIOLOGINNAME[i+1] );
    }
    
    AUDIOLOGINPASSWORD = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIOLOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIOLOGINPASSWORD__AnalogSerialOutput__ + i, AUDIOLOGINPASSWORD[i+1] );
    }
    
    AUDIOOBJECTID = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        AUDIOOBJECTID[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIOOBJECTID__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIOOBJECTID__AnalogSerialOutput__ + i, AUDIOOBJECTID[i+1] );
    }
    
    AUDIOINSTANCETAGS = new InOutArray<StringOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        AUDIOINSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIOINSTANCETAGS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIOINSTANCETAGS__AnalogSerialOutput__ + i, AUDIOINSTANCETAGS[i+1] );
    }
    
    CODECNAME = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CODECNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CODECNAME__AnalogSerialOutput__ + i, CODECNAME[i+1] );
    }
    
    CODECNAMECONTENT = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECNAMECONTENT[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CODECNAMECONTENT__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CODECNAMECONTENT__AnalogSerialOutput__ + i, CODECNAMECONTENT[i+1] );
    }
    
    CODECIP_ADDRESS = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CODECIP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CODECIP_ADDRESS__AnalogSerialOutput__ + i, CODECIP_ADDRESS[i+1] );
    }
    
    CODECLOGINNAME = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECLOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CODECLOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CODECLOGINNAME__AnalogSerialOutput__ + i, CODECLOGINNAME[i+1] );
    }
    
    CODECLOGINPASSWORD = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        CODECLOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( CODECLOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( CODECLOGINPASSWORD__AnalogSerialOutput__ + i, CODECLOGINPASSWORD[i+1] );
    }
    
    SOURCENAME = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCENAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCENAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SOURCENAME__AnalogSerialOutput__ + i, SOURCENAME[i+1] );
    }
    
    SOURCEIP_ADDRESS = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEIP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCEIP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SOURCEIP_ADDRESS__AnalogSerialOutput__ + i, SOURCEIP_ADDRESS[i+1] );
    }
    
    SOURCELOGINNAME = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCELOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCELOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SOURCELOGINNAME__AnalogSerialOutput__ + i, SOURCELOGINNAME[i+1] );
    }
    
    SOURCELOGINPASSWORD = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCELOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( SOURCELOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( SOURCELOGINPASSWORD__AnalogSerialOutput__ + i, SOURCELOGINPASSWORD[i+1] );
    }
    
    ENV_NAME = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( ENV_NAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( ENV_NAME__AnalogSerialOutput__ + i, ENV_NAME[i+1] );
    }
    
    ENV_IP_ADDRESS = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_IP_ADDRESS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( ENV_IP_ADDRESS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( ENV_IP_ADDRESS__AnalogSerialOutput__ + i, ENV_IP_ADDRESS[i+1] );
    }
    
    ENV_LOGINNAME = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_LOGINNAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( ENV_LOGINNAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( ENV_LOGINNAME__AnalogSerialOutput__ + i, ENV_LOGINNAME[i+1] );
    }
    
    ENV_LOGINPASSWORD = new InOutArray<StringOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        ENV_LOGINPASSWORD[i+1] = new Crestron.Logos.SplusObjects.StringOutput( ENV_LOGINPASSWORD__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( ENV_LOGINPASSWORD__AnalogSerialOutput__ + i, ENV_LOGINPASSWORD[i+1] );
    }
    
    MISCSTRINGS = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        MISCSTRINGS[i+1] = new Crestron.Logos.SplusObjects.StringOutput( MISCSTRINGS__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( MISCSTRINGS__AnalogSerialOutput__ + i, MISCSTRINGS[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_12___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_12___CallbackFn );
    
    SYSTEMSTART.OnDigitalChange.Add( new InputChangeHandlerWrapper( SYSTEMSTART_OnChange_0, false ) );
    SAVECONFIG.OnDigitalPush.Add( new InputChangeHandlerWrapper( SAVECONFIG_OnPush_1, false ) );
    FILENAME.OnSerialChange.Add( new InputChangeHandlerWrapper( FILENAME_OnChange_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_READ_CONFIG_V1_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_12___Callback;


const uint SYSTEMSTART__DigitalInput__ = 0;
const uint SAVECONFIG__DigitalInput__ = 1;
const uint FILENAME__AnalogSerialInput__ = 0;
const uint WRITE_ROOMNAME__AnalogSerialInput__ = 1;
const uint WRITE_ROOMPHONENUMBER__AnalogSerialInput__ = 2;
const uint WRITE_ROOMNOTOUCHOFFENABLE__AnalogSerialInput__ = 3;
const uint WRITE_ROOMNOTOUCHONENABLE__AnalogSerialInput__ = 4;
const uint WRITE_ROOMNOTOUCHPOWEROFF__AnalogSerialInput__ = 5;
const uint WRITE_ROOMROUTINGMODE__AnalogSerialInput__ = 6;
const uint WRITE_DISPLAYENABLED__AnalogSerialInput__ = 7;
const uint WRITE_DISPLAYNAME__AnalogSerialInput__ = 15;
const uint WRITE_DISPLAYID__AnalogSerialInput__ = 23;
const uint WRITE_DISPLAYIP_ADDRESS__AnalogSerialInput__ = 31;
const uint WRITE_DISPLAYLOGINNAME__AnalogSerialInput__ = 39;
const uint WRITE_DISPLAYLOGINPASSWORD__AnalogSerialInput__ = 47;
const uint WRITE_DISPLAYTYPE__AnalogSerialInput__ = 55;
const uint WRITE_DISPLAYPROTOCOL__AnalogSerialInput__ = 63;
const uint WRITE_DISPLAYPOWERONTIME__AnalogSerialInput__ = 71;
const uint WRITE_DISPLAYPOWEROFFTIME__AnalogSerialInput__ = 79;
const uint WRITE_DISPLAYIP_PORT__AnalogSerialInput__ = 87;
const uint WRITE_DISPLAYBAUD__AnalogSerialInput__ = 95;
const uint WRITE_DISPLAYPARITY__AnalogSerialInput__ = 103;
const uint WRITE_DISPLAYOUTPUTNUM__AnalogSerialInput__ = 111;
const uint WRITE_DISPLAYSCREENUP__AnalogSerialInput__ = 119;
const uint WRITE_DISPLAYSCREENDOWN__AnalogSerialInput__ = 127;
const uint WRITE_SWITCHERENABLED__AnalogSerialInput__ = 135;
const uint WRITE_SWITCHERIP_ADDRESS__AnalogSerialInput__ = 143;
const uint WRITE_SWITCHERLOGINNAME__AnalogSerialInput__ = 151;
const uint WRITE_SWITCHERLOGINPASSWORD__AnalogSerialInput__ = 159;
const uint WRITE_SWITCHERIP_PORT__AnalogSerialInput__ = 167;
const uint WRITE_SWITCHERBAUD__AnalogSerialInput__ = 175;
const uint WRITE_SWITCHERPARITY__AnalogSerialInput__ = 183;
const uint WRITE_AUDIOENABLED__AnalogSerialInput__ = 191;
const uint WRITE_AUDIOIP_ADDRESS__AnalogSerialInput__ = 195;
const uint WRITE_AUDIOLOGINNAME__AnalogSerialInput__ = 199;
const uint WRITE_AUDIOLOGINPASSWORD__AnalogSerialInput__ = 203;
const uint WRITE_AUDIOOBJECTID__AnalogSerialInput__ = 207;
const uint WRITE_AUDIOINSTANCETAGS__AnalogSerialInput__ = 211;
const uint WRITE_AUDIOIP_PORT__AnalogSerialInput__ = 275;
const uint WRITE_AUDIOBAUD__AnalogSerialInput__ = 279;
const uint WRITE_AUDIOPARITY__AnalogSerialInput__ = 283;
const uint WRITE_CODECENABLED__AnalogSerialInput__ = 287;
const uint WRITE_CODECNAME__AnalogSerialInput__ = 291;
const uint WRITE_CODECNAMECONTENT__AnalogSerialInput__ = 295;
const uint WRITE_CODECIP_ADDRESS__AnalogSerialInput__ = 299;
const uint WRITE_CODECLOGINNAME__AnalogSerialInput__ = 303;
const uint WRITE_CODECLOGINPASSWORD__AnalogSerialInput__ = 307;
const uint WRITE_CODECIP_PORT__AnalogSerialInput__ = 311;
const uint WRITE_CODECBAUD__AnalogSerialInput__ = 315;
const uint WRITE_CODECPARITY__AnalogSerialInput__ = 319;
const uint WRITE_CODECINPUTVIDEO__AnalogSerialInput__ = 323;
const uint WRITE_CODECOUTPUTVIDEO__AnalogSerialInput__ = 327;
const uint WRITE_CODECICON__AnalogSerialInput__ = 331;
const uint WRITE_SOURCEENABLED__AnalogSerialInput__ = 335;
const uint WRITE_SOURCENAME__AnalogSerialInput__ = 351;
const uint WRITE_SOURCEIP_ADDRESS__AnalogSerialInput__ = 367;
const uint WRITE_SOURCELOGINNAME__AnalogSerialInput__ = 383;
const uint WRITE_SOURCELOGINPASSWORD__AnalogSerialInput__ = 399;
const uint WRITE_SOURCEIP_PORT__AnalogSerialInput__ = 415;
const uint WRITE_SOURCEBAUD__AnalogSerialInput__ = 431;
const uint WRITE_SOURCEPARITY__AnalogSerialInput__ = 447;
const uint WRITE_SOURCEINPUTVIDEO__AnalogSerialInput__ = 463;
const uint WRITE_SOURCEICON__AnalogSerialInput__ = 479;
const uint WRITE_SOURCEUSESNOTOUCH__AnalogSerialInput__ = 495;
const uint WRITE_ENV_ENABLED__AnalogSerialInput__ = 511;
const uint WRITE_ENV_NAME__AnalogSerialInput__ = 515;
const uint WRITE_ENV_IP_ADDRESS__AnalogSerialInput__ = 519;
const uint WRITE_ENV_LOGINNAME__AnalogSerialInput__ = 523;
const uint WRITE_ENV_LOGINPASSWORD__AnalogSerialInput__ = 527;
const uint WRITE_ENV_IP_PORT__AnalogSerialInput__ = 531;
const uint WRITE_ENV_BAUD__AnalogSerialInput__ = 535;
const uint WRITE_ENV_PARITY__AnalogSerialInput__ = 539;
const uint WRITE_MISCENABLED__AnalogSerialInput__ = 543;
const uint WRITE_MISCSTRINGS__AnalogSerialInput__ = 559;
const uint WRITE_MISCANALOGS__AnalogSerialInput__ = 575;
const uint READCOMPLETE_FB__DigitalOutput__ = 0;
const uint WRITECOMPLETE_FB__DigitalOutput__ = 1;
const uint ROOMNOTOUCHOFFENABLE__DigitalOutput__ = 2;
const uint ROOMNOTOUCHONENABLE__DigitalOutput__ = 3;
const uint ROOMNAME__AnalogSerialOutput__ = 0;
const uint ROOMPHONENUMBER__AnalogSerialOutput__ = 1;
const uint ROOMNOTOUCHPOWEROFF__AnalogSerialOutput__ = 2;
const uint DISPLAYNUMBER__AnalogSerialOutput__ = 3;
const uint ROOMROUTINGMODE__AnalogSerialOutput__ = 4;
const uint DISPLAYNAME__AnalogSerialOutput__ = 5;
const uint DISPLAYID__AnalogSerialOutput__ = 13;
const uint DISPLAYIP_ADDRESS__AnalogSerialOutput__ = 21;
const uint DISPLAYLOGINNAME__AnalogSerialOutput__ = 29;
const uint DISPLAYLOGINPASSWORD__AnalogSerialOutput__ = 37;
const uint DISPLAYTYPE__AnalogSerialOutput__ = 45;
const uint DISPLAYPROTOCOL__AnalogSerialOutput__ = 53;
const uint DISPLAYPOWERONTIME__AnalogSerialOutput__ = 61;
const uint DISPLAYPOWEROFFTIME__AnalogSerialOutput__ = 69;
const uint DISPLAYIP_PORT__AnalogSerialOutput__ = 77;
const uint DISPLAYBAUD__AnalogSerialOutput__ = 85;
const uint DISPLAYPARITY__AnalogSerialOutput__ = 93;
const uint DISPLAYOUTPUTNUM__AnalogSerialOutput__ = 101;
const uint DISPLAYSCREENUP__AnalogSerialOutput__ = 109;
const uint DISPLAYSCREENDOWN__AnalogSerialOutput__ = 117;
const uint DISPLAYIPCONTROL__DigitalOutput__ = 4;
const uint DISPLAYUSED__DigitalOutput__ = 12;
const uint SWITCHERIP_ADDRESS__AnalogSerialOutput__ = 125;
const uint SWITCHERLOGINNAME__AnalogSerialOutput__ = 133;
const uint SWITCHERLOGINPASSWORD__AnalogSerialOutput__ = 141;
const uint SWITCHERIP_PORT__AnalogSerialOutput__ = 149;
const uint SWITCHERBAUD__AnalogSerialOutput__ = 157;
const uint SWITCHERPARITY__AnalogSerialOutput__ = 165;
const uint SWITCHERIPCONTROL__DigitalOutput__ = 20;
const uint SWITCHERUSED__DigitalOutput__ = 28;
const uint AUDIOIP_ADDRESS__AnalogSerialOutput__ = 173;
const uint AUDIOLOGINNAME__AnalogSerialOutput__ = 177;
const uint AUDIOLOGINPASSWORD__AnalogSerialOutput__ = 181;
const uint AUDIOOBJECTID__AnalogSerialOutput__ = 185;
const uint AUDIOINSTANCETAGS__AnalogSerialOutput__ = 189;
const uint AUDIOIP_PORT__AnalogSerialOutput__ = 253;
const uint AUDIOBAUD__AnalogSerialOutput__ = 257;
const uint AUDIOPARITY__AnalogSerialOutput__ = 261;
const uint AUDIOIPCONTROL__DigitalOutput__ = 36;
const uint AUDIOUSED__DigitalOutput__ = 40;
const uint CODECNAME__AnalogSerialOutput__ = 265;
const uint CODECNAMECONTENT__AnalogSerialOutput__ = 269;
const uint CODECIP_ADDRESS__AnalogSerialOutput__ = 273;
const uint CODECLOGINNAME__AnalogSerialOutput__ = 277;
const uint CODECLOGINPASSWORD__AnalogSerialOutput__ = 281;
const uint CODECIP_PORT__AnalogSerialOutput__ = 285;
const uint CODECBAUD__AnalogSerialOutput__ = 289;
const uint CODECPARITY__AnalogSerialOutput__ = 293;
const uint CODECINPUTVIDEO__AnalogSerialOutput__ = 297;
const uint CODECOUTPUTVIDEO__AnalogSerialOutput__ = 301;
const uint CODECICON__AnalogSerialOutput__ = 305;
const uint CODECIPCONTROL__DigitalOutput__ = 44;
const uint CODECUSED__DigitalOutput__ = 48;
const uint SOURCENAME__AnalogSerialOutput__ = 309;
const uint SOURCEIP_ADDRESS__AnalogSerialOutput__ = 325;
const uint SOURCELOGINNAME__AnalogSerialOutput__ = 341;
const uint SOURCELOGINPASSWORD__AnalogSerialOutput__ = 357;
const uint SOURCEIP_PORT__AnalogSerialOutput__ = 373;
const uint SOURCEBAUD__AnalogSerialOutput__ = 389;
const uint SOURCEPARITY__AnalogSerialOutput__ = 405;
const uint SOURCEINPUTVIDEO__AnalogSerialOutput__ = 421;
const uint SOURCEICON__AnalogSerialOutput__ = 437;
const uint SOURCEIPCONTROL__DigitalOutput__ = 52;
const uint SOURCEUSESNOTOUCH__DigitalOutput__ = 68;
const uint SOURCEUSED__DigitalOutput__ = 84;
const uint ENV_NAME__AnalogSerialOutput__ = 453;
const uint ENV_IP_ADDRESS__AnalogSerialOutput__ = 457;
const uint ENV_LOGINNAME__AnalogSerialOutput__ = 461;
const uint ENV_LOGINPASSWORD__AnalogSerialOutput__ = 465;
const uint ENV_IP_PORT__AnalogSerialOutput__ = 469;
const uint ENV_BAUD__AnalogSerialOutput__ = 473;
const uint ENV_PARITY__AnalogSerialOutput__ = 477;
const uint ENV_IPCONTROL__DigitalOutput__ = 100;
const uint ENV_USED__DigitalOutput__ = 104;
const uint MISCSTRINGS__AnalogSerialOutput__ = 481;
const uint MISCANALOGS__AnalogSerialOutput__ = 497;
const uint MISCUSED__DigitalOutput__ = 108;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SDEVICE : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  USINGIP = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  USED = 0;
    
    
    public SDEVICE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        
        
    }
    
}

}
