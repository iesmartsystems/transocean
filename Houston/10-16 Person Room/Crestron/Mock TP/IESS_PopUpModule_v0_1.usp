/*
Dealer Name: i.e.SmartSytems
System Name: IESS PopUp Module v0.1
System Number:
Programmer: Matthew Laletas
Comments: IESS PopUp Module v0.1
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_POPUP 16
#HELP "IESS - PopUp Module v0.1"
#HELP ""
#HELP "INPUTS:"
#HELP "Working on it"
#HELP ""
#HELP "OUTPUTS:"
#HELP "Working on it"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT PopUpInput[NUM_POPUP];
DIGITAL_OUTPUT PopUpOutput[NUM_POPUP];
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER gvPopUpQueue[NUM_POPUP];
/*******************************************************************************************
  Functions
*******************************************************************************************/
INTEGER_FUNCTION CheckQueueInputs( INTEGER lvInput )			//Returns whether or not the input is already in the queue, prevents duplicates
{
    INTEGER lvCounter, lvActive;
    FOR( lvCounter = 1 TO NUM_POPUP )								//Scroll through sources
    {
		IF( lvInput = gvPopUpQueue[lvCounter] )						//If input in the slot
	    	lvActive = 1;													//Already active this will be returned
    }
    IF( lvActive )													//If something was active
		RETURN ( lvActive );											//Return true
    ELSE															//Nothing was active
		RETURN ( 0 );													//Return nothing
}
FUNCTION FormatInputQueue( INTEGER lvInput, INTEGER lvActive )	//Moves around inputs in the queue respective to if the input is active or not
{
    INTEGER lvQueue, lvCounter;
    IF( lvActive && !CheckQueueInputs( lvInput ) )					//Active input that doesnt already occupy the queue
    {
		FOR( lvCounter = NUM_POPUP TO 2 STEP - 1 )						//Scroll through sources backwards up to the 2nd 
		{
	    	gvPopUpQueue[lvCounter] = gvPopUpQueue[lvCounter - 1];	//Move up the previous to the current (remember backwards)
		}
		gvPopUpQueue[1] = lvInput;									//Set input to the 1st
    }
    ELSE IF( !lvActive )											//Inactive
    {
		FOR( lvCounter = 1 TO NUM_POPUP )								//Scroll through sources normally
		{
	    	IF( gvPopUpQueue[lvCounter] = lvInput )						//If this input is in the queue
				gvPopUpQueue[lvCounter] = 0;									//Remove it
		}
		FOR( lvCounter = 1 TO ( NUM_POPUP - 1 ) )						//Scroll through sources ignore the last
		{ 
	   		IF( !gvPopUpQueue[lvCounter] )								//If the slot is empty
	    	{
				gvPopUpQueue[lvCounter] = gvPopUpQueue[lvCounter + 1];	//Move the next slot to the current
				gvPopUpQueue[lvCounter + 1] = 0;								//Next slot make 0
	    	}
		}
    }
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
CHANGE PopUpInput
{
	INTEGER lvIndex, lvCounter;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	FormatInputQueue( lvIndex, PopUpInput[lvIndex] );
    FOR( lvCounter = 1 TO NUM_POPUP )
	{
		IF( lvCounter != gvPopUpQueue[1] )
			PopUpOutput[lvCounter] = 0;	
	}
	IF( gvPopUpQueue[1] )
		PopUpOutput[gvPopUpQueue[1]] = 1;
}
/*******************************************************************************************
  Main()
*******************************************************************************************/