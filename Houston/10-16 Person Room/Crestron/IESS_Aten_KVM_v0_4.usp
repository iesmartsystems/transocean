/*
Dealer Name: i.e.SmartSytems
System Name: Aten KVM CS192#M RS-232
System Number:
Programmer: Jesus BArrera
Comments: Integrates with Aten KVM
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
// #ENABLE_DYNAMIC
// #SYMBOL_NAME ""
// #HINT ""
// #DEFINE_CONSTANT
// #CATEGORY "" 
// #PRINT_TO_TRACE
// #DIGITAL_EXPAND 
// #ANALOG_SERIAL_EXPAND 
// #OUTPUT_SHIFT 
// #HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
// #ENCODING_ASCII
// #ENCODING_UTF16
// #ENCODING_INHERIT_FROM_PARENT
// #ENCODING_INHERIT_FROM_PROGRAM
/*
#HELP_BEGIN
   (add additional lines of help lines)
#HELP_END
*/

/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
// #USER_LIBRARY ""

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT Connect, _SKIP_;
ANALOG_INPUT KVM_Input, KVM_Mode, _SKIP_;
STRING_INPUT KVM_RX$[255];
DIGITAL_OUTPUT Status_Connect, _SKIP_;
ANALOG_OUTPUT Status_Input, _SKIP_;
STRING_OUTPUT KVM_TX$;

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
STRUCTURE sKVM
{
	STRING	Heartbeat[15];
	INTEGER	Timer;
	STRING	Open[7];
	STRING	CR[4];
	STRING	ResetUSB[15];	 	
};

STRUCTURE sLocalKVM
{
	STRING cRXQueue[1000];	
};

sKVM KVM_Command;
sLocalKVM KVM_RX_Command;

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
STRING SocketBuff[255];													//Buffer for storing strings received from device
INTEGER GLBL_KVM_Status;

/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/
FUNCTION SendString (String lvIncoming)
{
 	KVM_TX$ = lvIncoming + KVM_Command.CR;
	WAIT( 300, ConnectionUpdate )
	{
		GLBL_KVM_Status = 0;
		Status_Connect = GLBL_KVM_Status;
	}		
}
FUNCTION StartHeartbeat()
{
	WAIT( KVM_Command.Timer, Heartbeat)
	{
		IF( GLBL_KVM_Status )
            SendString( KVM_Command.Heartbeat );
		ELSE
            SendString( KVM_Command.Open );
		StartHeartbeat();
	}
}
FUNCTION ResetUSB()
{
	WAIT( 300 )
		SendString( KVM_Command.ResetUSB );
}
/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
PUSH Connect
{
	StartHeartbeat();
}
CHANGE KVM_Input      			//sw i##
{
	INTEGER lvIndex;
	STRING lvString[15];
	lvIndex = KVM_Input;
	MAKESTRING(lvString, "sw i0%u", lvIndex);
	SendString( lvString );	
}
CHANGE KVM_Mode
{
	INTEGER lvIndex;
	STRING lvString[15];
	lvIndex = KVM_Mode;
	SWITCH( KVM_Mode )
	{
    	CASE(1): MAKESTRING( lvString, "display sst");
    	CASE(2): MAKESTRING( lvString, "display mst");
	}
	SendString( lvString );
}
CHANGE KVM_RX$
{
	STRING lvString[100], lvRX[100], lvTrash[100];
	INTEGER lvCounter, lvIndex, lvLevel;
	IF( LEN(KVM_RX$) )
		CANCELWAIT( ConnectionUpdate );
	KVM_RX_Command.cRXQueue = KVM_RX_Command.cRXQueue + KVM_RX$;
	WHILE( FIND("\x0D", KVM_RX_Command.cRXQueue ) )
	{
		lvRX = REMOVE ( "\x0D", KVM_RX_Command.cRXQueue );
		lvRX = REMOVEBYLENGTH ( LEN( lvRX ) -1, lvRX );
		IF( FIND( "Welcome", lvRX ) )
		{
			GLBL_KVM_Status = 1;
			Status_Connect = GLBL_KVM_Status;
			StartHeartbeat();
		}
		ELSE IF( FIND( "Please Open RS232 Function!", lvRX ) )
		{
			GLBL_KVM_Status = 0;
			Status_Connect = GLBL_KVM_Status;
			SendString( KVM_Command.Open );
		}
		ELSE IF( FIND( "Command OK", lvRX ) )
		{
			IF( FIND( "sw i01", lvRX ) )
			{
				Status_Input = 1;
				ResetUSB();
			}
			ELSE IF( FIND( "sw i02", lvRX ) )
			{
				Status_Input = 2;
				ResetUSB();
			}
		}
	}
}
/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/

Function Main()
{
	KVM_Command.Open			=	"open";
	KVM_Command.CR				=	"\x0D\x0A";
	KVM_Command.Heartbeat		=	"status on";
	KVM_Command.ResetUSB		=	"usbreset on";
	KVM_Command.Timer			= 	6000;
}


