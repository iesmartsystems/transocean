using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_NOTOUCH_V2_1
{
    public class UserModuleClass_IESS_NOTOUCH_V2_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput POWEROFFALL;
        Crestron.Logos.SplusObjects.DigitalInput ENABLENOTOUCH_ON;
        Crestron.Logos.SplusObjects.DigitalInput ENABLENOTOUCH_OFF;
        Crestron.Logos.SplusObjects.DigitalInput SYSTEMSTART;
        Crestron.Logos.SplusObjects.AnalogInput POWER_OFF_TIME;
        Crestron.Logos.SplusObjects.AnalogInput NUMBER_OF_DISPLAYS;
        Crestron.Logos.SplusObjects.StringInput FILENAME;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SOURCE_SYNC;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> SOURCE_NOTOUCH_ENABLED;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> DISPLAY_ROUTE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SOURCE_NAME;
        Crestron.Logos.SplusObjects.DigitalOutput NOTOUCHON_PULSE;
        Crestron.Logos.SplusObjects.DigitalOutput NOTOUCHOFF_PULSE;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ROUTE_SWITCHER;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ROUTE_DISPLAY;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> ROUTED_SOURCE_NAME;
        SNOTOUCH GVNOTOUCH;
        SDISPLAY [] GVDISPLAYS;
        ushort GVSTARTREAD = 0;
        private ushort CHECKSOURCESYNC (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 108;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 110;
                if ( Functions.TestForTrue  ( ( GVNOTOUCH.SOURCENOTOUCHENABLE[ LVCOUNTER ])  ) ) 
                    { 
                    __context__.SourceCodeLine = 112;
                    if ( Functions.TestForTrue  ( ( GVNOTOUCH.SOURCESYNC[ LVCOUNTER ])  ) ) 
                        {
                        __context__.SourceCodeLine = 113;
                        return (ushort)( 0) ; 
                        }
                    
                    } 
                
                __context__.SourceCodeLine = 108;
                } 
            
            __context__.SourceCodeLine = 116;
            return (ushort)( 1) ; 
            
            }
            
        private ushort CHECKAUTOOFF (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVCHECKDISPLAYS = 0;
            ushort LVCHECKDISPLAYSNOTOUCH = 0;
            
            
            __context__.SourceCodeLine = 121;
            LVCHECKDISPLAYS = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 122;
            LVCHECKDISPLAYSNOTOUCH = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 123;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)GVNOTOUCH.NUMBEROFDISPLAYS; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 125;
                if ( Functions.TestForTrue  ( ( ROUTE_SWITCHER[ LVCOUNTER ] .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 127;
                    LVCHECKDISPLAYS = (ushort) ( (LVCHECKDISPLAYS + 1) ) ; 
                    __context__.SourceCodeLine = 128;
                    if ( Functions.TestForTrue  ( ( GVNOTOUCH.SOURCENOTOUCHENABLE[ ROUTE_SWITCHER[ LVCOUNTER ] .Value ])  ) ) 
                        { 
                        __context__.SourceCodeLine = 130;
                        LVCHECKDISPLAYSNOTOUCH = (ushort) ( (LVCHECKDISPLAYSNOTOUCH + 1) ) ; 
                        __context__.SourceCodeLine = 131;
                        if ( Functions.TestForTrue  ( ( GVNOTOUCH.SOURCESYNC[ ROUTE_SWITCHER[ LVCOUNTER ] .Value ])  ) ) 
                            {
                            __context__.SourceCodeLine = 132;
                            return (ushort)( 0) ; 
                            }
                        
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 123;
                } 
            
            __context__.SourceCodeLine = 136;
            if ( Functions.TestForTrue  ( ( LVCHECKDISPLAYS)  ) ) 
                { 
                __context__.SourceCodeLine = 138;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCHECKDISPLAYSNOTOUCH == GVNOTOUCH.NUMBEROFDISPLAYS))  ) ) 
                    {
                    __context__.SourceCodeLine = 139;
                    return (ushort)( 1) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 140;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCHECKDISPLAYSNOTOUCH < GVNOTOUCH.NUMBEROFDISPLAYS ))  ) ) 
                        {
                        __context__.SourceCodeLine = 141;
                        return (ushort)( 0) ; 
                        }
                    
                    }
                
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void SETOUTPUTS (  SplusExecutionContext __context__, ushort LVINDEX , ushort LVOUTPUT ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 147;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == 0))  ) ) 
                { 
                __context__.SourceCodeLine = 149;
                ROUTE_DISPLAY [ LVOUTPUT]  .Value = (ushort) ( 99 ) ; 
                __context__.SourceCodeLine = 150;
                ROUTE_SWITCHER [ LVOUTPUT]  .Value = (ushort) ( LVINDEX ) ; 
                __context__.SourceCodeLine = 151;
                ROUTED_SOURCE_NAME [ LVOUTPUT]  .UpdateValue ( ""  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 155;
                ROUTE_DISPLAY [ LVOUTPUT]  .Value = (ushort) ( GVDISPLAYS[ LVOUTPUT ].SOURCEINPUT[ LVINDEX ] ) ; 
                __context__.SourceCodeLine = 156;
                ROUTE_SWITCHER [ LVOUTPUT]  .Value = (ushort) ( LVINDEX ) ; 
                __context__.SourceCodeLine = 157;
                ROUTED_SOURCE_NAME [ LVOUTPUT]  .UpdateValue ( SOURCE_NAME [ LVINDEX ]  ) ; 
                } 
            
            
            }
            
        private void STARTCOUNTDOWN (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 162;
            if ( Functions.TestForTrue  ( ( GVNOTOUCH.NOTOUCHOFFTIME)  ) ) 
                { 
                __context__.SourceCodeLine = 164;
                while ( Functions.TestForTrue  ( ( GVNOTOUCH.STATUSNOTOUCHACTIVE)  ) ) 
                    { 
                    __context__.SourceCodeLine = 166;
                    CreateWait ( "__SPLS_TMPVAR__WAITLABEL_13__" , 100 , __SPLS_TMPVAR__WAITLABEL_13___Callback ) ;
                    __context__.SourceCodeLine = 164;
                    } 
                
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_13___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            ushort LVCOUNTER = 0;
            
            __context__.SourceCodeLine = 169;
            GVNOTOUCH . COUNTDOWN = (uint) ( (GVNOTOUCH.COUNTDOWN - 1) ) ; 
            __context__.SourceCodeLine = 170;
            Trace( "Countdown {0:d}", (int)GVNOTOUCH.COUNTDOWN) ; 
            __context__.SourceCodeLine = 171;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVNOTOUCH.COUNTDOWN < 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 173;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)16; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 175;
                    SETOUTPUTS (  __context__ , (ushort)( 0 ), (ushort)( LVCOUNTER )) ; 
                    __context__.SourceCodeLine = 173;
                    } 
                
                __context__.SourceCodeLine = 177;
                Functions.Pulse ( 20, NOTOUCHOFF_PULSE ) ; 
                __context__.SourceCodeLine = 178;
                GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void UPDATEDISPLAYS (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVACTIVEINPUTS = 0;
        ushort LVMOD = 0;
        ushort LVDIV = 0;
        ushort LVCOUNT = 0;
        ushort LVINPUT = 0;
        
        
        __context__.SourceCodeLine = 187;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 189;
            if ( Functions.TestForTrue  ( ( GVNOTOUCH.INPUTQUEUE[ LVCOUNTER ])  ) ) 
                {
                __context__.SourceCodeLine = 190;
                LVACTIVEINPUTS = (ushort) ( LVCOUNTER ) ; 
                }
            
            __context__.SourceCodeLine = 187;
            } 
        
        __context__.SourceCodeLine = 192;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVACTIVEINPUTS > GVNOTOUCH.NUMBEROFDISPLAYS ))  ) ) 
            {
            __context__.SourceCodeLine = 193;
            LVACTIVEINPUTS = (ushort) ( GVNOTOUCH.NUMBEROFDISPLAYS ) ; 
            }
        
        __context__.SourceCodeLine = 194;
        if ( Functions.TestForTrue  ( ( LVACTIVEINPUTS)  ) ) 
            { 
            __context__.SourceCodeLine = 196;
            LVMOD = (ushort) ( Mod( GVNOTOUCH.NUMBEROFDISPLAYS , LVACTIVEINPUTS ) ) ; 
            __context__.SourceCodeLine = 197;
            LVDIV = (ushort) ( (GVNOTOUCH.NUMBEROFDISPLAYS / LVACTIVEINPUTS) ) ; 
            __context__.SourceCodeLine = 198;
            LVCOUNT = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 199;
            LVINPUT = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 200;
            if ( Functions.TestForTrue  ( ( LVMOD)  ) ) 
                { 
                __context__.SourceCodeLine = 202;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)GVNOTOUCH.NUMBEROFDISPLAYS; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 204;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNT > LVDIV ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 206;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNT == (LVDIV + 1)))  ) ) 
                            { 
                            __context__.SourceCodeLine = 208;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVINPUT > LVMOD ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 210;
                                LVCOUNT = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 211;
                                LVINPUT = (ushort) ( (LVINPUT + 1) ) ; 
                                } 
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 216;
                            LVCOUNT = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 217;
                            LVINPUT = (ushort) ( (LVINPUT + 1) ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 220;
                    ROUTE_DISPLAY [ LVCOUNTER]  .Value = (ushort) ( GVDISPLAYS[ LVCOUNTER ].SOURCEINPUT[ GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ] ) ; 
                    __context__.SourceCodeLine = 221;
                    ROUTE_SWITCHER [ LVCOUNTER]  .Value = (ushort) ( GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ) ; 
                    __context__.SourceCodeLine = 222;
                    ROUTED_SOURCE_NAME [ LVCOUNTER]  .UpdateValue ( SOURCE_NAME [ GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ]  ) ; 
                    __context__.SourceCodeLine = 223;
                    LVCOUNT = (ushort) ( (LVCOUNT + 1) ) ; 
                    __context__.SourceCodeLine = 202;
                    } 
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 228;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)GVNOTOUCH.NUMBEROFDISPLAYS; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 230;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNT > LVDIV ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 232;
                        LVCOUNT = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 233;
                        LVINPUT = (ushort) ( (LVINPUT + 1) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 235;
                    ROUTE_DISPLAY [ LVCOUNTER]  .Value = (ushort) ( GVDISPLAYS[ LVCOUNTER ].SOURCEINPUT[ GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ] ) ; 
                    __context__.SourceCodeLine = 236;
                    ROUTE_SWITCHER [ LVCOUNTER]  .Value = (ushort) ( GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ) ; 
                    __context__.SourceCodeLine = 237;
                    ROUTED_SOURCE_NAME [ LVCOUNTER]  .UpdateValue ( SOURCE_NAME [ GVNOTOUCH.INPUTQUEUE[ LVINPUT ] ]  ) ; 
                    __context__.SourceCodeLine = 238;
                    LVCOUNT = (ushort) ( (LVCOUNT + 1) ) ; 
                    __context__.SourceCodeLine = 228;
                    } 
                
                } 
            
            } 
        
        
        }
        
    private void TRIGGERNOTOUCH (  SplusExecutionContext __context__, ushort LVINDEX , ushort LVVALUE ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVSOURCEPREVUSED = 0;
        
        
        __context__.SourceCodeLine = 246;
        if ( Functions.TestForTrue  ( ( LVVALUE)  ) ) 
            { 
            __context__.SourceCodeLine = 248;
            if ( Functions.TestForTrue  ( ( GVNOTOUCH.SOURCENOTOUCHENABLE[ LVINDEX ])  ) ) 
                { 
                __context__.SourceCodeLine = 250;
                Functions.Pulse ( 20, NOTOUCHON_PULSE ) ; 
                __context__.SourceCodeLine = 251;
                GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 252;
                UPDATEDISPLAYS (  __context__  ) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 255;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVVALUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 257;
                if ( Functions.TestForTrue  ( ( Functions.Not( CHECKAUTOOFF( __context__ ) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 258;
                    GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 259;
                if ( Functions.TestForTrue  ( ( Functions.Not( CHECKSOURCESYNC( __context__ ) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 261;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)GVNOTOUCH.NUMBEROFDISPLAYS; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 263;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == ROUTE_SWITCHER[ LVCOUNTER ] .Value))  ) ) 
                            { 
                            __context__.SourceCodeLine = 265;
                            UPDATEDISPLAYS (  __context__  ) ; 
                            __context__.SourceCodeLine = 266;
                            GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 267;
                            return ; 
                            } 
                        
                        __context__.SourceCodeLine = 261;
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 271;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( CHECKAUTOOFF( __context__ ) ) && Functions.TestForTrue ( Functions.Not( GVNOTOUCH.STATUSNOTOUCHACTIVE ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 273;
                    GVNOTOUCH . COUNTDOWN = (uint) ( GVNOTOUCH.NOTOUCHOFFTIME ) ; 
                    __context__.SourceCodeLine = 274;
                    GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 275;
                    STARTCOUNTDOWN (  __context__  ) ; 
                    } 
                
                } 
            
            }
        
        
        }
        
    private ushort CHECKQUEUEINPUTS (  SplusExecutionContext __context__, ushort LVINPUT ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVACTIVE = 0;
        
        
        __context__.SourceCodeLine = 282;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 284;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINPUT == GVNOTOUCH.INPUTQUEUE[ LVCOUNTER ]))  ) ) 
                {
                __context__.SourceCodeLine = 285;
                LVACTIVE = (ushort) ( 1 ) ; 
                }
            
            __context__.SourceCodeLine = 282;
            } 
        
        __context__.SourceCodeLine = 287;
        if ( Functions.TestForTrue  ( ( LVACTIVE)  ) ) 
            {
            __context__.SourceCodeLine = 288;
            return (ushort)( LVACTIVE) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 290;
            return (ushort)( 0) ; 
            }
        
        
        return 0; // default return value (none specified in module)
        }
        
    private void FORMATINPUTQUEUE (  SplusExecutionContext __context__, ushort LVINPUT , ushort LVACTIVE ) 
        { 
        ushort LVQUEUE = 0;
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 296;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( LVACTIVE ) && Functions.TestForTrue ( Functions.Not( CHECKQUEUEINPUTS( __context__ , (ushort)( LVINPUT ) ) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 298;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 16 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)2; 
            int __FN_FORSTEP_VAL__1 = (int)Functions.ToLongInteger( -( 1 ) ); 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 300;
                GVNOTOUCH . INPUTQUEUE [ LVCOUNTER] = (ushort) ( GVNOTOUCH.INPUTQUEUE[ (LVCOUNTER - 1) ] ) ; 
                __context__.SourceCodeLine = 298;
                } 
            
            __context__.SourceCodeLine = 302;
            GVNOTOUCH . INPUTQUEUE [ 1] = (ushort) ( LVINPUT ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 304;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVACTIVE ))  ) ) 
                { 
                __context__.SourceCodeLine = 306;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)16; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 308;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVNOTOUCH.INPUTQUEUE[ LVCOUNTER ] == LVINPUT))  ) ) 
                        {
                        __context__.SourceCodeLine = 309;
                        GVNOTOUCH . INPUTQUEUE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                        }
                    
                    __context__.SourceCodeLine = 306;
                    } 
                
                __context__.SourceCodeLine = 311;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)(16 - 1); 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 313;
                    if ( Functions.TestForTrue  ( ( Functions.Not( GVNOTOUCH.INPUTQUEUE[ LVCOUNTER ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 315;
                        GVNOTOUCH . INPUTQUEUE [ LVCOUNTER] = (ushort) ( GVNOTOUCH.INPUTQUEUE[ (LVCOUNTER + 1) ] ) ; 
                        __context__.SourceCodeLine = 316;
                        GVNOTOUCH . INPUTQUEUE [ (LVCOUNTER + 1)] = (ushort) ( 0 ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 311;
                    } 
                
                } 
            
            }
        
        
        }
        
    private void PARSEDATAFROMCONFIG (  SplusExecutionContext __context__, CrestronString LVDATA ) 
        { 
        ushort LVINDEX = 0;
        ushort LVINSTANCEINDEX = 0;
        ushort LVCOUNTERINPUT = 0;
        
        CrestronString LVINDEXTEMP;
        CrestronString LVTRASH;
        CrestronString LVINPUTTEMP;
        LVINDEXTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVINPUTTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
        
        
        __context__.SourceCodeLine = 326;
        if ( Functions.TestForTrue  ( ( Functions.Find( "//" , LVDATA ))  ) ) 
            { 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 332;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Display " , LVDATA ))  ) ) 
                { 
                __context__.SourceCodeLine = 334;
                LVTRASH  .UpdateValue ( Functions.Remove ( "Display " , LVDATA )  ) ; 
                __context__.SourceCodeLine = 335;
                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                __context__.SourceCodeLine = 336;
                LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                __context__.SourceCodeLine = 337;
                LVINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                __context__.SourceCodeLine = 338;
                LVCOUNTERINPUT = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 339;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SourceInputs-" , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 341;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "SourceInputs-" , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 342;
                    while ( Functions.TestForTrue  ( ( Functions.Find( "," , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 344;
                        LVINPUTTEMP  .UpdateValue ( Functions.Remove ( "," , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 345;
                        LVINPUTTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINPUTTEMP ) - 1), LVINPUTTEMP )  ) ; 
                        __context__.SourceCodeLine = 346;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVINPUTTEMP ) > 0 ))  ) ) 
                            {
                            __context__.SourceCodeLine = 347;
                            GVDISPLAYS [ LVINDEX] . SOURCEINPUT [ LVCOUNTERINPUT] = (ushort) ( Functions.Atoi( LVINPUTTEMP ) ) ; 
                            }
                        
                        __context__.SourceCodeLine = 348;
                        LVCOUNTERINPUT = (ushort) ( (LVCOUNTERINPUT + 1) ) ; 
                        __context__.SourceCodeLine = 349;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTERINPUT > 16 ))  ) ) 
                            {
                            __context__.SourceCodeLine = 350;
                            break ; 
                            }
                        
                        __context__.SourceCodeLine = 351;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTERINPUT <= 16 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 353;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Atoi( LVDATA ) > 0 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 354;
                                GVDISPLAYS [ LVINDEX] . SOURCEINPUT [ LVCOUNTERINPUT] = (ushort) ( Functions.Atoi( LVINPUTTEMP ) ) ; 
                                }
                            
                            } 
                        
                        __context__.SourceCodeLine = 342;
                        } 
                    
                    } 
                
                } 
            
            } 
        
        
        }
        
    private void FILEOPENCONFIG (  SplusExecutionContext __context__ ) 
        { 
        ushort LVREAD = 0;
        
        short LVHANDLE = 0;
        
        CrestronString LVREADFILE;
        CrestronString LVREADLINE;
        CrestronString LVFILENAME;
        CrestronString LVTRASH;
        LVREADFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
        LVREADLINE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 366;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 368;
            LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
            __context__.SourceCodeLine = 369;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 370;
            __context__.SourceCodeLine = 373;
            LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 0) ) ) ; 
            
            __context__.SourceCodeLine = 375;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 377;
                LVREAD = (ushort) ( FileRead( (short)( LVHANDLE ) , LVREADFILE , (ushort)( 16383 ) ) ) ; 
                __context__.SourceCodeLine = 378;
                while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , LVREADFILE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 380;
                    LVREADLINE  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , LVREADFILE )  ) ; 
                    __context__.SourceCodeLine = 381;
                    LVREADLINE  .UpdateValue ( Functions.Remove ( (Functions.Length( LVREADLINE ) - 2), LVREADLINE )  ) ; 
                    __context__.SourceCodeLine = 382;
                    PARSEDATAFROMCONFIG (  __context__ , LVREADLINE) ; 
                    __context__.SourceCodeLine = 378;
                    } 
                
                __context__.SourceCodeLine = 385;
                LVTRASH  .UpdateValue ( Functions.Remove ( LVREADLINE , LVREADFILE )  ) ; 
                __context__.SourceCodeLine = 386;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVREADFILE ) > 2 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 387;
                    PARSEDATAFROMCONFIG (  __context__ , LVREADFILE) ; 
                    }
                
                __context__.SourceCodeLine = 388;
                LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                __context__.SourceCodeLine = 389;
                EndFileOperations ( ) ; 
                } 
            
            } 
        
        
        }
        
    object POWEROFFALL_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 399;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 401;
                SETOUTPUTS (  __context__ , (ushort)( 0 ), (ushort)( LVCOUNTER )) ; 
                __context__.SourceCodeLine = 402;
                Functions.Delay (  (int) ( 25 ) ) ; 
                __context__.SourceCodeLine = 399;
                } 
            
            __context__.SourceCodeLine = 404;
            GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object ENABLENOTOUCH_ON_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 408;
        GVNOTOUCH . NOTOUCHON = (ushort) ( ENABLENOTOUCH_ON  .Value ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ENABLENOTOUCH_OFF_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 413;
        GVNOTOUCH . NOTOUCHOFF = (ushort) ( ENABLENOTOUCH_OFF  .Value ) ; 
        __context__.SourceCodeLine = 414;
        if ( Functions.TestForTrue  ( ( GVNOTOUCH.NOTOUCHOFF)  ) ) 
            { 
            __context__.SourceCodeLine = 416;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( CHECKAUTOOFF( __context__ ) ) && Functions.TestForTrue ( Functions.Not( GVNOTOUCH.STATUSNOTOUCHACTIVE ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 418;
                GVNOTOUCH . COUNTDOWN = (uint) ( GVNOTOUCH.NOTOUCHOFFTIME ) ; 
                __context__.SourceCodeLine = 419;
                GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 420;
                STARTCOUNTDOWN (  __context__  ) ; 
                } 
            
            } 
        
        __context__.SourceCodeLine = 423;
        if ( Functions.TestForTrue  ( ( Functions.Not( ENABLENOTOUCH_OFF  .Value ))  ) ) 
            {
            __context__.SourceCodeLine = 424;
            GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOURCE_SYNC_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 429;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 430;
        GVNOTOUCH . SOURCESYNC [ LVINDEX] = (ushort) ( SOURCE_SYNC[ LVINDEX ] .Value ) ; 
        __context__.SourceCodeLine = 431;
        FORMATINPUTQUEUE (  __context__ , (ushort)( LVINDEX ), (ushort)( SOURCE_SYNC[ LVINDEX ] .Value )) ; 
        __context__.SourceCodeLine = 432;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( GVNOTOUCH.SOURCESYNC[ LVINDEX ] ) && Functions.TestForTrue ( GVNOTOUCH.NOTOUCHON )) ))  ) ) 
            {
            __context__.SourceCodeLine = 433;
            TRIGGERNOTOUCH (  __context__ , (ushort)( LVINDEX ), (ushort)( GVNOTOUCH.SOURCESYNC[ LVINDEX ] )) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 434;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Not( GVNOTOUCH.SOURCESYNC[ LVINDEX ] ) ) && Functions.TestForTrue ( GVNOTOUCH.NOTOUCHOFF )) ))  ) ) 
                {
                __context__.SourceCodeLine = 435;
                TRIGGERNOTOUCH (  __context__ , (ushort)( LVINDEX ), (ushort)( GVNOTOUCH.SOURCESYNC[ LVINDEX ] )) ; 
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOURCE_NOTOUCH_ENABLED_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 440;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 441;
        GVNOTOUCH . SOURCENOTOUCHENABLE [ LVINDEX] = (ushort) ( SOURCE_NOTOUCH_ENABLED[ LVINDEX ] .Value ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWER_OFF_TIME_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 445;
        GVNOTOUCH . NOTOUCHOFFTIME = (uint) ( POWER_OFF_TIME  .UintValue ) ; 
        __context__.SourceCodeLine = 446;
        if ( Functions.TestForTrue  ( ( GVNOTOUCH.STATUSNOTOUCHACTIVE)  ) ) 
            {
            __context__.SourceCodeLine = 447;
            GVNOTOUCH . COUNTDOWN = (uint) ( GVNOTOUCH.NOTOUCHOFFTIME ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object NUMBER_OF_DISPLAYS_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 451;
        GVNOTOUCH . NUMBEROFDISPLAYS = (ushort) ( NUMBER_OF_DISPLAYS  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISPLAY_ROUTE_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 456;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 457;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( DISPLAY_ROUTE[ LVINDEX ] .UshortValue <= 16 ))  ) ) 
            { 
            __context__.SourceCodeLine = 459;
            SETOUTPUTS (  __context__ , (ushort)( DISPLAY_ROUTE[ LVINDEX ] .UshortValue ), (ushort)( LVINDEX )) ; 
            __context__.SourceCodeLine = 460;
            if ( Functions.TestForTrue  ( ( DISPLAY_ROUTE[ LVINDEX ] .UshortValue)  ) ) 
                { 
                __context__.SourceCodeLine = 462;
                if ( Functions.TestForTrue  ( ( GVNOTOUCH.NOTOUCHON)  ) ) 
                    { 
                    __context__.SourceCodeLine = 464;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( GVNOTOUCH.SOURCENOTOUCHENABLE[ DISPLAY_ROUTE[ LVINDEX ] .UshortValue ] ) && Functions.TestForTrue ( Functions.Not( GVNOTOUCH.SOURCESYNC[ DISPLAY_ROUTE[ LVINDEX ] .UshortValue ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 466;
                        GVNOTOUCH . COUNTDOWN = (uint) ( GVNOTOUCH.NOTOUCHOFFTIME ) ; 
                        __context__.SourceCodeLine = 467;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Not( GVNOTOUCH.STATUSNOTOUCHACTIVE ) ) && Functions.TestForTrue ( CHECKAUTOOFF( __context__ ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 469;
                            GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 470;
                            STARTCOUNTDOWN (  __context__  ) ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 474;
                        GVNOTOUCH . STATUSNOTOUCHACTIVE = (ushort) ( 0 ) ; 
                        }
                    
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SOURCE_NAME_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 482;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SYSTEMSTART_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 486;
        GVSTARTREAD = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 487;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
            {
            __context__.SourceCodeLine = 488;
            FILEOPENCONFIG (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FILENAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 492;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 494;
            if ( Functions.TestForTrue  ( ( GVSTARTREAD)  ) ) 
                {
                __context__.SourceCodeLine = 495;
                FILEOPENCONFIG (  __context__  ) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTERDISP = 0;
    ushort LVCOUNTERINPUT = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 504;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTERDISP  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTERDISP  >= __FN_FORSTART_VAL__1) && (LVCOUNTERDISP  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTERDISP  <= __FN_FORSTART_VAL__1) && (LVCOUNTERDISP  >= __FN_FOREND_VAL__1) ) ; LVCOUNTERDISP  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 506;
            ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__2 = (ushort)16; 
            int __FN_FORSTEP_VAL__2 = (int)1; 
            for ( LVCOUNTERINPUT  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTERINPUT  >= __FN_FORSTART_VAL__2) && (LVCOUNTERINPUT  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTERINPUT  <= __FN_FORSTART_VAL__2) && (LVCOUNTERINPUT  >= __FN_FOREND_VAL__2) ) ; LVCOUNTERINPUT  += (ushort)__FN_FORSTEP_VAL__2) 
                { 
                __context__.SourceCodeLine = 508;
                GVDISPLAYS [ LVCOUNTERDISP] . SOURCEINPUT [ LVCOUNTERINPUT] = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 506;
                } 
            
            __context__.SourceCodeLine = 504;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVNOTOUCH  = new SNOTOUCH( this, true );
    GVNOTOUCH .PopulateCustomAttributeList( false );
    GVDISPLAYS  = new SDISPLAY[ 17 ];
    for( uint i = 0; i < 17; i++ )
    {
        GVDISPLAYS [i] = new SDISPLAY( this, true );
        GVDISPLAYS [i].PopulateCustomAttributeList( false );
        
    }
    
    POWEROFFALL = new Crestron.Logos.SplusObjects.DigitalInput( POWEROFFALL__DigitalInput__, this );
    m_DigitalInputList.Add( POWEROFFALL__DigitalInput__, POWEROFFALL );
    
    ENABLENOTOUCH_ON = new Crestron.Logos.SplusObjects.DigitalInput( ENABLENOTOUCH_ON__DigitalInput__, this );
    m_DigitalInputList.Add( ENABLENOTOUCH_ON__DigitalInput__, ENABLENOTOUCH_ON );
    
    ENABLENOTOUCH_OFF = new Crestron.Logos.SplusObjects.DigitalInput( ENABLENOTOUCH_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( ENABLENOTOUCH_OFF__DigitalInput__, ENABLENOTOUCH_OFF );
    
    SYSTEMSTART = new Crestron.Logos.SplusObjects.DigitalInput( SYSTEMSTART__DigitalInput__, this );
    m_DigitalInputList.Add( SYSTEMSTART__DigitalInput__, SYSTEMSTART );
    
    SOURCE_SYNC = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCE_SYNC[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SOURCE_SYNC__DigitalInput__ + i, SOURCE_SYNC__DigitalInput__, this );
        m_DigitalInputList.Add( SOURCE_SYNC__DigitalInput__ + i, SOURCE_SYNC[i+1] );
    }
    
    SOURCE_NOTOUCH_ENABLED = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCE_NOTOUCH_ENABLED[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( SOURCE_NOTOUCH_ENABLED__DigitalInput__ + i, SOURCE_NOTOUCH_ENABLED__DigitalInput__, this );
        m_DigitalInputList.Add( SOURCE_NOTOUCH_ENABLED__DigitalInput__ + i, SOURCE_NOTOUCH_ENABLED[i+1] );
    }
    
    NOTOUCHON_PULSE = new Crestron.Logos.SplusObjects.DigitalOutput( NOTOUCHON_PULSE__DigitalOutput__, this );
    m_DigitalOutputList.Add( NOTOUCHON_PULSE__DigitalOutput__, NOTOUCHON_PULSE );
    
    NOTOUCHOFF_PULSE = new Crestron.Logos.SplusObjects.DigitalOutput( NOTOUCHOFF_PULSE__DigitalOutput__, this );
    m_DigitalOutputList.Add( NOTOUCHOFF_PULSE__DigitalOutput__, NOTOUCHOFF_PULSE );
    
    POWER_OFF_TIME = new Crestron.Logos.SplusObjects.AnalogInput( POWER_OFF_TIME__AnalogSerialInput__, this );
    m_AnalogInputList.Add( POWER_OFF_TIME__AnalogSerialInput__, POWER_OFF_TIME );
    
    NUMBER_OF_DISPLAYS = new Crestron.Logos.SplusObjects.AnalogInput( NUMBER_OF_DISPLAYS__AnalogSerialInput__, this );
    m_AnalogInputList.Add( NUMBER_OF_DISPLAYS__AnalogSerialInput__, NUMBER_OF_DISPLAYS );
    
    DISPLAY_ROUTE = new InOutArray<AnalogInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        DISPLAY_ROUTE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( DISPLAY_ROUTE__AnalogSerialInput__ + i, DISPLAY_ROUTE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( DISPLAY_ROUTE__AnalogSerialInput__ + i, DISPLAY_ROUTE[i+1] );
    }
    
    ROUTE_SWITCHER = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        ROUTE_SWITCHER[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ROUTE_SWITCHER__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ROUTE_SWITCHER__AnalogSerialOutput__ + i, ROUTE_SWITCHER[i+1] );
    }
    
    ROUTE_DISPLAY = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        ROUTE_DISPLAY[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ROUTE_DISPLAY__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ROUTE_DISPLAY__AnalogSerialOutput__ + i, ROUTE_DISPLAY[i+1] );
    }
    
    FILENAME = new Crestron.Logos.SplusObjects.StringInput( FILENAME__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( FILENAME__AnalogSerialInput__, FILENAME );
    
    SOURCE_NAME = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCE_NAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( SOURCE_NAME__AnalogSerialInput__ + i, SOURCE_NAME__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( SOURCE_NAME__AnalogSerialInput__ + i, SOURCE_NAME[i+1] );
    }
    
    ROUTED_SOURCE_NAME = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        ROUTED_SOURCE_NAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( ROUTED_SOURCE_NAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( ROUTED_SOURCE_NAME__AnalogSerialOutput__ + i, ROUTED_SOURCE_NAME[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_13___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_13___CallbackFn );
    
    POWEROFFALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWEROFFALL_OnPush_0, false ) );
    ENABLENOTOUCH_ON.OnDigitalChange.Add( new InputChangeHandlerWrapper( ENABLENOTOUCH_ON_OnChange_1, false ) );
    ENABLENOTOUCH_OFF.OnDigitalChange.Add( new InputChangeHandlerWrapper( ENABLENOTOUCH_OFF_OnChange_2, false ) );
    for( uint i = 0; i < 16; i++ )
        SOURCE_SYNC[i+1].OnDigitalChange.Add( new InputChangeHandlerWrapper( SOURCE_SYNC_OnChange_3, false ) );
        
    for( uint i = 0; i < 16; i++ )
        SOURCE_NOTOUCH_ENABLED[i+1].OnDigitalChange.Add( new InputChangeHandlerWrapper( SOURCE_NOTOUCH_ENABLED_OnChange_4, false ) );
        
    POWER_OFF_TIME.OnAnalogChange.Add( new InputChangeHandlerWrapper( POWER_OFF_TIME_OnChange_5, false ) );
    NUMBER_OF_DISPLAYS.OnAnalogChange.Add( new InputChangeHandlerWrapper( NUMBER_OF_DISPLAYS_OnChange_6, false ) );
    for( uint i = 0; i < 16; i++ )
        DISPLAY_ROUTE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( DISPLAY_ROUTE_OnChange_7, false ) );
        
    for( uint i = 0; i < 16; i++ )
        SOURCE_NAME[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( SOURCE_NAME_OnChange_8, false ) );
        
    SYSTEMSTART.OnDigitalPush.Add( new InputChangeHandlerWrapper( SYSTEMSTART_OnPush_9, false ) );
    FILENAME.OnSerialChange.Add( new InputChangeHandlerWrapper( FILENAME_OnChange_10, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_NOTOUCH_V2_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_13___Callback;


const uint POWEROFFALL__DigitalInput__ = 0;
const uint ENABLENOTOUCH_ON__DigitalInput__ = 1;
const uint ENABLENOTOUCH_OFF__DigitalInput__ = 2;
const uint SYSTEMSTART__DigitalInput__ = 3;
const uint POWER_OFF_TIME__AnalogSerialInput__ = 0;
const uint NUMBER_OF_DISPLAYS__AnalogSerialInput__ = 1;
const uint FILENAME__AnalogSerialInput__ = 2;
const uint SOURCE_SYNC__DigitalInput__ = 4;
const uint SOURCE_NOTOUCH_ENABLED__DigitalInput__ = 20;
const uint DISPLAY_ROUTE__AnalogSerialInput__ = 3;
const uint SOURCE_NAME__AnalogSerialInput__ = 19;
const uint NOTOUCHON_PULSE__DigitalOutput__ = 0;
const uint NOTOUCHOFF_PULSE__DigitalOutput__ = 1;
const uint ROUTE_SWITCHER__AnalogSerialOutput__ = 0;
const uint ROUTE_DISPLAY__AnalogSerialOutput__ = 16;
const uint ROUTED_SOURCE_NAME__AnalogSerialOutput__ = 32;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SNOTOUCH : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  NOTOUCHON = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  NOTOUCHOFF = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  [] SOURCENOTOUCHENABLE;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] SOURCESYNC;
    
    [SplusStructAttribute(4, false, false)]
    public uint  NOTOUCHOFFTIME = 0;
    
    [SplusStructAttribute(5, false, false)]
    public uint  COUNTDOWN = 0;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  NUMBEROFDISPLAYS = 0;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  STATUSNOTOUCHACTIVE = 0;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  [] INPUTQUEUE;
    
    
    public SNOTOUCH( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        SOURCENOTOUCHENABLE  = new ushort[ 17 ];
        SOURCESYNC  = new ushort[ 17 ];
        INPUTQUEUE  = new ushort[ 17 ];
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SDISPLAY : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  [] SOURCEINPUT;
    
    
    public SDISPLAY( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        SOURCEINPUT  = new ushort[ 17 ];
        
        
    }
    
}

}
