#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_NoTouch_v2_1.h"

FUNCTION_MAIN( S2_IESS_NoTouch_v2_1 );
EVENT_HANDLER( S2_IESS_NoTouch_v2_1 );
DEFINE_ENTRYPOINT( S2_IESS_NoTouch_v2_1 );




















static unsigned short S2_IESS_NoTouch_v2_1__CHECKSOURCESYNC ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __FN_DSTINTRET_VAL__; 
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    __FN_DSTINTRET_VAL__ = 0;
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 108 );
    __FN_FOREND_VAL__1 = 16; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 110 );
        if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE  ), 0, __LVCOUNTER  )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 112 );
            if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVCOUNTER  )) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 113 );
                __FN_DSTINTRET_VAL__ = ( 0) ;
                goto S2_IESS_NoTouch_v2_1_Exit__CHECKSOURCESYNC ; 
                }
            
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 108 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 116 );
    __FN_DSTINTRET_VAL__ = ( 1) ;
    goto S2_IESS_NoTouch_v2_1_Exit__CHECKSOURCESYNC ; 
    
    S2_IESS_NoTouch_v2_1_Exit__CHECKSOURCESYNC:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 118 );
    return __FN_DSTINTRET_VAL__; 
    }
    
static unsigned short S2_IESS_NoTouch_v2_1__CHECKAUTOOFF ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __FN_DSTINTRET_VAL__; 
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVCHECKDISPLAYS; 
    unsigned short  __LVCHECKDISPLAYSNOTOUCH; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    __FN_DSTINTRET_VAL__ = 0;
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 121 );
    __LVCHECKDISPLAYS = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 122 );
    __LVCHECKDISPLAYSNOTOUCH = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 123 );
    __FN_FOREND_VAL__1 = GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ); 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 125 );
        if ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER , __LVCOUNTER  )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 127 );
            __LVCHECKDISPLAYS = (__LVCHECKDISPLAYS + 1); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 128 );
            if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE  ), 0, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER , __LVCOUNTER  )  )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 130 );
                __LVCHECKDISPLAYSNOTOUCH = (__LVCHECKDISPLAYSNOTOUCH + 1); 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 131 );
                if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER , __LVCOUNTER  )  )) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 132 );
                    __FN_DSTINTRET_VAL__ = ( 0) ;
                    goto S2_IESS_NoTouch_v2_1_Exit__CHECKAUTOOFF ; 
                    }
                
                } 
            
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 123 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 136 );
    if ( __LVCHECKDISPLAYS) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 138 );
        if ( (__LVCHECKDISPLAYSNOTOUCH == GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ))) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 139 );
            __FN_DSTINTRET_VAL__ = ( 1) ;
            goto S2_IESS_NoTouch_v2_1_Exit__CHECKAUTOOFF ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 140 );
            if ( (__LVCHECKDISPLAYSNOTOUCH < GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ))) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 141 );
                __FN_DSTINTRET_VAL__ = ( 0) ;
                goto S2_IESS_NoTouch_v2_1_Exit__CHECKAUTOOFF ; 
                }
            
            }
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__CHECKAUTOOFF:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 144 );
    return __FN_DSTINTRET_VAL__; 
    }
    
static void S2_IESS_NoTouch_v2_1__SETOUTPUTS ( unsigned short __LVINDEX , unsigned short __LVOUTPUT ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 147 );
    if ( (__LVINDEX == 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 149 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_DISPLAY ,__LVOUTPUT, 99) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 150 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER ,__LVOUTPUT, __LVINDEX) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 151 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_NoTouch_v2_1 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __ROUTED_SOURCE_NAME , __LVOUTPUT ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); }
        
        ; 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 155 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_DISPLAY ,__LVOUTPUT, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVOUTPUT )-> SDISPLAY__SOURCEINPUT  , 0, __LVINDEX  )) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 156 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER ,__LVOUTPUT, __LVINDEX) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 157 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_NoTouch_v2_1 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ),  GLOBAL_STRING_ARRAY( S2_IESS_NoTouch_v2_1, __SOURCE_NAME  )    ,  __LVINDEX  )  )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __ROUTED_SOURCE_NAME , __LVOUTPUT ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); }
        
        ; 
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__SETOUTPUTS:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    /* End Free local function variables */
    
    }
    
static void S2_IESS_NoTouch_v2_1__STARTCOUNTDOWN ( ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 162 );
    if ( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 164 );
        while ( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 166 );
            CREATE_WAIT( S2_IESS_NoTouch_v2_1, 100, __SPLS_TMPVAR__WAITLABEL_0__ );
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 164 );
            } 
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__STARTCOUNTDOWN:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__WAITLABEL_0__ )
    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 169 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN )= (GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN ) - 1); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 170 );
    Print( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , 16, "\xFA\xE0""Countdown %ld""\xFB", (long )( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN ) )) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 171 );
    if ( (GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN ) < 1)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 173 );
        __FN_FOREND_VAL__1 = 16; 
        __FN_FORINIT_VAL__1 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 175 );
            S2_IESS_NoTouch_v2_1__SETOUTPUTS ( 0, __LVCOUNTER) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 173 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 177 );
        Pulse ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , 20, __S2_IESS_NoTouch_v2_1_NOTOUCHOFF_PULSE_DIG_OUTPUT ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 178 );
        GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
        } 
    
    

S2_IESS_NoTouch_v2_1_Exit____SPLS_TMPVAR__WAITLABEL_0__:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
static void S2_IESS_NoTouch_v2_1__UPDATEDISPLAYS ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVACTIVEINPUTS; 
    unsigned short  __LVMOD; 
    unsigned short  __LVDIV; 
    unsigned short  __LVCOUNT; 
    unsigned short  __LVINPUT; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    short __FN_FOREND_VAL__3; 
    short __FN_FORINIT_VAL__3; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 187 );
    __FN_FOREND_VAL__1 = 16; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 189 );
        if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER  )) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 190 );
            __LVACTIVEINPUTS = __LVCOUNTER; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 187 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 192 );
    if ( (__LVACTIVEINPUTS > GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ))) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 193 );
        __LVACTIVEINPUTS = GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ); 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 194 );
    if ( __LVACTIVEINPUTS) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 196 );
        __LVMOD = DO_MOD(INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ), __LVACTIVEINPUTS); 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 197 );
        __LVDIV = DO_DIVIDE(INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ), __LVACTIVEINPUTS); 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 198 );
        __LVCOUNT = 1; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 199 );
        __LVINPUT = 1; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 200 );
        if ( __LVMOD) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 202 );
            __FN_FOREND_VAL__2 = GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ); 
            __FN_FORINIT_VAL__2 = 1; 
            for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__2) ; __LVCOUNTER  += __FN_FORINIT_VAL__2) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 204 );
                if ( (__LVCOUNT > __LVDIV)) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 206 );
                    if ( (__LVCOUNT == (__LVDIV + 1))) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 208 );
                        if ( (__LVINPUT > __LVMOD)) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 210 );
                            __LVCOUNT = 1; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 211 );
                            __LVINPUT = (__LVINPUT + 1); 
                            } 
                        
                        } 
                    
                    else 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 216 );
                        __LVCOUNT = 1; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 217 );
                        __LVINPUT = (__LVINPUT + 1); 
                        } 
                    
                    } 
                
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 220 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_DISPLAY ,__LVCOUNTER, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVCOUNTER )-> SDISPLAY__SOURCEINPUT  , 0, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )  )) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 221 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER ,__LVCOUNTER, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 222 );
                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) == 0 ) {
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_NoTouch_v2_1 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ),  GLOBAL_STRING_ARRAY( S2_IESS_NoTouch_v2_1, __SOURCE_NAME  )    ,  GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )  )  )  ; 
                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __ROUTED_SOURCE_NAME , __LVCOUNTER ) ; 
                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); }
                
                ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 223 );
                __LVCOUNT = (__LVCOUNT + 1); 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 202 );
                } 
            
            } 
        
        else 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 228 );
            __FN_FOREND_VAL__3 = GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ); 
            __FN_FORINIT_VAL__3 = 1; 
            for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__3 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__3 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__3) ; __LVCOUNTER  += __FN_FORINIT_VAL__3) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 230 );
                if ( (__LVCOUNT > __LVDIV)) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 232 );
                    __LVCOUNT = 1; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 233 );
                    __LVINPUT = (__LVINPUT + 1); 
                    } 
                
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 235 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_DISPLAY ,__LVCOUNTER, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVCOUNTER )-> SDISPLAY__SOURCEINPUT  , 0, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )  )) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 236 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER ,__LVCOUNTER, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 237 );
                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) == 0 ) {
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , GENERIC_STRING_OUTPUT( S2_IESS_NoTouch_v2_1 )  ,2 , "%s"  , GetStringArrayElement ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ),  GLOBAL_STRING_ARRAY( S2_IESS_NoTouch_v2_1, __SOURCE_NAME  )    ,  GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVINPUT  )  )  )  ; 
                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __ROUTED_SOURCE_NAME , __LVCOUNTER ) ; 
                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); }
                
                ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 238 );
                __LVCOUNT = (__LVCOUNT + 1); 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 228 );
                } 
            
            } 
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__UPDATEDISPLAYS:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static void S2_IESS_NoTouch_v2_1__TRIGGERNOTOUCH ( unsigned short __LVINDEX , unsigned short __LVVALUE ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVSOURCEPREVUSED; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 246 );
    if ( __LVVALUE) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 248 );
        if ( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE  ), 0, __LVINDEX  )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 250 );
            Pulse ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , 20, __S2_IESS_NoTouch_v2_1_NOTOUCHON_PULSE_DIG_OUTPUT ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 251 );
            GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 252 );
            S2_IESS_NoTouch_v2_1__UPDATEDISPLAYS ( ) ; 
            } 
        
        } 
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 255 );
        if ( !( __LVVALUE )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 257 );
            if ( !( S2_IESS_NoTouch_v2_1__CHECKAUTOOFF() )) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 258 );
                GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
                }
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 259 );
            if ( !( S2_IESS_NoTouch_v2_1__CHECKSOURCESYNC() )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 261 );
                __FN_FOREND_VAL__1 = GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS ); 
                __FN_FORINIT_VAL__1 = 1; 
                for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 263 );
                    if ( (__LVINDEX == GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__ROUTE_SWITCHER , __LVCOUNTER  ))) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 265 );
                        S2_IESS_NoTouch_v2_1__UPDATEDISPLAYS ( ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 266 );
                        GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 267 );
                        
                        goto S2_IESS_NoTouch_v2_1_Exit__TRIGGERNOTOUCH ; 
                        } 
                    
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 261 );
                    } 
                
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 271 );
            if ( (S2_IESS_NoTouch_v2_1__CHECKAUTOOFF() && !( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE ) ))) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 273 );
                GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN )= GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME ); 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 274 );
                GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 1; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 275 );
                S2_IESS_NoTouch_v2_1__STARTCOUNTDOWN ( ) ; 
                } 
            
            } 
        
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__TRIGGERNOTOUCH:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static unsigned short S2_IESS_NoTouch_v2_1__CHECKQUEUEINPUTS ( unsigned short __LVINPUT ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __FN_DSTINTRET_VAL__; 
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVACTIVE; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    __FN_DSTINTRET_VAL__ = 0;
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 282 );
    __FN_FOREND_VAL__1 = 16; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 284 );
        if ( (__LVINPUT == GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER  ))) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 285 );
            __LVACTIVE = 1; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 282 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 287 );
    if ( __LVACTIVE) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 288 );
        __FN_DSTINTRET_VAL__ = ( __LVACTIVE) ;
        goto S2_IESS_NoTouch_v2_1_Exit__CHECKQUEUEINPUTS ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 290 );
        __FN_DSTINTRET_VAL__ = ( 0) ;
        goto S2_IESS_NoTouch_v2_1_Exit__CHECKQUEUEINPUTS ; 
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__CHECKQUEUEINPUTS:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 293 );
    return __FN_DSTINTRET_VAL__; 
    }
    
static void S2_IESS_NoTouch_v2_1__FORMATINPUTQUEUE ( unsigned short __LVINPUT , unsigned short __LVACTIVE ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVQUEUE; 
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    short __FN_FOREND_VAL__3; 
    short __FN_FORINIT_VAL__3; 
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 296 );
    if ( (__LVACTIVE && !( S2_IESS_NoTouch_v2_1__CHECKQUEUEINPUTS( __LVINPUT ) ))) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 298 );
        __FN_FOREND_VAL__1 = 2; 
        __FN_FORINIT_VAL__1 = -( 1 ); 
        for( __LVCOUNTER = 16; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 300 );
             SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER , GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, (__LVCOUNTER - 1)  ) ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 298 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 302 );
         SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, 1 , __LVINPUT ); 
        } 
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 304 );
        if ( !( __LVACTIVE )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 306 );
            __FN_FOREND_VAL__2 = 16; 
            __FN_FORINIT_VAL__2 = 1; 
            for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__2) ; __LVCOUNTER  += __FN_FORINIT_VAL__2) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 308 );
                if ( (GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER  ) == __LVINPUT)) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 309 );
                     SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER , 0 ); 
                    }
                
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 306 );
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 311 );
            __FN_FOREND_VAL__3 = (16 - 1); 
            __FN_FORINIT_VAL__3 = 1; 
            for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__3 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__3 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__3) ; __LVCOUNTER  += __FN_FORINIT_VAL__3) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 313 );
                if ( !( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER  ) )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 315 );
                     SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, __LVCOUNTER , GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, (__LVCOUNTER + 1)  ) ); 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 316 );
                     SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE  ), 0, (__LVCOUNTER + 1) , 0 ); 
                    } 
                
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 311 );
                } 
            
            } 
        
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__FORMATINPUTQUEUE:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static void S2_IESS_NoTouch_v2_1__PARSEDATAFROMCONFIG ( struct StringHdr_s* __LVDATA ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    unsigned short  __LVINSTANCEINDEX; 
    unsigned short  __LVCOUNTERINPUT; 
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINDEXTEMP, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINDEXTEMP );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINPUTTEMP, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINPUTTEMP );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "//" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "Display " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( ":" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "SourceInputs-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_4__, sizeof( "," ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_4__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__, 65535 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1, 65535 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINDEXTEMP );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVINDEXTEMP, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVINPUTTEMP );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVINPUTTEMP, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "//" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "Display " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, ":" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "SourceInputs-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_4__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__, "," );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 65535 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__1, 65535 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 326 );
    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
        { 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 332 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 334 );
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 335 );
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 336 );
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 337 );
            __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 338 );
            __LVCOUNTERINPUT = 1; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 339 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 341 );
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 342 );
                while ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 344 );
                    FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVINPUTTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 345 );
                    FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINPUTTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINPUTTEMP  )  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVINPUTTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 346 );
                    if ( (Atoi( LOCAL_STRING_STRUCT( __LVINPUTTEMP  )  ) > 0)) 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 347 );
                         SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVINDEX )-> SDISPLAY__SOURCEINPUT  , 0, __LVCOUNTERINPUT , Atoi( LOCAL_STRING_STRUCT( __LVINPUTTEMP  )  ) ); 
                        }
                    
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 348 );
                    __LVCOUNTERINPUT = (__LVCOUNTERINPUT + 1); 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 349 );
                    if ( (__LVCOUNTERINPUT > 16)) 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 350 );
                        break ; 
                        }
                    
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 351 );
                    if ( (__LVCOUNTERINPUT <= 16)) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 353 );
                        if ( (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 354 );
                             SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVINDEX )-> SDISPLAY__SOURCEINPUT  , 0, __LVCOUNTERINPUT , Atoi( LOCAL_STRING_STRUCT( __LVINPUTTEMP  )  ) ); 
                            }
                        
                        } 
                    
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 342 );
                    } 
                
                } 
            
            } 
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__PARSEDATAFROMCONFIG:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVINDEXTEMP );
    FREE_LOCAL_STRING_STRUCT( __LVTRASH );
    FREE_LOCAL_STRING_STRUCT( __LVINPUTTEMP );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__1 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ );
    /* End Free local function variables */
    
    }
    
static void S2_IESS_NoTouch_v2_1__FILEOPENCONFIG ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVREAD; 
    short  __LVHANDLE; 
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADFILE, 16383 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADFILE );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADLINE, 255 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADLINE );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVFILENAME, 127 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVFILENAME );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH, 63 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "\\NVRAM\\" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__, 16383 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__ );
    
    CREATE_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1, 16383 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADFILE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVREADFILE, 16383 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVREADLINE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVREADLINE, 255 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVFILENAME );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVFILENAME, 127 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 63 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "\\NVRAM\\" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 16383 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__1, 16383 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 366 );
    if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FILENAME  )  ) > 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 368 );
        FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FILENAME  )   )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVFILENAME  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 369 );
        StartFileOperations ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 370 );
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 371 );
        __LVHANDLE = FileOpen( LOCAL_STRING_STRUCT( __LVFILENAME  )  , 0 ); 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 375 );
        if ( (__LVHANDLE >= 0)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 377 );
            __LVREAD = FileRead( __LVHANDLE , LOCAL_STRING_STRUCT( __LVREADFILE  )  , 16383 ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 378 );
            while ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVREADFILE  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 380 );
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )   , LOCAL_STRING_STRUCT( __LVREADFILE  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVREADLINE  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 381 );
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVREADLINE  )  ) - 2), LOCAL_STRING_STRUCT( __LVREADLINE  )  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVREADLINE  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 382 );
                S2_IESS_NoTouch_v2_1__PARSEDATAFROMCONFIG (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVREADLINE  )  ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 378 );
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 385 );
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , LOCAL_STRING_STRUCT( __LVREADLINE  )  , LOCAL_STRING_STRUCT( __LVREADFILE  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 386 );
            if ( (Len( LOCAL_STRING_STRUCT( __LVREADFILE  )  ) > 2)) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 387 );
                S2_IESS_NoTouch_v2_1__PARSEDATAFROMCONFIG (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVREADFILE  )  ) ; 
                }
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 388 );
            __LVREAD = FileClose( __LVHANDLE ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 389 );
            EndFileOperations ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ) ; 
            } 
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__FILEOPENCONFIG:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVREADFILE );
    FREE_LOCAL_STRING_STRUCT( __LVREADLINE );
    FREE_LOCAL_STRING_STRUCT( __LVFILENAME );
    FREE_LOCAL_STRING_STRUCT( __LVTRASH );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__1 );
    /* End Free local function variables */
    
    }
    
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00000 /*PowerOffAll*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 399 );
    __FN_FOREND_VAL__1 = 16; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 401 );
        S2_IESS_NoTouch_v2_1__SETOUTPUTS ( 0, __LVCOUNTER) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 402 );
        Delay ( 25) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 399 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 404 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
    
    S2_IESS_NoTouch_v2_1_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00001 /*EnableNoTouch_On*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 408 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHON )= GetDigitalInput( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), __S2_IESS_NoTouch_v2_1_ENABLENOTOUCH_ON_DIG_INPUT ); 
    
    S2_IESS_NoTouch_v2_1_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00002 /*EnableNoTouch_Off*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 413 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFF )= GetDigitalInput( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), __S2_IESS_NoTouch_v2_1_ENABLENOTOUCH_OFF_DIG_INPUT ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 414 );
    if ( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFF )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 416 );
        if ( (S2_IESS_NoTouch_v2_1__CHECKAUTOOFF() && !( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE ) ))) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 418 );
            GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN )= GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 419 );
            GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 1; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 420 );
            S2_IESS_NoTouch_v2_1__STARTCOUNTDOWN ( ) ; 
            } 
        
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 423 );
    if ( !( GetDigitalInput( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), __S2_IESS_NoTouch_v2_1_ENABLENOTOUCH_OFF_DIG_INPUT ) )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 424 );
        GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_2:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00003 /*Source_Sync*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 429 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_NoTouch_v2_1 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 430 );
     SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVINDEX , GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__SOURCE_SYNC , __LVINDEX  ) ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 431 );
    S2_IESS_NoTouch_v2_1__FORMATINPUTQUEUE ( __LVINDEX, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__SOURCE_SYNC , __LVINDEX  )) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 432 );
    if ( (GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVINDEX  ) && GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHON ))) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 433 );
        S2_IESS_NoTouch_v2_1__TRIGGERNOTOUCH ( __LVINDEX, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVINDEX  )) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 434 );
        if ( (!( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVINDEX  ) ) && GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFF ))) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 435 );
            S2_IESS_NoTouch_v2_1__TRIGGERNOTOUCH ( __LVINDEX, GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, __LVINDEX  )) ; 
            }
        
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_3:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00004 /*Source_NoTouch_Enabled*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 440 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_NoTouch_v2_1 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 441 );
     SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE  ), 0, __LVINDEX , GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__SOURCE_NOTOUCH_ENABLED , __LVINDEX  ) ); 
    
    S2_IESS_NoTouch_v2_1_Exit__Event_4:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00005 /*Power_Off_Time*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 445 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME )= GetAnalogInput( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), __S2_IESS_NoTouch_v2_1_POWER_OFF_TIME_ANALOG_INPUT ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 446 );
    if ( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 447 );
        GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN )= GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME ); 
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_5:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00006 /*Number_Of_Displays*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 451 );
    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NUMBEROFDISPLAYS )= GetAnalogInput( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), __S2_IESS_NoTouch_v2_1_NUMBER_OF_DISPLAYS_ANALOG_INPUT ); 
    
    S2_IESS_NoTouch_v2_1_Exit__Event_6:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00007 /*Display_Route*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 456 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_NoTouch_v2_1 ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 457 );
    if ( (GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__DISPLAY_ROUTE , __LVINDEX  ) <= 16)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 459 );
        S2_IESS_NoTouch_v2_1__SETOUTPUTS ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__DISPLAY_ROUTE , __LVINDEX  ), __LVINDEX) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 460 );
        if ( GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__DISPLAY_ROUTE , __LVINDEX  )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 462 );
            if ( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHON )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 464 );
                if ( (GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE  ), 0, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__DISPLAY_ROUTE , __LVINDEX  )  ) && !( GetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC  ), 0, GetInOutArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), &Globals->S2_IESS_NoTouch_v2_1.__DISPLAY_ROUTE , __LVINDEX  )  ) ))) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 466 );
                    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__COUNTDOWN )= GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__NOTOUCHOFFTIME ); 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 467 );
                    if ( (!( GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE ) ) && S2_IESS_NoTouch_v2_1__CHECKAUTOOFF())) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 469 );
                        GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 1; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 470 );
                        S2_IESS_NoTouch_v2_1__STARTCOUNTDOWN ( ) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 474 );
                    GLOBAL_STRUCT_FIELD( S2_IESS_NoTouch_v2_1, __GVNOTOUCH,SNOTOUCH__STATUSNOTOUCHACTIVE )= 0; 
                    }
                
                } 
            
            } 
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_7:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00008 /*Source_Name*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 482 );
    __LVINDEX = GetLocalLastModifiedArrayIndex ( S2_IESS_NoTouch_v2_1 ); 
    
    S2_IESS_NoTouch_v2_1_Exit__Event_8:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00009 /*SystemStart*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 486 );
    Globals->S2_IESS_NoTouch_v2_1.__GVSTARTREAD = 1; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 487 );
    if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FILENAME  )  ) > 0)) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 488 );
        S2_IESS_NoTouch_v2_1__FILEOPENCONFIG ( ) ; 
        }
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_9:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 0000A /*FileName*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 492 );
    if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_NoTouch_v2_1, __FILENAME  )  ) > 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 494 );
        if ( Globals->S2_IESS_NoTouch_v2_1.__GVSTARTREAD) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 495 );
            S2_IESS_NoTouch_v2_1__FILEOPENCONFIG ( ) ; 
            }
        
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__Event_10:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/
int S2_IESS_NoTouch_v2_1_SNOTOUCH_Constructor ( START_STRUCTURE_DEFINITION( S2_IESS_NoTouch_v2_1, SNOTOUCH ) * me, int nVerbose )
{
    InitIntArray( (struct IntArrayHdr_s* ) &me->SNOTOUCH__SOURCENOTOUCHENABLE, 0, 16, "SNOTOUCH__SOURCENOTOUCHENABLE" );
    InitIntArray( (struct IntArrayHdr_s* ) &me->SNOTOUCH__SOURCESYNC, 0, 16, "SNOTOUCH__SOURCESYNC" );
    InitIntArray( (struct IntArrayHdr_s* ) &me->SNOTOUCH__INPUTQUEUE, 0, 16, "SNOTOUCH__INPUTQUEUE" );
    return 0;
}
int S2_IESS_NoTouch_v2_1_SDISPLAY_Constructor ( START_STRUCTURE_DEFINITION( S2_IESS_NoTouch_v2_1, SDISPLAY ) * me, int nVerbose )
{
    InitIntArray( (struct IntArrayHdr_s* ) &me->SDISPLAY__SOURCEINPUT, 0, 16, "SDISPLAY__SOURCEINPUT" );
    return 0;
}

/********************************************************************************
  Destructor
********************************************************************************/
int S2_IESS_NoTouch_v2_1_SNOTOUCH_Destructor ( START_STRUCTURE_DEFINITION( S2_IESS_NoTouch_v2_1, SNOTOUCH ) * me, int nVerbose )
{
    return 0;
}
int S2_IESS_NoTouch_v2_1_SDISPLAY_Destructor ( START_STRUCTURE_DEFINITION( S2_IESS_NoTouch_v2_1, SDISPLAY ) * me, int nVerbose )
{
    return 0;
}

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_NoTouch_v2_1_POWEROFFALL_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00000 /*PowerOffAll*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
                
            }
            break;
            
        case __S2_IESS_NoTouch_v2_1_ENABLENOTOUCH_ON_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00001 /*EnableNoTouch_On*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_ENABLENOTOUCH_OFF_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00002 /*EnableNoTouch_Off*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_SYSTEMSTART_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00009 /*SystemStart*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
                
            }
            break;
            
        case __S2_IESS_NoTouch_v2_1_SOURCE_SYNC_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00003 /*Source_Sync*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_SOURCE_NOTOUCH_ENABLED_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00004 /*Source_NoTouch_Enabled*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_NoTouch_v2_1_POWER_OFF_TIME_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00005 /*Power_Off_Time*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_NUMBER_OF_DISPLAYS_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00006 /*Number_Of_Displays*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_DISPLAY_ROUTE_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00007 /*Display_Route*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_NoTouch_v2_1 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_NoTouch_v2_1_FILENAME_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 0000A /*FileName*/, 0 );
            break;
            
        case __S2_IESS_NoTouch_v2_1_SOURCE_NAME_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_NoTouch_v2_1, 00008 /*Source_Name*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_NoTouch_v2_1 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_NoTouch_v2_1 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_NoTouch_v2_1 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_INPUT_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_SYNC ) ;
    CHECK_INPUT_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_NOTOUCH_ENABLED ) ;
    CHECK_INPUT_ARRAY ( S2_IESS_NoTouch_v2_1, __DISPLAY_ROUTE ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_NAME ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_NoTouch_v2_1 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_NoTouch_v2_1 )
{
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTERDISP; 
    unsigned short  __LVCOUNTERINPUT; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_NoTouch_v2_1 );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_SYNC, IO_TYPE_DIGITAL_INPUT, __S2_IESS_NoTouch_v2_1_SOURCE_SYNC_DIG_INPUT, __S2_IESS_NoTouch_v2_1_SOURCE_SYNC_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_NOTOUCH_ENABLED, IO_TYPE_DIGITAL_INPUT, __S2_IESS_NoTouch_v2_1_SOURCE_NOTOUCH_ENABLED_DIG_INPUT, __S2_IESS_NoTouch_v2_1_SOURCE_NOTOUCH_ENABLED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __DISPLAY_ROUTE, IO_TYPE_ANALOG_INPUT, __S2_IESS_NoTouch_v2_1_DISPLAY_ROUTE_ANALOG_INPUT, __S2_IESS_NoTouch_v2_1_DISPLAY_ROUTE_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __ROUTE_SWITCHER, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTE_SWITCHER_ANALOG_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTE_SWITCHER_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __ROUTE_DISPLAY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTE_DISPLAY_ANALOG_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTE_DISPLAY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_NoTouch_v2_1, __ROUTED_SOURCE_NAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTED_SOURCE_NAME_STRING_OUTPUT, __S2_IESS_NoTouch_v2_1_ROUTED_SOURCE_NAME_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_NoTouch_v2_1, __FILENAME, e_INPUT_TYPE_STRING, __S2_IESS_NoTouch_v2_1_FILENAME_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_NoTouch_v2_1, __FILENAME, __S2_IESS_NoTouch_v2_1_FILENAME_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_NoTouch_v2_1, __SOURCE_NAME, e_INPUT_TYPE_STRING, __S2_IESS_NoTouch_v2_1_SOURCE_NAME_ARRAY_NUM_ELEMS, __S2_IESS_NoTouch_v2_1_SOURCE_NAME_ARRAY_NUM_CHARS, __S2_IESS_NoTouch_v2_1_SOURCE_NAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_NoTouch_v2_1, __SOURCE_NAME );
    
    {
        
        {
            INITIALIZE_GLOBAL_STRUCT_INTARRAY( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCENOTOUCHENABLE, 0, 16 );
            INITIALIZE_GLOBAL_STRUCT_INTARRAY( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__SOURCESYNC, 0, 16 );
            INITIALIZE_GLOBAL_STRUCT_INTARRAY( S2_IESS_NoTouch_v2_1, __GVNOTOUCH, SNOTOUCH__INPUTQUEUE, 0, 16 );
            
        }
    }
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __S2_IESS_NoTouch_v2_1_GVDISPLAYS_STRUCT_MAX_LEN );
    
    {
        unsigned short i = 0;
        for( i = 0; i <= 16; i++ )
        {
            InitIntArray( (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, i )->SDISPLAY__SOURCEINPUT, 0, 16, "SDISPLAY__SOURCEINPUT" );
            
        }
    }
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_NoTouch_v2_1, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 504 );
    __FN_FOREND_VAL__1 = 16; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTERDISP = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTERDISP  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTERDISP  >= __FN_FOREND_VAL__1) ; __LVCOUNTERDISP  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 506 );
        __FN_FOREND_VAL__2 = 16; 
        __FN_FORINIT_VAL__2 = 1; 
        for( __LVCOUNTERINPUT = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTERINPUT  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTERINPUT  >= __FN_FOREND_VAL__2) ; __LVCOUNTERINPUT  += __FN_FORINIT_VAL__2) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 508 );
             SetIntArrayElement( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), (struct IntArrayHdr_s* ) & GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_NoTouch_v2_1, __GVDISPLAYS, SDISPLAY, __LVCOUNTERDISP )-> SDISPLAY__SOURCEINPUT  , 0, __LVCOUNTERINPUT , 1 ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 506 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_NoTouch_v2_1 ), 504 );
        } 
    
    
    S2_IESS_NoTouch_v2_1_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }


