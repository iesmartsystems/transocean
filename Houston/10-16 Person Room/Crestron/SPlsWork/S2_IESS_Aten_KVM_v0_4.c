#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Aten_KVM_v0_4.h"

FUNCTION_MAIN( S2_IESS_Aten_KVM_v0_4 );
EVENT_HANDLER( S2_IESS_Aten_KVM_v0_4 );
DEFINE_ENTRYPOINT( S2_IESS_Aten_KVM_v0_4 );

static void S2_IESS_Aten_KVM_v0_4__SENDSTRING ( struct StringHdr_s* __LVINCOMING ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 128 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , GENERIC_STRING_OUTPUT( S2_IESS_Aten_KVM_v0_4 )  ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVINCOMING  )  ,    & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__CR ) )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_KVM_TX$_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Aten_KVM_v0_4 ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 129 );
    CREATE_WAIT( S2_IESS_Aten_KVM_v0_4, 300, CONNECTIONUPDATE );
    
    
    S2_IESS_Aten_KVM_v0_4_Exit__SENDSTRING:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_Aten_KVM_v0_4, CONNECTIONUPDATE )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 131 );
    Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 132 );
    SetDigital ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_STATUS_CONNECT_DIG_OUTPUT, Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS) ; 
    

S2_IESS_Aten_KVM_v0_4_Exit__CONNECTIONUPDATE:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
static void S2_IESS_Aten_KVM_v0_4__STARTHEARTBEAT ( ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 137 );
    CREATE_WAIT( S2_IESS_Aten_KVM_v0_4, GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND,SKVM__TIMER ), HEARTBEAT );
    
    
    S2_IESS_Aten_KVM_v0_4_Exit__STARTHEARTBEAT:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_Aten_KVM_v0_4, HEARTBEAT )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 139 );
    if ( Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 140 );
        S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__HEARTBEAT )) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 142 );
        S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__OPEN )) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 143 );
    S2_IESS_Aten_KVM_v0_4__STARTHEARTBEAT ( ) ; 
    

S2_IESS_Aten_KVM_v0_4_Exit__HEARTBEAT:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
static void S2_IESS_Aten_KVM_v0_4__RESETUSB ( ) 
    { 
    /* Begin local function variable declarations */
    
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 148 );
    CREATE_WAIT( S2_IESS_Aten_KVM_v0_4, 300, __SPLS_TMPVAR__WAITLABEL_0__ );
    
    
    S2_IESS_Aten_KVM_v0_4_Exit__RESETUSB:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__WAITLABEL_0__ )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    {
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 149 );
    S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__RESETUSB )) ; 
    }

S2_IESS_Aten_KVM_v0_4_Exit____SPLS_TMPVAR__WAITLABEL_0__:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00000 /*Connect*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 157 );
    S2_IESS_Aten_KVM_v0_4__STARTHEARTBEAT ( ) ; 
    
    S2_IESS_Aten_KVM_v0_4_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00001 /*KVM_Input*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVSTRING, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 163 );
    __LVINDEX = GetAnalogInput( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_KVM_INPUT_ANALOG_INPUT ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 164 );
    FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,7 , "sw i0%u"  , (unsigned short )( __LVINDEX ) )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , LOCAL_STRING_STRUCT( __LVSTRING  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 165 );
    S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVSTRING  )  ) ; 
    
    S2_IESS_Aten_KVM_v0_4_Exit__Event_1:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __LVSTRING );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00002 /*KVM_Mode*/ )

    {
    /* Begin local function variable declarations */
    
    unsigned short  __LVINDEX; 
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVSTRING, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 50 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 171 );
    __LVINDEX = GetAnalogInput( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_KVM_MODE_ANALOG_INPUT ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 172 );
    
        {
        int __SPLS_TMPVAR__SWTCH_1__ = ( GetAnalogInput( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_KVM_MODE_ANALOG_INPUT )) ;
        
            { 
            if ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 174 );
                FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,11 , "display sst" , __FN_DST_STR__  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , LOCAL_STRING_STRUCT( __LVSTRING  )   ,2 , "%s" , __FN_DST_STR__  ) ; 
                }
            
            else if ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) )
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 175 );
                FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,11 , "display mst" , __FN_DST_STR__  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , LOCAL_STRING_STRUCT( __LVSTRING  )   ,2 , "%s" , __FN_DST_STR__  ) ; 
                }
            
            } 
            
        }
        
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 177 );
    S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVSTRING  )  ) ; 
    
    S2_IESS_Aten_KVM_v0_4_Exit__Event_2:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __LVSTRING );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00003 /*KVM_RX$*/ )

    {
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING, 100 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVRX, 100 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVRX );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVTRASH, 100 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVTRASH );
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVINDEX; 
    unsigned short  __LVLEVEL; 
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "\x0D""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "Welcome" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( "Please Open RS232 Function!" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "Command OK" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_4__, sizeof( "sw i01" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_4__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_5__, sizeof( "sw i02" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_5__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__, 1000 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__1, 1000 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__1 );
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVSTRING );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVSTRING, 100 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVRX );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVRX, 100 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 100 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "\x0D""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "Welcome" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, "Please Open RS232 Function!" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "Command OK" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_4__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__, "sw i01" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_5__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__, "sw i02" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__ );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__, 1000 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR__1, 1000 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 183 );
    if ( Len( GLOBAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __KVM_RX$  )  )) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 184 );
        CancelWait ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), "CONNECTIONUPDATE" ) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 185 );
    FormatString(INSTANCE_PTR(S2_IESS_Aten_KVM_v0_4),LOCAL_STRING_STRUCT( __FN_DST_STR__ ) ,4,"%s%s",&GLOBAL_STRUCT_FIELD(S2_IESS_Aten_KVM_v0_4,__KVM_RX_COMMAND,SLOCALKVM__CRXQUEUE),GLOBAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __KVM_RX$  ) );
FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_RX_COMMAND, SLOCALKVM__CRXQUEUE )  ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 186 );
    while ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )  , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_RX_COMMAND, SLOCALKVM__CRXQUEUE ) , 1 , 1 )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 188 );
        FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )   ,   & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_RX_COMMAND, SLOCALKVM__CRXQUEUE )  , 1  )  )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , LOCAL_STRING_STRUCT( __LVRX  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 189 );
        FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ,  LOCAL_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVRX  )  ) - 1), LOCAL_STRING_STRUCT( __LVRX  )  )  )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , LOCAL_STRING_STRUCT( __LVRX  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 190 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVRX  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 192 );
            Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS = 1; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 193 );
            SetDigital ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_STATUS_CONNECT_DIG_OUTPUT, Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 194 );
            S2_IESS_Aten_KVM_v0_4__STARTHEARTBEAT ( ) ; 
            } 
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 196 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )  , LOCAL_STRING_STRUCT( __LVRX  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 198 );
                Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS = 0; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 199 );
                SetDigital ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_STATUS_CONNECT_DIG_OUTPUT, Globals->S2_IESS_Aten_KVM_v0_4.__GLBL_KVM_STATUS) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 200 );
                S2_IESS_Aten_KVM_v0_4__SENDSTRING (  (struct StringHdr_s* )  (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__OPEN )) ; 
                } 
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 202 );
                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )  , LOCAL_STRING_STRUCT( __LVRX  )  , 1 , 1 )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 204 );
                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )  , LOCAL_STRING_STRUCT( __LVRX  )  , 1 , 1 )) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 206 );
                        SetAnalog ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_STATUS_INPUT_ANALOG_OUTPUT, 1) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 207 );
                        S2_IESS_Aten_KVM_v0_4__RESETUSB ( ) ; 
                        } 
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 209 );
                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )  , LOCAL_STRING_STRUCT( __LVRX  )  , 1 , 1 )) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 211 );
                            SetAnalog ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), __S2_IESS_Aten_KVM_v0_4_STATUS_INPUT_ANALOG_OUTPUT, 2) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 212 );
                            S2_IESS_Aten_KVM_v0_4__RESETUSB ( ) ; 
                            } 
                        
                        }
                    
                    } 
                
                }
            
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 186 );
        } 
    
    
    S2_IESS_Aten_KVM_v0_4_Exit__Event_3:
    /* Begin Free local function variables */
FREE_LOCAL_STRING_STRUCT( __LVSTRING );
FREE_LOCAL_STRING_STRUCT( __LVRX );
FREE_LOCAL_STRING_STRUCT( __LVTRASH );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
FREE_LOCAL_STRING_STRUCT( __FN_DST_STR__1 );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ );
FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ );
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/
int S2_IESS_Aten_KVM_v0_4_SKVM_Constructor ( START_STRUCTURE_DEFINITION( S2_IESS_Aten_KVM_v0_4, SKVM ) * me, int nVerbose )
{
    InitStringStruct( (struct StringHdr_s* ) &me->SKVM__HEARTBEAT, e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_HEARTBEAT_STRING_MAX_LEN, "SKVM__HEARTBEAT" );
    InitStringStruct( (struct StringHdr_s* ) &me->SKVM__OPEN, e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_OPEN_STRING_MAX_LEN, "SKVM__OPEN" );
    InitStringStruct( (struct StringHdr_s* ) &me->SKVM__CR, e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_CR_STRING_MAX_LEN, "SKVM__CR" );
    InitStringStruct( (struct StringHdr_s* ) &me->SKVM__RESETUSB, e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_RESETUSB_STRING_MAX_LEN, "SKVM__RESETUSB" );
    return 0;
}
int S2_IESS_Aten_KVM_v0_4_SLOCALKVM_Constructor ( START_STRUCTURE_DEFINITION( S2_IESS_Aten_KVM_v0_4, SLOCALKVM ) * me, int nVerbose )
{
    InitStringStruct( (struct StringHdr_s* ) &me->SLOCALKVM__CRXQUEUE, e_INPUT_TYPE_NONE, SLOCALKVM__S2_IESS_Aten_KVM_v0_4_CRXQUEUE_STRING_MAX_LEN, "SLOCALKVM__CRXQUEUE" );
    return 0;
}

/********************************************************************************
  Destructor
********************************************************************************/
int S2_IESS_Aten_KVM_v0_4_SKVM_Destructor ( START_STRUCTURE_DEFINITION( S2_IESS_Aten_KVM_v0_4, SKVM ) * me, int nVerbose )
{
    return 0;
}
int S2_IESS_Aten_KVM_v0_4_SLOCALKVM_Destructor ( START_STRUCTURE_DEFINITION( S2_IESS_Aten_KVM_v0_4, SLOCALKVM ) * me, int nVerbose )
{
    return 0;
}

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Aten_KVM_v0_4_CONNECT_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00000 /*Connect*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Aten_KVM_v0_4_KVM_INPUT_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00001 /*KVM_Input*/, 0 );
            break;
            
        case __S2_IESS_Aten_KVM_v0_4_KVM_MODE_ANALOG_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00002 /*KVM_Mode*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Aten_KVM_v0_4 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Aten_KVM_v0_4_KVM_RX$_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Aten_KVM_v0_4, 00003 /*KVM_RX$*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Aten_KVM_v0_4 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Aten_KVM_v0_4 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Aten_KVM_v0_4 )
    {
    SAVE_GLOBAL_POINTERS ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Aten_KVM_v0_4 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Aten_KVM_v0_4 )
{
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "open" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( "status on" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "usbreset on" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR___M, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR___M );
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_Aten_KVM_v0_4 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "open" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, "status on" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "usbreset on" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Aten_KVM_v0_4, __FN_DST_STR___M );
    INITIALIZE_LOCAL_STRING_STRUCT( __FN_DST_STR___M, 50 );
    
    
    /* End local function variable declarations */
    
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Aten_KVM_v0_4, __KVM_RX$, e_INPUT_TYPE_STRING, __S2_IESS_Aten_KVM_v0_4_KVM_RX$_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Aten_KVM_v0_4, __KVM_RX$, __S2_IESS_Aten_KVM_v0_4_KVM_RX$_STRING_INPUT );
    
    {
        
        {
            InitStringStruct( (struct StringHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__HEARTBEAT ), e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_HEARTBEAT_STRING_MAX_LEN, "SKVM__HEARTBEAT" );
            InitStringStruct( (struct StringHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__OPEN ), e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_OPEN_STRING_MAX_LEN, "SKVM__OPEN" );
            InitStringStruct( (struct StringHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__CR ), e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_CR_STRING_MAX_LEN, "SKVM__CR" );
            InitStringStruct( (struct StringHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__RESETUSB ), e_INPUT_TYPE_NONE, SKVM__S2_IESS_Aten_KVM_v0_4_RESETUSB_STRING_MAX_LEN, "SKVM__RESETUSB" );
            
        }
    }
    
    {
        
        {
            InitStringStruct( (struct StringHdr_s* ) & GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_RX_COMMAND, SLOCALKVM__CRXQUEUE ), e_INPUT_TYPE_NONE, SLOCALKVM__S2_IESS_Aten_KVM_v0_4_CRXQUEUE_STRING_MAX_LEN, "SLOCALKVM__CRXQUEUE" );
            
        }
    }
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Aten_KVM_v0_4, __SOCKETBUFF, e_INPUT_TYPE_NONE, __S2_IESS_Aten_KVM_v0_4_SOCKETBUFF_STRING_MAX_LEN );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Aten_KVM_v0_4, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 225 );
    FormatString(INSTANCE_PTR(S2_IESS_Aten_KVM_v0_4),LOCAL_STRING_STRUCT( __FN_DST_STR___M ) ,2,"%s",LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ ) );
FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__OPEN )  ,2 , "%s"  , __FN_DST_STR___M ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 226 );
    FormatString(INSTANCE_PTR(S2_IESS_Aten_KVM_v0_4),LOCAL_STRING_STRUCT( __FN_DST_STR___M ) ,2,"%s",LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ ) );
FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__CR )  ,2 , "%s"  , __FN_DST_STR___M ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 227 );
    FormatString(INSTANCE_PTR(S2_IESS_Aten_KVM_v0_4),LOCAL_STRING_STRUCT( __FN_DST_STR___M ) ,2,"%s",LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ ) );
FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__HEARTBEAT )  ,2 , "%s"  , __FN_DST_STR___M ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 228 );
    FormatString(INSTANCE_PTR(S2_IESS_Aten_KVM_v0_4),LOCAL_STRING_STRUCT( __FN_DST_STR___M ) ,2,"%s",LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ ) );
FormatString ( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ) , (struct StringHdr_s* )& GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND, SKVM__RESETUSB )  ,2 , "%s"  , __FN_DST_STR___M ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Aten_KVM_v0_4 ), 229 );
    GLOBAL_STRUCT_FIELD( S2_IESS_Aten_KVM_v0_4, __KVM_COMMAND,SKVM__TIMER )= 6000; 
    
    S2_IESS_Aten_KVM_v0_4_Exit__MAIN:/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_STRING_STRUCT( __FN_DST_STR___M );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }


