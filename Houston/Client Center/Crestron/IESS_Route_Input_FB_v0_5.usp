/*
Dealer Name: i.e.SmartSytems
System Name: IESS Route Input FB v0.5
System Number:
Programmer: Matthew Laletas (edited by Jesus Barrera)
Comments: IESS Route Input FB v0.5
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_INPUT 16
#DEFINE_CONSTANT NUM_OUTPUT 16
#DEFINE_CONSTANT NUM_DISPLAY 16
#HELP_PDF_FILE "IESS Route Input FB v0_5.pdf"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
ANALOG_INPUT SwitcherFB_Output[NUM_OUTPUT], _SKIP_, SourceInput[NUM_INPUT], _SKIP_, OutputNum_Display[NUM_DISPLAY], _SKIP_;
STRING_INPUT InputNames[NUM_INPUT][50];
STRING_OUTPUT LastSelectedSourceName;
ANALOG_OUTPUT LastSelectedSourceNum, _SKIP_, SourceNum_Display[NUM_DISPLAY], _SKIP_;
STRING_OUTPUT SourceName_Display[NUM_DISPLAY];
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER gvSourceInput[NUM_INPUT];
INTEGER gvDisplayOutput[NUM_DISPLAY];
/*******************************************************************************************
  Functions
*******************************************************************************************/
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
CHANGE SwitcherFB_Output
{
	INTEGER lvOutputIndex, lvSourceIndex, lvCounter, lvBreak;
	lvOutputIndex = GETLASTMODIFIEDARRAYINDEX();
	IF( SwitcherFB_Output[lvOutputIndex] = 0 )
	{
		LastSelectedSourceName = "";
		LastSelectedSourceNum = 0;
		FOR( lvCounter = 0 TO NUM_DISPLAY )
		{
			IF( gvDisplayOutput[lvCounter] = lvOutputIndex )
			{
				SourceNum_Display[lvCounter] = 0;
				SourceName_Display[lvCounter] = "";	
			}
		}	
	}
	ELSE
	{
		FOR( lvSourceIndex = 1 TO NUM_INPUT )
		{
			IF( SwitcherFB_Output[lvOutputIndex] = gvSourceInput[lvSourceIndex] )
			{
				LastSelectedSourceName = InputNames[lvSourceIndex];
				LastSelectedSourceNum = lvSourceIndex;
				FOR( lvCounter = 0 TO NUM_DISPLAY )
				{
					IF( gvDisplayOutput[lvCounter] = lvOutputIndex )
					{
						SourceNum_Display[lvCounter] = lvSourceIndex;
						SourceName_Display[lvCounter] = InputNames[lvSourceIndex];
						lvBreak = 1;
						BREAK;
					}
				}
				IF( lvBreak )
					BREAK;
			}
		}
	}
}
CHANGE SourceInput
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	gvSourceInput[lvIndex] = SourceInput[lvIndex];
}

CHANGE OutputNum_Display
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
    IF( OutputNum_Display[lvIndex] > 0 && OutputNum_Display[lvIndex] <= NUM_DISPLAY )
		gvDisplayOutput[lvIndex] = OutputNum_Display[lvIndex];
}
/*******************************************************************************************
  Main()
*******************************************************************************************/
FUNCTION Main()
{
	INTEGER lvCounter;
	/*FOR( lvCounter = 1 TO NUM_INPUT )
	{
		gvSourceInput[lvCounter] = lvCounter;
	}*/
	FOR( lvCounter = 1 TO NUM_DISPLAY )
	{
		gvDisplayOutput[lvCounter] = lvCounter;
	}
}

