/*
Dealer Name: i.e.SmartSytems
System Name: IESS NoTouch v2_0
System Number:
Programmer: Matthew Laletas, edited by Jesus Barrera
Comments: IESS NoTouch v2_0
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#DEFINE_CONSTANT NUM_SOURCES 16
#DEFINE_CONSTANT NUM_DISPLAYS 16
#HELP "IESS NoTouch v1_0"
#HELP ""
#HELP "INPUTS:"
#HELP "ResetManualRoute: If a user manually routes a source it will disable NoTouch until either this gets pulsed or the" 
#HELP "		room gets powered off."
#HELP "PowerOffAll: Switches all outputs to 0 (RouteSwitcher_Display) and 99 (InputSelect_Display)."
#HELP "SignalSync_Source[x]: When this is held high will NoTouch if the respective EnableNoTouch_Source[x] is high."
#HELP "EnableNoTouch_Source[x]: Enable this when you want the respective SYNC[x] to be NoTouchable."
#HELP "PowerOffTime: Send an analog value (120d = 120 secs, 2mins) to turn off all outputs when all NoTouch sources do"
#HELP "		not have sync."
#HELP "NumberOfDisplays: Send an analog value designating how many displays you are using."
#HELP "RouteSourceToDisplay[x]: Allows manual selection for each source, the x represents which output."
#HELP "Name_Source[x]: If you want to use strings indicating what source is routed then enter the strings for each source."
#HELP ""
#HELP "OUTPUTS:"
#HELP "RouteSwitcher_Display[x]: Send this into the switch under the appropiate output."
#HELP "InputSelect_Display[x]: Send this into the Display Module input."
#HELP "SourceName_Display[x]: Shows what source is routed to the respective output, this requires you to enter strings"
#HELP "		into the Name_Source[x] input."
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT PowerOffAll, _SKIP_, EnableNoTouch_On, EnableNoTouch_Off, _SKIP_, SystemStart, _SKIP_;
ANALOG_INPUT Power_Off_Time,_SKIP_, Number_Of_Displays, _SKIP_;
STRING_INPUT FileName[255], _SKIP_;
DIGITAL_INPUT Source_Sync[NUM_SOURCES], _SKIP_, Source_NoTouch_Enabled[NUM_SOURCES], _SKIP_;
ANALOG_INPUT Display_Route[NUM_DISPLAYS], _SKIP_;
STRING_INPUT Source_Name[NUM_SOURCES][31];

DIGITAL_OUTPUT NoTouchOn_Pulse, NoTouchOff_Pulse, _SKIP_;
ANALOG_OUTPUT Route_Switcher[NUM_DISPLAYS], _SKIP_, Route_Display[NUM_DISPLAYS], _SKIP_;
STRING_OUTPUT Routed_Source_Name[NUM_DISPLAYS];
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
STRUCTURE sNoTouch
{
	INTEGER NoTouchOn;
	INTEGER NoTouchOff;
	INTEGER SourceNoTouchEnable[NUM_SOURCES];
	INTEGER SourceSync[NUM_SOURCES];
	LONG_INTEGER NoTouchOffTime;
	LONG_INTEGER Countdown;
	INTEGER NumberOfDisplays;
	INTEGER StatusNoTouchActive;
	INTEGER InputQueue[NUM_SOURCES];
};
STRUCTURE sDisplay
{
	INTEGER SourceInput[NUM_SOURCES];
};
sNoTouch gvNoTouch;
sDisplay gvDisplays[NUM_DISPLAYS];
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
INTEGER gvStartRead;
/*******************************************************************************************
  Functions
*******************************************************************************************/
INTEGER_FUNCTION CheckSourceSync()											//Confirms that all syncs are off with a 1, if there are any syncs it returns 0
{
	INTEGER lvCounter;
	FOR( lvCounter = 1 TO NUM_SOURCES )										//Run through sources
	{
		IF( gvNoTouch.SourceNoTouchEnable[lvCounter] )								//NoTouch is enabled for this source
		{
			IF( gvNoTouch.SourceSync[lvCounter] )										//Sync is detected
				RETURN( 0 );												//Dont start countdown
		}
	}
	RETURN( 1 );										
}
INTEGER_FUNCTION CheckAutoOff()												//Confirms that all syncs are off with a 1, if there are any syncs it returns 0
{
	INTEGER lvCounter, lvCheckDisplays, lvCheckDisplaysNoTouch;
	lvCheckDisplays = 0;
	lvCheckDisplaysNoTouch = 0;
	FOR( lvCounter = 1 TO gvNoTouch.NumberofDisplays )								//Loop through displays
	{
		IF( Route_Switcher[lvCounter] )										//Display has source route
		{
			lvCheckDisplays = lvCheckDisplays + 1;										
			IF( gvNoTouch.SourceNoTouchEnable[Route_Switcher[lvCounter]] )				//Source is Notouch Enabled
			{
				lvCheckDisplaysNoTouch = lvCheckDisplaysNoTouch + 1;
				IF( gvNoTouch.SourceSync[Route_Switcher[lvCounter]] )					//Sync is detected
					RETURN( 0 );											//Dont start countdown
			}
		}
	}																		
	IF( lvCheckDisplays )													//Display has source routed
	{
		IF( lvCheckDisplaysNoTouch = gvNoTouch.NumberofDisplays ) 					//NoTouch enabled for all displays
			RETURN( 1 );													//Start Countdown
		ELSE IF( lvCheckDisplaysNoTouch < gvNoTouch.NumberofDisplays )				//Source being used by a non no touch
			RETURN( 0 );													//Dont start countdown
	}										
}
FUNCTION SetOutputs( INTEGER lvIndex, INTEGER lvOutput )		//Routes Inputs to Outputs
{
	INTEGER lvCounter;
	IF( lvIndex = 0 )												//Off
	{	
		Route_Display[lvOutput] = 99;									//Set Analog output to 99 for Off
		Route_Switcher[lvOutput] = lvIndex;								//Set Analog output to the source
		Routed_Source_Name[lvOutput] = "";							//Set name to empty
	}
	ELSE															//On
	{
		Route_Display[lvOutput] = gvDisplays[lvOutput].SourceInput[lvIndex];				//Set Analog output to corresponding display input
		Route_Switcher[lvOutput] = lvIndex;								//Set Analog output to the source
		Routed_Source_Name[lvOutput] = Source_Name[lvIndex];			//Set name to to select source
	}
}
FUNCTION StartCountdown()										//Begins countdown to turn off displays
{
	IF( gvNoTouch.NoTouchOffTime )												//Something is entered into this analog value
	{
		WHILE( gvNoTouch.StatusNoTouchActive )
		{
			WAIT( 100 )							//Same value but * 100 for hundrenths of seconds
			{
				INTEGER lvCounter;
				gvNoTouch.Countdown = gvNoTouch.Countdown - 1;
				TRACE( "Countdown %ld", gvNoTouch.Countdown );
				IF( gvNoTouch.Countdown < 1 )
				{
					FOR( lvCounter = 1 TO NUM_DISPLAYS )						//Loop through all displays
					{
						SetOutputs( 0, lvCounter );									//Turn off the display								
					}
					PULSE( 20, NoTouchOff_Pulse );
					gvNoTouch.StatusNoTouchActive = 0;
				} 
			}
		}
	}
}
FUNCTION UpdateDisplays()										//NoTouch feature with the auto-distribution with the input queue
{
	INTEGER lvCounter, lvActiveInputs, lvMod, lvDiv, lvCount, lvInput;
	FOR( lvCounter = 1 TO NUM_SOURCES )								//Scroll through all inputs
	{
		IF( gvNoTouch.InputQueue[lvCounter] )							//Find the highest index that is synced
			lvActiveInputs = lvCounter;										//Set it up
	}
	IF( lvActiveInputs > gvNoTouch.NumberOfDisplays )				//If the # of inputs exceeds the # of Displays
		lvActiveInputs = gvNoTouch.NumberOfDisplays;					//Set it up with max of displays
	IF( lvActiveInputs )											//Prevent divide by 0 mistake, :<
	{
		lvMod = gvNoTouch.NumberOfDisplays % lvActiveInputs;			//Remainder
		lvDiv = gvNoTouch.NumberOfDisplays / lvActiveInputs;			//Divisor
		lvCount = 1;
		lvInput = 1;
		IF( lvMod )														//If there is a remainder
		{
			FOR( lvCounter = 1 TO gvNoTouch.NumberOfDisplays )				//Loop through displays
			{
	            IF( lvCount > lvDiv )											//Divisor counter more than the divisor
				{
					IF( lvCount = ( lvDiv + 1 ) )									//Divisor counter is equal to the divisor by one accounting for the cases of the mod
					{
						IF( lvInput > lvMod )											//Input is more than the mod so start over as normal, if it is less than or more importantly equal to mod dont do anything
						{
							lvCount = 1;													//Start divisor counter over
							lvInput = lvInput + 1;											//Next input
						}
					}
					ELSE															//Divisor counter is more than the divisor plus so start over as normal
					{
						lvCount = 1;													//Start divisor counter over
						lvInput = lvInput + 1;											//Next input
					} 
				}
				Route_Display[lvCounter] = gvDisplays[lvCounter].SourceInput[gvNoTouch.InputQueue[lvInput]];//Switch the display
				Route_Switcher[lvCounter] = gvNoTouch.InputQueue[lvInput];				//Switch the output
				Routed_Source_Name[lvCounter] = Source_Name[gvNoTouch.InputQueue[lvInput]];//Set name to to select source
				lvCount = lvCount + 1;											//Add the divisor counter
			}
		}
		ELSE															//No remainder much easier
		{
			FOR( lvCounter = 1 TO gvNoTouch.NumberOfDisplays )						//Loop through displays
			{
				IF( lvCount > lvDiv )											//Divisor counter is more than the divisor so start over as normal
				{
					lvCount = 1;													//Start divisor counter over
					lvInput = lvInput + 1;											//Next input
				}
				Route_Display[lvCounter] = gvDisplays[lvCounter].SourceInput[gvNoTouch.InputQueue[lvInput]];//Switch the display
				Route_Switcher[lvCounter] = gvNoTouch.InputQueue[lvInput];				//Switch the output
				Routed_Source_Name[lvCounter] = Source_Name[gvNoTouch.InputQueue[lvInput]];//Set name to to select source
				lvCount = lvCount + 1;											//Add the divisor counter
			}
		}
	}
}
FUNCTION TriggerNoTouch( INTEGER lvIndex, INTEGER lvValue )		//Start NoTouch
{
	INTEGER lvCounter, lvSourcePrevUsed;
	IF( lvValue )													//Sync detected
	{
		IF( gvNoTouch.SourceNoTouchEnable[lvIndex] )								//Input has NoTouch Enabled
		{
			PULSE( 20, NoTouchOn_Pulse );
			gvNoTouch.StatusNoTouchActive = 0;
			UpdateDisplays();
		}
	}
	ELSE IF( !lvValue )												//Sync not detected
	{							
		IF( !CheckAutoOff() )											//No Touch disabled or Active Sync being used
			gvNoTouch.StatusNoTouchActive = 0;								//Disable countdown
		IF( !CheckSourceSync() )										//Still active No Touch source
		{
			FOR( lvCounter = 1 TO gvNoTouch.NumberOfDisplays )						//Loop through displays
			{
				IF( lvIndex = Route_Switcher[lvCounter] )				//Source disconnected was being used
				{
					UpdateDisplays();												//Update displays with active NoTouch sources
					gvNoTouch.StatusNoTouchActive = 0;								//Disable countdown
					RETURN;											
				}
			}
		}
		IF( CheckAutoOff() && !gvNoTouch.StatusNoTouchActive )			//Displays are black, Countdown is not running
		{
			gvNoTouch.Countdown = gvNoTouch.NoTouchOffTime;
			gvNoTouch.StatusNoTouchActive = 1;
			StartCountdown();												//Run the Countdown																				
		}
	}
}
INTEGER_FUNCTION CheckQueueInputs( INTEGER lvInput )			//Returns whether or not the input is already in the queue, prevents duplicates
{
    INTEGER lvCounter, lvActive;
    FOR( lvCounter = 1 TO NUM_SOURCES )								//Scroll through sources
    {
		IF( lvInput = gvNoTouch.InputQueue[lvCounter] )					//If input in the slot
	    	lvActive = 1;													//Already active this will be returned
    }
    IF( lvActive )													//If something was active
		RETURN ( lvActive );											//Return true
    ELSE															//Nothing was active
		RETURN ( 0 );													//Return nothing
}

FUNCTION FormatInputQueue( INTEGER lvInput, INTEGER lvActive )	//Moves around inputs in the queue respective to if the input is active or not
{
    INTEGER lvQueue, lvCounter;
    IF( lvActive && !CheckQueueInputs( lvInput ) )					//Active input that doesnt already occupy the queue
    {
		FOR( lvCounter = NUM_SOURCES TO 2 STEP - 1 )						//Scroll through sources backwards up to the 2nd 
		{
	    	gvNoTouch.InputQueue[lvCounter] = gvNoTouch.InputQueue[lvCounter - 1];	//Move up the previous to the current (remember backwards)
		}
		gvNoTouch.InputQueue[1] = lvInput;									//Set input to the 1st
    }
    ELSE IF( !lvActive )											//Inactive
    {
		FOR( lvCounter = 1 TO NUM_SOURCES )								//Scroll through sources normally
		{
	    	IF( gvNoTouch.InputQueue[lvCounter] = lvInput )						//If this input is in the queue
				gvNoTouch.InputQueue[lvCounter] = 0;									//Remove it
		}
		FOR( lvCounter = 1 TO ( NUM_SOURCES - 1 ) )						//Scroll through sources ignore the last
		{ 
	   		IF( !gvNoTouch.InputQueue[lvCounter] )								//If the slot is empty
	    	{
				gvNoTouch.InputQueue[lvCounter] = gvNoTouch.InputQueue[lvCounter + 1];	//Move the next slot to the current
				gvNoTouch.InputQueue[lvCounter + 1] = 0;								//Next slot make 0
	    	}
		}
    }
}
FUNCTION ParseDataFromConfig( STRING lvData )				//Parse data fun
{
    INTEGER lvIndex, lvInstanceIndex, lvCounterInput;
	STRING lvIndexTemp[31], lvTrash[31], lvInputTemp[7];
    //Comment ignore
    IF( FIND( "//", lvData ) )										//Comment ignore
    {
    }
    //No comments so go go go
    ELSE															//No Comment
    {
		IF( FIND( "Display ", lvData ) )								//Displays Section
		{
		    lvTrash = REMOVE( "Display ", lvData );
		    lvIndexTemp = REMOVE( ":", lvData );
		    lvIndexTemp = REMOVEBYLENGTH( LEN( lvIndexTemp ) - 1, lvIndexTemp );
		    lvIndex = ATOI( lvIndexTemp );	
			lvCounterInput = 1;
			IF( FIND( "SourceInputs-", lvData ) )
			{
				lvTrash = REMOVE( "SourceInputs-", lvData );
				WHILE( FIND( ",", lvData ) )
				{
					lvInputTemp = REMOVE( ",", lvData );
					lvInputTemp = REMOVEBYLENGTH( LEN( lvInputTemp ) - 1, lvInputTemp );
					IF( ATOI( lvInputTemp ) > 0 )
						gvDisplays[lvIndex].SourceInput[lvCounterInput] = ATOI( lvInputTemp );
					lvCounterInput = lvCounterInput + 1;
					IF( lvCounterInput > NUM_SOURCES )
						BREAK;
					IF( lvCounterInput <= NUM_SOURCES )
					{
						IF( ATOI( lvData ) > 0 )
							gvDisplays[lvIndex].SourceInput[lvCounterInput] = ATOI( lvInputTemp );
					}
				}
			}
		}
	}
}
FUNCTION FileOpenConfig()
{
	INTEGER lvRead;
	SIGNED_INTEGER lvHandle;
	STRING lvReadFile[16383], lvReadline[255], lvFileName[127], lvTrash[63];
	IF( LEN( FileName ) > 0 )
	{
		lvFileName = "\\NVRAM\\" + FileName;
		STARTFILEOPERATIONS();
		#IF_SERIES2
			lvHandle = FILEOPEN( lvFileName, _O_RDONLY );
		#ELSE
			lvHandle = FILEOPENSHARED( lvFileName, _O_TEXT | _O_RDONLY );
		#ENDIF
		IF( lvHandle >= 0 )												//File opened successfully
		{
			lvRead = FILEREAD( lvHandle, lvReadFile, 16383 );
			WHILE( FIND( "\x0D\x0A", lvReadFile ) )
			{
				lvReadLine = REMOVE( "\x0D\x0A", lvReadFile );
				lvReadLine = REMOVEBYLENGTH( LEN( lvReadLine ) - 2, lvReadLine );
				ParseDataFromConfig( lvReadLine );	
			}
			//Check Last Line
			lvTrash = REMOVE( lvReadLine, lvReadFile );
			IF( LEN( lvReadFile ) > 2 )
				ParseDataFromConfig( lvReadFile );
			lvRead = FILECLOSE( lvHandle );
			ENDFILEOPERATIONS();
		}
	}
}
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
PUSH PowerOffAll
{
	INTEGER lvCounter;
	FOR( lvCounter = 1 TO NUM_DISPLAYS )
	{
		SetOutputs( 0, lvCounter );												//Turn off the system								
	}
	gvNoTouch.StatusNoTouchActive = 0;
}
CHANGE EnableNoTouch_On
{
	gvNoTouch.NoTouchOn = EnableNoTouch_On;	

}
CHANGE EnableNoTouch_Off
{
	gvNoTouch.NoTouchOff = EnableNoTouch_Off;
	IF( gvNoTouch.NoTouchOff )
	{
		IF( CheckAutoOff() && !gvNoTouch.StatusNoTouchActive )			//Displays are black, Countdown is not running
		{
			gvNoTouch.Countdown = gvNoTouch.NoTouchOffTime;
			gvNoTouch.StatusNoTouchActive = 1;
			StartCountdown();										//Run the Countdown																				
		}
	}
	IF( !EnableNoTouch_Off )
		gvNoTouch.StatusNoTouchActive = 0;		
}
CHANGE Source_Sync
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	gvNoTouch.SourceSync[lvIndex] = Source_Sync[lvIndex];
	FormatInputQueue( lvIndex, Source_Sync[lvIndex] );					//Setup the queue
	IF( gvNoTouch.SourceSync[lvIndex] && gvNoTouch.NoTouchOn )
		TriggerNoTouch( lvIndex, gvNoTouch.SourceSync[lvIndex] );				//Trigger NoTouch on
	ELSE IF( !gvNoTouch.SourceSync[lvIndex] && gvNoTouch.NoTouchOff )
		TriggerNoTouch( lvIndex, gvNoTouch.SourceSync[lvIndex] );				//Trigger NoTouch off
}
CHANGE Source_NoTouch_Enabled
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	gvNoTouch.SourceNoTouchEnable[lvIndex] = Source_NoTouch_Enabled[lvIndex];
}
CHANGE Power_Off_Time
{
	gvNoTouch.NoTouchOffTime = Power_Off_Time;
	IF( gvNoTouch.StatusNoTouchActive )
		gvNoTouch.Countdown = gvNoTouch.NoTouchOffTime; 
}
CHANGE Number_Of_Displays
{
	gvNoTouch.NumberOfDisplays = Number_Of_Displays;
}
CHANGE Display_Route
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
	IF( Display_Route[lvIndex] <= NUM_SOURCES )			//Valid selection
	{
		SetOutputs( Display_Route[lvIndex], lvIndex );//Set up all the outputs based on the input
		IF( Display_Route[lvIndex] )						//Only check if its positive
		{
			IF( gvNoTouch.NoTouchOn )
			{
				IF( gvNoTouch.SourceNoTouchEnable[Display_Route[lvIndex]] && !gvNoTouch.SourceSync[Display_Route[lvIndex]] )//No touch is enabled and sync is not active for this manual selection
				{
					gvNoTouch.Countdown = gvNoTouch.NoTouchOffTime;
					IF( !gvNoTouch.StatusNoTouchActive && CheckAutoOff() )
					{
						gvNoTouch.StatusNoTouchActive = 1;
						StartCountdown();												//Run the Countdown
					}
				}
				ELSE
					gvNoTouch.StatusNoTouchActive = 0;
			}
		}
	}
}
CHANGE Source_Name
{
	INTEGER lvIndex;
	lvIndex = GETLASTMODIFIEDARRAYINDEX();
}
PUSH SystemStart
{
	gvStartRead = 1;
	IF( LEN( FileName ) > 0 )
		FileOpenConfig();
}
CHANGE FileName
{
	IF( LEN( FileName ) > 0 )
	{
		IF( gvStartRead )
     	   FileOpenConfig();
	}
}
/******************************************************************************************
  Main()
*******************************************************************************************/
FUNCTION Main()
{
	INTEGER lvCounterDisp, lvCounterInput;
	FOR( lvCounterDisp = 1 TO NUM_DISPLAYS )								//Loop through all displays
	{
		FOR( lvCounterInput = 1 TO NUM_SOURCES )								//Loop through all inputs
		{
			gvDisplays[lvCounterDisp].SourceInput[lvCounterInput] = 1;				//Initialize input maps to 1
		}
	}
}
