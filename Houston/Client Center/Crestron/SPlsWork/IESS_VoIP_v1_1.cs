using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_VOIP_V1_1
{
    public class UserModuleClass_IESS_VOIP_V1_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput STATUSCALL_CONNECTED_FB;
        Crestron.Logos.SplusObjects.DigitalInput STATUSCALL_DISCONNECTED_FB;
        Crestron.Logos.SplusObjects.DigitalInput DIAL_SEND_END;
        Crestron.Logos.SplusObjects.DigitalInput VOIP_POPUP;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_0;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_1;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_2;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_3;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_4;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_5;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_6;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_7;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_8;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_9;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_STAR;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_POUND;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_DOT;
        Crestron.Logos.SplusObjects.DigitalInput KEYPAD_BACKSPACE_CLEAR;
        Crestron.Logos.SplusObjects.DigitalInput ANSWER;
        Crestron.Logos.SplusObjects.DigitalInput DECLINE;
        Crestron.Logos.SplusObjects.DigitalInput JOINCALL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> CALL_STATE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> CALL_STATE_TEXT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> CALL_STATE_CID_NAME;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> CALL_STATE_CID_NUM;
        Crestron.Logos.SplusObjects.DigitalOutput DIAL_SEND;
        Crestron.Logos.SplusObjects.DigitalOutput DIAL_END;
        Crestron.Logos.SplusObjects.DigitalOutput VOIP_POPUP_ON;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_0;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_1;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_2;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_3;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_4;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_5;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_6;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_7;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_8;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_9;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_STAR;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_POUND;
        Crestron.Logos.SplusObjects.DigitalOutput DTMF_DOT;
        Crestron.Logos.SplusObjects.DigitalOutput ANSWERCALL;
        Crestron.Logos.SplusObjects.DigitalOutput DECLINECALL;
        Crestron.Logos.SplusObjects.StringOutput DIALSTRING;
        Crestron.Logos.SplusObjects.AnalogOutput CALL_SELECTED;
        Crestron.Logos.SplusObjects.AnalogOutput CALL_SELECTED_STATE;
        Crestron.Logos.SplusObjects.StringOutput CALL_SELECTED_STATE_TEXT;
        Crestron.Logos.SplusObjects.StringOutput CALL_SELECTED_STATE_CID_NAME;
        Crestron.Logos.SplusObjects.StringOutput CALL_SELECTED_STATE_CID_NUM;
        ushort GLBL_POPUP = 0;
        ushort GLBL_CALLSELECTED = 0;
        ushort GLBL_INCOMINGSELECTED = 0;
        ushort [] GLBL_CALLACTIVE;
        CrestronString [] GLBL_DIALSTRING;
        private void CALLSELECTION (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            
            __context__.SourceCodeLine = 87;
            GLBL_CALLSELECTED = (ushort) ( LVINDEX ) ; 
            __context__.SourceCodeLine = 88;
            CALL_SELECTED  .Value = (ushort) ( LVINDEX ) ; 
            __context__.SourceCodeLine = 89;
            CALL_SELECTED_STATE  .Value = (ushort) ( CALL_STATE[ LVINDEX ] .UshortValue ) ; 
            __context__.SourceCodeLine = 90;
            CALL_SELECTED_STATE_TEXT  .UpdateValue ( CALL_STATE_TEXT [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 91;
            CALL_SELECTED_STATE_CID_NAME  .UpdateValue ( CALL_STATE_CID_NAME [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 92;
            CALL_SELECTED_STATE_CID_NUM  .UpdateValue ( CALL_STATE_CID_NUM [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 93;
            DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ LVINDEX ]  ) ; 
            
            }
            
        private void CHANGECALLINACTIVESELECTION (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVCALL = 0;
            
            
            __context__.SourceCodeLine = 98;
            LVCALL = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 99;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 6 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)1; 
            int __FN_FORSTEP_VAL__1 = (int)Functions.ToLongInteger( -( 1 ) ); 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 101;
                if ( Functions.TestForTrue  ( ( Functions.Not( GLBL_CALLACTIVE[ LVCOUNTER ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 103;
                    LVCALL = (ushort) ( LVCOUNTER ) ; 
                    } 
                
                __context__.SourceCodeLine = 99;
                } 
            
            __context__.SourceCodeLine = 106;
            if ( Functions.TestForTrue  ( ( LVCALL)  ) ) 
                {
                __context__.SourceCodeLine = 107;
                CALLSELECTION (  __context__ , (ushort)( LVCALL )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 109;
                CALLSELECTION (  __context__ , (ushort)( 1 )) ; 
                }
            
            
            }
            
        private void CHANGECALLACTIVESELECTION (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVCALL = 0;
            
            
            __context__.SourceCodeLine = 114;
            LVCALL = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 115;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)6; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 117;
                if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ LVCOUNTER ])  ) ) 
                    { 
                    __context__.SourceCodeLine = 119;
                    LVCALL = (ushort) ( LVCOUNTER ) ; 
                    } 
                
                __context__.SourceCodeLine = 115;
                } 
            
            __context__.SourceCodeLine = 122;
            if ( Functions.TestForTrue  ( ( LVCALL)  ) ) 
                {
                __context__.SourceCodeLine = 123;
                CALLSELECTION (  __context__ , (ushort)( LVCALL )) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 125;
                CALLSELECTION (  __context__ , (ushort)( 1 )) ; 
                }
            
            
            }
            
        object DIAL_SEND_END_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 132;
                if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
                    {
                    __context__.SourceCodeLine = 133;
                    Functions.Pulse ( 20, DIAL_END ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 135;
                    Functions.Pulse ( 20, DIAL_SEND ) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object VOIP_POPUP_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 139;
            GLBL_POPUP = (ushort) ( Functions.Not( GLBL_POPUP ) ) ; 
            __context__.SourceCodeLine = 140;
            VOIP_POPUP_ON  .Value = (ushort) ( GLBL_POPUP ) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object STATUSCALL_CONNECTED_FB_OnChange_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 144;
        if ( Functions.TestForTrue  ( ( STATUSCALL_CONNECTED_FB  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 146;
            GLBL_CALLACTIVE [ 1] = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 150;
            GLBL_CALLACTIVE [ 1] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 151;
            DIALSTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 152;
            GLBL_DIALSTRING [ 1 ]  .UpdateValue ( ""  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object STATUSCALL_DISCONNECTED_FB_OnChange_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 157;
        if ( Functions.TestForTrue  ( ( STATUSCALL_DISCONNECTED_FB  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 159;
            GLBL_CALLACTIVE [ 1] = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 160;
            DIALSTRING  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 161;
            GLBL_DIALSTRING [ 1 ]  .UpdateValue ( ""  ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 165;
            GLBL_CALLACTIVE [ 1] = (ushort) ( 1 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_0_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 170;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 171;
            Functions.Pulse ( 20, DTMF_0 ) ; 
            }
        
        __context__.SourceCodeLine = 172;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "0"  ) ; 
        __context__.SourceCodeLine = 173;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_1_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 177;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 178;
            Functions.Pulse ( 20, DTMF_1 ) ; 
            }
        
        __context__.SourceCodeLine = 179;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "1"  ) ; 
        __context__.SourceCodeLine = 180;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_2_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 184;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 185;
            Functions.Pulse ( 20, DTMF_2 ) ; 
            }
        
        __context__.SourceCodeLine = 186;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "2"  ) ; 
        __context__.SourceCodeLine = 187;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_3_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 191;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 192;
            Functions.Pulse ( 20, DTMF_3 ) ; 
            }
        
        __context__.SourceCodeLine = 193;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "3"  ) ; 
        __context__.SourceCodeLine = 194;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_4_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 198;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 199;
            Functions.Pulse ( 20, DTMF_4 ) ; 
            }
        
        __context__.SourceCodeLine = 200;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "4"  ) ; 
        __context__.SourceCodeLine = 201;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_5_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 205;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 206;
            Functions.Pulse ( 20, DTMF_5 ) ; 
            }
        
        __context__.SourceCodeLine = 207;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "5"  ) ; 
        __context__.SourceCodeLine = 208;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_6_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 212;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 213;
            Functions.Pulse ( 20, DTMF_6 ) ; 
            }
        
        __context__.SourceCodeLine = 214;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "6"  ) ; 
        __context__.SourceCodeLine = 215;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_7_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 219;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 220;
            Functions.Pulse ( 20, DTMF_7 ) ; 
            }
        
        __context__.SourceCodeLine = 221;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "7"  ) ; 
        __context__.SourceCodeLine = 222;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_8_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 226;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 227;
            Functions.Pulse ( 20, DTMF_8 ) ; 
            }
        
        __context__.SourceCodeLine = 228;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "8"  ) ; 
        __context__.SourceCodeLine = 229;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_9_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 233;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 234;
            Functions.Pulse ( 20, DTMF_9 ) ; 
            }
        
        __context__.SourceCodeLine = 235;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "9"  ) ; 
        __context__.SourceCodeLine = 236;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_STAR_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 240;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 241;
            Functions.Pulse ( 20, DTMF_STAR ) ; 
            }
        
        __context__.SourceCodeLine = 242;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "*"  ) ; 
        __context__.SourceCodeLine = 243;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_POUND_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 247;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 248;
            Functions.Pulse ( 20, DTMF_POUND ) ; 
            }
        
        __context__.SourceCodeLine = 249;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "#"  ) ; 
        __context__.SourceCodeLine = 250;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_DOT_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 254;
        if ( Functions.TestForTrue  ( ( GLBL_CALLACTIVE[ GLBL_CALLSELECTED ])  ) ) 
            {
            __context__.SourceCodeLine = 255;
            Functions.Pulse ( 20, DTMF_DOT ) ; 
            }
        
        __context__.SourceCodeLine = 256;
        GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ] + "."  ) ; 
        __context__.SourceCodeLine = 257;
        DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object KEYPAD_BACKSPACE_CLEAR_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 261;
        CreateWait ( "CLEAR" , 100 , CLEAR_Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void CLEAR_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 263;
            if ( Functions.TestForTrue  ( ( KEYPAD_BACKSPACE_CLEAR  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 265;
                GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 266;
                DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object KEYPAD_BACKSPACE_CLEAR_OnRelease_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        
        
        __context__.SourceCodeLine = 273;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( GLBL_DIALSTRING[ GLBL_CALLSELECTED ] ) > 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 275;
            LVSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
            __context__.SourceCodeLine = 276;
            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( GLBL_DIALSTRING[ GLBL_CALLSELECTED ] ) - 1), LVSTRING )  ) ; 
            __context__.SourceCodeLine = 277;
            GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  .UpdateValue ( LVSTRING  ) ; 
            __context__.SourceCodeLine = 278;
            DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object JOINCALL_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 283;
        CHANGECALLINACTIVESELECTION (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALL_STATE_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 288;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 289;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (CALL_STATE[ LVINDEX ] .UshortValue == 4) ) || Functions.TestForTrue ( Functions.BoolToInt (CALL_STATE[ LVINDEX ] .UshortValue == 6) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (CALL_STATE[ LVINDEX ] .UshortValue == 7) )) ) ) || Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( CALL_STATE[ LVINDEX ] .UshortValue >= 13 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( CALL_STATE[ LVINDEX ] .UshortValue <= 18 ) )) ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 291;
            GLBL_CALLACTIVE [ LVINDEX] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 292;
            GLBL_DIALSTRING [ LVINDEX ]  .UpdateValue ( ""  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 294;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALL_STATE[ LVINDEX ] .UshortValue == 8))  ) ) 
                {
                __context__.SourceCodeLine = 295;
                GLBL_INCOMINGSELECTED = (ushort) ( LVINDEX ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 296;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CALL_STATE[ LVINDEX ] .UshortValue == 3))  ) ) 
                    { 
                    __context__.SourceCodeLine = 298;
                    GLBL_CALLACTIVE [ LVINDEX] = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 299;
                    GLBL_DIALSTRING [ LVINDEX ]  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 300;
                    CHANGECALLACTIVESELECTION (  __context__  ) ; 
                    } 
                
                }
            
            }
        
        __context__.SourceCodeLine = 302;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == GLBL_CALLSELECTED))  ) ) 
            { 
            __context__.SourceCodeLine = 304;
            CALL_SELECTED_STATE  .Value = (ushort) ( CALL_STATE[ LVINDEX ] .UshortValue ) ; 
            __context__.SourceCodeLine = 305;
            DIALSTRING  .UpdateValue ( GLBL_DIALSTRING [ GLBL_CALLSELECTED ]  ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALL_STATE_TEXT_OnChange_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 311;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 312;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == GLBL_CALLSELECTED))  ) ) 
            {
            __context__.SourceCodeLine = 313;
            CALL_SELECTED_STATE_TEXT  .UpdateValue ( CALL_STATE_TEXT [ LVINDEX ]  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALL_STATE_CID_NAME_OnChange_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 318;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 319;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == GLBL_CALLSELECTED))  ) ) 
            {
            __context__.SourceCodeLine = 320;
            CALL_SELECTED_STATE_CID_NAME  .UpdateValue ( CALL_STATE_CID_NAME [ LVINDEX ]  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CALL_STATE_CID_NUM_OnChange_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 325;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 326;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINDEX == GLBL_CALLSELECTED))  ) ) 
            {
            __context__.SourceCodeLine = 327;
            CALL_SELECTED_STATE_CID_NUM  .UpdateValue ( CALL_STATE_CID_NUM [ LVINDEX ]  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ANSWER_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 331;
        CALLSELECTION (  __context__ , (ushort)( GLBL_INCOMINGSELECTED )) ; 
        __context__.SourceCodeLine = 332;
        Functions.Pulse ( 20, ANSWERCALL ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DECLINE_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 336;
        CALL_SELECTED  .Value = (ushort) ( GLBL_INCOMINGSELECTED ) ; 
        __context__.SourceCodeLine = 337;
        DECLINECALL  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 338;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_23__" , 20 , __SPLS_TMPVAR__WAITLABEL_23___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_23___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 340;
            DECLINECALL  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 341;
            CALL_SELECTED  .Value = (ushort) ( GLBL_CALLSELECTED ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 346;
        CALLSELECTION (  __context__ , (ushort)( 1 )) ; 
        __context__.SourceCodeLine = 347;
        CALL_SELECTED  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 348;
        GLBL_INCOMINGSELECTED = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_CALLACTIVE  = new ushort[ 7 ];
    GLBL_DIALSTRING  = new CrestronString[ 7 ];
    for( uint i = 0; i < 7; i++ )
        GLBL_DIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
    
    STATUSCALL_CONNECTED_FB = new Crestron.Logos.SplusObjects.DigitalInput( STATUSCALL_CONNECTED_FB__DigitalInput__, this );
    m_DigitalInputList.Add( STATUSCALL_CONNECTED_FB__DigitalInput__, STATUSCALL_CONNECTED_FB );
    
    STATUSCALL_DISCONNECTED_FB = new Crestron.Logos.SplusObjects.DigitalInput( STATUSCALL_DISCONNECTED_FB__DigitalInput__, this );
    m_DigitalInputList.Add( STATUSCALL_DISCONNECTED_FB__DigitalInput__, STATUSCALL_DISCONNECTED_FB );
    
    DIAL_SEND_END = new Crestron.Logos.SplusObjects.DigitalInput( DIAL_SEND_END__DigitalInput__, this );
    m_DigitalInputList.Add( DIAL_SEND_END__DigitalInput__, DIAL_SEND_END );
    
    VOIP_POPUP = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_POPUP__DigitalInput__, this );
    m_DigitalInputList.Add( VOIP_POPUP__DigitalInput__, VOIP_POPUP );
    
    KEYPAD_0 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_0__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_0__DigitalInput__, KEYPAD_0 );
    
    KEYPAD_1 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_1__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_1__DigitalInput__, KEYPAD_1 );
    
    KEYPAD_2 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_2__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_2__DigitalInput__, KEYPAD_2 );
    
    KEYPAD_3 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_3__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_3__DigitalInput__, KEYPAD_3 );
    
    KEYPAD_4 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_4__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_4__DigitalInput__, KEYPAD_4 );
    
    KEYPAD_5 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_5__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_5__DigitalInput__, KEYPAD_5 );
    
    KEYPAD_6 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_6__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_6__DigitalInput__, KEYPAD_6 );
    
    KEYPAD_7 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_7__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_7__DigitalInput__, KEYPAD_7 );
    
    KEYPAD_8 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_8__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_8__DigitalInput__, KEYPAD_8 );
    
    KEYPAD_9 = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_9__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_9__DigitalInput__, KEYPAD_9 );
    
    KEYPAD_STAR = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_STAR__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_STAR__DigitalInput__, KEYPAD_STAR );
    
    KEYPAD_POUND = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_POUND__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_POUND__DigitalInput__, KEYPAD_POUND );
    
    KEYPAD_DOT = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_DOT__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_DOT__DigitalInput__, KEYPAD_DOT );
    
    KEYPAD_BACKSPACE_CLEAR = new Crestron.Logos.SplusObjects.DigitalInput( KEYPAD_BACKSPACE_CLEAR__DigitalInput__, this );
    m_DigitalInputList.Add( KEYPAD_BACKSPACE_CLEAR__DigitalInput__, KEYPAD_BACKSPACE_CLEAR );
    
    ANSWER = new Crestron.Logos.SplusObjects.DigitalInput( ANSWER__DigitalInput__, this );
    m_DigitalInputList.Add( ANSWER__DigitalInput__, ANSWER );
    
    DECLINE = new Crestron.Logos.SplusObjects.DigitalInput( DECLINE__DigitalInput__, this );
    m_DigitalInputList.Add( DECLINE__DigitalInput__, DECLINE );
    
    JOINCALL = new Crestron.Logos.SplusObjects.DigitalInput( JOINCALL__DigitalInput__, this );
    m_DigitalInputList.Add( JOINCALL__DigitalInput__, JOINCALL );
    
    DIAL_SEND = new Crestron.Logos.SplusObjects.DigitalOutput( DIAL_SEND__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIAL_SEND__DigitalOutput__, DIAL_SEND );
    
    DIAL_END = new Crestron.Logos.SplusObjects.DigitalOutput( DIAL_END__DigitalOutput__, this );
    m_DigitalOutputList.Add( DIAL_END__DigitalOutput__, DIAL_END );
    
    VOIP_POPUP_ON = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_POPUP_ON__DigitalOutput__, this );
    m_DigitalOutputList.Add( VOIP_POPUP_ON__DigitalOutput__, VOIP_POPUP_ON );
    
    DTMF_0 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_0__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_0__DigitalOutput__, DTMF_0 );
    
    DTMF_1 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_1__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_1__DigitalOutput__, DTMF_1 );
    
    DTMF_2 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_2__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_2__DigitalOutput__, DTMF_2 );
    
    DTMF_3 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_3__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_3__DigitalOutput__, DTMF_3 );
    
    DTMF_4 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_4__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_4__DigitalOutput__, DTMF_4 );
    
    DTMF_5 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_5__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_5__DigitalOutput__, DTMF_5 );
    
    DTMF_6 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_6__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_6__DigitalOutput__, DTMF_6 );
    
    DTMF_7 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_7__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_7__DigitalOutput__, DTMF_7 );
    
    DTMF_8 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_8__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_8__DigitalOutput__, DTMF_8 );
    
    DTMF_9 = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_9__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_9__DigitalOutput__, DTMF_9 );
    
    DTMF_STAR = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_STAR__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_STAR__DigitalOutput__, DTMF_STAR );
    
    DTMF_POUND = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_POUND__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_POUND__DigitalOutput__, DTMF_POUND );
    
    DTMF_DOT = new Crestron.Logos.SplusObjects.DigitalOutput( DTMF_DOT__DigitalOutput__, this );
    m_DigitalOutputList.Add( DTMF_DOT__DigitalOutput__, DTMF_DOT );
    
    ANSWERCALL = new Crestron.Logos.SplusObjects.DigitalOutput( ANSWERCALL__DigitalOutput__, this );
    m_DigitalOutputList.Add( ANSWERCALL__DigitalOutput__, ANSWERCALL );
    
    DECLINECALL = new Crestron.Logos.SplusObjects.DigitalOutput( DECLINECALL__DigitalOutput__, this );
    m_DigitalOutputList.Add( DECLINECALL__DigitalOutput__, DECLINECALL );
    
    CALL_STATE = new InOutArray<AnalogInput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        CALL_STATE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( CALL_STATE__AnalogSerialInput__ + i, CALL_STATE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( CALL_STATE__AnalogSerialInput__ + i, CALL_STATE[i+1] );
    }
    
    CALL_SELECTED = new Crestron.Logos.SplusObjects.AnalogOutput( CALL_SELECTED__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CALL_SELECTED__AnalogSerialOutput__, CALL_SELECTED );
    
    CALL_SELECTED_STATE = new Crestron.Logos.SplusObjects.AnalogOutput( CALL_SELECTED_STATE__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CALL_SELECTED_STATE__AnalogSerialOutput__, CALL_SELECTED_STATE );
    
    CALL_STATE_TEXT = new InOutArray<StringInput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        CALL_STATE_TEXT[i+1] = new Crestron.Logos.SplusObjects.StringInput( CALL_STATE_TEXT__AnalogSerialInput__ + i, CALL_STATE_TEXT__AnalogSerialInput__, 127, this );
        m_StringInputList.Add( CALL_STATE_TEXT__AnalogSerialInput__ + i, CALL_STATE_TEXT[i+1] );
    }
    
    CALL_STATE_CID_NAME = new InOutArray<StringInput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        CALL_STATE_CID_NAME[i+1] = new Crestron.Logos.SplusObjects.StringInput( CALL_STATE_CID_NAME__AnalogSerialInput__ + i, CALL_STATE_CID_NAME__AnalogSerialInput__, 127, this );
        m_StringInputList.Add( CALL_STATE_CID_NAME__AnalogSerialInput__ + i, CALL_STATE_CID_NAME[i+1] );
    }
    
    CALL_STATE_CID_NUM = new InOutArray<StringInput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        CALL_STATE_CID_NUM[i+1] = new Crestron.Logos.SplusObjects.StringInput( CALL_STATE_CID_NUM__AnalogSerialInput__ + i, CALL_STATE_CID_NUM__AnalogSerialInput__, 127, this );
        m_StringInputList.Add( CALL_STATE_CID_NUM__AnalogSerialInput__ + i, CALL_STATE_CID_NUM[i+1] );
    }
    
    DIALSTRING = new Crestron.Logos.SplusObjects.StringOutput( DIALSTRING__AnalogSerialOutput__, this );
    m_StringOutputList.Add( DIALSTRING__AnalogSerialOutput__, DIALSTRING );
    
    CALL_SELECTED_STATE_TEXT = new Crestron.Logos.SplusObjects.StringOutput( CALL_SELECTED_STATE_TEXT__AnalogSerialOutput__, this );
    m_StringOutputList.Add( CALL_SELECTED_STATE_TEXT__AnalogSerialOutput__, CALL_SELECTED_STATE_TEXT );
    
    CALL_SELECTED_STATE_CID_NAME = new Crestron.Logos.SplusObjects.StringOutput( CALL_SELECTED_STATE_CID_NAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( CALL_SELECTED_STATE_CID_NAME__AnalogSerialOutput__, CALL_SELECTED_STATE_CID_NAME );
    
    CALL_SELECTED_STATE_CID_NUM = new Crestron.Logos.SplusObjects.StringOutput( CALL_SELECTED_STATE_CID_NUM__AnalogSerialOutput__, this );
    m_StringOutputList.Add( CALL_SELECTED_STATE_CID_NUM__AnalogSerialOutput__, CALL_SELECTED_STATE_CID_NUM );
    
    CLEAR_Callback = new WaitFunction( CLEAR_CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_23___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_23___CallbackFn );
    
    DIAL_SEND_END.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIAL_SEND_END_OnPush_0, false ) );
    VOIP_POPUP.OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_POPUP_OnPush_1, false ) );
    STATUSCALL_CONNECTED_FB.OnDigitalChange.Add( new InputChangeHandlerWrapper( STATUSCALL_CONNECTED_FB_OnChange_2, false ) );
    STATUSCALL_DISCONNECTED_FB.OnDigitalChange.Add( new InputChangeHandlerWrapper( STATUSCALL_DISCONNECTED_FB_OnChange_3, false ) );
    KEYPAD_0.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_0_OnPush_4, false ) );
    KEYPAD_1.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_1_OnPush_5, false ) );
    KEYPAD_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_2_OnPush_6, false ) );
    KEYPAD_3.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_3_OnPush_7, false ) );
    KEYPAD_4.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_4_OnPush_8, false ) );
    KEYPAD_5.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_5_OnPush_9, false ) );
    KEYPAD_6.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_6_OnPush_10, false ) );
    KEYPAD_7.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_7_OnPush_11, false ) );
    KEYPAD_8.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_8_OnPush_12, false ) );
    KEYPAD_9.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_9_OnPush_13, false ) );
    KEYPAD_STAR.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_STAR_OnPush_14, false ) );
    KEYPAD_POUND.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_POUND_OnPush_15, false ) );
    KEYPAD_DOT.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_DOT_OnPush_16, false ) );
    KEYPAD_BACKSPACE_CLEAR.OnDigitalPush.Add( new InputChangeHandlerWrapper( KEYPAD_BACKSPACE_CLEAR_OnPush_17, false ) );
    KEYPAD_BACKSPACE_CLEAR.OnDigitalRelease.Add( new InputChangeHandlerWrapper( KEYPAD_BACKSPACE_CLEAR_OnRelease_18, false ) );
    JOINCALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( JOINCALL_OnPush_19, false ) );
    for( uint i = 0; i < 6; i++ )
        CALL_STATE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( CALL_STATE_OnChange_20, false ) );
        
    for( uint i = 0; i < 6; i++ )
        CALL_STATE_TEXT[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( CALL_STATE_TEXT_OnChange_21, false ) );
        
    for( uint i = 0; i < 6; i++ )
        CALL_STATE_CID_NAME[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( CALL_STATE_CID_NAME_OnChange_22, false ) );
        
    for( uint i = 0; i < 6; i++ )
        CALL_STATE_CID_NUM[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( CALL_STATE_CID_NUM_OnChange_23, false ) );
        
    ANSWER.OnDigitalPush.Add( new InputChangeHandlerWrapper( ANSWER_OnPush_24, false ) );
    DECLINE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DECLINE_OnPush_25, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_VOIP_V1_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction CLEAR_Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_23___Callback;


const uint STATUSCALL_CONNECTED_FB__DigitalInput__ = 0;
const uint STATUSCALL_DISCONNECTED_FB__DigitalInput__ = 1;
const uint DIAL_SEND_END__DigitalInput__ = 2;
const uint VOIP_POPUP__DigitalInput__ = 3;
const uint KEYPAD_0__DigitalInput__ = 4;
const uint KEYPAD_1__DigitalInput__ = 5;
const uint KEYPAD_2__DigitalInput__ = 6;
const uint KEYPAD_3__DigitalInput__ = 7;
const uint KEYPAD_4__DigitalInput__ = 8;
const uint KEYPAD_5__DigitalInput__ = 9;
const uint KEYPAD_6__DigitalInput__ = 10;
const uint KEYPAD_7__DigitalInput__ = 11;
const uint KEYPAD_8__DigitalInput__ = 12;
const uint KEYPAD_9__DigitalInput__ = 13;
const uint KEYPAD_STAR__DigitalInput__ = 14;
const uint KEYPAD_POUND__DigitalInput__ = 15;
const uint KEYPAD_DOT__DigitalInput__ = 16;
const uint KEYPAD_BACKSPACE_CLEAR__DigitalInput__ = 17;
const uint ANSWER__DigitalInput__ = 18;
const uint DECLINE__DigitalInput__ = 19;
const uint JOINCALL__DigitalInput__ = 20;
const uint CALL_STATE__AnalogSerialInput__ = 0;
const uint CALL_STATE_TEXT__AnalogSerialInput__ = 6;
const uint CALL_STATE_CID_NAME__AnalogSerialInput__ = 12;
const uint CALL_STATE_CID_NUM__AnalogSerialInput__ = 18;
const uint DIAL_SEND__DigitalOutput__ = 0;
const uint DIAL_END__DigitalOutput__ = 1;
const uint VOIP_POPUP_ON__DigitalOutput__ = 2;
const uint DTMF_0__DigitalOutput__ = 3;
const uint DTMF_1__DigitalOutput__ = 4;
const uint DTMF_2__DigitalOutput__ = 5;
const uint DTMF_3__DigitalOutput__ = 6;
const uint DTMF_4__DigitalOutput__ = 7;
const uint DTMF_5__DigitalOutput__ = 8;
const uint DTMF_6__DigitalOutput__ = 9;
const uint DTMF_7__DigitalOutput__ = 10;
const uint DTMF_8__DigitalOutput__ = 11;
const uint DTMF_9__DigitalOutput__ = 12;
const uint DTMF_STAR__DigitalOutput__ = 13;
const uint DTMF_POUND__DigitalOutput__ = 14;
const uint DTMF_DOT__DigitalOutput__ = 15;
const uint ANSWERCALL__DigitalOutput__ = 16;
const uint DECLINECALL__DigitalOutput__ = 17;
const uint DIALSTRING__AnalogSerialOutput__ = 0;
const uint CALL_SELECTED__AnalogSerialOutput__ = 1;
const uint CALL_SELECTED_STATE__AnalogSerialOutput__ = 2;
const uint CALL_SELECTED_STATE_TEXT__AnalogSerialOutput__ = 3;
const uint CALL_SELECTED_STATE_CID_NAME__AnalogSerialOutput__ = 4;
const uint CALL_SELECTED_STATE_CID_NUM__AnalogSerialOutput__ = 5;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
