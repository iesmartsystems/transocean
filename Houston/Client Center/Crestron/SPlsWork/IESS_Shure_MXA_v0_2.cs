using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_SHURE_MXA_V0_2
{
    public class UserModuleClass_IESS_SHURE_MXA_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.DigitalInput LED_ON;
        Crestron.Logos.SplusObjects.DigitalInput LED_OFF;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.DigitalOutput LED_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput LED_OFF_FB;
        Crestron.Logos.SplusObjects.DigitalOutput MUTE_BUTTON_ON_FB;
        Crestron.Logos.SplusObjects.DigitalOutput MUTE_BUTTON_OFF_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        SplusTcpClient AUDIOCLIENT;
        SAUDIO AUDIODEVICE;
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 89;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 91;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 92;
                    Trace( "Shure RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 94;
                    Trace( "Shure TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETTXQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            
            __context__.SourceCodeLine = 100;
            AUDIODEVICE . TXQUEUE  .UpdateValue ( AUDIODEVICE . TXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 101;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , AUDIODEVICE.TXQUEUE , 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 103;
                LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , AUDIODEVICE . TXQUEUE )  ) ; 
                __context__.SourceCodeLine = 104;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 105;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 3 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 107;
                    LVTEMP  .UpdateValue ( "< " + LVTEMP + " >"  ) ; 
                    __context__.SourceCodeLine = 108;
                    Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                    __context__.SourceCodeLine = 109;
                    SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                    } 
                
                __context__.SourceCodeLine = 101;
                } 
            
            
            }
            
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 116;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 118;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 120;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 122;
                    LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                    } 
                
                }
            
            
            }
            
        object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )
        
            { 
            SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
                
                __context__.SourceCodeLine = 131;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 132;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 133;
                CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
                __context__.SourceCodeLine = 134;
                if ( Functions.TestForTrue  ( ( AUDIODEVICE.MUTEREQUEST)  ) ) 
                    {
                    __context__.SourceCodeLine = 135;
                    SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDLEDON) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 137;
                    SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDLEDOFF) ; 
                    }
                
                __context__.SourceCodeLine = 138;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_29__" , 500 , __SPLS_TMPVAR__WAITLABEL_29___Callback ) ;
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SocketInfo__ ); }
            return this;
            
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_29___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            {
            __context__.SourceCodeLine = 139;
            SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLLLED) ; 
            }
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 145;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 146;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 147;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 151;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 152;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 154;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 155;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 159;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 160;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 168;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 169;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 170;
        while ( Functions.TestForTrue  ( ( Functions.Find( ">" , AUDIODEVICE.RXQUEUE , 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 172;
            LVSTRING  .UpdateValue ( Functions.Remove ( ">" , AUDIODEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 173;
            SETDEBUG (  __context__ , LVSTRING, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 174;
            if ( Functions.TestForTrue  ( ( Functions.Find( "REP DEV_LED_IN_STATE OFF" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 176;
                LED_OFF_FB  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 177;
                LED_ON_FB  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 178;
                AUDIODEVICE . STATUSLED = (ushort) ( 1 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 180;
                if ( Functions.TestForTrue  ( ( Functions.Find( "REP DEV_LED_IN_STATE ON" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 182;
                    LED_ON_FB  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 183;
                    LED_OFF_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 184;
                    AUDIODEVICE . STATUSLED = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 186;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "EXT_SWITCH_OUT_STATE OFF" , LVSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 188;
                        MUTE_BUTTON_OFF_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 189;
                        MUTE_BUTTON_ON_FB  .Value = (ushort) ( 1 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 191;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "EXT_SWITCH_OUT_STATE ON" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 193;
                            MUTE_BUTTON_ON_FB  .Value = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 194;
                            MUTE_BUTTON_OFF_FB  .Value = (ushort) ( 1 ) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 170;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 201;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 203;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 204;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 208;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 209;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 215;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 219;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 223;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
            {
            __context__.SourceCodeLine = 224;
            SETTXQUEUE (  __context__ , MANUALCMD) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LED_ON_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 228;
        AUDIODEVICE . MUTEREQUEST = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 229;
        SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDLEDON) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LED_OFF_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 233;
        AUDIODEVICE . MUTEREQUEST = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 234;
        SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDLEDOFF) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 238;
        SETTXQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLLLED) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 246;
        AUDIODEVICE . IPPORT = (ushort) ( 2202 ) ; 
        __context__.SourceCodeLine = 247;
        AUDIODEVICE . COMMANDLEDON  .UpdateValue ( "SET DEV_LED_IN_STATE OFF"  ) ; 
        __context__.SourceCodeLine = 248;
        AUDIODEVICE . COMMANDLEDOFF  .UpdateValue ( "SET DEV_LED_IN_STATE ON"  ) ; 
        __context__.SourceCodeLine = 249;
        AUDIODEVICE . COMMANDPOLLLED  .UpdateValue ( "GET DEV_LED_IN_STATE"  ) ; 
        __context__.SourceCodeLine = 250;
        AUDIODEVICE . COMMANDPOLLBUTTONSTATE  .UpdateValue ( "GET EXT_SWITCH_OUT_STATE"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 1023, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    LED_ON = new Crestron.Logos.SplusObjects.DigitalInput( LED_ON__DigitalInput__, this );
    m_DigitalInputList.Add( LED_ON__DigitalInput__, LED_ON );
    
    LED_OFF = new Crestron.Logos.SplusObjects.DigitalInput( LED_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( LED_OFF__DigitalInput__, LED_OFF );
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    LED_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( LED_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( LED_ON_FB__DigitalOutput__, LED_ON_FB );
    
    LED_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( LED_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( LED_OFF_FB__DigitalOutput__, LED_OFF_FB );
    
    MUTE_BUTTON_ON_FB = new Crestron.Logos.SplusObjects.DigitalOutput( MUTE_BUTTON_ON_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( MUTE_BUTTON_ON_FB__DigitalOutput__, MUTE_BUTTON_ON_FB );
    
    MUTE_BUTTON_OFF_FB = new Crestron.Logos.SplusObjects.DigitalOutput( MUTE_BUTTON_OFF_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( MUTE_BUTTON_OFF_FB__DigitalOutput__, MUTE_BUTTON_OFF_FB );
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    __SPLS_TMPVAR__WAITLABEL_29___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_29___CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_5, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    LED_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( LED_ON_OnPush_8, false ) );
    LED_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( LED_OFF_OnPush_9, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_10, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_SHURE_MXA_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_29___Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint LED_ON__DigitalInput__ = 3;
const uint LED_OFF__DigitalInput__ = 4;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint MANUALCMD__AnalogSerialInput__ = 2;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint LED_ON_FB__DigitalOutput__ = 1;
const uint LED_OFF_FB__DigitalOutput__ = 2;
const uint MUTE_BUTTON_ON_FB__DigitalOutput__ = 3;
const uint MUTE_BUTTON_OFF_FB__DigitalOutput__ = 4;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSRXACTIVITY = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  ACTIVITYCYCLES = 0;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  MUTEREQUEST = 0;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  STATUSLED = 0;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  COMMANDLEDON;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  COMMANDLEDOFF;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  COMMANDPOLLLED;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  COMMANDPOLLBUTTONSTATE;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDLEDON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDLEDOFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLLED  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        COMMANDPOLLBUTTONSTATE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        
        
    }
    
}

}
