using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_ROUTE_CONVERSION_NVX_FB_V0_1
{
    public class UserModuleClass_IESS_ROUTE_CONVERSION_NVX_FB_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SWITCHERFB_OUTPUT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> SOURCEINPUT;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INPUTNAMES;
        Crestron.Logos.SplusObjects.StringOutput LASTSELECTEDINPUTSOURCENAME;
        Crestron.Logos.SplusObjects.AnalogOutput LASTSELECTEDINPUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> SWITCHERSOURCE;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VIDEOOUTPUTSOURCENAME;
        ushort GVLASTSELECTED = 0;
        ushort [] GVSWITCHEROUTPUT;
        object SWITCHERFB_OUTPUT_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVINDEX = 0;
                ushort LVCOUNTER = 0;
                
                
                __context__.SourceCodeLine = 76;
                LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
                __context__.SourceCodeLine = 77;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SWITCHERFB_OUTPUT[ LVINDEX ] == ""))  ) ) 
                    { 
                    __context__.SourceCodeLine = 79;
                    GVLASTSELECTED = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 80;
                    GVSWITCHEROUTPUT [ LVINDEX] = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 81;
                    LASTSELECTEDINPUTSOURCENAME  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 82;
                    LASTSELECTEDINPUT  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 83;
                    SWITCHERSOURCE [ LVINDEX]  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 84;
                    VIDEOOUTPUTSOURCENAME [ LVINDEX]  .UpdateValue ( ""  ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 88;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)16; 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 90;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SWITCHERFB_OUTPUT[ LVINDEX ] == SOURCEINPUT[ LVCOUNTER ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 92;
                            GVLASTSELECTED = (ushort) ( LVINDEX ) ; 
                            __context__.SourceCodeLine = 93;
                            GVSWITCHEROUTPUT [ LVINDEX] = (ushort) ( LVCOUNTER ) ; 
                            __context__.SourceCodeLine = 94;
                            VIDEOOUTPUTSOURCENAME [ LVINDEX]  .UpdateValue ( INPUTNAMES [ LVCOUNTER ]  ) ; 
                            __context__.SourceCodeLine = 95;
                            LASTSELECTEDINPUTSOURCENAME  .UpdateValue ( INPUTNAMES [ LVCOUNTER ]  ) ; 
                            __context__.SourceCodeLine = 96;
                            LASTSELECTEDINPUT  .Value = (ushort) ( LVCOUNTER ) ; 
                            __context__.SourceCodeLine = 97;
                            SWITCHERSOURCE [ LVINDEX]  .Value = (ushort) ( LVCOUNTER ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 88;
                        } 
                    
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object INPUTNAMES_OnChange_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            ushort LVINDEX = 0;
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 105;
            LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
            __context__.SourceCodeLine = 106;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 108;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVSWITCHEROUTPUT[ LVCOUNTER ] == LVINDEX))  ) ) 
                    {
                    __context__.SourceCodeLine = 109;
                    VIDEOOUTPUTSOURCENAME [ LVCOUNTER]  .UpdateValue ( INPUTNAMES [ LVINDEX ]  ) ; 
                    }
                
                __context__.SourceCodeLine = 106;
                } 
            
            __context__.SourceCodeLine = 111;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVLASTSELECTED == LVINDEX))  ) ) 
                {
                __context__.SourceCodeLine = 112;
                LASTSELECTEDINPUTSOURCENAME  .UpdateValue ( INPUTNAMES [ LVINDEX ]  ) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GVSWITCHEROUTPUT  = new ushort[ 17 ];
    
    LASTSELECTEDINPUT = new Crestron.Logos.SplusObjects.AnalogOutput( LASTSELECTEDINPUT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( LASTSELECTEDINPUT__AnalogSerialOutput__, LASTSELECTEDINPUT );
    
    SWITCHERSOURCE = new InOutArray<AnalogOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SWITCHERSOURCE[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( SWITCHERSOURCE__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( SWITCHERSOURCE__AnalogSerialOutput__ + i, SWITCHERSOURCE[i+1] );
    }
    
    SWITCHERFB_OUTPUT = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SWITCHERFB_OUTPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( SWITCHERFB_OUTPUT__AnalogSerialInput__ + i, SWITCHERFB_OUTPUT__AnalogSerialInput__, 63, this );
        m_StringInputList.Add( SWITCHERFB_OUTPUT__AnalogSerialInput__ + i, SWITCHERFB_OUTPUT[i+1] );
    }
    
    SOURCEINPUT = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SOURCEINPUT[i+1] = new Crestron.Logos.SplusObjects.StringInput( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT__AnalogSerialInput__, 63, this );
        m_StringInputList.Add( SOURCEINPUT__AnalogSerialInput__ + i, SOURCEINPUT[i+1] );
    }
    
    INPUTNAMES = new InOutArray<StringInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        INPUTNAMES[i+1] = new Crestron.Logos.SplusObjects.StringInput( INPUTNAMES__AnalogSerialInput__ + i, INPUTNAMES__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INPUTNAMES__AnalogSerialInput__ + i, INPUTNAMES[i+1] );
    }
    
    LASTSELECTEDINPUTSOURCENAME = new Crestron.Logos.SplusObjects.StringOutput( LASTSELECTEDINPUTSOURCENAME__AnalogSerialOutput__, this );
    m_StringOutputList.Add( LASTSELECTEDINPUTSOURCENAME__AnalogSerialOutput__, LASTSELECTEDINPUTSOURCENAME );
    
    VIDEOOUTPUTSOURCENAME = new InOutArray<StringOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        VIDEOOUTPUTSOURCENAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VIDEOOUTPUTSOURCENAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VIDEOOUTPUTSOURCENAME__AnalogSerialOutput__ + i, VIDEOOUTPUTSOURCENAME[i+1] );
    }
    
    
    for( uint i = 0; i < 16; i++ )
        SWITCHERFB_OUTPUT[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( SWITCHERFB_OUTPUT_OnChange_0, false ) );
        
    for( uint i = 0; i < 16; i++ )
        INPUTNAMES[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INPUTNAMES_OnChange_1, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_ROUTE_CONVERSION_NVX_FB_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint SWITCHERFB_OUTPUT__AnalogSerialInput__ = 0;
const uint SOURCEINPUT__AnalogSerialInput__ = 16;
const uint INPUTNAMES__AnalogSerialInput__ = 32;
const uint LASTSELECTEDINPUTSOURCENAME__AnalogSerialOutput__ = 0;
const uint LASTSELECTEDINPUT__AnalogSerialOutput__ = 1;
const uint SWITCHERSOURCE__AnalogSerialOutput__ = 2;
const uint VIDEOOUTPUTSOURCENAME__AnalogSerialOutput__ = 18;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
