using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_VADDIO_ROBOSHOT_V0_2
{
    public class UserModuleClass_IESS_VADDIO_ROBOSHOT_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput IP_CONNECTED;
        Crestron.Logos.SplusObjects.DigitalInput PAN_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput PAN_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput TILT_UP;
        Crestron.Logos.SplusObjects.DigitalInput TILT_DOWN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_IN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_OUT;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_RECALL;
        Crestron.Logos.SplusObjects.DigitalInput PRESET_SAVE;
        Crestron.Logos.SplusObjects.AnalogInput PRESET_SELECT;
        Crestron.Logos.SplusObjects.StringInput DEVICE_RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput DEVICE_TX__DOLLAR__;
        SCOMMAND COMMAND;
        CrestronString GLBL_STRING;
        CrestronString GLBL_RXQUEUE;
        private void BUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
            { 
            
            __context__.SourceCodeLine = 109;
            DEVICE_TX__DOLLAR__  .UpdateValue ( COMMAND . STX + LVINCOMING + COMMAND . ETX  ) ; 
            
            }
            
        private void CAMERALOGIN (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 113;
            BUILDSTRING (  __context__ , COMMAND.USERNAME) ; 
            
            }
            
        object IP_CONNECTED_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 121;
                CAMERALOGIN (  __context__  ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object PAN_LEFT_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 125;
            BUILDSTRING (  __context__ , COMMAND.PANLEFT) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object PAN_LEFT_OnRelease_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 129;
        BUILDSTRING (  __context__ , COMMAND.PANSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 133;
        BUILDSTRING (  __context__ , COMMAND.PANRIGHT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnRelease_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 137;
        BUILDSTRING (  __context__ , COMMAND.PANSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 141;
        BUILDSTRING (  __context__ , COMMAND.TILTUP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnRelease_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 145;
        BUILDSTRING (  __context__ , COMMAND.TILTSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 149;
        BUILDSTRING (  __context__ , COMMAND.TILTDOWN) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnRelease_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 153;
        BUILDSTRING (  __context__ , COMMAND.TILTSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 157;
        BUILDSTRING (  __context__ , COMMAND.ZOOMIN) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnRelease_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 161;
        BUILDSTRING (  __context__ , COMMAND.ZOOMSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 165;
        BUILDSTRING (  __context__ , COMMAND.ZOOMOUT) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnRelease_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 169;
        BUILDSTRING (  __context__ , COMMAND.ZOOMSTOP) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_RECALL_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        __context__.SourceCodeLine = 174;
        LVSTRING  .UpdateValue ( COMMAND . PRESETRECALL + Functions.ItoA (  (int) ( PRESET_SELECT  .UshortValue ) )  ) ; 
        __context__.SourceCodeLine = 175;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_SAVE_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        
        
        __context__.SourceCodeLine = 180;
        LVSTRING  .UpdateValue ( COMMAND . PRESETSAVE + Functions.ItoA (  (int) ( PRESET_SELECT  .UshortValue ) )  ) ; 
        __context__.SourceCodeLine = 181;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEVICE_RX__DOLLAR___OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVRX;
        CrestronString LVSTRING;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16, this );
        
        
        __context__.SourceCodeLine = 198;
        LVRX  .UpdateValue ( DEVICE_RX__DOLLAR__  ) ; 
        __context__.SourceCodeLine = 199;
        if ( Functions.TestForTrue  ( ( Functions.Find( "Last login:" , LVRX ))  ) ) 
            { 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 203;
            if ( Functions.TestForTrue  ( ( Functions.Find( "login:" , LVRX ))  ) ) 
                {
                __context__.SourceCodeLine = 204;
                BUILDSTRING (  __context__ , COMMAND.USERNAME) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 205;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Password:" , LVRX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 206;
                    BUILDSTRING (  __context__ , COMMAND.PASSWORD) ; 
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 216;
        COMMAND . PANLEFT  .UpdateValue ( "camera pan left"  ) ; 
        __context__.SourceCodeLine = 217;
        COMMAND . PANRIGHT  .UpdateValue ( "camera pan right"  ) ; 
        __context__.SourceCodeLine = 218;
        COMMAND . PANSTOP  .UpdateValue ( "camera pan stop"  ) ; 
        __context__.SourceCodeLine = 219;
        COMMAND . TILTUP  .UpdateValue ( "camera tilt up"  ) ; 
        __context__.SourceCodeLine = 220;
        COMMAND . TILTDOWN  .UpdateValue ( "camera tilt down"  ) ; 
        __context__.SourceCodeLine = 221;
        COMMAND . TILTSTOP  .UpdateValue ( "camera tilt stop"  ) ; 
        __context__.SourceCodeLine = 222;
        COMMAND . ZOOMIN  .UpdateValue ( "camera zoom in"  ) ; 
        __context__.SourceCodeLine = 223;
        COMMAND . ZOOMOUT  .UpdateValue ( "camera zoom out"  ) ; 
        __context__.SourceCodeLine = 224;
        COMMAND . ZOOMSTOP  .UpdateValue ( "camera zoom stop"  ) ; 
        __context__.SourceCodeLine = 225;
        COMMAND . PRESETRECALL  .UpdateValue ( "camera preset recall "  ) ; 
        __context__.SourceCodeLine = 226;
        COMMAND . PRESETSAVE  .UpdateValue ( "camera preset store "  ) ; 
        __context__.SourceCodeLine = 227;
        COMMAND . STX  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 228;
        COMMAND . ETX  .UpdateValue ( "\u000D"  ) ; 
        __context__.SourceCodeLine = 229;
        COMMAND . USERNAME  .UpdateValue ( "admin"  ) ; 
        __context__.SourceCodeLine = 230;
        COMMAND . PASSWORD  .UpdateValue ( "password"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_STRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
    GLBL_RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
    COMMAND  = new SCOMMAND( this, true );
    COMMAND .PopulateCustomAttributeList( false );
    
    IP_CONNECTED = new Crestron.Logos.SplusObjects.DigitalInput( IP_CONNECTED__DigitalInput__, this );
    m_DigitalInputList.Add( IP_CONNECTED__DigitalInput__, IP_CONNECTED );
    
    PAN_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_LEFT__DigitalInput__, PAN_LEFT );
    
    PAN_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_RIGHT__DigitalInput__, PAN_RIGHT );
    
    TILT_UP = new Crestron.Logos.SplusObjects.DigitalInput( TILT_UP__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_UP__DigitalInput__, TILT_UP );
    
    TILT_DOWN = new Crestron.Logos.SplusObjects.DigitalInput( TILT_DOWN__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_DOWN__DigitalInput__, TILT_DOWN );
    
    ZOOM_IN = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_IN__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_IN__DigitalInput__, ZOOM_IN );
    
    ZOOM_OUT = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_OUT__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_OUT__DigitalInput__, ZOOM_OUT );
    
    PRESET_RECALL = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_RECALL__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_RECALL__DigitalInput__, PRESET_RECALL );
    
    PRESET_SAVE = new Crestron.Logos.SplusObjects.DigitalInput( PRESET_SAVE__DigitalInput__, this );
    m_DigitalInputList.Add( PRESET_SAVE__DigitalInput__, PRESET_SAVE );
    
    PRESET_SELECT = new Crestron.Logos.SplusObjects.AnalogInput( PRESET_SELECT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( PRESET_SELECT__AnalogSerialInput__, PRESET_SELECT );
    
    DEVICE_RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( DEVICE_RX__DOLLAR____AnalogSerialInput__, 100, this );
    m_StringInputList.Add( DEVICE_RX__DOLLAR____AnalogSerialInput__, DEVICE_RX__DOLLAR__ );
    
    DEVICE_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( DEVICE_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( DEVICE_TX__DOLLAR____AnalogSerialOutput__, DEVICE_TX__DOLLAR__ );
    
    
    IP_CONNECTED.OnDigitalPush.Add( new InputChangeHandlerWrapper( IP_CONNECTED_OnPush_0, false ) );
    PAN_LEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnPush_1, false ) );
    PAN_LEFT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnRelease_2, false ) );
    PAN_RIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnPush_3, false ) );
    PAN_RIGHT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnRelease_4, false ) );
    TILT_UP.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_UP_OnPush_5, false ) );
    TILT_UP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_UP_OnRelease_6, false ) );
    TILT_DOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnPush_7, false ) );
    TILT_DOWN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnRelease_8, false ) );
    ZOOM_IN.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnPush_9, false ) );
    ZOOM_IN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnRelease_10, false ) );
    ZOOM_OUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnPush_11, false ) );
    ZOOM_OUT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnRelease_12, false ) );
    PRESET_RECALL.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_RECALL_OnPush_13, false ) );
    PRESET_SAVE.OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_SAVE_OnPush_14, false ) );
    DEVICE_RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( DEVICE_RX__DOLLAR___OnChange_15, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_VADDIO_ROBOSHOT_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint IP_CONNECTED__DigitalInput__ = 0;
const uint PAN_LEFT__DigitalInput__ = 1;
const uint PAN_RIGHT__DigitalInput__ = 2;
const uint TILT_UP__DigitalInput__ = 3;
const uint TILT_DOWN__DigitalInput__ = 4;
const uint ZOOM_IN__DigitalInput__ = 5;
const uint ZOOM_OUT__DigitalInput__ = 6;
const uint PRESET_RECALL__DigitalInput__ = 7;
const uint PRESET_SAVE__DigitalInput__ = 8;
const uint PRESET_SELECT__AnalogSerialInput__ = 0;
const uint DEVICE_RX__DOLLAR____AnalogSerialInput__ = 1;
const uint DEVICE_TX__DOLLAR____AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SCOMMAND : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  PANLEFT;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  PANRIGHT;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  PANSTOP;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  TILTUP;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  TILTDOWN;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  TILTSTOP;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  ZOOMIN;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  ZOOMOUT;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  ZOOMSTOP;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  PRESETRECALL;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  PRESETSAVE;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  STX;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  USERNAME;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  PASSWORD;
    
    
    public SCOMMAND( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        PANLEFT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        PANRIGHT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        PANSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TILTUP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TILTDOWN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TILTSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        ZOOMIN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        ZOOMOUT  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        ZOOMSTOP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        PRESETRECALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        PRESETSAVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, Owner );
        USERNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        PASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        
        
    }
    
}

}
