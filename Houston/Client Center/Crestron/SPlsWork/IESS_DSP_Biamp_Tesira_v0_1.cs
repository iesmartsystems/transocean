using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using Interface;

namespace UserModule_IESS_DSP_BIAMP_TESIRA_V0_1
{
    public class UserModuleClass_IESS_DSP_BIAMP_TESIRA_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        SplusTcpClient AUDIOCLIENT;
        UShortParameter UNIQUE_ID;
        Interface.SSH_Interface USERCLASS;
        SAUDIO AUDIODEVICE;
        ushort CONNECTION_STATUS = 0;
        ushort GVVOIPCOUNTER = 0;
        uint GVQUEUEPACER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 121;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 123;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
                    { 
                    __context__.SourceCodeLine = 125;
                    CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 126;
                    USERCLASS . Disconnect ( ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 128;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                        { 
                        __context__.SourceCodeLine = 130;
                        LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 132;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 133;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 135;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 137;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
                        { 
                        __context__.SourceCodeLine = 139;
                        CONNECTION_STATUS = (ushort) ( 1 ) ; 
                        __context__.SourceCodeLine = 140;
                        USERCLASS . Connect ( AUDIODEVICE.IPADDRESS .ToString(), (int)( AUDIODEVICE.IPPORT ), AUDIODEVICE.LOGINNAME .ToString(), AUDIODEVICE.LOGINPASSWORD .ToString()) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 142;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                            { 
                            __context__.SourceCodeLine = 144;
                            LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                            } 
                        
                        }
                    
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 155;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 157;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 159;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 160;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 162;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 163;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 165;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 166;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 169;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 171;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 172;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 173;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 175;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 176;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 178;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 179;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 180;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 181;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 183;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 191;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 193;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 195;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 196;
                LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                __context__.SourceCodeLine = 197;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 199;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 200;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 202;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 203;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 207;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                __context__.SourceCodeLine = 208;
                LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                } 
            
            __context__.SourceCodeLine = 211;
            LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 212;
            LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
            __context__.SourceCodeLine = 214;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 215;
                LVVOLUMELEVEL = (uint) ( 0 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 216;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 217;
                    LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 218;
            return (uint)( LVVOLUMELEVEL) ; 
            
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 222;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 224;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 225;
                    Trace( "Biamp RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 227;
                    Trace( "Biamp TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            
            __context__.SourceCodeLine = 232;
            AUDIODEVICE . TXQUEUE  .UpdateValue ( AUDIODEVICE . TXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 233;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , AUDIODEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 235;
                Functions.ProcessLogic ( ) ; 
                __context__.SourceCodeLine = 236;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_24__" , GVQUEUEPACER , __SPLS_TMPVAR__WAITLABEL_24___Callback ) ;
                __context__.SourceCodeLine = 233;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_24___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            __context__.SourceCodeLine = 240;
            LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , AUDIODEVICE . TXQUEUE )  ) ; 
            __context__.SourceCodeLine = 241;
            LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
            __context__.SourceCodeLine = 242;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                { 
                __context__.SourceCodeLine = 244;
                if ( Functions.TestForTrue  ( ( Functions.Find( "INIT:START" , LVTEMP ))  ) ) 
                    {
                    __context__.SourceCodeLine = 245;
                    GVQUEUEPACER = (uint) ( 30 ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 246;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "INIT:END" , LVTEMP ))  ) ) 
                        {
                        __context__.SourceCodeLine = 247;
                        GVQUEUEPACER = (uint) ( 5 ) ; 
                        }
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 250;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "Poll " , LVTEMP ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 252;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "Poll " , LVTEMP )  ) ; 
                            __context__.SourceCodeLine = 253;
                            LVTRASH  .UpdateValue ( Functions.Remove ( " " , LVTEMP )  ) ; 
                            __context__.SourceCodeLine = 254;
                            LVINDEX = (ushort) ( Functions.Atoi( LVTRASH ) ) ; 
                            __context__.SourceCodeLine = 255;
                            AUDIODEVICE . LASTPOLLINDEX = (ushort) ( LVINDEX ) ; 
                            __context__.SourceCodeLine = 256;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "2nd " , LVTEMP ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 258;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "2nd " , LVTEMP )  ) ; 
                                __context__.SourceCodeLine = 259;
                                LVTRASH  .UpdateValue ( Functions.Remove ( " " , LVTEMP )  ) ; 
                                __context__.SourceCodeLine = 260;
                                LVINDEX2ND = (ushort) ( Functions.Atoi( LVTRASH ) ) ; 
                                __context__.SourceCodeLine = 261;
                                AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( LVINDEX2ND ) ; 
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 264;
                        LVTEMP  .UpdateValue ( LVTEMP + AUDIODEVICE . ETX  ) ; 
                        __context__.SourceCodeLine = 265;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
                            { 
                            __context__.SourceCodeLine = 267;
                            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                                {
                                __context__.SourceCodeLine = 268;
                                USERCLASS . Command_In ( LVTEMP .ToString()) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 270;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
                                { 
                                __context__.SourceCodeLine = 272;
                                if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                                    {
                                    __context__.SourceCodeLine = 273;
                                    Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 276;
                                TX  .UpdateValue ( LVTEMP  ) ; 
                                }
                            
                            }
                        
                        __context__.SourceCodeLine = 277;
                        SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                        } 
                    
                    }
                
                } 
            
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void VOIPUPDATEAPPEARANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 286;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " callAppearance " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ LVINDEX ] ) )  ) ; 
        __context__.SourceCodeLine = 287;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        }
        
    private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 292;
        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATEAPPEARANCE[ LVINDEX , AUDIODEVICE.STATUSVOIPAPPEARANCE[ LVINDEX ] ] ))  ) ) 
            {
            __context__.SourceCodeLine = 293;
            AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
            }
        
        else 
            { 
            __context__.SourceCodeLine = 296;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " dtmf " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVNUM  ) ; 
            __context__.SourceCodeLine = 297;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        }
        
    private void POLLDEVICE (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 304;
        SETQUEUE (  __context__ , "INIT:START") ; 
        __context__.SourceCodeLine = 305;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 307;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.INSTANCETAGSPOLL[ LVCOUNTER ])  ) ) 
                { 
                __context__.SourceCodeLine = 309;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 311;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) )  ) ; 
                    __context__.SourceCodeLine = 312;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 313;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 315;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 317;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) )  ) ; 
                        __context__.SourceCodeLine = 318;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 319;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 321;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 323;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                            __context__.SourceCodeLine = 324;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 325;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 327;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 329;
                                AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 330;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                                __context__.SourceCodeLine = 331;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 332;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 333;
                                AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( 2 ) ; 
                                __context__.SourceCodeLine = 334;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                                __context__.SourceCodeLine = 335;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 336;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 338;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 340;
                                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                                    __context__.SourceCodeLine = 341;
                                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                                    __context__.SourceCodeLine = 342;
                                    SETQUEUE (  __context__ , LVSTRING) ; 
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 305;
            } 
        
        __context__.SourceCodeLine = 346;
        SETQUEUE (  __context__ , "INIT:END") ; 
        
        }
        
    private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 351;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
            { 
            __context__.SourceCodeLine = 353;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 355;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                __context__.SourceCodeLine = 356;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 357;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                __context__.SourceCodeLine = 358;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 359;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get minLevel " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 360;
                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVINDEX ) ) + " 2nd 3 " + LVSTRING  ) ; 
                __context__.SourceCodeLine = 361;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 362;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get maxLevel " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 363;
                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVINDEX ) ) + " 2nd 4 " + LVSTRING  ) ; 
                __context__.SourceCodeLine = 364;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 365;
                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 367;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 369;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 371;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 373;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe sourceSelection " + INSTANCETAGS [ LVINDEX ]  ) ; 
                        __context__.SourceCodeLine = 374;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 375;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 377;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 379;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 381;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 383;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ]  ) ; 
                                __context__.SourceCodeLine = 384;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 385;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 386;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get levelOutMin " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 387;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVINDEX ) ) + " 2nd 3 " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 388;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 389;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " get levelOutMax " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 390;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVINDEX ) ) + " 2nd 4 " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 391;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 392;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 394;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 396;
                                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 398;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 400;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 401;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 402;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 403;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-autoAnswer"  ) ; 
                                        __context__.SourceCodeLine = 404;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 405;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe callState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-callState"  ) ; 
                                        __context__.SourceCodeLine = 406;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 407;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-dndEnable"  ) ; 
                                        __context__.SourceCodeLine = 408;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 409;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe ringing " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-ringing"  ) ; 
                                        __context__.SourceCodeLine = 410;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 411;
                                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe cidUser " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-cidUser"  ) ; 
                                        __context__.SourceCodeLine = 412;
                                        SETQUEUE (  __context__ , LVSTRING) ; 
                                        __context__.SourceCodeLine = 413;
                                        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 415;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 417;
                                            GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                            __context__.SourceCodeLine = 418;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 419;
                                                AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 420;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-autoAnswer"  ) ; 
                                            __context__.SourceCodeLine = 421;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 422;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe callState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-callState"  ) ; 
                                            __context__.SourceCodeLine = 423;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 424;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-dndEnable"  ) ; 
                                            __context__.SourceCodeLine = 425;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 426;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe ringing " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-ringing"  ) ; 
                                            __context__.SourceCodeLine = 427;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            __context__.SourceCodeLine = 428;
                                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + " subscribe cidUser " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + " " + INSTANCETAGS [ LVINDEX ] + "-cidUser"  ) ; 
                                            __context__.SourceCodeLine = 429;
                                            SETQUEUE (  __context__ , LVSTRING) ; 
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        
        }
        
    private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 436;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 438;
            INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
            __context__.SourceCodeLine = 436;
            } 
        
        __context__.SourceCodeLine = 440;
        POLLDEVICE (  __context__  ) ; 
        __context__.SourceCodeLine = 441;
        SETQUEUE (  __context__ , "INIT:END") ; 
        
        }
        
    private void NEGOTIATION (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 445;
        SETQUEUE (  __context__ , "INIT:START") ; 
        __context__.SourceCodeLine = 446;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0018") ; 
        __context__.SourceCodeLine = 447;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0020") ; 
        __context__.SourceCodeLine = 448;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0023") ; 
        __context__.SourceCodeLine = 449;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0027") ; 
        __context__.SourceCodeLine = 450;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0024") ; 
        __context__.SourceCodeLine = 451;
        SETQUEUE (  __context__ , "\u00FF\u00FE\u0003") ; 
        __context__.SourceCodeLine = 452;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0001") ; 
        __context__.SourceCodeLine = 453;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0022") ; 
        __context__.SourceCodeLine = 454;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u001F") ; 
        __context__.SourceCodeLine = 455;
        SETQUEUE (  __context__ , "\u00FF\u00FE\u0005") ; 
        __context__.SourceCodeLine = 456;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0021") ; 
        __context__.SourceCodeLine = 457;
        SETQUEUE (  __context__ , "\u00FF\u00FE\u0001") ; 
        __context__.SourceCodeLine = 458;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0006") ; 
        __context__.SourceCodeLine = 459;
        SETQUEUE (  __context__ , "\u00FF\u00FC\u0000") ; 
        __context__.SourceCodeLine = 460;
        SETQUEUE (  __context__ , "\u00FF\u00FE\u0003") ; 
        __context__.SourceCodeLine = 461;
        SETQUEUE (  __context__ , "\u00FF\u00FE\u0001") ; 
        __context__.SourceCodeLine = 463;
        DEVICEONLINE (  __context__  ) ; 
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVTEMP;
        CrestronString LVSTRING;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 470;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , AUDIODEVICE.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 472;
            LVRX  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , AUDIODEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 473;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 2), LVRX )  ) ; 
            __context__.SourceCodeLine = 474;
            SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 475;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
                {
                __context__.SourceCodeLine = 476;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
                }
            
            __context__.SourceCodeLine = 477;
            if ( Functions.TestForTrue  ( ( Functions.Find( "publishToken\u0022:\u0022" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 479;
                LVTRASH  .UpdateValue ( Functions.Remove ( "publishToken\u0022:\u0022" , LVRX )  ) ; 
                __context__.SourceCodeLine = 480;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)64; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 482;
                    if ( Functions.TestForTrue  ( ( Functions.Find( INSTANCETAGS[ LVCOUNTER ] , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 484;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "value\u0022:" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 486;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSTYPE [ LVCOUNTER ]  ) ; 
                            __context__.SourceCodeLine = 487;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SVOL") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MVOL") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "RCMB") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SSEL") )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 489;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "value\u0022:" , LVRX )  ) ; 
                                __context__.SourceCodeLine = 490;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 492;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 493;
                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 495;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 497;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 498;
                                        VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 502;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 503;
                                            LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 505;
                                            LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 506;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "SSEL"))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 507;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( LVVOL ) ; 
                                            }
                                        
                                        else 
                                            { 
                                            __context__.SourceCodeLine = 510;
                                            AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            __context__.SourceCodeLine = 511;
                                            VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                            __context__.SourceCodeLine = 512;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 513;
                                                AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 517;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "VOIP") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "POTS") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 519;
                                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 0 ) ;
                                    ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                    int __FN_FORSTEP_VAL__2 = (int)1; 
                                    for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                        { 
                                        __context__.SourceCodeLine = 521;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 523;
                                            LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                            __context__.SourceCodeLine = 524;
                                            break ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 519;
                                        } 
                                    
                                    __context__.SourceCodeLine = 527;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOIPNUM > 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOIPNUM <= 2 ) )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 529;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 531;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "autoAnswer" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 533;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 534;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 536;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "dndEnable" , LVRX ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 538;
                                                    AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 539;
                                                    VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 541;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "ringing" , LVRX ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 542;
                                                        VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 544;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 546;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "autoAnswer" , LVRX ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 548;
                                                    AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 549;
                                                    VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 551;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "dndEnable" , LVRX ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 553;
                                                        AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 554;
                                                        VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 556;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "ringing" , LVRX ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 557;
                                                            VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 561;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "cidUser" , LVRX ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 562;
                                                    LVTEMP  .UpdateValue ( "cidUser"  ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 563;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "callState" , LVRX ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 564;
                                                        LVTEMP  .UpdateValue ( "callState"  ) ; 
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                }
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 480;
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 572;
                if ( Functions.TestForTrue  ( ( Functions.Find( "+OK " , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 574;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "+OK " , LVRX )  ) ; 
                    __context__.SourceCodeLine = 575;
                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.LASTPOLLINDEX)  ) ) 
                        { 
                        __context__.SourceCodeLine = 577;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "value\u0022:" , LVRX ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 579;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "value\u0022:" , LVRX )  ) ; 
                            __context__.SourceCodeLine = 580;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSTYPE [ AUDIODEVICE.LASTPOLLINDEX ]  ) ; 
                            __context__.SourceCodeLine = 581;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SMAT") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MMAT") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "LGCS") )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 583;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 585;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 586;
                                    VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 588;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 590;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 591;
                                        VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 594;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "ROUT") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SSEL") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 596;
                                    GROUP_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 598;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "RCMB"))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 600;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVRX ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 602;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 603;
                                            VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 605;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVRX ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 607;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ AUDIODEVICE.LASTPOLLINDEX] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 608;
                                                VOLUMEMUTE_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 612;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 2))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 613;
                                                    GROUP_FB [ AUDIODEVICE.LASTPOLLINDEX]  .Value = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 614;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 3))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 616;
                                                        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                            {
                                                            __context__.SourceCodeLine = 617;
                                                            LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 619;
                                                            LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                            }
                                                        
                                                        __context__.SourceCodeLine = 620;
                                                        AUDIODEVICE . VOLUMEMIN [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 622;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 4))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 624;
                                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                                {
                                                                __context__.SourceCodeLine = 625;
                                                                LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 627;
                                                                LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                                }
                                                            
                                                            __context__.SourceCodeLine = 628;
                                                            AUDIODEVICE . VOLUMEMAX [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 632;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "SVOL") ) || Functions.TestForTrue ( Functions.BoolToInt (LVSTRING == "MVOL") )) ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 634;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 3))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 636;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 637;
                                                    LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 639;
                                                    LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 640;
                                                AUDIODEVICE . VOLUMEMIN [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 642;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.LASTPOLLSECONDINDEX == 4))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 644;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVRX ))  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 645;
                                                        LVVOL = (short) ( (Functions.Atoi( LVRX ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 647;
                                                        LVVOL = (short) ( Functions.Atoi( LVRX ) ) ; 
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 648;
                                                    AUDIODEVICE . VOLUMEMAX [ AUDIODEVICE.LASTPOLLINDEX] = (short) ( LVVOL ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 653;
                    AUDIODEVICE . LASTPOLLINDEX = (ushort) ( 0 ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 470;
            } 
        
        
        }
        
    private void REGDELEGATES (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 660;
        // RegisterDelegate( USERCLASS , SENDFROMDEVICE , PROCESSFROMDEVICE ) 
        USERCLASS .SendFromDevice  = PROCESSFROMDEVICE; ; 
        __context__.SourceCodeLine = 661;
        // RegisterDelegate( USERCLASS , SENDCONNECTIONSTATUS , PROCESSCONNECTIONSTATUS ) 
        USERCLASS .SendConnectionStatus  = PROCESSCONNECTIONSTATUS; ; 
        __context__.SourceCodeLine = 662;
        // RegisterDelegate( USERCLASS , SENDFINGERPRINT , PROCESSFINGERPRINT ) 
        USERCLASS .SendFingerprint  = PROCESSFINGERPRINT; ; 
        
        }
        
    public void PROCESSFROMDEVICE ( SimplSharpString RETURNFROMDEVICE ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
            
            __context__.SourceCodeLine = 666;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
                { 
                __context__.SourceCodeLine = 668;
                AUDIODEVICE . RXQUEUE  =  ( AUDIODEVICE . RXQUEUE + RETURNFROMDEVICE  .ToString()  )  .ToString() ; 
                __context__.SourceCodeLine = 669;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 670;
                    PARSEFEEDBACK (  __context__  ) ; 
                    }
                
                } 
            
            
            
        }
        finally { ObjectFinallyHandler(); }
        }
        
    public void PROCESSFINGERPRINT ( SimplSharpString FINGERPRINT ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
            
            
            
        }
        finally { ObjectFinallyHandler(); }
        }
        
    private void RETRYCONNECTION (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 679;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
            { 
            __context__.SourceCodeLine = 681;
            USERCLASS . Connect ( AUDIODEVICE.IPADDRESS .ToString(), (int)( AUDIODEVICE.IPPORT ), AUDIODEVICE.LOGINNAME .ToString(), AUDIODEVICE.LOGINPASSWORD .ToString()) ; 
            } 
        
        
        }
        
    private void FNACCEPT_NEW_KEY (  SplusExecutionContext __context__ ) 
        { 
        
        __context__.SourceCodeLine = 686;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
            { 
            __context__.SourceCodeLine = 688;
            CONNECTION_STATUS = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 689;
            CONNECT_STATUS_FB  .Value = (ushort) ( CONNECTION_STATUS ) ; 
            __context__.SourceCodeLine = 690;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 691;
            USERCLASS . Accept_New_Key ( ) ; 
            __context__.SourceCodeLine = 692;
            USERCLASS . Connect ( AUDIODEVICE.IPADDRESS .ToString(), (int)( AUDIODEVICE.IPPORT ), AUDIODEVICE.LOGINNAME .ToString(), AUDIODEVICE.LOGINPASSWORD .ToString()) ; 
            } 
        
        
        }
        
    public void PROCESSCONNECTIONSTATUS ( SimplSharpString CONNECTEDVALUE ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusSimplSharpDelegateThreadStartCode();
            
            __context__.SourceCodeLine = 697;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
                { 
                __context__.SourceCodeLine = 699;
                Functions.ProcessLogic ( ) ; 
                __context__.SourceCodeLine = 700;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECTEDVALUE .ToString() == "True"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 702;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECTION_STATUS != 2))  ) ) 
                        { 
                        __context__.SourceCodeLine = 704;
                        CONNECTION_STATUS = (ushort) ( 2 ) ; 
                        __context__.SourceCodeLine = 705;
                        CONNECT_STATUS_FB  .Value = (ushort) ( CONNECTION_STATUS ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 707;
                    CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 708;
                    AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 709;
                    CreateWait ( "__SPLS_TMPVAR__WAITLABEL_25__" , 500 , __SPLS_TMPVAR__WAITLABEL_25___Callback ) ;
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 712;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECTEDVALUE .ToString() == "False"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 714;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECTION_STATUS != 3))  ) ) 
                            { 
                            __context__.SourceCodeLine = 716;
                            CONNECTION_STATUS = (ushort) ( 3 ) ; 
                            __context__.SourceCodeLine = 717;
                            CONNECT_STATUS_FB  .Value = (ushort) ( CONNECTION_STATUS ) ; 
                            } 
                        
                        __context__.SourceCodeLine = 719;
                        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 720;
                        AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 721;
                        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 722;
                        AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
                        __context__.SourceCodeLine = 723;
                        Functions.ProcessLogic ( ) ; 
                        __context__.SourceCodeLine = 724;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.STATUSCONNECTREQUEST == 1))  ) ) 
                            { 
                            __context__.SourceCodeLine = 726;
                            CONNECTION_STATUS = (ushort) ( 1 ) ; 
                            __context__.SourceCodeLine = 727;
                            CONNECT_STATUS_FB  .Value = (ushort) ( CONNECTION_STATUS ) ; 
                            __context__.SourceCodeLine = 728;
                            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_26__" , 5 , __SPLS_TMPVAR__WAITLABEL_26___Callback ) ;
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 732;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECTEDVALUE .ToString() == "Non-Matching Fingerprint"))  ) ) 
                            { 
                            __context__.SourceCodeLine = 734;
                            FNACCEPT_NEW_KEY (  __context__  ) ; 
                            __context__.SourceCodeLine = 735;
                            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
                            } 
                        
                        }
                    
                    }
                
                } 
            
            
            
        }
        finally { ObjectFinallyHandler(); }
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_25___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            {
            __context__.SourceCodeLine = 710;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_26___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 729;
            RETRYCONNECTION (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 63, this );
        
        
        __context__.SourceCodeLine = 746;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 748;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 749;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 750;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            __context__.SourceCodeLine = 751;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_27__" , 200 , __SPLS_TMPVAR__WAITLABEL_27___Callback ) ;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_27___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 752;
            NEGOTIATION (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 758;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 760;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 761;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 762;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 763;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 768;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 770;
            CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
            __context__.SourceCodeLine = 771;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
                { 
                __context__.SourceCodeLine = 773;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 774;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 778;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 779;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 780;
                CONNECT_FB  .Value = (ushort) ( 0 ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 787;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 23))  ) ) 
            { 
            __context__.SourceCodeLine = 789;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCONNECTED ))  ) ) 
                { 
                __context__.SourceCodeLine = 791;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 792;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                } 
            
            __context__.SourceCodeLine = 794;
            AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
            __context__.SourceCodeLine = 795;
            Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
            __context__.SourceCodeLine = 796;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ))  ) ) 
                {
                __context__.SourceCodeLine = 797;
                PARSEFEEDBACK (  __context__  ) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 802;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 804;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 805;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 809;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 810;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 815;
        POLLDEVICE (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 819;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 820;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 824;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 828;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 832;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        __context__.SourceCodeLine = 833;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 22))  ) ) 
            { 
            __context__.SourceCodeLine = 835;
            REGDELEGATES (  __context__  ) ; 
            __context__.SourceCodeLine = 836;
            USERCLASS . Accept_Any_Key ( (int)( 1 )) ; 
            __context__.SourceCodeLine = 837;
            USERCLASS . Unique_ID ( (ushort)( UNIQUE_ID  .Value )) ; 
            __context__.SourceCodeLine = 838;
            USERCLASS . LoadSettings ( ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 843;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 847;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 853;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 854;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 855;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 857;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 858;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 859;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 861;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 862;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 864;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 865;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 868;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 870;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 872;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                __context__.SourceCodeLine = 873;
                POLLDEVICE (  __context__  ) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 879;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 880;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 882;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 883;
            CreateWait ( "VOLUP" , 20 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 880;
            } 
        
        __context__.SourceCodeLine = 913;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            __context__.SourceCodeLine = 886;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 888;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 889;
                MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 890;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 891;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 892;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 893;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 894;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 895;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 899;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] != AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ]))  ) ) 
                    { 
                    __context__.SourceCodeLine = 901;
                    AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ; 
                    __context__.SourceCodeLine = 902;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 903;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 904;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            {
                            __context__.SourceCodeLine = 905;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 906;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 907;
                    AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                    } 
                
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 917;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 918;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 920;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 921;
            CreateWait ( "VOLDN" , 20 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 918;
            } 
        
        __context__.SourceCodeLine = 952;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            
            __context__.SourceCodeLine = 924;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 926;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 927;
                MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 928;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 929;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 930;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 931;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 932;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 933;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 938;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] != AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ]))  ) ) 
                    { 
                    __context__.SourceCodeLine = 940;
                    AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ; 
                    __context__.SourceCodeLine = 941;
                    MakeString ( LVSTRING , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                    __context__.SourceCodeLine = 942;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                        {
                        __context__.SourceCodeLine = 943;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 944;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            {
                            __context__.SourceCodeLine = 945;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 946;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 947;
                    AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                    } 
                
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 957;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 958;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 960;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 961;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 963;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 965;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                __context__.SourceCodeLine = 966;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 967;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 968;
                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                __context__.SourceCodeLine = 969;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 971;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 973;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                    __context__.SourceCodeLine = 974;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 975;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 976;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 977;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 979;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 981;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                        __context__.SourceCodeLine = 982;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 983;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 984;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 985;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 987;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 989;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                            __context__.SourceCodeLine = 990;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 991;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 992;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 993;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 999;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1000;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1002;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 1003;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1005;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1007;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                __context__.SourceCodeLine = 1008;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1009;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 1010;
                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                __context__.SourceCodeLine = 1011;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1013;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1015;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                    __context__.SourceCodeLine = 1016;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1017;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 1018;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 1019;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1021;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1023;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                        __context__.SourceCodeLine = 1024;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1025;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1026;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 1027;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1029;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1031;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                            __context__.SourceCodeLine = 1032;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1033;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1034;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 1035;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1041;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1042;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1044;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1046;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                __context__.SourceCodeLine = 1047;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1049;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1051;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                    __context__.SourceCodeLine = 1052;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1053;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 1054;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 1055;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1057;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1059;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                        __context__.SourceCodeLine = 1060;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1061;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1062;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 1063;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1065;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1067;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                            __context__.SourceCodeLine = 1068;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1069;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1070;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 1071;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1073;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1075;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " false"  ) ; 
                                __context__.SourceCodeLine = 1076;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1077;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 1078;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 1079;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1084;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1086;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set mute " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                __context__.SourceCodeLine = 1087;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1089;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1091;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                    __context__.SourceCodeLine = 1092;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1093;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                    __context__.SourceCodeLine = 1094;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 1095;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1097;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1099;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                        __context__.SourceCodeLine = 1100;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1101;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1102;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 1103;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1105;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1107;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                            __context__.SourceCodeLine = 1108;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1109;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1110;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 1111;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1113;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1115;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " true"  ) ; 
                                __context__.SourceCodeLine = 1116;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1117;
                                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get state " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 1118;
                                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                                __context__.SourceCodeLine = 1119;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1127;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1128;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1129;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1131;
            MakeString ( LVSTRING , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1132;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 1133;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set level " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 1134;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1135;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set levelOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + LVSTRING  ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 1136;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1143;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1144;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1146;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
            __context__.SourceCodeLine = 1147;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1148;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get group " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1149;
            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
            __context__.SourceCodeLine = 1150;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1151;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 1153;
                if ( Functions.TestForTrue  ( ( Functions.Find( AUDIODEVICE.INSTANCETAGSNAME[ AUDIODEVICE.LASTINDEX ] , AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1155;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ] + " get muteOut " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) )  ) ; 
                    __context__.SourceCodeLine = 1156;
                    LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " " + LVSTRING  ) ; 
                    __context__.SourceCodeLine = 1157;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                __context__.SourceCodeLine = 1151;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1161;
            if ( Functions.TestForTrue  ( ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1163;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                __context__.SourceCodeLine = 1164;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1165;
                LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get input " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                __context__.SourceCodeLine = 1166;
                LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                __context__.SourceCodeLine = 1167;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1169;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1171;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set sourceSelection " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                    __context__.SourceCodeLine = 1172;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1174;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1176;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                        __context__.SourceCodeLine = 1177;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        __context__.SourceCodeLine = 1178;
                        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspoint " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 1179;
                        LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                        __context__.SourceCodeLine = 1180;
                        SETQUEUE (  __context__ , LVSTRING) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1182;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1184;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " set crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + " " + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) )  ) ; 
                            __context__.SourceCodeLine = 1185;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            __context__.SourceCodeLine = 1186;
                            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + " get crosspointLevelState " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 1187;
                            LVSTRING  .UpdateValue ( "Poll " + Functions.ItoA (  (int) ( AUDIODEVICE.LASTINDEX ) ) + " " + LVSTRING  ) ; 
                            __context__.SourceCodeLine = 1188;
                            SETQUEUE (  __context__ , LVSTRING) ; 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1193;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1194;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1199;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1200;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1202;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 1203;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1207;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 1208;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1214;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1215;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
        __context__.SourceCodeLine = 1216;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1221;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1222;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set autoAnswer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
        __context__.SourceCodeLine = 1223;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1228;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1229;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1231;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
            __context__.SourceCodeLine = 1232;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1236;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
            __context__.SourceCodeLine = 1237;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1243;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1244;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " true"  ) ; 
        __context__.SourceCodeLine = 1245;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1250;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1251;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " set dndEnable " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " false"  ) ; 
        __context__.SourceCodeLine = 1252;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1257;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1258;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1260;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " answer " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1261;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1267;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1268;
        LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " end " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
        __context__.SourceCodeLine = 1269;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1274;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1275;
        AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( (AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] + 1) ) ; 
        __context__.SourceCodeLine = 1276;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] > 6 ))  ) ) 
            {
            __context__.SourceCodeLine = 1277;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 1278;
        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1283;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1284;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1286;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " lconf " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1287;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1293;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1294;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1296;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " redial " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1297;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1302;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1306;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1310;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1314;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1318;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1322;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1326;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1330;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1334;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1338;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1342;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        uint LVVOL = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
        
        
        __context__.SourceCodeLine = 1348;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1352;
        NEGOTIATION (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1357;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1358;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1360;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATEAPPEARANCE[ AUDIODEVICE.LASTINDEX , AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1362;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1364;
                    LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " dial " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) ) + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1366;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    } 
                
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1374;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1375;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1377;
            LVSTRING  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] + " onHook " + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ] ) ) + " " + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 1379;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1385;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1386;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1390;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1391;
        AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOIP_CALLAPPEARANCE[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        __context__.SourceCodeLine = 1392;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( AUDIODEVICE.STATUSVOIPAPPEARANCE[ AUDIODEVICE.LASTINDEX ] > 6 ))  ) ) 
            {
            __context__.SourceCodeLine = 1393;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ AUDIODEVICE.LASTINDEX] = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 1394;
        VOIPUPDATEAPPEARANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1402;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1403;
        GVQUEUEPACER = (uint) ( 5 ) ; 
        __context__.SourceCodeLine = 1405;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1406;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( "default"  ) ; 
        __context__.SourceCodeLine = 1407;
        AUDIODEVICE . ETX  .UpdateValue ( "\u000D"  ) ; 
        __context__.SourceCodeLine = 1408;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1410;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 40 ) ) ) ; 
            __context__.SourceCodeLine = 1411;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 10 ) ; 
            __context__.SourceCodeLine = 1412;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1408;
            } 
        
        __context__.SourceCodeLine = 1414;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)2; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
            {
            __context__.SourceCodeLine = 1415;
            AUDIODEVICE . STATUSVOIPAPPEARANCE [ LVCOUNTER] = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1414;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 8191, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 511, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 127, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    UNIQUE_ID = new UShortParameter( UNIQUE_ID__Parameter__, this );
    m_ParameterList.Add( UNIQUE_ID__Parameter__, UNIQUE_ID );
    
    __SPLS_TMPVAR__WAITLABEL_24___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_24___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_25___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_25___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_26___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_26___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_27___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_27___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    USERCLASS  = new Interface.SSH_Interface();
    
    
}

public UserModuleClass_IESS_DSP_BIAMP_TESIRA_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_24___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_25___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_26___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_27___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint INSTANCETAGS__AnalogSerialInput__ = 200;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 264;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;
const uint UNIQUE_ID__Parameter__ = 10;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [,] STATUSVOIPCALLSTATEAPPEARANCE;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSVOIPAPPEARANCE;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(9, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(10, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(11, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(12, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(14, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(15, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(16, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(17, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(18, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(20, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(24, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(25, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(26, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(27, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(28, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(29, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(30, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(31, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPAPPEARANCE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOIPCALLSTATEAPPEARANCE  = new ushort[ 3,7 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32767, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
