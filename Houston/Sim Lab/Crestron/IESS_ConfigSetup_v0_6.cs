using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_CONFIGSETUP_V0_6
{
    public class UserModuleClass_IESS_CONFIGSETUP_V0_6 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput SAVECONFIG;
        Crestron.Logos.SplusObjects.DigitalInput LOADCONFIG;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> ENABLE_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> LISTENUP_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> LISTENDOWN_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> LISTENMUTE_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> TALKUP_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> TALKDOWN_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> TALKMUTE_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> ENABLE_ALERT;
        Crestron.Logos.SplusObjects.StringInput FILENAME;
        Crestron.Logos.SplusObjects.DigitalOutput LOADCOMPLETE_FB;
        Crestron.Logos.SplusObjects.DigitalOutput WRITECOMPLETE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> ENABLED_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> LISTENMUTE_ON_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> LISTENMUTE_OFF_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> TALKMUTE_ON_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> TALKMUTE_OFF_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> ENABLED_ALERT;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMENABLEDCOUNT;
        Crestron.Logos.SplusObjects.AnalogOutput ROOMCOMBO;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> LISTENVOL_ROOM;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> TALKVOL_ROOM;
        SROOM [] ROOM;
        SALERTS [] ALERT;
        ushort GVSTEP = 0;
        ushort GVROOMA = 0;
        ushort GVROOMB = 0;
        ushort GVROOMC = 0;
        ushort GVALERTENABLECOUNT = 0;
        short GVMAXVOL = 0;
        short GVMINVOL = 0;
        private void UPDATELISTENMUTE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            
            __context__.SourceCodeLine = 95;
            if ( Functions.TestForTrue  ( ( ROOM[ LVINDEX ].LISTENMUTE)  ) ) 
                { 
                __context__.SourceCodeLine = 97;
                LISTENMUTE_ON_ROOM [ LVINDEX]  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 98;
                LISTENMUTE_OFF_ROOM [ LVINDEX]  .Value = (ushort) ( 0 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 102;
                LISTENMUTE_ON_ROOM [ LVINDEX]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 103;
                LISTENMUTE_OFF_ROOM [ LVINDEX]  .Value = (ushort) ( 1 ) ; 
                } 
            
            
            }
            
        private void UPDATETALKMUTE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            
            __context__.SourceCodeLine = 109;
            if ( Functions.TestForTrue  ( ( ROOM[ LVINDEX ].TALKMUTE)  ) ) 
                { 
                __context__.SourceCodeLine = 111;
                TALKMUTE_ON_ROOM [ LVINDEX]  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 112;
                TALKMUTE_OFF_ROOM [ LVINDEX]  .Value = (ushort) ( 0 ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 116;
                TALKMUTE_ON_ROOM [ LVINDEX]  .Value = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 117;
                TALKMUTE_OFF_ROOM [ LVINDEX]  .Value = (ushort) ( 1 ) ; 
                } 
            
            
            }
            
        private void ROOMCONFIGURATION (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVVALUE = 0;
            
            
            __context__.SourceCodeLine = 125;
            LVVALUE = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 126;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 128;
                if ( Functions.TestForTrue  ( ( ROOM[ LVCOUNTER ].ENABLED)  ) ) 
                    { 
                    __context__.SourceCodeLine = 130;
                    
                        {
                        int __SPLS_TMPVAR__SWTCH_1__ = ((int)LVCOUNTER);
                        
                            { 
                            if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 1) ) ) ) 
                                {
                                __context__.SourceCodeLine = 132;
                                LVVALUE = (ushort) ( (LVVALUE + GVROOMA) ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 2) ) ) ) 
                                {
                                __context__.SourceCodeLine = 133;
                                LVVALUE = (ushort) ( (LVVALUE + GVROOMB) ) ; 
                                }
                            
                            else if  ( Functions.TestForTrue  (  ( __SPLS_TMPVAR__SWTCH_1__ == ( 3) ) ) ) 
                                {
                                __context__.SourceCodeLine = 134;
                                LVVALUE = (ushort) ( (LVVALUE + GVROOMC) ) ; 
                                }
                            
                            } 
                            
                        }
                        
                    
                    } 
                
                __context__.SourceCodeLine = 126;
                } 
            
            __context__.SourceCodeLine = 138;
            ROOMCOMBO  .Value = (ushort) ( LVVALUE ) ; 
            
            }
            
        private void COUNTENABLEDROOMS (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNT = 0;
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 143;
            LVCOUNT = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 144;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)3; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 146;
                if ( Functions.TestForTrue  ( ( ROOM[ LVCOUNTER ].ENABLED)  ) ) 
                    {
                    __context__.SourceCodeLine = 147;
                    LVCOUNT = (ushort) ( (LVCOUNT + 1) ) ; 
                    }
                
                __context__.SourceCodeLine = 144;
                } 
            
            __context__.SourceCodeLine = 149;
            ROOMENABLEDCOUNT  .Value = (ushort) ( LVCOUNT ) ; 
            __context__.SourceCodeLine = 150;
            ROOMCONFIGURATION (  __context__  ) ; 
            
            }
            
        private void COUNTENABLEDALERTS (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 155;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 157;
                if ( Functions.TestForTrue  ( ( ALERT[ LVCOUNTER ].ENABLED)  ) ) 
                    {
                    __context__.SourceCodeLine = 158;
                    GVALERTENABLECOUNT = (ushort) ( (GVALERTENABLECOUNT + 1) ) ; 
                    }
                
                __context__.SourceCodeLine = 155;
                } 
            
            
            }
            
        private void PARSEDATAFROMCONFIG (  SplusExecutionContext __context__, CrestronString LVDATA ) 
            { 
            CrestronString LVDEVICE;
            CrestronString LVINDEXTEMP;
            CrestronString LVTRASH;
            LVDEVICE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            LVINDEXTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINSTANCEINDEX = 0;
            ushort LVROOMINDEX = 0;
            ushort LVALERTINDEX = 0;
            
            
            __context__.SourceCodeLine = 165;
            Trace( "Parse String: {0}", LVDATA ) ; 
            __context__.SourceCodeLine = 167;
            if ( Functions.TestForTrue  ( ( Functions.Find( "//" , LVDATA ))  ) ) 
                { 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 173;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Room " , LVDATA ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 175;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Room " , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 176;
                    LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                    __context__.SourceCodeLine = 177;
                    LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                    __context__.SourceCodeLine = 178;
                    LVROOMINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                    __context__.SourceCodeLine = 179;
                    Trace( "Parse String: Room #{0:d}", (short)LVROOMINDEX) ; 
                    __context__.SourceCodeLine = 180;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Enable-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 182;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "Enable-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 183;
                        ROOM [ LVROOMINDEX] . ENABLED = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                        __context__.SourceCodeLine = 184;
                        ENABLED_ROOM [ LVROOMINDEX]  .Value = (ushort) ( ROOM[ LVROOMINDEX ].ENABLED ) ; 
                        __context__.SourceCodeLine = 185;
                        Trace( "Parse String: Room #{0:d}, Enable-{1:d}", (short)LVROOMINDEX, (short)ROOM[ LVROOMINDEX ].ENABLED) ; 
                        } 
                    
                    __context__.SourceCodeLine = 187;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "ListenVol-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 189;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "ListenVol-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 190;
                        ROOM [ LVROOMINDEX] . LISTENVOL = (short) ( Functions.Atoi( LVDATA ) ) ; 
                        __context__.SourceCodeLine = 191;
                        LISTENVOL_ROOM [ LVROOMINDEX]  .Value = (ushort) ( ROOM[ LVROOMINDEX ].LISTENVOL ) ; 
                        __context__.SourceCodeLine = 192;
                        Trace( "Parse String: Room #{0:d}, ListenVol-{1:d}", (short)LVROOMINDEX, (short)ROOM[ LVROOMINDEX ].LISTENVOL) ; 
                        } 
                    
                    __context__.SourceCodeLine = 194;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "TalkVol-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 196;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "TalkVol-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 197;
                        ROOM [ LVROOMINDEX] . TALKVOL = (short) ( Functions.Atoi( LVDATA ) ) ; 
                        __context__.SourceCodeLine = 198;
                        TALKVOL_ROOM [ LVROOMINDEX]  .Value = (ushort) ( ROOM[ LVROOMINDEX ].TALKVOL ) ; 
                        __context__.SourceCodeLine = 199;
                        Trace( "Parse String: Room #{0:d}, TalkVol-{1:d}", (short)LVROOMINDEX, (short)ROOM[ LVROOMINDEX ].TALKVOL) ; 
                        } 
                    
                    __context__.SourceCodeLine = 201;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "ListenMute-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 203;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "ListenMute-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 204;
                        ROOM [ LVROOMINDEX] . LISTENMUTE = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                        __context__.SourceCodeLine = 205;
                        UPDATELISTENMUTE (  __context__ , (ushort)( LVROOMINDEX )) ; 
                        __context__.SourceCodeLine = 206;
                        Trace( "Parse String: Room #{0:d}, ListenMute-{1:d}", (short)LVROOMINDEX, (short)ROOM[ LVROOMINDEX ].LISTENMUTE) ; 
                        } 
                    
                    __context__.SourceCodeLine = 208;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "TalkMute-" , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 210;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "TalkMute-" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 211;
                        ROOM [ LVROOMINDEX] . TALKMUTE = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                        __context__.SourceCodeLine = 212;
                        UPDATETALKMUTE (  __context__ , (ushort)( LVROOMINDEX )) ; 
                        __context__.SourceCodeLine = 213;
                        Trace( "Parse String: Room #{0:d}, TalkMute-{1:d}", (short)LVROOMINDEX, (short)ROOM[ LVROOMINDEX ].TALKMUTE) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 216;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Alert " , LVDATA ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 218;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "Alert " , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 219;
                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( ":" , LVDATA )  ) ; 
                        __context__.SourceCodeLine = 220;
                        LVINDEXTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINDEXTEMP ) - 1), LVINDEXTEMP )  ) ; 
                        __context__.SourceCodeLine = 221;
                        LVALERTINDEX = (ushort) ( Functions.Atoi( LVINDEXTEMP ) ) ; 
                        __context__.SourceCodeLine = 222;
                        Trace( "Parse String: Alert #{0:d}", (short)LVALERTINDEX) ; 
                        __context__.SourceCodeLine = 223;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "Enable-" , LVDATA ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 225;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "Enable-" , LVDATA )  ) ; 
                            __context__.SourceCodeLine = 226;
                            ALERT [ LVALERTINDEX] . ENABLED = (ushort) ( Functions.Atoi( LVDATA ) ) ; 
                            __context__.SourceCodeLine = 227;
                            ENABLED_ALERT [ LVALERTINDEX]  .Value = (ushort) ( ALERT[ LVALERTINDEX ].ENABLED ) ; 
                            __context__.SourceCodeLine = 228;
                            Trace( "Parse String: Alert #{0:d}, Enable-{1:d}", (short)LVALERTINDEX, (short)ALERT[ LVALERTINDEX ].ENABLED) ; 
                            } 
                        
                        } 
                    
                    }
                
                } 
            
            
            }
            
        private void FILEOPENCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVREADFILE;
            CrestronString LVREADLINE;
            CrestronString LVFILENAME;
            CrestronString LVTRASH;
            LVREADFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
            LVREADLINE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 238;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 240;
                LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
                __context__.SourceCodeLine = 241;
                StartFileOperations ( ) ; 
                __context__.SourceCodeLine = 242;
                LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 0) ) ) ; 
                __context__.SourceCodeLine = 243;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 245;
                    LVREAD = (ushort) ( FileRead( (short)( LVHANDLE ) , LVREADFILE , (ushort)( 16383 ) ) ) ; 
                    __context__.SourceCodeLine = 246;
                    while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , LVREADFILE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 248;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , LVREADFILE )  ) ; 
                        __context__.SourceCodeLine = 249;
                        LVREADLINE  .UpdateValue ( Functions.Remove ( (Functions.Length( LVREADLINE ) - 2), LVREADLINE )  ) ; 
                        __context__.SourceCodeLine = 250;
                        Trace( "CONFIG READ: {0}", LVREADLINE ) ; 
                        __context__.SourceCodeLine = 251;
                        PARSEDATAFROMCONFIG (  __context__ , LVREADLINE) ; 
                        __context__.SourceCodeLine = 246;
                        } 
                    
                    __context__.SourceCodeLine = 254;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVREADFILE ) > 2 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 256;
                        Trace( "CONFIG READ: {0}", LVREADFILE ) ; 
                        __context__.SourceCodeLine = 257;
                        PARSEDATAFROMCONFIG (  __context__ , LVREADFILE) ; 
                        } 
                    
                    __context__.SourceCodeLine = 260;
                    LVTRASH  .UpdateValue ( Functions.Remove ( LVREADLINE , LVREADFILE )  ) ; 
                    __context__.SourceCodeLine = 261;
                    LOADCOMPLETE_FB  .Value = (ushort) ( 1 ) ; 
                    __context__.SourceCodeLine = 262;
                    LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 265;
                    LOADCOMPLETE_FB  .Value = (ushort) ( 0 ) ; 
                    }
                
                __context__.SourceCodeLine = 266;
                COUNTENABLEDROOMS (  __context__  ) ; 
                __context__.SourceCodeLine = 267;
                COUNTENABLEDALERTS (  __context__  ) ; 
                __context__.SourceCodeLine = 268;
                EndFileOperations ( ) ; 
                } 
            
            
            }
            
        private void WRITEDATATOCONFIG (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            ushort LVCOUNTERTAGS = 0;
            ushort LVREAD = 0;
            
            short LVHANDLE = 0;
            
            CrestronString LVFILENAME;
            CrestronString LVWRITEFILE;
            LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVWRITEFILE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, this );
            
            
            __context__.SourceCodeLine = 277;
            LVFILENAME  .UpdateValue ( "\\NVRAM\\" + FILENAME  ) ; 
            __context__.SourceCodeLine = 278;
            LVWRITEFILE  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 279;
            StartFileOperations ( ) ; 
            __context__.SourceCodeLine = 280;
            LVHANDLE = (short) ( FileDelete( LVFILENAME ) ) ; 
            __context__.SourceCodeLine = 281;
            LVHANDLE = (short) ( FileOpenShared( LVFILENAME ,(ushort) (16384 | 1) ) ) ; 
            __context__.SourceCodeLine = 282;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVHANDLE >= 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 284;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)3; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 286;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + " SETUP\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 287;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":Enable-" + Functions.ItoA (  (int) ( ROOM[ LVCOUNTER ].ENABLED ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 288;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":ListenVol-" + Functions.ItoA (  (int) ( ROOM[ LVCOUNTER ].LISTENVOL ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 289;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":TalkVol-" + Functions.ItoA (  (int) ( ROOM[ LVCOUNTER ].TALKVOL ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 290;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":ListenMute-" + Functions.ItoA (  (int) ( ROOM[ LVCOUNTER ].LISTENMUTE ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 291;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Room " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":TalkMute-" + Functions.ItoA (  (int) ( ROOM[ LVCOUNTER ].TALKMUTE ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 284;
                    } 
                
                __context__.SourceCodeLine = 293;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)16; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 295;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "//ALERTS SETUP\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 296;
                    LVWRITEFILE  .UpdateValue ( LVWRITEFILE + "Alert " + Functions.ItoA (  (int) ( LVCOUNTER ) ) + ":Enable-" + Functions.ItoA (  (int) ( ALERT[ LVCOUNTER ].ENABLED ) ) + "\u000D\u000A"  ) ; 
                    __context__.SourceCodeLine = 293;
                    } 
                
                __context__.SourceCodeLine = 298;
                FileWrite (  (short) ( LVHANDLE ) , LVWRITEFILE ,  (ushort) ( Functions.Length( LVWRITEFILE ) ) ) ; 
                } 
            
            __context__.SourceCodeLine = 300;
            LVREAD = (ushort) ( FileClose( (short)( LVHANDLE ) ) ) ; 
            __context__.SourceCodeLine = 301;
            EndFileOperations ( ) ; 
            __context__.SourceCodeLine = 302;
            Functions.Pulse ( 500, WRITECOMPLETE_FB ) ; 
            __context__.SourceCodeLine = 303;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_3__" , 100 , __SPLS_TMPVAR__WAITLABEL_3___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_3___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 304;
            FILEOPENCONFIG (  __context__  ) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void RESETVALUES (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 309;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)3; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 311;
            ROOM [ LVCOUNTER] . ENABLED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 312;
            ROOM [ LVCOUNTER] . LISTENVOL = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 313;
            ROOM [ LVCOUNTER] . TALKVOL = (short) ( 0 ) ; 
            __context__.SourceCodeLine = 314;
            ROOM [ LVCOUNTER] . LISTENMUTE = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 315;
            ROOM [ LVCOUNTER] . TALKMUTE = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 316;
            ENABLED_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 317;
            LISTENMUTE_ON_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 318;
            LISTENMUTE_OFF_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 319;
            TALKMUTE_ON_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 320;
            TALKMUTE_OFF_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 321;
            LISTENVOL_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 322;
            TALKVOL_ROOM [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 309;
            } 
        
        __context__.SourceCodeLine = 324;
        ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__2 = (ushort)16; 
        int __FN_FORSTEP_VAL__2 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
            { 
            __context__.SourceCodeLine = 326;
            ALERT [ LVCOUNTER] . ENABLED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 327;
            ENABLED_ALERT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 324;
            } 
        
        __context__.SourceCodeLine = 329;
        GVALERTENABLECOUNT = (ushort) ( 0 ) ; 
        
        }
        
    private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
        { 
        ushort LVVOLUMERANGE = 0;
        ushort LVBARGRAPHMAX = 0;
        ushort LVVOLVALUE = 0;
        ushort LVVOL = 0;
        ushort LVVOLPERCENT = 0;
        ushort LVBARVALUE = 0;
        
        uint LVVOLUMEMULTIPLIER = 0;
        
        short LVVOLUMELEVEL = 0;
        short LVFMIN = 0;
        short LVFMAX = 0;
        
        
        __context__.SourceCodeLine = 336;
        Trace( "Vol Converter: Volume Incoming - {0:d}", (short)LVVOLUMEINCOMING) ; 
        __context__.SourceCodeLine = 337;
        LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
        __context__.SourceCodeLine = 339;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 341;
            LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
            __context__.SourceCodeLine = 342;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 344;
                LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 345;
                LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 347;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 348;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                    }
                
                }
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 351;
            LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
            }
        
        __context__.SourceCodeLine = 352;
        LVVOLUMELEVEL = (short) ( (((LVVOLUMEINCOMING + 100) * LVBARGRAPHMAX) / LVVOLUMERANGE) ) ; 
        __context__.SourceCodeLine = 353;
        return (short)( LVVOLUMELEVEL) ; 
        
        }
        
    private void UPDATEROOMENABLE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        
        __context__.SourceCodeLine = 357;
        if ( Functions.TestForTrue  ( ( Functions.Not( ROOM[ LVINDEX ].ENABLED ))  ) ) 
            { 
            __context__.SourceCodeLine = 359;
            ROOM [ LVINDEX] . LISTENMUTE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 360;
            UPDATELISTENMUTE (  __context__ , (ushort)( LVINDEX )) ; 
            __context__.SourceCodeLine = 361;
            ROOM [ LVINDEX] . TALKMUTE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 362;
            UPDATETALKMUTE (  __context__ , (ushort)( LVINDEX )) ; 
            } 
        
        
        }
        
    object LOADCONFIG_OnPush_0 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            
            __context__.SourceCodeLine = 370;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 371;
                FILEOPENCONFIG (  __context__  ) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object SAVECONFIG_OnPush_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 375;
        WRITEDATATOCONFIG (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ENABLE_ROOM_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 380;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 381;
        if ( Functions.TestForTrue  ( ( Functions.Not( ROOM[ LVINDEX ].ENABLED ))  ) ) 
            {
            __context__.SourceCodeLine = 382;
            ROOM [ LVINDEX] . ENABLED = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 384;
            ROOM [ LVINDEX] . ENABLED = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 385;
        ENABLED_ROOM [ LVINDEX]  .Value = (ushort) ( ROOM[ LVINDEX ].ENABLED ) ; 
        __context__.SourceCodeLine = 386;
        UPDATEROOMENABLE (  __context__ , (ushort)( LVINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LISTENUP_ROOM_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVVOL = 0;
        
        
        __context__.SourceCodeLine = 391;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 392;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (ROOM[ LVINDEX ].LISTENVOL + GVSTEP) > GVMAXVOL ))  ) ) 
            {
            __context__.SourceCodeLine = 393;
            ROOM [ LVINDEX] . LISTENVOL = (short) ( GVMAXVOL ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 395;
            ROOM [ LVINDEX] . LISTENVOL = (short) ( (ROOM[ LVINDEX ].LISTENVOL + GVSTEP) ) ; 
            }
        
        __context__.SourceCodeLine = 396;
        LVVOL = (ushort) ( VOLUMECONVERTER( __context__ , (short)( GVMINVOL ) , (short)( GVMAXVOL ) , (short)( ROOM[ LVINDEX ].LISTENVOL ) ) ) ; 
        __context__.SourceCodeLine = 397;
        LISTENVOL_ROOM [ LVINDEX]  .Value = (ushort) ( LVVOL ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LISTENDOWN_ROOM_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVVOL = 0;
        
        
        __context__.SourceCodeLine = 402;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 403;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (ROOM[ LVINDEX ].LISTENVOL - GVSTEP) < GVMINVOL ))  ) ) 
            {
            __context__.SourceCodeLine = 404;
            ROOM [ LVINDEX] . LISTENVOL = (short) ( GVMINVOL ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 406;
            ROOM [ LVINDEX] . LISTENVOL = (short) ( (ROOM[ LVINDEX ].LISTENVOL - GVSTEP) ) ; 
            }
        
        __context__.SourceCodeLine = 407;
        LVVOL = (ushort) ( VOLUMECONVERTER( __context__ , (short)( GVMINVOL ) , (short)( GVMAXVOL ) , (short)( ROOM[ LVINDEX ].LISTENVOL ) ) ) ; 
        __context__.SourceCodeLine = 408;
        LISTENVOL_ROOM [ LVINDEX]  .Value = (ushort) ( LVVOL ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LISTENMUTE_ROOM_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 413;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 414;
        if ( Functions.TestForTrue  ( ( Functions.Not( ROOM[ LVINDEX ].LISTENMUTE ))  ) ) 
            {
            __context__.SourceCodeLine = 415;
            ROOM [ LVINDEX] . LISTENMUTE = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 417;
            ROOM [ LVINDEX] . LISTENMUTE = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 418;
        UPDATELISTENMUTE (  __context__ , (ushort)( LVINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TALKUP_ROOM_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVVOL = 0;
        
        
        __context__.SourceCodeLine = 423;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 424;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (ROOM[ LVINDEX ].TALKVOL + GVSTEP) > GVMAXVOL ))  ) ) 
            {
            __context__.SourceCodeLine = 425;
            ROOM [ LVINDEX] . TALKVOL = (short) ( GVMAXVOL ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 427;
            ROOM [ LVINDEX] . TALKVOL = (short) ( (ROOM[ LVINDEX ].TALKVOL + GVSTEP) ) ; 
            }
        
        __context__.SourceCodeLine = 428;
        LVVOL = (ushort) ( VOLUMECONVERTER( __context__ , (short)( GVMINVOL ) , (short)( GVMAXVOL ) , (short)( ROOM[ LVINDEX ].TALKVOL ) ) ) ; 
        __context__.SourceCodeLine = 429;
        TALKVOL_ROOM [ LVINDEX]  .Value = (ushort) ( LVVOL ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TALKDOWN_ROOM_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVVOL = 0;
        
        
        __context__.SourceCodeLine = 434;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 435;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (ROOM[ LVINDEX ].TALKVOL - GVSTEP) < GVMINVOL ))  ) ) 
            {
            __context__.SourceCodeLine = 436;
            ROOM [ LVINDEX] . TALKVOL = (short) ( GVMINVOL ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 438;
            ROOM [ LVINDEX] . TALKVOL = (short) ( (ROOM[ LVINDEX ].TALKVOL - GVSTEP) ) ; 
            }
        
        __context__.SourceCodeLine = 439;
        LVVOL = (ushort) ( VOLUMECONVERTER( __context__ , (short)( GVMINVOL ) , (short)( GVMAXVOL ) , (short)( ROOM[ LVINDEX ].TALKVOL ) ) ) ; 
        __context__.SourceCodeLine = 440;
        TALKVOL_ROOM [ LVINDEX]  .Value = (ushort) ( LVVOL ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TALKMUTE_ROOM_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        
        __context__.SourceCodeLine = 445;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 446;
        if ( Functions.TestForTrue  ( ( Functions.Not( ROOM[ LVINDEX ].TALKMUTE ))  ) ) 
            {
            __context__.SourceCodeLine = 447;
            ROOM [ LVINDEX] . TALKMUTE = (ushort) ( 1 ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 449;
            ROOM [ LVINDEX] . TALKMUTE = (ushort) ( 0 ) ; 
            }
        
        __context__.SourceCodeLine = 450;
        UPDATETALKMUTE (  __context__ , (ushort)( LVINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object FILENAME_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 454;
        RESETVALUES (  __context__  ) ; 
        __context__.SourceCodeLine = 455;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( FILENAME ) > 0 ))  ) ) 
            {
            __context__.SourceCodeLine = 456;
            FILEOPENCONFIG (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ENABLE_ALERT_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 461;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 462;
        if ( Functions.TestForTrue  ( ( ALERT[ LVINDEX ].ENABLED)  ) ) 
            {
            __context__.SourceCodeLine = 463;
            ALERT [ LVINDEX] . ENABLED = (ushort) ( 0 ) ; 
            }
        
        else 
            { 
            __context__.SourceCodeLine = 466;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVALERTENABLECOUNT < 4 ))  ) ) 
                {
                __context__.SourceCodeLine = 467;
                ALERT [ LVINDEX] . ENABLED = (ushort) ( 1 ) ; 
                }
            
            } 
        
        __context__.SourceCodeLine = 469;
        ENABLED_ALERT [ LVINDEX]  .Value = (ushort) ( ALERT[ LVINDEX ].ENABLED ) ; 
        __context__.SourceCodeLine = 470;
        GVALERTENABLECOUNT = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 471;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 473;
            if ( Functions.TestForTrue  ( ( ALERT[ LVCOUNTER ].ENABLED)  ) ) 
                {
                __context__.SourceCodeLine = 474;
                GVALERTENABLECOUNT = (ushort) ( (GVALERTENABLECOUNT + 1) ) ; 
                }
            
            __context__.SourceCodeLine = 471;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 483;
        GVSTEP = (ushort) ( 3 ) ; 
        __context__.SourceCodeLine = 484;
        GVMAXVOL = (short) ( 20 ) ; 
        __context__.SourceCodeLine = 485;
        GVMINVOL = (short) ( Functions.ToSignedInteger( -( 100 ) ) ) ; 
        __context__.SourceCodeLine = 486;
        GVROOMA = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 487;
        GVROOMB = (ushort) ( 2 ) ; 
        __context__.SourceCodeLine = 488;
        GVROOMC = (ushort) ( 4 ) ; 
        __context__.SourceCodeLine = 489;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)16; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 490;
            ALERT [ LVCOUNTER] . ENABLED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 489;
            }
        
        __context__.SourceCodeLine = 491;
        GVALERTENABLECOUNT = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    ROOM  = new SROOM[ 4 ];
    for( uint i = 0; i < 4; i++ )
    {
        ROOM [i] = new SROOM( this, true );
        ROOM [i].PopulateCustomAttributeList( false );
        
    }
    ALERT  = new SALERTS[ 17 ];
    for( uint i = 0; i < 17; i++ )
    {
        ALERT [i] = new SALERTS( this, true );
        ALERT [i].PopulateCustomAttributeList( false );
        
    }
    
    SAVECONFIG = new Crestron.Logos.SplusObjects.DigitalInput( SAVECONFIG__DigitalInput__, this );
    m_DigitalInputList.Add( SAVECONFIG__DigitalInput__, SAVECONFIG );
    
    LOADCONFIG = new Crestron.Logos.SplusObjects.DigitalInput( LOADCONFIG__DigitalInput__, this );
    m_DigitalInputList.Add( LOADCONFIG__DigitalInput__, LOADCONFIG );
    
    ENABLE_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        ENABLE_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( ENABLE_ROOM__DigitalInput__ + i, ENABLE_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLE_ROOM__DigitalInput__ + i, ENABLE_ROOM[i+1] );
    }
    
    LISTENUP_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENUP_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( LISTENUP_ROOM__DigitalInput__ + i, LISTENUP_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( LISTENUP_ROOM__DigitalInput__ + i, LISTENUP_ROOM[i+1] );
    }
    
    LISTENDOWN_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENDOWN_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( LISTENDOWN_ROOM__DigitalInput__ + i, LISTENDOWN_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( LISTENDOWN_ROOM__DigitalInput__ + i, LISTENDOWN_ROOM[i+1] );
    }
    
    LISTENMUTE_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENMUTE_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( LISTENMUTE_ROOM__DigitalInput__ + i, LISTENMUTE_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( LISTENMUTE_ROOM__DigitalInput__ + i, LISTENMUTE_ROOM[i+1] );
    }
    
    TALKUP_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKUP_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( TALKUP_ROOM__DigitalInput__ + i, TALKUP_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( TALKUP_ROOM__DigitalInput__ + i, TALKUP_ROOM[i+1] );
    }
    
    TALKDOWN_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKDOWN_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( TALKDOWN_ROOM__DigitalInput__ + i, TALKDOWN_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( TALKDOWN_ROOM__DigitalInput__ + i, TALKDOWN_ROOM[i+1] );
    }
    
    TALKMUTE_ROOM = new InOutArray<DigitalInput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKMUTE_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( TALKMUTE_ROOM__DigitalInput__ + i, TALKMUTE_ROOM__DigitalInput__, this );
        m_DigitalInputList.Add( TALKMUTE_ROOM__DigitalInput__ + i, TALKMUTE_ROOM[i+1] );
    }
    
    ENABLE_ALERT = new InOutArray<DigitalInput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        ENABLE_ALERT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( ENABLE_ALERT__DigitalInput__ + i, ENABLE_ALERT__DigitalInput__, this );
        m_DigitalInputList.Add( ENABLE_ALERT__DigitalInput__ + i, ENABLE_ALERT[i+1] );
    }
    
    LOADCOMPLETE_FB = new Crestron.Logos.SplusObjects.DigitalOutput( LOADCOMPLETE_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( LOADCOMPLETE_FB__DigitalOutput__, LOADCOMPLETE_FB );
    
    WRITECOMPLETE_FB = new Crestron.Logos.SplusObjects.DigitalOutput( WRITECOMPLETE_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( WRITECOMPLETE_FB__DigitalOutput__, WRITECOMPLETE_FB );
    
    ENABLED_ROOM = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        ENABLED_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( ENABLED_ROOM__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( ENABLED_ROOM__DigitalOutput__ + i, ENABLED_ROOM[i+1] );
    }
    
    LISTENMUTE_ON_ROOM = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENMUTE_ON_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( LISTENMUTE_ON_ROOM__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( LISTENMUTE_ON_ROOM__DigitalOutput__ + i, LISTENMUTE_ON_ROOM[i+1] );
    }
    
    LISTENMUTE_OFF_ROOM = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENMUTE_OFF_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( LISTENMUTE_OFF_ROOM__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( LISTENMUTE_OFF_ROOM__DigitalOutput__ + i, LISTENMUTE_OFF_ROOM[i+1] );
    }
    
    TALKMUTE_ON_ROOM = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKMUTE_ON_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( TALKMUTE_ON_ROOM__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( TALKMUTE_ON_ROOM__DigitalOutput__ + i, TALKMUTE_ON_ROOM[i+1] );
    }
    
    TALKMUTE_OFF_ROOM = new InOutArray<DigitalOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKMUTE_OFF_ROOM[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( TALKMUTE_OFF_ROOM__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( TALKMUTE_OFF_ROOM__DigitalOutput__ + i, TALKMUTE_OFF_ROOM[i+1] );
    }
    
    ENABLED_ALERT = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        ENABLED_ALERT[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( ENABLED_ALERT__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( ENABLED_ALERT__DigitalOutput__ + i, ENABLED_ALERT[i+1] );
    }
    
    ROOMENABLEDCOUNT = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMENABLEDCOUNT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMENABLEDCOUNT__AnalogSerialOutput__, ROOMENABLEDCOUNT );
    
    ROOMCOMBO = new Crestron.Logos.SplusObjects.AnalogOutput( ROOMCOMBO__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( ROOMCOMBO__AnalogSerialOutput__, ROOMCOMBO );
    
    LISTENVOL_ROOM = new InOutArray<AnalogOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        LISTENVOL_ROOM[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( LISTENVOL_ROOM__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( LISTENVOL_ROOM__AnalogSerialOutput__ + i, LISTENVOL_ROOM[i+1] );
    }
    
    TALKVOL_ROOM = new InOutArray<AnalogOutput>( 3, this );
    for( uint i = 0; i < 3; i++ )
    {
        TALKVOL_ROOM[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( TALKVOL_ROOM__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( TALKVOL_ROOM__AnalogSerialOutput__ + i, TALKVOL_ROOM[i+1] );
    }
    
    FILENAME = new Crestron.Logos.SplusObjects.StringInput( FILENAME__AnalogSerialInput__, 63, this );
    m_StringInputList.Add( FILENAME__AnalogSerialInput__, FILENAME );
    
    __SPLS_TMPVAR__WAITLABEL_3___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_3___CallbackFn );
    
    LOADCONFIG.OnDigitalPush.Add( new InputChangeHandlerWrapper( LOADCONFIG_OnPush_0, false ) );
    SAVECONFIG.OnDigitalPush.Add( new InputChangeHandlerWrapper( SAVECONFIG_OnPush_1, false ) );
    for( uint i = 0; i < 3; i++ )
        ENABLE_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( ENABLE_ROOM_OnPush_2, false ) );
        
    for( uint i = 0; i < 3; i++ )
        LISTENUP_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( LISTENUP_ROOM_OnPush_3, false ) );
        
    for( uint i = 0; i < 3; i++ )
        LISTENDOWN_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( LISTENDOWN_ROOM_OnPush_4, false ) );
        
    for( uint i = 0; i < 3; i++ )
        LISTENMUTE_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( LISTENMUTE_ROOM_OnPush_5, false ) );
        
    for( uint i = 0; i < 3; i++ )
        TALKUP_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( TALKUP_ROOM_OnPush_6, false ) );
        
    for( uint i = 0; i < 3; i++ )
        TALKDOWN_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( TALKDOWN_ROOM_OnPush_7, false ) );
        
    for( uint i = 0; i < 3; i++ )
        TALKMUTE_ROOM[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( TALKMUTE_ROOM_OnPush_8, false ) );
        
    FILENAME.OnSerialChange.Add( new InputChangeHandlerWrapper( FILENAME_OnChange_9, false ) );
    for( uint i = 0; i < 16; i++ )
        ENABLE_ALERT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( ENABLE_ALERT_OnPush_10, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_CONFIGSETUP_V0_6 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_3___Callback;


const uint SAVECONFIG__DigitalInput__ = 0;
const uint LOADCONFIG__DigitalInput__ = 1;
const uint ENABLE_ROOM__DigitalInput__ = 2;
const uint LISTENUP_ROOM__DigitalInput__ = 5;
const uint LISTENDOWN_ROOM__DigitalInput__ = 8;
const uint LISTENMUTE_ROOM__DigitalInput__ = 11;
const uint TALKUP_ROOM__DigitalInput__ = 14;
const uint TALKDOWN_ROOM__DigitalInput__ = 17;
const uint TALKMUTE_ROOM__DigitalInput__ = 20;
const uint ENABLE_ALERT__DigitalInput__ = 23;
const uint FILENAME__AnalogSerialInput__ = 0;
const uint LOADCOMPLETE_FB__DigitalOutput__ = 0;
const uint WRITECOMPLETE_FB__DigitalOutput__ = 1;
const uint ENABLED_ROOM__DigitalOutput__ = 2;
const uint LISTENMUTE_ON_ROOM__DigitalOutput__ = 5;
const uint LISTENMUTE_OFF_ROOM__DigitalOutput__ = 8;
const uint TALKMUTE_ON_ROOM__DigitalOutput__ = 11;
const uint TALKMUTE_OFF_ROOM__DigitalOutput__ = 14;
const uint ENABLED_ALERT__DigitalOutput__ = 17;
const uint ROOMENABLEDCOUNT__AnalogSerialOutput__ = 0;
const uint ROOMCOMBO__AnalogSerialOutput__ = 1;
const uint LISTENVOL_ROOM__AnalogSerialOutput__ = 2;
const uint TALKVOL_ROOM__AnalogSerialOutput__ = 5;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SROOM : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  ENABLED = 0;
    
    [SplusStructAttribute(1, false, false)]
    public short  LISTENVOL = 0;
    
    [SplusStructAttribute(2, false, false)]
    public short  TALKVOL = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  LISTENMUTE = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  TALKMUTE = 0;
    
    
    public SROOM( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        
        
    }
    
}
[SplusStructAttribute(-1, true, false)]
public class SALERTS : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  ENABLED = 0;
    
    
    public SALERTS( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        
        
    }
    
}

}
