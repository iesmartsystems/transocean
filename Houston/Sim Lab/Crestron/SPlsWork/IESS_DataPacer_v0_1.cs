using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DATAPACER_V0_1
{
    public class UserModuleClass_IESS_DATAPACER_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput LOAD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> DIGITAL_IN;
        Crestron.Logos.SplusObjects.AnalogInput TIMER;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> ANALOG_IN;
        Crestron.Logos.SplusObjects.DigitalOutput BUSY;
        Crestron.Logos.SplusObjects.DigitalOutput COMPLETE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> DIGITAL_OUT;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> ANALOG_OUT;
        ushort GVANALOGINDEX = 0;
        ushort GVDIGITALINDEX = 0;
        ushort GVTIMER = 0;
        private void LOADANALOGS (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 72;
            ANALOG_OUT [ GVANALOGINDEX]  .Value = (ushort) ( ANALOG_IN[ GVANALOGINDEX ] .UshortValue ) ; 
            __context__.SourceCodeLine = 73;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVANALOGINDEX == 6))  ) ) 
                { 
                __context__.SourceCodeLine = 75;
                GVANALOGINDEX = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 76;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_15__" , GVTIMER , __SPLS_TMPVAR__WAITLABEL_15___Callback ) ;
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 84;
                CreateWait ( "__SPLS_TMPVAR__WAITLABEL_16__" , GVTIMER , __SPLS_TMPVAR__WAITLABEL_16___Callback ) ;
                } 
            
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_15___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            __context__.SourceCodeLine = 78;
            Functions.Pulse ( 25, COMPLETE ) ; 
            __context__.SourceCodeLine = 79;
            BUSY  .Value = (ushort) ( 0 ) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_16___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 86;
            GVANALOGINDEX = (ushort) ( (GVANALOGINDEX + 1) ) ; 
            __context__.SourceCodeLine = 87;
            LOADANALOGS (  __context__  ) ; 
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void LOADDIGITALS (  SplusExecutionContext __context__ ) 
    { 
    
    __context__.SourceCodeLine = 94;
    DIGITAL_OUT [ GVDIGITALINDEX]  .Value = (ushort) ( DIGITAL_IN[ GVDIGITALINDEX ] .Value ) ; 
    __context__.SourceCodeLine = 95;
    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GVDIGITALINDEX == 12))  ) ) 
        { 
        __context__.SourceCodeLine = 97;
        GVDIGITALINDEX = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 98;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_17__" , GVTIMER , __SPLS_TMPVAR__WAITLABEL_17___Callback ) ;
        } 
    
    else 
        { 
        __context__.SourceCodeLine = 103;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_18__" , GVTIMER , __SPLS_TMPVAR__WAITLABEL_18___Callback ) ;
        } 
    
    
    }
    
public void __SPLS_TMPVAR__WAITLABEL_17___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 99;
            LOADANALOGS (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

public void __SPLS_TMPVAR__WAITLABEL_18___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 105;
            GVDIGITALINDEX = (ushort) ( (GVDIGITALINDEX + 1) ) ; 
            __context__.SourceCodeLine = 106;
            LOADDIGITALS (  __context__  ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object LOAD_OnPush_0 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 116;
        BUSY  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 117;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)12; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 118;
            DIGITAL_OUT [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 117;
            }
        
        __context__.SourceCodeLine = 119;
        LOADDIGITALS (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TIMER_OnChange_1 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 123;
        GVTIMER = (ushort) ( TIMER  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 130;
        GVDIGITALINDEX = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 131;
        GVANALOGINDEX = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 132;
        GVTIMER = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    
    LOAD = new Crestron.Logos.SplusObjects.DigitalInput( LOAD__DigitalInput__, this );
    m_DigitalInputList.Add( LOAD__DigitalInput__, LOAD );
    
    DIGITAL_IN = new InOutArray<DigitalInput>( 12, this );
    for( uint i = 0; i < 12; i++ )
    {
        DIGITAL_IN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( DIGITAL_IN__DigitalInput__ + i, DIGITAL_IN__DigitalInput__, this );
        m_DigitalInputList.Add( DIGITAL_IN__DigitalInput__ + i, DIGITAL_IN[i+1] );
    }
    
    BUSY = new Crestron.Logos.SplusObjects.DigitalOutput( BUSY__DigitalOutput__, this );
    m_DigitalOutputList.Add( BUSY__DigitalOutput__, BUSY );
    
    COMPLETE = new Crestron.Logos.SplusObjects.DigitalOutput( COMPLETE__DigitalOutput__, this );
    m_DigitalOutputList.Add( COMPLETE__DigitalOutput__, COMPLETE );
    
    DIGITAL_OUT = new InOutArray<DigitalOutput>( 12, this );
    for( uint i = 0; i < 12; i++ )
    {
        DIGITAL_OUT[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( DIGITAL_OUT__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( DIGITAL_OUT__DigitalOutput__ + i, DIGITAL_OUT[i+1] );
    }
    
    TIMER = new Crestron.Logos.SplusObjects.AnalogInput( TIMER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( TIMER__AnalogSerialInput__, TIMER );
    
    ANALOG_IN = new InOutArray<AnalogInput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        ANALOG_IN[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( ANALOG_IN__AnalogSerialInput__ + i, ANALOG_IN__AnalogSerialInput__, this );
        m_AnalogInputList.Add( ANALOG_IN__AnalogSerialInput__ + i, ANALOG_IN[i+1] );
    }
    
    ANALOG_OUT = new InOutArray<AnalogOutput>( 6, this );
    for( uint i = 0; i < 6; i++ )
    {
        ANALOG_OUT[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( ANALOG_OUT__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( ANALOG_OUT__AnalogSerialOutput__ + i, ANALOG_OUT[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_15___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_15___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_16___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_16___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_17___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_17___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_18___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_18___CallbackFn );
    
    LOAD.OnDigitalPush.Add( new InputChangeHandlerWrapper( LOAD_OnPush_0, false ) );
    TIMER.OnAnalogChange.Add( new InputChangeHandlerWrapper( TIMER_OnChange_1, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DATAPACER_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_15___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_16___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_17___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_18___Callback;


const uint LOAD__DigitalInput__ = 0;
const uint DIGITAL_IN__DigitalInput__ = 1;
const uint TIMER__AnalogSerialInput__ = 0;
const uint ANALOG_IN__AnalogSerialInput__ = 1;
const uint BUSY__DigitalOutput__ = 0;
const uint COMPLETE__DigitalOutput__ = 1;
const uint DIGITAL_OUT__DigitalOutput__ = 2;
const uint ANALOG_OUT__AnalogSerialOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
