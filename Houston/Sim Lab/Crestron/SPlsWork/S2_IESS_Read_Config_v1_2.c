#include "TypeDefs.h"
#include "Globals.h"
#include "FnctList.h"
#include "Library.h"
#include "SimplSig.h"
#include "S2_IESS_Read_Config_v1_2.h"

FUNCTION_MAIN( S2_IESS_Read_Config_v1_2 );
EVENT_HANDLER( S2_IESS_Read_Config_v1_2 );
DEFINE_ENTRYPOINT( S2_IESS_Read_Config_v1_2 );








static void S2_IESS_Read_Config_v1_2__STOPSYSTEM ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    short __FN_FOREND_VAL__3; 
    short __FN_FORINIT_VAL__3; 
    short __FN_FOREND_VAL__4; 
    short __FN_FORINIT_VAL__4; 
    short __FN_FOREND_VAL__5; 
    short __FN_FORINIT_VAL__5; 
    short __FN_FOREND_VAL__6; 
    short __FN_FORINIT_VAL__6; 
    short __FN_FOREND_VAL__7; 
    short __FN_FORINIT_VAL__7; 
    short __FN_FOREND_VAL__8; 
    short __FN_FORINIT_VAL__8; 
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 149 );
    Globals->S2_IESS_Read_Config_v1_2.__GLBL_MAXDISPLAYS = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 150 );
    Globals->S2_IESS_Read_Config_v1_2.__GLBL_STARTREAD = 0; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 151 );
    SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_READCOMPLETE_FB_DIG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 152 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 153 );
    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    SetSerial( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMPHONENUMBER_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 ) );
    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
    
    ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 154 );
    SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHOFFENABLE_DIG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 155 );
    SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHONENABLE_DIG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 156 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHPOWEROFF_ANALOG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 157 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMROUTINGMODE_ANALOG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 158 );
    SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_DISPLAYNUMBER_ANALOG_OUTPUT, 0) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 159 );
    __FN_FOREND_VAL__1 = 8; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 161 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 162 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 163 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYIPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 164 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 165 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 166 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYID , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 167 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYIP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 168 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 169 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 170 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYTYPE ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 171 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPROTOCOL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 172 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPOWERONTIME ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 173 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPOWEROFFTIME ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 174 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYIP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 175 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYBAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 176 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 177 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYOUTPUTNUM ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 178 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYSCREENUP ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 179 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYSCREENDOWN ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 159 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 181 );
    __FN_FOREND_VAL__2 = 8; 
    __FN_FORINIT_VAL__2 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__2) ; __LVCOUNTER  += __FN_FORINIT_VAL__2) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 183 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 184 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 185 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERIPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 186 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 187 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERIP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 188 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 189 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 190 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERBAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 191 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERPARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 192 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERIP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 181 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 194 );
    __FN_FOREND_VAL__3 = 4; 
    __FN_FORINIT_VAL__3 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__3 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__3 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__3) ; __LVCOUNTER  += __FN_FORINIT_VAL__3) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 196 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 197 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 198 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOIPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 199 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 200 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOIP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 201 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOLOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 202 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOLOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 203 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOOBJECTID , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 204 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOBAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 205 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOPARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 206 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOIP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 194 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 208 );
    __FN_FOREND_VAL__4 = 64; 
    __FN_FORINIT_VAL__4 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__4 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__4 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__4) ; __LVCOUNTER  += __FN_FORINIT_VAL__4) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 210 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOINSTANCETAGS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 208 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 212 );
    __FN_FOREND_VAL__5 = 4; 
    __FN_FORINIT_VAL__5 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__5 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__5 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__5) ; __LVCOUNTER  += __FN_FORINIT_VAL__5) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 214 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 215 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 216 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECIPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 217 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 218 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 219 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECNAMECONTENT , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 220 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECIP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 221 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECLOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 222 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECLOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 223 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECBAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 224 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECPARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 225 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECIP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 226 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECINPUTVIDEO ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 227 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECOUTPUTVIDEO ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 228 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECICON ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 212 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 230 );
    __FN_FOREND_VAL__6 = 16; 
    __FN_FORINIT_VAL__6 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__6 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__6 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__6) ; __LVCOUNTER  += __FN_FORINIT_VAL__6) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 232 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 233 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 234 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEIPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 235 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 236 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEUSESNOTOUCH ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 237 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCENAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 238 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCEIP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 239 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCELOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 240 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCELOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 241 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEBAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 242 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEPARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 243 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEIP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 244 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEINPUTVIDEO ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 245 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEICON ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 230 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 247 );
    __FN_FOREND_VAL__7 = 4; 
    __FN_FORINIT_VAL__7 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__7 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__7 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__7) ; __LVCOUNTER  += __FN_FORINIT_VAL__7) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 249 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 250 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 251 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_IPCONTROL ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 252 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_USED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 253 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_NAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 254 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_IP_ADDRESS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 255 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_LOGINNAME , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 256 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_LOGINPASSWORD , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 257 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_BAUD ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 258 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_PARITY ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 259 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_IP_PORT ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 247 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 261 );
    __FN_FOREND_VAL__8 = 16; 
    __FN_FORINIT_VAL__8 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__8 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__8 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__8) ; __LVCOUNTER  += __FN_FORINIT_VAL__8) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 263 );
        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVMISC, SDEVICE, __LVCOUNTER )->SDEVICE__USED = 0; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 264 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__MISCUSED ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 265 );
        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __MISCSTRINGS , __LVCOUNTER ) ; 
        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
        
        ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 266 );
        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__MISCANALOGS ,__LVCOUNTER, 0) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 261 );
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__STOPSYSTEM:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    /* End Free local function variables */
    
    }
    
static void S2_IESS_Read_Config_v1_2__SHOWDIGITALS ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    short __FN_FOREND_VAL__3; 
    short __FN_FORINIT_VAL__3; 
    short __FN_FOREND_VAL__4; 
    short __FN_FORINIT_VAL__4; 
    short __FN_FOREND_VAL__5; 
    short __FN_FORINIT_VAL__5; 
    short __FN_FOREND_VAL__6; 
    short __FN_FORINIT_VAL__6; 
    short __FN_FOREND_VAL__7; 
    short __FN_FORINIT_VAL__7; 
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 272 );
    if ( Globals->S2_IESS_Read_Config_v1_2.__GVROOMNOTOUCHOFF) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 273 );
        SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHOFFENABLE_DIG_OUTPUT, 1) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 275 );
        SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHOFFENABLE_DIG_OUTPUT, 0) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 276 );
    if ( Globals->S2_IESS_Read_Config_v1_2.__GVROOMNOTOUCHON) 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 277 );
        SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHONENABLE_DIG_OUTPUT, 1) ; 
        }
    
    else 
        {
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 279 );
        SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHONENABLE_DIG_OUTPUT, 0) ; 
        }
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 281 );
    __FN_FOREND_VAL__1 = 8; 
    __FN_FORINIT_VAL__1 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 283 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 284 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYIPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 286 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYIPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 287 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 288 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 290 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 281 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 292 );
    __FN_FOREND_VAL__2 = 8; 
    __FN_FORINIT_VAL__2 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__2) ; __LVCOUNTER  += __FN_FORINIT_VAL__2) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 294 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 295 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERIPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 297 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERIPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 298 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 299 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 301 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 292 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 304 );
    __FN_FOREND_VAL__3 = 4; 
    __FN_FORINIT_VAL__3 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__3 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__3 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__3) ; __LVCOUNTER  += __FN_FORINIT_VAL__3) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 306 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 307 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOIPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 309 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOIPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 310 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 311 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 313 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 304 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 315 );
    __FN_FOREND_VAL__4 = 4; 
    __FN_FORINIT_VAL__4 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__4 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__4 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__4) ; __LVCOUNTER  += __FN_FORINIT_VAL__4) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 317 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 318 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECIPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 320 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECIPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 321 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 322 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 324 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 315 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 326 );
    __FN_FOREND_VAL__5 = 16; 
    __FN_FORINIT_VAL__5 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__5 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__5 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__5) ; __LVCOUNTER  += __FN_FORINIT_VAL__5) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 328 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 329 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEIPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 331 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEIPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 332 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 333 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 335 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 326 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 338 );
    __FN_FOREND_VAL__6 = 4; 
    __FN_FORINIT_VAL__6 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__6 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__6 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__6) ; __LVCOUNTER  += __FN_FORINIT_VAL__6) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 340 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVCOUNTER )->SDEVICE__USINGIP) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 341 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_IPCONTROL ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 343 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_IPCONTROL ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 344 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 345 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_USED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 347 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_USED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 338 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 349 );
    __FN_FOREND_VAL__7 = 16; 
    __FN_FORINIT_VAL__7 = 1; 
    for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__7 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__7 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__7) ; __LVCOUNTER  += __FN_FORINIT_VAL__7) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 351 );
        if ( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVMISC, SDEVICE, __LVCOUNTER )->SDEVICE__USED) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 352 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__MISCUSED ,__LVCOUNTER, 1) ; 
            }
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 354 );
            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__MISCUSED ,__LVCOUNTER, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 349 );
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__SHOWDIGITALS:
/* Begin Free local function variables */
    /* End Free local function variables */
    
    }
    
static struct StringHdr_s* S2_IESS_Read_Config_v1_2__PARSEASCIIHEX ( struct StringHdr_s*  __FN_DSTRET_STR__  , struct StringHdr_s* __LVASCII ) 
    { 
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDATAHEX, 255 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDATAHEX );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTEMP, 63 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTEMP );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH, 63 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "$" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( ",$" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 256 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 256 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2, 256 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDATAHEX );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVDATAHEX, 255 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTEMP );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTEMP, 63 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 63 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "$" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, ",$" );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 256 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 256 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2, 256 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 362 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATAHEX  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 363 );
    while ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVASCII  )  , 1 , 1 )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 365 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )  , LOCAL_STRING_STRUCT( __LVASCII  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 367 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )   , LOCAL_STRING_STRUCT( __LVASCII  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 368 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVTEMP  )  ) - 2), LOCAL_STRING_STRUCT( __LVTEMP  )  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 369 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVTEMP  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 371 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )   , LOCAL_STRING_STRUCT( __LVTEMP  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 372 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVDATAHEX  )  ,  Chr (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , hextoi( LOCAL_STRING_STRUCT( __LVTEMP  )  ))  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATAHEX  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                } 
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 375 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVDATAHEX  )  ,  Chr (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , hextoi( LOCAL_STRING_STRUCT( __LVTEMP  )  ))  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATAHEX  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                }
            
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 363 );
        } 
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 378 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , Left ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , LOCAL_STRING_STRUCT( __LVASCII  )  , 2) , LOCAL_STRING_STRUCT( __LVASCII  )    , 1  )  )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTEMP  )   ,2 , "%s"  , __FN_DST_STR__2 ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 379 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVDATAHEX  )  ,  Chr (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , hextoi( LOCAL_STRING_STRUCT( __LVTEMP  )  ))  )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATAHEX  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 380 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVDATAHEX  )  ,  LOCAL_STRING_STRUCT( __LVASCII  )   )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATAHEX  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 381 );
    FormatString( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  __FN_DSTRET_STR__, 2, "%s", ( LOCAL_STRING_STRUCT( __LVDATAHEX  )  ) );
    goto S2_IESS_Read_Config_v1_2_Exit__PARSEASCIIHEX ; 
    
    S2_IESS_Read_Config_v1_2_Exit__PARSEASCIIHEX:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVDATAHEX );
    FREE_LOCAL_STRING_STRUCT( __LVTEMP );
    FREE_LOCAL_STRING_STRUCT( __LVTRASH );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    /* End Free local function variables */
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 383 );
    return __FN_DSTRET_STR__; 
    }
    
static void S2_IESS_Read_Config_v1_2__PARSEDATAFROMCONFIG ( struct StringHdr_s* __LVDATA ) 
    { 
    /* Begin local function variable declarations */
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDEVICE, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDEVICE );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVINDEXTEMP, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVINDEXTEMP );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH, 50 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    
    unsigned short  __LVINDEX; 
    unsigned short  __LVINSTANCEINDEX; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "//" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "Room: " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( "RoomName-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "NoTouchOffEnable-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__, sizeof( "NoTouchOnEnable-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__, sizeof( "NoTouchPowerOff-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__, sizeof( "PhoneNumber-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__, sizeof( "RoutingMode-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__, sizeof( "Display " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__, sizeof( "Display" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__, sizeof( ":" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__, sizeof( "Switcher " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__, sizeof( "Switcher" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__, sizeof( "Audio " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__, sizeof( "InputAudio" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__, sizeof( "Audio" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__, sizeof( "Codec " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__, sizeof( "Codec" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__, sizeof( "Source " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__, sizeof( "Source" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__, sizeof( "ENV " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__, sizeof( "ENV" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__, sizeof( "Misc " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__, sizeof( "Misc" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__, sizeof( "ControlDeviceType-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__, sizeof( "ControlBaud-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__, sizeof( "ControlParity-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__, sizeof( "ControlID-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__, sizeof( "ControlProtocol-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__, sizeof( "PowerOnTime-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__, sizeof( "PowerOffTime-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__, sizeof( "ControlIP-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__, sizeof( "$" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__, sizeof( "ControlIPPort-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__, sizeof( "ControlLogin-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__, sizeof( "ControlPassword-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__, sizeof( "ObjectID-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__, sizeof( "InstanceTags " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__, sizeof( "-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__, sizeof( "InputVideo-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__, sizeof( "IconType-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__, sizeof( "Name-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__, sizeof( "ContentName-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__, sizeof( "OutputVideo-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__, sizeof( "ScreenUp-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__, sizeof( "ScreenDown-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__, sizeof( "Strings-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__, sizeof( "Analogs-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__, sizeof( "UsesNoTouch-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__, sizeof( "1" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__, sizeof( "0" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 256 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 256 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVDEVICE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVDEVICE, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVINDEXTEMP );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVINDEXTEMP, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 50 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "//" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "Room: " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, "RoomName-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "NoTouchOffEnable-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__, "NoTouchOnEnable-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__, "NoTouchPowerOff-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__, "PhoneNumber-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__, "RoutingMode-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__, "Display " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__, "Display" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__, ":" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__, "Switcher " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__, "Switcher" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__, "Audio " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__, "InputAudio" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__, "Audio" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__, "Codec " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__, "Codec" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__, "Source " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__, "Source" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__, "ENV " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__, "ENV" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__, "Misc " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__, "Misc" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__, "ControlDeviceType-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__, "ControlBaud-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__, "ControlParity-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__, "ControlID-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__, "ControlProtocol-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__, "PowerOnTime-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__, "PowerOffTime-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__, "ControlIP-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__, "$" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__, "ControlIPPort-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__, "ControlLogin-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__, "ControlPassword-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__, "ObjectID-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__, "InstanceTags " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__, "-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__, "InputVideo-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__, "IconType-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__, "Name-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__, "ContentName-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__, "OutputVideo-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__, "ScreenUp-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__, "ScreenDown-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__, "Strings-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__, "Analogs-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__, "UsesNoTouch-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__, "1" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__, "0" );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 256 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 256 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 388 );
    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
        { 
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 395 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 397 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 398 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 400 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 401 );
                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                SetSerial( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNAME_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 ) );
                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                
                ; 
                } 
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 404 );
                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 406 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 407 );
                    if ( ((Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) == 0) || (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) == 1))) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 409 );
                        Globals->S2_IESS_Read_Config_v1_2.__GVROOMNOTOUCHOFF = Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ); 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 410 );
                        SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHOFFENABLE_DIG_OUTPUT, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 414 );
                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 416 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 417 );
                        if ( ((Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) == 0) || (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) == 1))) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 419 );
                            Globals->S2_IESS_Read_Config_v1_2.__GVROOMNOTOUCHON = Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ); 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 420 );
                            SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHONENABLE_DIG_OUTPUT, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                            } 
                        
                        } 
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 424 );
                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 426 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 427 );
                            if ( (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 428 );
                                SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMNOTOUCHPOWEROFF_ANALOG_OUTPUT, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 431 );
                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                { 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 433 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 434 );
                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                SetSerial( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMPHONENUMBER_STRING_OUTPUT, GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 ) );
                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                
                                ; 
                                } 
                            
                            else 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 436 );
                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                    { 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 438 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 439 );
                                    if ( (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                                        {
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 440 );
                                        SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_ROOMROUTINGMODE_ANALOG_OUTPUT, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                        }
                                    
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 445 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 447 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 448 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 449 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 450 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 451 );
                __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 452 );
                if ( (__LVINDEX > Globals->S2_IESS_Read_Config_v1_2.__GLBL_MAXDISPLAYS)) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 453 );
                    Globals->S2_IESS_Read_Config_v1_2.__GLBL_MAXDISPLAYS = __LVINDEX; 
                    }
                
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 454 );
                if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 455 );
                    GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                    }
                
                } 
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 458 );
                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 460 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ )    )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 461 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 462 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 463 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 464 );
                    __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 465 );
                    if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 466 );
                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                        }
                    
                    } 
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 469 );
                    if ( (Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 ) && !( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 ) ))) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 471 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ )    )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 472 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 473 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 474 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 475 );
                        __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 476 );
                        if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 477 );
                            GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                            }
                        
                        } 
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 480 );
                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 482 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )    )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 483 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 484 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 485 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 486 );
                            __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 487 );
                            if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 488 );
                                GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                                }
                            
                            } 
                        
                        else 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 491 );
                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                { 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 493 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ )    )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 494 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 495 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 496 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 497 );
                                __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 498 );
                                if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                                    {
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 499 );
                                    GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                                    }
                                
                                } 
                            
                            else 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 502 );
                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                    { 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 504 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ )    )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 505 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 506 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 507 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 508 );
                                    __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 509 );
                                    if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                                        {
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 510 );
                                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                                        }
                                    
                                    } 
                                
                                else 
                                    {
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 513 );
                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                        { 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 515 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )    )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDEVICE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 516 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 517 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 518 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ) - 1), LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  )  )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVINDEXTEMP  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 519 );
                                        __LVINDEX = Atoi( LOCAL_STRING_STRUCT( __LVINDEXTEMP  )  ); 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 520 );
                                        if ( !( GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVMISC, SDEVICE, __LVINDEX )->SDEVICE__USED )) 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 521 );
                                            GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVMISC, SDEVICE, __LVINDEX )->SDEVICE__USED = 1; 
                                            }
                                        
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 525 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 527 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 528 );
            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 529 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYTYPE ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                }
            
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 532 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 534 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 535 );
            if ( (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 537 );
                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 539 );
                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYBAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 540 );
                    GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                    } 
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 542 );
                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 544 );
                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERBAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 545 );
                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                        } 
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 547 );
                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 549 );
                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOBAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 550 );
                            GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                            } 
                        
                        else 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 552 );
                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                { 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 554 );
                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECBAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 555 );
                                GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                                } 
                            
                            else 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 557 );
                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                    { 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 559 );
                                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEBAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 560 );
                                    GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                                    } 
                                
                                else 
                                    {
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 562 );
                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                        { 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 564 );
                                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_BAUD ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 565 );
                                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 0; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 570 );
        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 572 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 573 );
            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 574 );
                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                }
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 575 );
                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 576 );
                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERPARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                    }
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 577 );
                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 578 );
                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOPARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                        }
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 579 );
                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 580 );
                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECPARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                            }
                        
                        else 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 581 );
                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 582 );
                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEPARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                }
                            
                            else 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 583 );
                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                    {
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 584 );
                                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_PARITY ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 587 );
            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 589 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 590 );
                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 591 );
                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYID , __LVINDEX ) ; 
                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                    
                    ; 
                    }
                
                } 
            
            else 
                {
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 594 );
                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 596 );
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 597 );
                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 598 );
                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPROTOCOL ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                        }
                    
                    } 
                
                else 
                    {
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 601 );
                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 603 );
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 604 );
                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 605 );
                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPOWERONTIME ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 608 );
                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                            { 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 610 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 611 );
                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 612 );
                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYPOWEROFFTIME ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                }
                            
                            } 
                        
                        else 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 615 );
                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                { 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 617 );
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 618 );
                                if ( (Len( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 6)) 
                                    { 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 620 );
                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                        {
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 621 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , S2_IESS_Read_Config_v1_2__PARSEASCIIHEX (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVDATA  )  )  )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVDATA  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                        }
                                    
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 622 );
                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                        { 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 624 );
                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYIP_ADDRESS , __LVINDEX ) ; 
                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                        
                                        ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 625 );
                                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                        } 
                                    
                                    else 
                                        {
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 627 );
                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                            { 
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 629 );
                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERIP_ADDRESS , __LVINDEX ) ; 
                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                            
                                            ; 
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 630 );
                                            GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                            } 
                                        
                                        else 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 632 );
                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                { 
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 634 );
                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOIP_ADDRESS , __LVINDEX ) ; 
                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                
                                                ; 
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 635 );
                                                GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                                } 
                                            
                                            else 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 637 );
                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                    { 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 639 );
                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECIP_ADDRESS , __LVINDEX ) ; 
                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                    
                                                    ; 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 640 );
                                                    GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 642 );
                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                        { 
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 644 );
                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCEIP_ADDRESS , __LVINDEX ) ; 
                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                        
                                                        ; 
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 645 );
                                                        GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 647 );
                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                            { 
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 649 );
                                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_IP_ADDRESS , __LVINDEX ) ; 
                                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                            
                                                            ; 
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 650 );
                                                            GET_GLOBAL_STRUCTARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __LVINDEX )->SDEVICE__USINGIP = 1; 
                                                            } 
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 655 );
                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                    { 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 657 );
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 658 );
                                    if ( (Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                                        { 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 660 );
                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 661 );
                                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYIP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                            }
                                        
                                        else 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 662 );
                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 663 );
                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SWITCHERIP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                }
                                            
                                            else 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 664 );
                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 665 );
                                                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__AUDIOIP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 666 );
                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 667 );
                                                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECIP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 668 );
                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 669 );
                                                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEIP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 670 );
                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 671 );
                                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__ENV_IP_PORT ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 675 );
                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                        { 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 677 );
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 678 );
                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 679 );
                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINNAME , __LVINDEX ) ; 
                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                            
                                            ; 
                                            }
                                        
                                        else 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 680 );
                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 681 );
                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINNAME , __LVINDEX ) ; 
                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                
                                                ; 
                                                }
                                            
                                            else 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 682 );
                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 683 );
                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOLOGINNAME , __LVINDEX ) ; 
                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                    
                                                    ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 684 );
                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 685 );
                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECLOGINNAME , __LVINDEX ) ; 
                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                        
                                                        ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 686 );
                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 687 );
                                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCELOGINNAME , __LVINDEX ) ; 
                                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                            
                                                            ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 688 );
                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 689 );
                                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_LOGINNAME , __LVINDEX ) ; 
                                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                
                                                                ; 
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 692 );
                                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                            { 
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 694 );
                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 695 );
                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 696 );
                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINPASSWORD , __LVINDEX ) ; 
                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                
                                                ; 
                                                }
                                            
                                            else 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 697 );
                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 698 );
                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINPASSWORD , __LVINDEX ) ; 
                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                    
                                                    ; 
                                                    }
                                                
                                                else 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 699 );
                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 700 );
                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOLOGINPASSWORD , __LVINDEX ) ; 
                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                        
                                                        ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 701 );
                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 702 );
                                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECLOGINPASSWORD , __LVINDEX ) ; 
                                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                            
                                                            ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 703 );
                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 704 );
                                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCELOGINPASSWORD , __LVINDEX ) ; 
                                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                
                                                                ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 705 );
                                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                    {
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 706 );
                                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_LOGINPASSWORD , __LVINDEX ) ; 
                                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                    
                                                                    ; 
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            } 
                                        
                                        else 
                                            {
                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 709 );
                                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                { 
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 711 );
                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 712 );
                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                    { 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 714 );
                                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 715 );
                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , S2_IESS_Read_Config_v1_2__PARSEASCIIHEX (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVDATA  )  )  )  ; 
                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOOBJECTID , __LVINDEX ) ; 
                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                        
                                                        ; 
                                                        }
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 717 );
                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOOBJECTID , __LVINDEX ) ; 
                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                        
                                                        ; 
                                                        }
                                                    
                                                    } 
                                                
                                                } 
                                            
                                            else 
                                                {
                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 721 );
                                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                    { 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 723 );
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 724 );
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 725 );
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVTRASH  )  ) - 1), LOCAL_STRING_STRUCT( __LVTRASH  )  )  )  ; 
                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 726 );
                                                    __LVINSTANCEINDEX = Atoi( LOCAL_STRING_STRUCT( __LVTRASH  )  ); 
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 727 );
                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                        { 
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 729 );
                                                        if ( ((__LVINSTANCEINDEX > 0) && (__LVINSTANCEINDEX <= 64))) 
                                                            { 
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 731 );
                                                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 732 );
                                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , S2_IESS_Read_Config_v1_2__PARSEASCIIHEX (  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVDATA  )  )  )  ; 
                                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOINSTANCETAGS , __LVINSTANCEINDEX ) ; 
                                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                
                                                                ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 734 );
                                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __AUDIOINSTANCETAGS , __LVINSTANCEINDEX ) ; 
                                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                
                                                                ; 
                                                                }
                                                            
                                                            } 
                                                        
                                                        } 
                                                    
                                                    } 
                                                
                                                else 
                                                    {
                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 739 );
                                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                        { 
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 741 );
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 742 );
                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 743 );
                                                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECINPUTVIDEO ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                            }
                                                        
                                                        else 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 744 );
                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 745 );
                                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEINPUTVIDEO ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                }
                                                            
                                                            }
                                                        
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 748 );
                                                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                            { 
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 750 );
                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 751 );
                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 752 );
                                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECICON ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                }
                                                            
                                                            else 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 753 );
                                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                    {
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 754 );
                                                                    SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEICON ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                    }
                                                                
                                                                }
                                                            
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 757 );
                                                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                { 
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 759 );
                                                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                    { 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 761 );
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 762 );
                                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                        {
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 763 );
                                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECNAMECONTENT , __LVINDEX ) ; 
                                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                        
                                                                        ; 
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    { 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 767 );
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 768 );
                                                                    if ( (Len( LOCAL_STRING_STRUCT( __LVDATA  )  ) > 0)) 
                                                                        { 
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 770 );
                                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                            {
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 771 );
                                                                            if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                            SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __CODECNAME , __LVINDEX ) ; 
                                                                            ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                            
                                                                            ; 
                                                                            }
                                                                        
                                                                        else 
                                                                            {
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 772 );
                                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                                                {
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 773 );
                                                                                if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                                SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __DISPLAYNAME , __LVINDEX ) ; 
                                                                                ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                                
                                                                                ; 
                                                                                }
                                                                            
                                                                            else 
                                                                                {
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 774 );
                                                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                                    {
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 775 );
                                                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __SOURCENAME , __LVINDEX ) ; 
                                                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                                    
                                                                                    ; 
                                                                                    }
                                                                                
                                                                                else 
                                                                                    {
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 776 );
                                                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                                        {
                                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 777 );
                                                                                        if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                                        SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __ENV_NAME , __LVINDEX ) ; 
                                                                                        ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                                        
                                                                                        ; 
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        } 
                                                                    
                                                                    } 
                                                                
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 782 );
                                                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                    { 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 784 );
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 785 );
                                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                                        {
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 786 );
                                                                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYOUTPUTNUM ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                        }
                                                                    
                                                                    else 
                                                                        {
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 787 );
                                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 4))) 
                                                                            {
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 788 );
                                                                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__CODECOUTPUTVIDEO ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 791 );
                                                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                        { 
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 793 );
                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 794 );
                                                                        if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                                            {
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 795 );
                                                                            SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYSCREENUP ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                            }
                                                                        
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 798 );
                                                                        if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                            { 
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 800 );
                                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 801 );
                                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 8))) 
                                                                                {
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 802 );
                                                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__DISPLAYSCREENDOWN ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                                }
                                                                            
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 805 );
                                                                            if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                                { 
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 807 );
                                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 808 );
                                                                                if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                                    {
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 809 );
                                                                                    if( ObtainStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) == 0 ) {
                                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , GENERIC_STRING_OUTPUT( S2_IESS_Read_Config_v1_2 )  ,2 , "%s"  , LOCAL_STRING_STRUCT( __LVDATA  )   )  ; 
                                                                                    SET_STRING_OUT_ARRAY_ELEMENT( S2_IESS_Read_Config_v1_2, __MISCSTRINGS , __LVINDEX ) ; 
                                                                                    ReleaseStringOutputSemaphore( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); }
                                                                                    
                                                                                    ; 
                                                                                    }
                                                                                
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 811 );
                                                                                if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                                    { 
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 813 );
                                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                                    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 814 );
                                                                                    if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                                        {
                                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 815 );
                                                                                        SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__MISCANALOGS ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                                        }
                                                                                    
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 817 );
                                                                                    if ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__ )  , LOCAL_STRING_STRUCT( __LVDATA  )  , 1 , 1 )) 
                                                                                        { 
                                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 819 );
                                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__ )   , LOCAL_STRING_STRUCT( __LVDATA  )    , 1  )  )  ; 
                                                                                        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                                                                                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 820 );
                                                                                        if ( ((CompareStrings( LOCAL_STRING_STRUCT( __LVDATA  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__ ) , 1 ) == 0) || (CompareStrings( LOCAL_STRING_STRUCT( __LVDATA  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ ) , 1 ) == 0))) 
                                                                                            { 
                                                                                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 822 );
                                                                                            if ( (((CompareStrings( LOCAL_STRING_STRUCT( __LVDEVICE  ) , LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ ) , 1 ) == 0) && (__LVINDEX > 0)) && (__LVINDEX <= 16))) 
                                                                                                {
                                                                                                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 823 );
                                                                                                SetOutputArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), &Globals->S2_IESS_Read_Config_v1_2.__SOURCEUSESNOTOUCH ,__LVINDEX, Atoi( LOCAL_STRING_STRUCT( __LVDATA  )  )) ; 
                                                                                                }
                                                                                            
                                                                                            } 
                                                                                        
                                                                                        } 
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__PARSEDATAFROMCONFIG:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVDEVICE );
    FREE_LOCAL_STRING_STRUCT( __LVINDEXTEMP );
    FREE_LOCAL_STRING_STRUCT( __LVTRASH );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ );
    /* End Free local function variables */
    
    }
    
static void S2_IESS_Read_Config_v1_2__FILEOPENCONFIG ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVREAD; 
    short  __LVHANDLE; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADFILE, 16383 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADFILE );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADLINE, 255 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADLINE );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME, 127 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH, 63 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "\\NVRAM\\" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADFILE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVREADFILE, 16383 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVREADLINE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVREADLINE, 255 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVFILENAME, 127 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVTRASH );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVTRASH, 63 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "\\NVRAM\\" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 16383 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 833 );
    if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FILENAME  )  ) > 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 835 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FILENAME  )   )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVFILENAME  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 836 );
        StartFileOperations ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 837 );
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 838 );
        __LVHANDLE = FileOpen( LOCAL_STRING_STRUCT( __LVFILENAME  )  , 0 ); 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 842 );
        if ( (__LVHANDLE >= 0)) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 844 );
            __LVREAD = FileRead( __LVHANDLE , LOCAL_STRING_STRUCT( __LVREADFILE  )  , 16383 ); 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 845 );
            while ( Find( LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )  , LOCAL_STRING_STRUCT( __LVREADFILE  )  , 1 , 1 )) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 847 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )   , LOCAL_STRING_STRUCT( __LVREADFILE  )    , 1  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVREADLINE  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 848 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , RemoveByLength ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , (Len( LOCAL_STRING_STRUCT( __LVREADLINE  )  ) - 2), LOCAL_STRING_STRUCT( __LVREADLINE  )  )  )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVREADLINE  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 849 );
                Print( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , 18, "\xFA\xE0""CONFIG READ: %s""\xFB", LOCAL_STRING_STRUCT( __LVREADLINE  )  ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 850 );
                S2_IESS_Read_Config_v1_2__PARSEDATAFROMCONFIG (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVREADLINE  )  ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 845 );
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 853 );
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,2 , "%s"  , Remove ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , LOCAL_STRING_STRUCT( __LVREADLINE  )  , LOCAL_STRING_STRUCT( __LVREADFILE  )    , 1  )  )  ; 
            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVTRASH  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 854 );
            if ( (Len( LOCAL_STRING_STRUCT( __LVREADFILE  )  ) > 2)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 856 );
                Print( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , 18, "\xFA\xE0""CONFIG READ: %s""\xFB", LOCAL_STRING_STRUCT( __LVREADFILE  )  ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 857 );
                S2_IESS_Read_Config_v1_2__PARSEDATAFROMCONFIG (  (struct StringHdr_s* )  LOCAL_STRING_STRUCT( __LVREADFILE  )  ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 860 );
            SetAnalog ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_DISPLAYNUMBER_ANALOG_OUTPUT, Globals->S2_IESS_Read_Config_v1_2.__GLBL_MAXDISPLAYS) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 861 );
            SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_READCOMPLETE_FB_DIG_OUTPUT, 1) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 862 );
            S2_IESS_Read_Config_v1_2__SHOWDIGITALS ( ) ; 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 863 );
            __LVREAD = FileClose( __LVHANDLE ); 
            } 
        
        else 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 866 );
            SetDigital ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_READCOMPLETE_FB_DIG_OUTPUT, 0) ; 
            }
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 867 );
        EndFileOperations ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) ; 
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__FILEOPENCONFIG:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVREADFILE );
    FREE_LOCAL_STRING_STRUCT( __LVREADLINE );
    FREE_LOCAL_STRING_STRUCT( __LVFILENAME );
    FREE_LOCAL_STRING_STRUCT( __LVTRASH );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    /* End Free local function variables */
    
    }
    
static void S2_IESS_Read_Config_v1_2__WRITEDATATOCONFIG ( ) 
    { 
    /* Begin local function variable declarations */
    
    unsigned short  __LVCOUNTER; 
    unsigned short  __LVCOUNTERTAGS; 
    unsigned short  __LVREAD; 
    short  __LVHANDLE; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME, 127 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVWRITEFILE, 16383 );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVWRITEFILE );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__, sizeof( "\\NVRAM\\" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__, sizeof( "" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__, sizeof( "//ROOM SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__, sizeof( "Room: RoomName-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__, sizeof( "\x0D""\x0A""Room: PhoneNumber-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__, sizeof( "\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__, sizeof( "Room: NoTouchPowerOff-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__, sizeof( "Room: NoTouchOffEnable-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__, sizeof( "Room: NoTouchOnEnable-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__, sizeof( "\x0D""\x0A""Room: RoutingMode-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__, sizeof( "\x0D""\x0A""//DISPLAY(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__ );
    
    short __FN_FOREND_VAL__1; 
    short __FN_FORINIT_VAL__1; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__, sizeof( "//Display " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__, sizeof( "Display " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__, sizeof( ": SourceName-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__, sizeof( "\x0D""\x0A""Display " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__, sizeof( ": ControlID-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__, sizeof( ": ControlIP-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__, sizeof( ": ControlLogin-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__, sizeof( ": ControlPassword-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__, sizeof( ": ControlDeviceType-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__, sizeof( ": ControlProtocol-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__, sizeof( ": PowerOnTime-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__, sizeof( ": PowerOffTime-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__, sizeof( ": ControlIPPort-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__, sizeof( ": ControlBaud-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__, sizeof( ": ControlParity-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__, sizeof( ": OutputVideo-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__, sizeof( ": ScreenUp-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__, sizeof( ": ScreenDown-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__, sizeof( "//SWITCHER(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__ );
    
    short __FN_FOREND_VAL__2; 
    short __FN_FORINIT_VAL__2; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__, sizeof( "//Switcher " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__, sizeof( "Switcher " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__, sizeof( "\x0D""\x0A""Switcher " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__, sizeof( "//AUDIO(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__ );
    
    short __FN_FOREND_VAL__3; 
    short __FN_FORINIT_VAL__3; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__, sizeof( "//Audio " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__, sizeof( "Audio " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__, sizeof( "\x0D""\x0A""Audio " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__, sizeof( ": ObjectID-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__ );
    
    short __FN_FOREND_VAL__4; 
    short __FN_FORINIT_VAL__4; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__, sizeof( "Audio 1: InstanceTags " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__, sizeof( "-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__, sizeof( "//CODEC(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__ );
    
    short __FN_FOREND_VAL__5; 
    short __FN_FORINIT_VAL__5; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__, sizeof( "//Codec " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__, sizeof( "Codec " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__, sizeof( "\x0D""\x0A""Codec " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__, sizeof( ": ContentName-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__, sizeof( ": InputVideo-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__, sizeof( ": IconType-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__, sizeof( "//SOURCE(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__ );
    
    short __FN_FOREND_VAL__6; 
    short __FN_FORINIT_VAL__6; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__, sizeof( "//Source " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__, sizeof( "Source " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__, sizeof( "\x0D""\x0A""Source " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_51__, sizeof( ": UsesNoTouch-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_51__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_52__, sizeof( "//ENVIRONMENT(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_52__ );
    
    short __FN_FOREND_VAL__7; 
    short __FN_FORINIT_VAL__7; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_53__, sizeof( "//ENV " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_53__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_54__, sizeof( "ENV " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_54__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_55__, sizeof( "\x0D""\x0A""ENV " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_55__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_56__, sizeof( "//MISC(S)_SETUP\x0D""\x0A""" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_56__ );
    
    short __FN_FOREND_VAL__8; 
    short __FN_FORINIT_VAL__8; 
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_57__, sizeof( "Misc " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_57__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_58__, sizeof( ": Strings-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_58__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_59__, sizeof( "\x0D""\x0A""Misc " ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_59__ );
    
    CREATE_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_60__, sizeof( ": Analogs-" ) );
    DECLARE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_60__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__3, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__3 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__4, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__4 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__5, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__5 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__6, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__6 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__7, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__7 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__8, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__8 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__9, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__9 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__10, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__10 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__11, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__11 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__12, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__12 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__13, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__13 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__14, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__14 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__15, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__15 );
    
    CREATE_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__16, 16383 );
    DECLARE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__16 );
    
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVFILENAME );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVFILENAME, 127 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __LVWRITEFILE );
    INITIALIZE_LOCAL_STRING_STRUCT( __LVWRITEFILE, 16383 );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_0__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__, "\\NVRAM\\" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_1__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__, "" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_2__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__, "//ROOM SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_3__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__, "Room: RoomName-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_4__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__, "\x0D""\x0A""Room: PhoneNumber-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_5__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__, "\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_6__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__, "Room: NoTouchPowerOff-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_7__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__, "Room: NoTouchOffEnable-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_8__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__, "Room: NoTouchOnEnable-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_9__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__, "\x0D""\x0A""Room: RoutingMode-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_10__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__, "\x0D""\x0A""//DISPLAY(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_11__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__, "//Display " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_12__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__, "Display " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_13__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__, ": SourceName-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_14__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__, "\x0D""\x0A""Display " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_15__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__, ": ControlID-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_16__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__, ": ControlIP-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_17__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__, ": ControlLogin-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_18__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__, ": ControlPassword-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_19__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__, ": ControlDeviceType-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_20__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__, ": ControlProtocol-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_21__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__, ": PowerOnTime-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_22__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__, ": PowerOffTime-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_23__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__, ": ControlIPPort-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_24__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__, ": ControlBaud-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_25__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__, ": ControlParity-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_26__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__, ": OutputVideo-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_27__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__, ": ScreenUp-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_28__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__, ": ScreenDown-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_29__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__, "//SWITCHER(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_30__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__, "//Switcher " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_31__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__, "Switcher " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_32__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__, "\x0D""\x0A""Switcher " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_33__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__, "//AUDIO(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_34__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__, "//Audio " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_35__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__, "Audio " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_36__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__, "\x0D""\x0A""Audio " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_37__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__, ": ObjectID-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_38__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__, "Audio 1: InstanceTags " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_39__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__, "-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_40__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__, "//CODEC(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_41__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__, "//Codec " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_42__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__, "Codec " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_43__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__, "\x0D""\x0A""Codec " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_44__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__, ": ContentName-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_45__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__, ": InputVideo-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_46__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__, ": IconType-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_47__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__, "//SOURCE(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_48__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__, "//Source " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_49__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__, "Source " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_50__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__, "\x0D""\x0A""Source " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_51__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_51__, ": UsesNoTouch-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_52__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_52__, "//ENVIRONMENT(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_53__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_53__, "//ENV " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_54__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_54__, "ENV " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_55__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__, "\x0D""\x0A""ENV " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_56__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_56__, "//MISC(S)_SETUP\x0D""\x0A""" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_57__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_57__, "Misc " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_58__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_58__, ": Strings-" );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_59__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_59__, "\x0D""\x0A""Misc " );
    
    ALLOCATE_LOCAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__LOCALSTR_60__ );
    SET_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_60__, ": Analogs-" );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__3 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__3, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__4 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__4, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__5 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__5, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__6 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__6, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__7 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__7, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__8 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__8, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__9 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__9, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__10 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__10, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__11 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__11, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__12 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__12, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__13 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__13, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__14 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__14, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__15 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__15, 16383 );
    
    ALLOCATE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__16 );
    INITIALIZE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__16, 16383 );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 875 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FILENAME  )   )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVFILENAME  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 876 );
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,2 , "%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ )    )  ; 
    FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 877 );
    StartFileOperations ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) ; 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 878 );
    __LVHANDLE = FileDelete( LOCAL_STRING_STRUCT( __LVFILENAME  )  ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 879 );
    __LVHANDLE = FileOpenShared( LOCAL_STRING_STRUCT( __LVFILENAME  )  , (16384 | 1) ); 
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 880 );
    if ( (__LVHANDLE >= 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 882 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,32 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  ,  LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNAME  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMPHONENUMBER  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHPOWEROFF  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHOFFENABLE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHONENABLE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ )   ,  GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __WRITE_ROOMROUTINGMODE  )   )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 886 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 887 );
        __FN_FOREND_VAL__1 = 8; 
        __FN_FORINIT_VAL__1 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__1 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__1 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__1) ; __LVCOUNTER  += __FN_FORINIT_VAL__1) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 889 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 891 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__16 )    ,130 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYID  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYTYPE  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPROTOCOL  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__8 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWERONTIME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__9 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWEROFFTIME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__10 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__11 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYBAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__12 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__13 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYOUTPUTNUM  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__14 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENUP  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__15 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENDOWN  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__16 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 887 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 904 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 905 );
        __FN_FOREND_VAL__2 = 8; 
        __FN_FORINIT_VAL__2 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__2 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__2 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__2) ; __LVCOUNTER  += __FN_FORINIT_VAL__2) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 907 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 909 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    ,58 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERBAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERPARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__7 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 905 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 916 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 917 );
        __FN_FOREND_VAL__3 = 4; 
        __FN_FORINIT_VAL__3 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__3 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__3 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__3) ; __LVCOUNTER  += __FN_FORINIT_VAL__3) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 919 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 921 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__8 )    ,66 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOOBJECTID  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOBAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOPARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__8 ) ; 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 926 );
                if ( (__LVCOUNTER == 1)) 
                    { 
                    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 928 );
                    __FN_FOREND_VAL__4 = 64; 
                    __FN_FORINIT_VAL__4 = 1; 
                    for( __LVCOUNTERTAGS = 1; (__FN_FORINIT_VAL__4 > 0)  ? ((short)__LVCOUNTERTAGS  <= __FN_FOREND_VAL__4 ) : ((short)__LVCOUNTERTAGS  >= __FN_FOREND_VAL__4) ; __LVCOUNTERTAGS  += __FN_FORINIT_VAL__4) 
                        { 
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 930 );
                        if ( (Len( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOINSTANCETAGS  )  ,  __LVCOUNTERTAGS  ) ) > 4)) 
                            {
                            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 931 );
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    ,12 , "%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTERTAGS) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOINSTANCETAGS  )    ,  __LVCOUNTERTAGS  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                            FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__1 ) ; 
                            }
                        
                        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 928 );
                        } 
                    
                    } 
                
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 917 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 936 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 937 );
        __FN_FOREND_VAL__5 = 4; 
        __FN_FORINIT_VAL__5 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__5 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__5 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__5) ; __LVCOUNTER  += __FN_FORINIT_VAL__5) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 939 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 941 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__12 )    ,98 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAMECONTENT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECBAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__8 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECPARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__9 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECINPUTVIDEO  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__10 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECOUTPUTVIDEO  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__11 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECICON  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__12 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 937 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 951 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 952 );
        __FN_FOREND_VAL__6 = 16; 
        __FN_FORINIT_VAL__6 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__6 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__6 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__6) ; __LVCOUNTER  += __FN_FORINIT_VAL__6) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 954 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 956 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__11 )    ,90 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCENAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEBAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEPARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__8 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEINPUTVIDEO  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__9 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEICON  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__10 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_51__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEUSESNOTOUCH  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__11 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 952 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 965 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_52__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 966 );
        __FN_FOREND_VAL__7 = 4; 
        __FN_FORINIT_VAL__7 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__7 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__7 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__7) ; __LVCOUNTER  += __FN_FORINIT_VAL__7) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 968 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_ENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 970 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__8 )    ,66 , "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_53__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )   ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_54__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_NAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_ADDRESS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__3 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINNAME  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__4 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINPASSWORD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__5 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_PORT  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__6 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_BAUD  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__7 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_PARITY  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__8 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 966 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 977 );
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    ,4 , "%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_56__ )    )  ; 
        FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__ ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 978 );
        __FN_FOREND_VAL__8 = 16; 
        __FN_FORINIT_VAL__8 = 1; 
        for( __LVCOUNTER = 1; (__FN_FORINIT_VAL__8 > 0)  ? ((short)__LVCOUNTER  <= __FN_FOREND_VAL__8 ) : ((short)__LVCOUNTER  >= __FN_FOREND_VAL__8) ; __LVCOUNTER  += __FN_FORINIT_VAL__8) 
            { 
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 980 );
            if ( (Atoi( GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCENABLED  )  ,  __LVCOUNTER  ) ) == 1)) 
                { 
                UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 982 );
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__2 )    ,20 , "%s%s%s%s%s%s%s%s%s%s"  , LOCAL_STRING_STRUCT( __LVWRITEFILE  )  ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_57__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__ )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_58__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCSTRINGS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_59__ )   ,  Itoa ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ,  LOCAL_DYNAMIC_STRING_STRUCT( __FN_DST_STR__1 )    , __LVCOUNTER) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_60__ )   ,  GetStringArrayElement ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ),  GLOBAL_STRING_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCANALOGS  )    ,  __LVCOUNTER  ) ,   LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ )    )  ; 
                FormatString ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , LOCAL_STRING_STRUCT( __LVWRITEFILE  )   ,2 , "%s"  , __FN_DST_STR__2 ) ; 
                } 
            
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 978 );
            } 
        
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 986 );
        FileWrite ( __LVHANDLE, LOCAL_STRING_STRUCT( __LVWRITEFILE  )  , Len( LOCAL_STRING_STRUCT( __LVWRITEFILE  )  )) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 987 );
        __LVREAD = FileClose( __LVHANDLE ); 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 988 );
        EndFileOperations ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 989 );
        S2_IESS_Read_Config_v1_2__STOPSYSTEM ( ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 990 );
        Pulse ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) , 500, __S2_IESS_Read_Config_v1_2_WRITECOMPLETE_FB_DIG_OUTPUT ) ; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 991 );
        CREATE_WAIT( S2_IESS_Read_Config_v1_2, 100, __SPLS_TMPVAR__WAITLABEL_0__ );
        
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__WRITEDATATOCONFIG:
/* Begin Free local function variables */
    FREE_LOCAL_STRING_STRUCT( __LVFILENAME );
    FREE_LOCAL_STRING_STRUCT( __LVWRITEFILE );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_0__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_1__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_2__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_3__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_4__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_5__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_6__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_7__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_8__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_9__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_10__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_11__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_12__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__1 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_13__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_14__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__2 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_15__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__3 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_16__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__4 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_17__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__5 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_18__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__6 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_19__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__7 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_20__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__8 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_21__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__9 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_22__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__10 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_23__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__11 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_24__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__12 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_25__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__13 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_26__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__14 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_27__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__15 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_28__ );
    FREE_LOCAL_DYNAMIC_AUTOGROW_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FN_DST_STR__16 );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_29__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_30__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_31__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_32__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_33__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_34__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_35__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_36__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_37__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_38__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_39__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_40__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_41__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_42__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_43__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_44__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_45__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_46__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_47__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_48__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_49__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_50__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_51__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_52__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_53__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_54__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_55__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_56__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_57__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_58__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_59__ );
    FREE_LOCAL_STRING_STRUCT( __SPLS_TMPVAR__LOCALSTR_60__ );
    /* End Free local function variables */
    
    }
    
DEFINE_WAITEVENT( S2_IESS_Read_Config_v1_2, __SPLS_TMPVAR__WAITLABEL_0__ )
    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    
    
    /* End local function variable declarations */
    
    {
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 992 );
    S2_IESS_Read_Config_v1_2__FILEOPENCONFIG ( ) ; 
    }

S2_IESS_Read_Config_v1_2_Exit____SPLS_TMPVAR__WAITLABEL_0__:
    
    /* Begin Free local function variables */
    /* End Free local function variables */
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }
DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00000 /*SystemStart*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1000 );
    if ( GetDigitalInput( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), __S2_IESS_Read_Config_v1_2_SYSTEMSTART_DIG_INPUT )) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1002 );
        Globals->S2_IESS_Read_Config_v1_2.__GLBL_STARTREAD = 1; 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1003 );
        if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FILENAME  )  ) > 0)) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1004 );
            S2_IESS_Read_Config_v1_2__FILEOPENCONFIG ( ) ; 
            }
        
        } 
    
    else 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1008 );
        S2_IESS_Read_Config_v1_2__STOPSYSTEM ( ) ; 
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__Event_0:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00001 /*SaveConfig*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1013 );
    S2_IESS_Read_Config_v1_2__WRITEDATATOCONFIG ( ) ; 
    
    S2_IESS_Read_Config_v1_2_Exit__Event_1:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}

DEFINE_INDEPENDENT_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00002 /*FileName*/ )

    {
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    CheckStack( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) );
    
    
    /* End local function variable declarations */
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1017 );
    if ( (Len( GLOBAL_STRING_STRUCT( S2_IESS_Read_Config_v1_2, __FILENAME  )  ) > 0)) 
        { 
        UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1019 );
        if ( Globals->S2_IESS_Read_Config_v1_2.__GLBL_STARTREAD) 
            {
            UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1020 );
            S2_IESS_Read_Config_v1_2__FILEOPENCONFIG ( ) ; 
            }
        
        } 
    
    
    S2_IESS_Read_Config_v1_2_Exit__Event_2:
    /* Begin Free local function variables */
/* End Free local function variables */
RESTORE_GLOBAL_POINTERS ;

}


    
    

/********************************************************************************
  Constructor
********************************************************************************/
int S2_IESS_Read_Config_v1_2_SDEVICE_Constructor ( START_STRUCTURE_DEFINITION( S2_IESS_Read_Config_v1_2, SDEVICE ) * me, int nVerbose )
{
    return 0;
}

/********************************************************************************
  Destructor
********************************************************************************/
int S2_IESS_Read_Config_v1_2_SDEVICE_Destructor ( START_STRUCTURE_DEFINITION( S2_IESS_Read_Config_v1_2, SDEVICE ) * me, int nVerbose )
{
    return 0;
}

/********************************************************************************
  static void ProcessDigitalEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessDigitalEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Read_Config_v1_2_SYSTEMSTART_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                
            }
            else /*Release*/
            {
                
            }
            SAFESPAWN_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00000 /*SystemStart*/, 0 );
            break;
            
        case __S2_IESS_Read_Config_v1_2_SAVECONFIG_DIG_INPUT :
            if ( Signal->Value /*Push*/ )
            {
                SAFESPAWN_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00001 /*SaveConfig*/, 0 );
                
            }
            else /*Release*/
            {
                SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
                
            }
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessAnalogEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessAnalogEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessStringEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessStringEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Read_Config_v1_2 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        case __S2_IESS_Read_Config_v1_2_FILENAME_STRING_INPUT :
            SAFESPAWN_EVENTHANDLER( S2_IESS_Read_Config_v1_2, 00002 /*FileName*/, 0 );
            break;
            
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketConnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketConnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketDisconnectEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketReceiveEvent( struct Signal_s *Signal )
{
    if ( UPDATE_INPUT_STRING( S2_IESS_Read_Config_v1_2 ) == 1 ) return ;
    
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  static void ProcessSocketStatusEvent( struct Signal_s *Signal )
********************************************************************************/
static void ProcessSocketStatusEvent( struct Signal_s *Signal )
{
    switch ( Signal->SignalNumber )
    {
        default :
            SetSymbolEventStart ( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ) ); 
            break ;
        
    }
}

/********************************************************************************
  EVENT_HANDLER( S2_IESS_Read_Config_v1_2 )
********************************************************************************/
EVENT_HANDLER( S2_IESS_Read_Config_v1_2 )
    {
    SAVE_GLOBAL_POINTERS ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYID ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYTYPE ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPROTOCOL ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWERONTIME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWEROFFTIME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYBAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYOUTPUTNUM ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENUP ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENDOWN ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERBAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERPARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOOBJECTID ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOINSTANCETAGS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOBAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOPARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAMECONTENT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECBAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECPARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECINPUTVIDEO ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECOUTPUTVIDEO ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECICON ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCENAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEBAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEPARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEINPUTVIDEO ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEICON ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEUSESNOTOUCH ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_ENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_NAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_ADDRESS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINNAME ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINPASSWORD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_PORT ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_BAUD ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_PARITY ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCENABLED ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCSTRINGS ) ;
    CHECK_STRING_INPUT_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCANALOGS ) ;
    switch ( Signal->Type )
        {
        case e_SIGNAL_TYPE_DIGITAL :
            ProcessDigitalEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_ANALOG :
            ProcessAnalogEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STRING :
            ProcessStringEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_CONNECT :
            ProcessSocketConnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_DISCONNECT :
            ProcessSocketDisconnectEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_RECEIVE :
            ProcessSocketReceiveEvent( Signal );
            break ;
        case e_SIGNAL_TYPE_STATUS :
            ProcessSocketStatusEvent( Signal );
            break ;
        }
        
    RESTORE_GLOBAL_POINTERS ;
    
    }
    
/********************************************************************************
  FUNCTION_MAIN( S2_IESS_Read_Config_v1_2 )
********************************************************************************/
FUNCTION_MAIN( S2_IESS_Read_Config_v1_2 )
{
    /* Begin local function variable declarations */
    
    SAVE_GLOBAL_POINTERS ;
    SET_INSTANCE_POINTER ( S2_IESS_Read_Config_v1_2 );
    
    
    /* End local function variable declarations */
    
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYID, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYID_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYID_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYIP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYLOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYLOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYLOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYLOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYLOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYTYPE, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYTYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYTYPE_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYPROTOCOL, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPROTOCOL_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPROTOCOL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYPOWERONTIME, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPOWERONTIME_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPOWERONTIME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYPOWEROFFTIME, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPOWEROFFTIME_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPOWEROFFTIME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYIP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYBAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYBAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYBAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYPARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYPARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYOUTPUTNUM, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYOUTPUTNUM_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYOUTPUTNUM_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYSCREENUP, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYSCREENUP_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYSCREENUP_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYSCREENDOWN, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYSCREENDOWN_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYSCREENDOWN_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYIPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYIPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __DISPLAYUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_DISPLAYUSED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERIP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERLOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERLOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERLOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERLOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERLOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERIP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERBAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERBAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERBAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERPARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERPARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERPARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERIPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERIPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SWITCHERUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_SWITCHERUSED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOIP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOLOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOLOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOLOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOLOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOLOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOLOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOOBJECTID, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOOBJECTID_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOOBJECTID_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOINSTANCETAGS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOINSTANCETAGS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOINSTANCETAGS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOIP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOBAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOBAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOBAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOPARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOPARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOPARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOIPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOIPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __AUDIOUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_AUDIOUSED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECNAMECONTENT, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECNAMECONTENT_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECNAMECONTENT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECIP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECLOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECLOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECLOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECLOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECLOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECLOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECIP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECBAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECBAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECBAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECPARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECPARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECPARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECINPUTVIDEO, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECINPUTVIDEO_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECINPUTVIDEO_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECOUTPUTVIDEO, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECOUTPUTVIDEO_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECOUTPUTVIDEO_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECICON, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECICON_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECICON_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECIPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECIPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __CODECUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_CODECUSED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCENAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCENAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCENAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEIP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCELOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCELOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCELOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCELOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCELOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCELOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEIP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEBAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEBAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEBAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEPARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEPARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEPARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEINPUTVIDEO, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEINPUTVIDEO_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEINPUTVIDEO_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEICON, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEICON_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEICON_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEIPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEIPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEUSESNOTOUCH, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEUSESNOTOUCH_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEUSESNOTOUCH_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __SOURCEUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_SOURCEUSED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_NAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_NAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_NAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_IP_ADDRESS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IP_ADDRESS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IP_ADDRESS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_LOGINNAME, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_LOGINNAME_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_LOGINNAME_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_LOGINPASSWORD, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_LOGINPASSWORD_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_LOGINPASSWORD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_IP_PORT, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IP_PORT_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IP_PORT_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_BAUD, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_BAUD_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_BAUD_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_PARITY, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_PARITY_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_PARITY_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_IPCONTROL, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IPCONTROL_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_IPCONTROL_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __ENV_USED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_USED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_ENV_USED_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __MISCSTRINGS, IO_TYPE_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCSTRINGS_STRING_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCSTRINGS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __MISCANALOGS, IO_TYPE_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCANALOGS_ANALOG_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCANALOGS_ARRAY_LENGTH );
    INITIALIZE_IO_ARRAY ( S2_IESS_Read_Config_v1_2, __MISCUSED, IO_TYPE_DIGITAL_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCUSED_DIG_OUTPUT, __S2_IESS_Read_Config_v1_2_MISCUSED_ARRAY_LENGTH );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __FILENAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_FILENAME_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __FILENAME, __S2_IESS_Read_Config_v1_2_FILENAME_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNAME_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNAME, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNAME_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMPHONENUMBER, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMPHONENUMBER_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMPHONENUMBER, __S2_IESS_Read_Config_v1_2_WRITE_ROOMPHONENUMBER_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHOFFENABLE, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHOFFENABLE_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHOFFENABLE, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHOFFENABLE_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHONENABLE, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHONENABLE_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHONENABLE, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHONENABLE_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHPOWEROFF, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHPOWEROFF_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMNOTOUCHPOWEROFF, __S2_IESS_Read_Config_v1_2_WRITE_ROOMNOTOUCHPOWEROFF_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMROUTINGMODE, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ROOMROUTINGMODE_STRING_MAX_LEN );
    REGISTER_GLOBAL_INPUT_STRING ( S2_IESS_Read_Config_v1_2, __WRITE_ROOMROUTINGMODE, __S2_IESS_Read_Config_v1_2_WRITE_ROOMROUTINGMODE_STRING_INPUT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYID, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYID_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYID_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYID_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYID );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYLOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYLOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYTYPE, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYTYPE_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYTYPE_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYTYPE_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYTYPE );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPROTOCOL, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPROTOCOL_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPROTOCOL_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPROTOCOL_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPROTOCOL );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWERONTIME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWERONTIME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWERONTIME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWERONTIME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWERONTIME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWEROFFTIME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWEROFFTIME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWEROFFTIME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPOWEROFFTIME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPOWEROFFTIME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYIP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYIP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYBAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYBAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYBAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYBAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYBAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYPARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYPARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYOUTPUTNUM, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYOUTPUTNUM_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYOUTPUTNUM_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYOUTPUTNUM_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYOUTPUTNUM );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENUP, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENUP_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENUP_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENUP_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENUP );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENDOWN, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENDOWN_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENDOWN_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_DISPLAYSCREENDOWN_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_DISPLAYSCREENDOWN );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERLOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERLOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERIP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERIP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERBAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERBAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERBAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERBAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERBAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERPARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERPARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERPARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SWITCHERPARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SWITCHERPARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOLOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOLOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOOBJECTID, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOOBJECTID_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOOBJECTID_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOOBJECTID_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOOBJECTID );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOINSTANCETAGS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOINSTANCETAGS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOINSTANCETAGS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOINSTANCETAGS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOINSTANCETAGS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOIP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOIP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOBAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOBAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOBAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOBAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOBAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOPARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOPARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOPARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_AUDIOPARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_AUDIOPARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAMECONTENT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAMECONTENT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAMECONTENT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECNAMECONTENT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECNAMECONTENT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECLOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECLOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECIP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECIP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECBAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECBAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECBAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECBAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECBAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECPARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECPARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECPARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECPARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECPARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECINPUTVIDEO, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECINPUTVIDEO_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECINPUTVIDEO_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECINPUTVIDEO_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECINPUTVIDEO );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECOUTPUTVIDEO, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECOUTPUTVIDEO_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECOUTPUTVIDEO_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECOUTPUTVIDEO_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECOUTPUTVIDEO );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_CODECICON, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_CODECICON_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_CODECICON_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_CODECICON_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_CODECICON );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCENAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCENAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCENAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCENAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCENAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCELOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCELOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEIP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEIP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEBAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEBAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEBAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEBAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEBAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEPARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEPARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEPARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEPARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEPARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEINPUTVIDEO, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEINPUTVIDEO_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEINPUTVIDEO_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEINPUTVIDEO_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEINPUTVIDEO );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEICON, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEICON_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEICON_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEICON_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEICON );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEUSESNOTOUCH, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEUSESNOTOUCH_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEUSESNOTOUCH_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_SOURCEUSESNOTOUCH_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_SOURCEUSESNOTOUCH );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_ENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_ENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_ENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_ENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_ENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_NAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_NAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_NAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_NAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_NAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_ADDRESS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_ADDRESS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_ADDRESS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_ADDRESS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_ADDRESS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINNAME, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINNAME_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINNAME_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINNAME_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINNAME );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINPASSWORD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINPASSWORD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINPASSWORD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_LOGINPASSWORD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_LOGINPASSWORD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_PORT, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_PORT_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_PORT_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_IP_PORT_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_IP_PORT );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_BAUD, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_BAUD_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_BAUD_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_BAUD_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_BAUD );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_ENV_PARITY, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_ENV_PARITY_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_PARITY_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_ENV_PARITY_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_ENV_PARITY );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCENABLED, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_MISCENABLED_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_MISCENABLED_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_MISCENABLED_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCENABLED );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCSTRINGS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_MISCSTRINGS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_MISCSTRINGS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_MISCSTRINGS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCSTRINGS );
    INITIALIZE_GLOBAL_STRING_INPUT_ARRAY( S2_IESS_Read_Config_v1_2, __WRITE_MISCANALOGS, e_INPUT_TYPE_STRING, __S2_IESS_Read_Config_v1_2_WRITE_MISCANALOGS_ARRAY_NUM_ELEMS, __S2_IESS_Read_Config_v1_2_WRITE_MISCANALOGS_ARRAY_NUM_CHARS, __S2_IESS_Read_Config_v1_2_WRITE_MISCANALOGS_STRING_INPUT );
    REGISTER_GLOBAL_INPUT_STRING_ARRAY ( S2_IESS_Read_Config_v1_2, __WRITE_MISCANALOGS );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVDISPLAY, SDEVICE, __S2_IESS_Read_Config_v1_2_GVDISPLAY_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVSWITCHER, SDEVICE, __S2_IESS_Read_Config_v1_2_GVSWITCHER_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVAUDIO, SDEVICE, __S2_IESS_Read_Config_v1_2_GVAUDIO_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVCODEC, SDEVICE, __S2_IESS_Read_Config_v1_2_GVCODEC_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVSOURCE, SDEVICE, __S2_IESS_Read_Config_v1_2_GVSOURCE_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVENV, SDEVICE, __S2_IESS_Read_Config_v1_2_GVENV_STRUCT_MAX_LEN );
    INITIALIZE_GLOBAL_STRUCT_ARRAY ( S2_IESS_Read_Config_v1_2, __GVMISC, SDEVICE, __S2_IESS_Read_Config_v1_2_GVMISC_STRUCT_MAX_LEN );
    
    INITIALIZE_GLOBAL_STRING_STRUCT ( S2_IESS_Read_Config_v1_2, sGenericOutStr, e_INPUT_TYPE_NONE, GENERIC_STRING_OUTPUT_LEN );
    
    
    
    
    
    UpdateSourceCodeLine( INSTANCE_PTR( S2_IESS_Read_Config_v1_2 ), 1026 );
    Globals->S2_IESS_Read_Config_v1_2.__GLBL_MAXDISPLAYS = 0; 
    
    S2_IESS_Read_Config_v1_2_Exit__MAIN:/* Begin Free local function variables */
    /* End Free local function variables */
    
    RESTORE_GLOBAL_POINTERS ;
    return 0 ;
    }

