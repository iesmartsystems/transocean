using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DRILL_CHAIR_V0_1
{
    public class UserModuleClass_IESS_DRILL_CHAIR_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.DigitalOutput LOGGED_IN_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> BUTTON_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> SWITCH_FB;
        SplusTcpClient TCPCLIENT;
        STCPCLIENT GVDRILLCHAIR;
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 90;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 92;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 93;
                    Trace( "Drill Chair RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 95;
                    Trace( "Drill Chair TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 101;
            LVTEMP  .UpdateValue ( LVSTRING + GVDRILLCHAIR . ETX  ) ; 
            __context__.SourceCodeLine = 102;
            Functions.SocketSend ( TCPCLIENT , LVTEMP ) ; 
            __context__.SourceCodeLine = 103;
            SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
            
            }
            
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 108;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 110;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( TCPCLIENT ) ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 112;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 114;
                    if ( Functions.TestForTrue  ( ( Functions.Not( GVDRILLCHAIR.STATUSCONNECTED ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 116;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( GVDRILLCHAIR.IPADDRESS ) > 4 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( GVDRILLCHAIR.IPPORT > 0 ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 118;
                            LVSTATUS = (short) ( Functions.SocketConnectClient( TCPCLIENT , GVDRILLCHAIR.IPADDRESS , (ushort)( GVDRILLCHAIR.IPPORT ) , (ushort)( 0 ) ) ) ; 
                            } 
                        
                        } 
                    
                    } 
                
                }
            
            
            }
            
        private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
            { 
            ushort LVNUM = 0;
            ushort LVINDEX = 0;
            
            CrestronString LVRX;
            CrestronString LVTRASH;
            CrestronString LVTEMP;
            LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 127;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000D\u000A" , GVDRILLCHAIR.RXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 129;
                if ( Functions.TestForTrue  ( ( Functions.Find( "Username>>" , GVDRILLCHAIR.RXQUEUE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 131;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Username>>" , GVDRILLCHAIR . RXQUEUE )  ) ; 
                    __context__.SourceCodeLine = 132;
                    SETQUEUE (  __context__ , GVDRILLCHAIR.LOGINUSER) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 134;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Password>>" , GVDRILLCHAIR.RXQUEUE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 136;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "Password>>" , GVDRILLCHAIR . RXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 137;
                        SETQUEUE (  __context__ , GVDRILLCHAIR.LOGINPASS) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 141;
                        LVRX  .UpdateValue ( Functions.Remove ( "\u000D\u000A" , GVDRILLCHAIR . RXQUEUE )  ) ; 
                        __context__.SourceCodeLine = 142;
                        LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 2), LVRX )  ) ; 
                        __context__.SourceCodeLine = 143;
                        SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
                        __context__.SourceCodeLine = 144;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "Verification Successful" , LVRX ))  ) ) 
                            {
                            __context__.SourceCodeLine = 145;
                            LOGGED_IN_FB  .Value = (ushort) ( 1 ) ; 
                            }
                        
                        else 
                            {
                            __context__.SourceCodeLine = 146;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "Invalid Username or Password:" , LVRX ))  ) ) 
                                {
                                __context__.SourceCodeLine = 147;
                                LOGGED_IN_FB  .Value = (ushort) ( 0 ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 148;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "Button B" , LVRX ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 150;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "Button B" , LVRX )  ) ; 
                                    __context__.SourceCodeLine = 151;
                                    LVINDEX = (ushort) ( Functions.Atoi( Functions.Remove( 2 , LVRX ) ) ) ; 
                                    __context__.SourceCodeLine = 152;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
                                    __context__.SourceCodeLine = 153;
                                    LVNUM = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                    __context__.SourceCodeLine = 154;
                                    BUTTON_FB [ LVINDEX]  .Value = (ushort) ( LVNUM ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 156;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "Switch S" , LVRX ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 158;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "Switch S" , LVRX )  ) ; 
                                        __context__.SourceCodeLine = 159;
                                        LVINDEX = (ushort) ( Functions.Atoi( Functions.Remove( 2 , LVRX ) ) ) ; 
                                        __context__.SourceCodeLine = 160;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
                                        __context__.SourceCodeLine = 161;
                                        LVNUM = (ushort) ( Functions.Atoi( LVRX ) ) ; 
                                        __context__.SourceCodeLine = 162;
                                        SWITCH_FB [ LVINDEX]  .Value = (ushort) ( LVNUM ) ; 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    }
                
                __context__.SourceCodeLine = 127;
                } 
            
            
            }
            
        private void RETRYCONNECTION (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 169;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            __context__.SourceCodeLine = 170;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            
            }
            
        object TCPCLIENT_OnSocketConnect_0 ( Object __Info__ )
        
            { 
            SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
                
                __context__.SourceCodeLine = 179;
                GVDRILLCHAIR . STATUSCONNECTED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 180;
                GVDRILLCHAIR . RECONNECTING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 181;
                CONNECT_FB  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 182;
                CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
                __context__.SourceCodeLine = 183;
                CreateWait ( "CHECKLOGINSTATUS" , 500 , CHECKLOGINSTATUS_Callback ) ;
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SocketInfo__ ); }
            return this;
            
        }
        
    public void CHECKLOGINSTATUS_CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            
            __context__.SourceCodeLine = 185;
            if ( Functions.TestForTrue  ( ( Functions.Not( LOGGED_IN_FB  .Value ))  ) ) 
                {
                __context__.SourceCodeLine = 186;
                RETRYCONNECTION (  __context__  ) ; 
                }
            
            
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
object TCPCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 192;
        GVDRILLCHAIR . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 193;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 194;
        CONNECT_STATUS_FB  .Value = (ushort) ( TCPCLIENT.SocketStatus ) ; 
        __context__.SourceCodeLine = 195;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( GVDRILLCHAIR.STATUSCONNECTREQUEST ) && Functions.TestForTrue ( Functions.Not( GVDRILLCHAIR.RECONNECTING ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 197;
            GVDRILLCHAIR . RECONNECTING = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 198;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 199;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Not( GVDRILLCHAIR.STATUSCONNECTED ) ) && Functions.TestForTrue ( GVDRILLCHAIR.RECONNECTING )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 201;
                CreateWait ( "RECONNECTIP" , 3000 , RECONNECTIP_Callback ) ;
                __context__.SourceCodeLine = 199;
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public void RECONNECTIP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 202;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object TCPCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 208;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 209;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 211;
            GVDRILLCHAIR . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 212;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 216;
            GVDRILLCHAIR . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 217;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 223;
        GVDRILLCHAIR . RXQUEUE  .UpdateValue ( GVDRILLCHAIR . RXQUEUE + TCPCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 224;
        Functions.ClearBuffer ( TCPCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 225;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 229;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 231;
            GVDRILLCHAIR . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 232;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 236;
            GVDRILLCHAIR . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 237;
            GVDRILLCHAIR . RECONNECTING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 238;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 243;
        GVDRILLCHAIR . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 247;
        GVDRILLCHAIR . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 251;
        GVDRILLCHAIR . LOGINUSER  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 255;
        GVDRILLCHAIR . LOGINPASS  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 259;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 269;
        GVDRILLCHAIR . IPPORT = (ushort) ( 6000 ) ; 
        __context__.SourceCodeLine = 270;
        GVDRILLCHAIR . LOGINUSER  .UpdateValue ( "drillsim"  ) ; 
        __context__.SourceCodeLine = 271;
        GVDRILLCHAIR . LOGINPASS  .UpdateValue ( "drillsim"  ) ; 
        __context__.SourceCodeLine = 272;
        GVDRILLCHAIR . ETX  .UpdateValue ( "\u000D"  ) ; 
        __context__.SourceCodeLine = 273;
        GVDRILLCHAIR . CUTOFF  .UpdateValue ( "\u000D\u000A"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    TCPCLIENT  = new SplusTcpClient ( 2047, this );
    GVDRILLCHAIR  = new STCPCLIENT( this, true );
    GVDRILLCHAIR .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    LOGGED_IN_FB = new Crestron.Logos.SplusObjects.DigitalOutput( LOGGED_IN_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( LOGGED_IN_FB__DigitalOutput__, LOGGED_IN_FB );
    
    BUTTON_FB = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        BUTTON_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( BUTTON_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( BUTTON_FB__DigitalOutput__ + i, BUTTON_FB[i+1] );
    }
    
    SWITCH_FB = new InOutArray<DigitalOutput>( 16, this );
    for( uint i = 0; i < 16; i++ )
    {
        SWITCH_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( SWITCH_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( SWITCH_FB__DigitalOutput__ + i, SWITCH_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 31, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    CHECKLOGINSTATUS_Callback = new WaitFunction( CHECKLOGINSTATUS_CallbackFn );
    RECONNECTIP_Callback = new WaitFunction( RECONNECTIP_CallbackFn );
    
    TCPCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketConnect_0, false ) );
    TCPCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketDisconnect_1, false ) );
    TCPCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketStatus_2, false ) );
    TCPCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( TCPCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_5, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_6, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_7, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_8, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_9, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_10, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DRILL_CHAIR_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction CHECKLOGINSTATUS_Callback;
private WaitFunction RECONNECTIP_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint MANUALCMD__AnalogSerialInput__ = 4;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint LOGGED_IN_FB__DigitalOutput__ = 1;
const uint BUTTON_FB__DigitalOutput__ = 2;
const uint SWITCH_FB__DigitalOutput__ = 18;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class STCPCLIENT : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  RECONNECTING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  LOGINUSER;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  LOGINPASS;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(8, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  CUTOFF;
    
    
    public STCPCLIENT( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINUSER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        CUTOFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
