using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_QSYS_V0_8
{
    public class UserModuleClass_IESS_DSP_QSC_QSYS_V0_8 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> TRACK__POUND___PLAYER;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> STATUS_PLAYER;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> USED_FILENAME;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> FILENAME_PLAYER;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> AUDIO_FILENAME;
        SplusTcpClient AUDIOCLIENT;
        SAUDIO AUDIODEVICE;
        ushort GVVOIPCOUNTER = 0;
        ushort GVBARGRAPHMAX = 0;
        ushort GVVOLUMERANGE = 0;
        ushort GVPLAYER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 126;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 128;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                __context__.SourceCodeLine = 129;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 130;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 132;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 134;
                    LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 0 ) ) ) ; 
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 144;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 146;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 148;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 149;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 151;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 152;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 154;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 155;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 158;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 160;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 161;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 162;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 164;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 165;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 167;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 168;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 169;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 170;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 171;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 179;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 180;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMEINCOMING >= LVMAXIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 181;
                return (uint)( 65535) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 182;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMEINCOMING <= LVMINIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 183;
                    return (uint)( 0) ; 
                    }
                
                else 
                    { 
                    __context__.SourceCodeLine = 187;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 189;
                        LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                        __context__.SourceCodeLine = 190;
                        LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                        __context__.SourceCodeLine = 191;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 193;
                            LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                            __context__.SourceCodeLine = 194;
                            LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 196;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                                {
                                __context__.SourceCodeLine = 197;
                                LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                                }
                            
                            }
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 201;
                        LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                        __context__.SourceCodeLine = 202;
                        LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 205;
                    LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
                    __context__.SourceCodeLine = 206;
                    LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
                    __context__.SourceCodeLine = 208;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 209;
                        LVVOLUMELEVEL = (uint) ( 0 ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 210;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                            {
                            __context__.SourceCodeLine = 211;
                            LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                            }
                        
                        }
                    
                    __context__.SourceCodeLine = 212;
                    return (uint)( LVVOLUMELEVEL) ; 
                    } 
                
                }
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 217;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 219;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 220;
                    Trace( "QSYS RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 222;
                    Trace( "QSYS TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            
            __context__.SourceCodeLine = 229;
            LVTEMP  .UpdateValue ( LVSTRING + AUDIODEVICE . ETX  ) ; 
            __context__.SourceCodeLine = 230;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 1710))  ) ) 
                {
                __context__.SourceCodeLine = 231;
                Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 233;
                TX  .UpdateValue ( LVTEMP  ) ; 
                }
            
            __context__.SourceCodeLine = 234;
            SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
            
            }
            
        private void POLLPLAYER (  SplusExecutionContext __context__, ushort LVPLAYER ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 239;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVPLAYER ]  ) ; 
            __context__.SourceCodeLine = 240;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022playing\u0022 }"  ) ; 
            __context__.SourceCodeLine = 241;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 242;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        private void POLLPLAYERFILE (  SplusExecutionContext __context__, ushort LVPLAYER ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 247;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVPLAYER ]  ) ; 
            __context__.SourceCodeLine = 248;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022filename\u0022 }"  ) ; 
            __context__.SourceCodeLine = 249;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 250;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        private void DELAYPOLLPLAYER (  SplusExecutionContext __context__, ushort LVPLAYER ) 
            { 
            
            __context__.SourceCodeLine = 255;
            GVPLAYER = (ushort) ( LVPLAYER ) ; 
            __context__.SourceCodeLine = 256;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_51__" , 100 , __SPLS_TMPVAR__WAITLABEL_51___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_51___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            __context__.SourceCodeLine = 259;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ GVPLAYER ]  ) ; 
            __context__.SourceCodeLine = 260;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022playing\u0022 }"  ) ; 
            __context__.SourceCodeLine = 261;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 262;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
        { 
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 268;
        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATE[ LVINDEX ] ))  ) ) 
            {
            __context__.SourceCodeLine = 269;
            AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
            }
        
        else 
            { 
            __context__.SourceCodeLine = 272;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ LVINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 273;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.pinpad." + LVNUM + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 274;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 275;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        
        }
        
    private void POLLINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVINDEXCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 282;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 283;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 284;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 285;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 286;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 288;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 290;
                if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 292;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                    __context__.SourceCodeLine = 293;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 295;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 298;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 300;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 301;
                            LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 303;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 305;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 306;
                                LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 308;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 310;
                                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                    ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                    int __FN_FORSTEP_VAL__1 = (int)1; 
                                    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                        { 
                                        __context__.SourceCodeLine = 312;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                        __context__.SourceCodeLine = 313;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 314;
                                            LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 310;
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 317;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 320;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 321;
                                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022bypass\u0022 }"  ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 322;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "PLYR" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 324;
                                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022filename\u0022 }, { \u0022Name\u0022: \u0022playing\u0022 }"  ) ; 
                                                } 
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 326;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 327;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 328;
        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 330;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 331;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 332;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
            __context__.SourceCodeLine = 333;
            LVSTRING  .UpdateValue ( LVSTRING + ".mute\u0022 }"  ) ; 
            __context__.SourceCodeLine = 334;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        __context__.SourceCodeLine = 336;
        SETQUEUE (  __context__ , LVSTRING) ; 
        
        }
        
    private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVINDEXCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 342;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
            { 
            __context__.SourceCodeLine = 344;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AddComponentControl\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022, \u0022Component\u0022 : "  ) ; 
            __context__.SourceCodeLine = 345;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 346;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 348;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                __context__.SourceCodeLine = 355;
                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 357;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 359;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                    __context__.SourceCodeLine = 360;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 362;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 364;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                        __context__.SourceCodeLine = 365;
                        LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                        __context__.SourceCodeLine = 366;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 368;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 370;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                            __context__.SourceCodeLine = 371;
                            LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                            __context__.SourceCodeLine = 379;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 381;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 384;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 386;
                                    GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                    __context__.SourceCodeLine = 387;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                        {
                                        __context__.SourceCodeLine = 388;
                                        AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                        }
                                    
                                    __context__.SourceCodeLine = 389;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                                    __context__.SourceCodeLine = 390;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 },"  ) ; 
                                    __context__.SourceCodeLine = 391;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022recent.calls\u0022 }"  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 393;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 395;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 396;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 397;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 399;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 401;
                                            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                                            ushort __FN_FOREND_VAL__1 = (ushort)AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ]; 
                                            int __FN_FORSTEP_VAL__1 = (int)1; 
                                            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                                                { 
                                                __context__.SourceCodeLine = 403;
                                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( LVCOUNTER ) ) + "\u0022 }"  ) ; 
                                                __context__.SourceCodeLine = 404;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVCOUNTER < AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ))  ) ) 
                                                    {
                                                    __context__.SourceCodeLine = 405;
                                                    LVSTRING  .UpdateValue ( LVSTRING + ", "  ) ; 
                                                    }
                                                
                                                __context__.SourceCodeLine = 401;
                                                } 
                                            
                                            __context__.SourceCodeLine = 407;
                                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 409;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 411;
                                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022bypass\u0022 }"  ) ; 
                                                __context__.SourceCodeLine = 412;
                                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 414;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "PLYR" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                                    { 
                                                    } 
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 417;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } } }"  ) ; 
            __context__.SourceCodeLine = 418;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 419;
            POLLINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
            } 
        
        
        }
        
    private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
        { 
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 425;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 427;
            INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
            __context__.SourceCodeLine = 425;
            } 
        
        __context__.SourceCodeLine = 429;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_52__" , 200 , __SPLS_TMPVAR__WAITLABEL_52___Callback ) ;
        
        }
        
    public void __SPLS_TMPVAR__WAITLABEL_52___CallbackFn( object stateInfo )
    {
    
        try
        {
            Wait __LocalWait__ = (Wait)stateInfo;
            SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
            __LocalWait__.RemoveFromList();
            
            {
            __context__.SourceCodeLine = 430;
            SETQUEUE (  __context__ , AUDIODEVICE.COMMANDAUTOPOLL) ; 
            }
        
        
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        
    }
    
private void CHECKINSTANCETAGS (  SplusExecutionContext __context__, CrestronString LVINSTANCETAG , CrestronString LVPARSEDSTRING ) 
    { 
    ushort LVCOUNTER = 0;
    ushort LVVOIPNUM = 0;
    ushort LVVOIPCOUNTER = 0;
    ushort LVMUTE = 0;
    ushort LVFILE = 0;
    ushort LVVALUE = 0;
    ushort LVSTEP = 0;
    
    short LVVOL = 0;
    
    CrestronString LVVOLUME;
    CrestronString LVSTRING;
    CrestronString LVTRASH;
    CrestronString LVINSTANCEINDEX;
    CrestronString LVCOMPARESTRING;
    CrestronString LVMUTES;
    CrestronString LVFILENAME;
    CrestronString LVCHOICES;
    CrestronString LVTEMP;
    LVVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
    LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    LVINSTANCEINDEX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    LVCOMPARESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
    LVMUTES  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
    LVFILENAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    LVCHOICES  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, this );
    
    
    __context__.SourceCodeLine = 437;
    ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
    ushort __FN_FOREND_VAL__1 = (ushort)64; 
    int __FN_FORSTEP_VAL__1 = (int)1; 
    for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
        { 
        __context__.SourceCodeLine = 439;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINSTANCETAG == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
            { 
            __context__.SourceCodeLine = 441;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 443;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022gain\u0022" , LVPARSEDSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 445;
                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                    __context__.SourceCodeLine = 446;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                    __context__.SourceCodeLine = 447;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 449;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 450;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 452;
                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 453;
                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                            __context__.SourceCodeLine = 454;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                {
                                __context__.SourceCodeLine = 455;
                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                }
                            
                            else 
                                {
                                __context__.SourceCodeLine = 457;
                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                }
                            
                            __context__.SourceCodeLine = 458;
                            AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                            __context__.SourceCodeLine = 459;
                            VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                            __context__.SourceCodeLine = 460;
                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                {
                                __context__.SourceCodeLine = 461;
                                AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                __context__.SourceCodeLine = 465;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute\u0022" , LVPARSEDSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 467;
                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                    __context__.SourceCodeLine = 468;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                    __context__.SourceCodeLine = 469;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 471;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 472;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 474;
                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                            __context__.SourceCodeLine = 475;
                            VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 477;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 479;
                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                __context__.SourceCodeLine = 480;
                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                } 
                            
                            }
                        
                        } 
                    
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 485;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 487;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022output." , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 489;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 490;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022output." , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 491;
                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 492;
                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                        __context__.SourceCodeLine = 493;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                            { 
                            __context__.SourceCodeLine = 495;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "gain\u0022" , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 497;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 498;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "gain\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 499;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 501;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 502;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 504;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 505;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                        __context__.SourceCodeLine = 506;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 507;
                                            LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 509;
                                            LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 510;
                                        AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                        __context__.SourceCodeLine = 511;
                                        VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                        __context__.SourceCodeLine = 512;
                                        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 513;
                                            AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 517;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "mute\u0022" , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 519;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 520;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "mute\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 521;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 523;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 524;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 525;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 527;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 528;
                                        VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 530;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 532;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 533;
                                            VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 539;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "wall." , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 541;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 542;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022wall." , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 543;
                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 544;
                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                        __context__.SourceCodeLine = 545;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                            { 
                            __context__.SourceCodeLine = 547;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "open\u0022,\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 549;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 550;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "open\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 551;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 553;
                                    AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 555;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 557;
                                        AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 558;
                                        AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 559;
                                        GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            __context__.SourceCodeLine = 562;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 564;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 565;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 566;
                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 567;
                                if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSWALL[ LVCOUNTER ])  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 569;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "G" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 571;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "G" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 572;
                                        AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        __context__.SourceCodeLine = 573;
                                        GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 580;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 582;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022select." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 584;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 585;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022select." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 586;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 587;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 588;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 590;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 592;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 593;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 595;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 596;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 1), LVVOLUME )  ) ; 
                                        __context__.SourceCodeLine = 597;
                                        AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVVOLUME ) ) ; 
                                        __context__.SourceCodeLine = 598;
                                        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOLUMEMUTE[ LVCOUNTER ] ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 599;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 604;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 606;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 607;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022mute." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 608;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 609;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 610;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 612;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 614;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 615;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 617;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                        __context__.SourceCodeLine = 618;
                                        GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 620;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 622;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 623;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 629;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 631;
                            LVCOMPARESTRING  .UpdateValue ( "\u0022Name\u0022:\u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) ) + ".gain\u0022"  ) ; 
                            __context__.SourceCodeLine = 632;
                            if ( Functions.TestForTrue  ( ( Functions.Find( LVCOMPARESTRING , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 634;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 635;
                                LVTRASH  .UpdateValue ( Functions.Remove ( LVCOMPARESTRING , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 636;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 638;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 639;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 641;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 642;
                                        LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                        __context__.SourceCodeLine = 643;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 644;
                                            LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                            }
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 646;
                                            LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 647;
                                        AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                        __context__.SourceCodeLine = 648;
                                        LVVOL = (short) ( (((LVVOL + 100) * GVBARGRAPHMAX) / GVVOLUMERANGE) ) ; 
                                        __context__.SourceCodeLine = 649;
                                        VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( LVVOL ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 653;
                            LVCOMPARESTRING  .UpdateValue ( "\u0022Name\u0022:\u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) ) + ".mute\u0022"  ) ; 
                            __context__.SourceCodeLine = 654;
                            if ( Functions.TestForTrue  ( ( Functions.Find( LVCOMPARESTRING , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 656;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 657;
                                LVTRASH  .UpdateValue ( Functions.Remove ( LVCOMPARESTRING , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 658;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Value\u0022:" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 660;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Value\u0022:" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 661;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "." , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 663;
                                        LVMUTES  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 664;
                                        LVMUTES  .UpdateValue ( Functions.Remove ( (Functions.Length( LVMUTES ) - 1), LVMUTES )  ) ; 
                                        __context__.SourceCodeLine = 665;
                                        LVMUTE = (ushort) ( Functions.Atoi( LVMUTES ) ) ; 
                                        __context__.SourceCodeLine = 666;
                                        AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( LVMUTE ) ; 
                                        __context__.SourceCodeLine = 667;
                                        VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( LVMUTE ) ; 
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 672;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 674;
                                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                                ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                int __FN_FORSTEP_VAL__2 = (int)1; 
                                for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                    { 
                                    __context__.SourceCodeLine = 676;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 678;
                                        LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                        __context__.SourceCodeLine = 679;
                                        break ; 
                                        } 
                                    
                                    __context__.SourceCodeLine = 674;
                                    } 
                                
                                __context__.SourceCodeLine = 682;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 684;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 685;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 686;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 688;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 689;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 690;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 691;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 693;
                                            AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 694;
                                            VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 696;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 698;
                                                AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 699;
                                                VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 703;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 705;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 706;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 707;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 709;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 710;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 711;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 712;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 714;
                                            AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 715;
                                            VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 717;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 719;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 720;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 724;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 726;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 727;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 728;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 730;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 731;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 732;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 733;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 735;
                                            AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 736;
                                            VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 738;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 740;
                                                AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 741;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 759;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.status\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 761;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 762;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.status\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 763;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 765;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 766;
                                        LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 767;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Dialing - " , LVSTRING ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 768;
                                            VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 769;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Incoming Call" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 771;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "Incoming Call" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 772;
                                            VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                            __context__.SourceCodeLine = 773;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( " - " , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 775;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( " - " , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 776;
                                                VOIP_CALLINCOMINGNAME_FB [ LVVOIPNUM]  .UpdateValue ( LVSTRING  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 780;
                                            VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 783;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 785;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 786;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 787;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Choices\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 789;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "Text\u005C\u0022:\u005C\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 791;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "Text\u005C\u0022:\u005C\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 792;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u005C\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 793;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 794;
                                            AUDIODEVICE . STATUSVOIPRECENTCALL [ LVVOIPNUM ]  .UpdateValue ( LVSTRING  ) ; 
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 799;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 801;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 802;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022load." , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 804;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022load." , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 805;
                                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 806;
                                        LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                        __context__.SourceCodeLine = 807;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Color\u0022:\u0022@" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 809;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Color\u0022:\u0022@" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 810;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 811;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 812;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVSTRING == "7F7F"))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 813;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVINSTANCEINDEX ) ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 817;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 819;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022bypass\u0022" , LVPARSEDSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 821;
                                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                            __context__.SourceCodeLine = 822;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022bypass\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 823;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 825;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 826;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "no" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 828;
                                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 829;
                                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 831;
                                                    if ( Functions.TestForTrue  ( ( Functions.Find( "bypassed" , LVSTRING ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 833;
                                                        AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 834;
                                                        VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                        } 
                                                    
                                                    }
                                                
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 839;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "PLYR" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 841;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022filename\u0022" , LVPARSEDSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 843;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022filename\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 844;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 846;
                                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                                    __context__.SourceCodeLine = 847;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 848;
                                                    LVFILENAME  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 849;
                                                    LVFILENAME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVFILENAME ) - 1), LVFILENAME )  ) ; 
                                                    __context__.SourceCodeLine = 850;
                                                    FILENAME_PLAYER [ LVCOUNTER]  .UpdateValue ( LVFILENAME  ) ; 
                                                    } 
                                                
                                                __context__.SourceCodeLine = 852;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Choices\u0022:[\u0022" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 854;
                                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                                    __context__.SourceCodeLine = 855;
                                                    LVFILE = (ushort) ( 1 ) ; 
                                                    __context__.SourceCodeLine = 856;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Choices\u0022:[\u0022" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 857;
                                                    LVCHOICES  .UpdateValue ( Functions.Remove ( "]" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 858;
                                                    while ( Functions.TestForTrue  ( ( Functions.Find( "\u0022" , LVCHOICES ))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 860;
                                                        LVFILENAME  .UpdateValue ( Functions.Remove ( "\u0022" , LVCHOICES )  ) ; 
                                                        __context__.SourceCodeLine = 861;
                                                        LVFILENAME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVFILENAME ) - 1), LVFILENAME )  ) ; 
                                                        __context__.SourceCodeLine = 862;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVFILENAME ) > 3 ))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 864;
                                                            AUDIODEVICE . AUDIOTRACK [ LVFILE ]  .UpdateValue ( LVFILENAME  ) ; 
                                                            __context__.SourceCodeLine = 865;
                                                            AUDIO_FILENAME [ LVFILE]  .UpdateValue ( AUDIODEVICE . AUDIOTRACK [ LVFILE ]  ) ; 
                                                            __context__.SourceCodeLine = 866;
                                                            LVFILE = (ushort) ( (LVFILE + 1) ) ; 
                                                            } 
                                                        
                                                        __context__.SourceCodeLine = 858;
                                                        } 
                                                    
                                                    __context__.SourceCodeLine = 869;
                                                    LVFILE = (ushort) ( (LVFILE - 1) ) ; 
                                                    __context__.SourceCodeLine = 870;
                                                    ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                                                    ushort __FN_FOREND_VAL__3 = (ushort)64; 
                                                    int __FN_FORSTEP_VAL__3 = (int)1; 
                                                    for ( LVSTEP  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVSTEP  >= __FN_FORSTART_VAL__3) && (LVSTEP  <= __FN_FOREND_VAL__3) ) : ( (LVSTEP  <= __FN_FORSTART_VAL__3) && (LVSTEP  >= __FN_FOREND_VAL__3) ) ; LVSTEP  += (ushort)__FN_FORSTEP_VAL__3) 
                                                        {
                                                        __context__.SourceCodeLine = 871;
                                                        USED_FILENAME [ LVSTEP]  .Value = (ushort) ( 0 ) ; 
                                                        __context__.SourceCodeLine = 870;
                                                        }
                                                    
                                                    __context__.SourceCodeLine = 872;
                                                    ushort __FN_FORSTART_VAL__4 = (ushort) ( 1 ) ;
                                                    ushort __FN_FOREND_VAL__4 = (ushort)LVFILE; 
                                                    int __FN_FORSTEP_VAL__4 = (int)1; 
                                                    for ( LVSTEP  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (LVSTEP  >= __FN_FORSTART_VAL__4) && (LVSTEP  <= __FN_FOREND_VAL__4) ) : ( (LVSTEP  <= __FN_FORSTART_VAL__4) && (LVSTEP  >= __FN_FOREND_VAL__4) ) ; LVSTEP  += (ushort)__FN_FORSTEP_VAL__4) 
                                                        {
                                                        __context__.SourceCodeLine = 873;
                                                        USED_FILENAME [ LVSTEP]  .Value = (ushort) ( 1 ) ; 
                                                        __context__.SourceCodeLine = 872;
                                                        }
                                                    
                                                    } 
                                                
                                                } 
                                            
                                            __context__.SourceCodeLine = 876;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022playing\u0022" , LVPARSEDSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 878;
                                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                                __context__.SourceCodeLine = 879;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022playing\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 880;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Value\u0022:" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 882;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Value\u0022:" , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 883;
                                                    LVTEMP  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 884;
                                                    LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 1), LVTEMP )  ) ; 
                                                    __context__.SourceCodeLine = 885;
                                                    LVVALUE = (ushort) ( Functions.Atoi( LVTEMP ) ) ; 
                                                    __context__.SourceCodeLine = 886;
                                                    STATUS_PLAYER [ LVCOUNTER]  .Value = (ushort) ( LVVALUE ) ; 
                                                    __context__.SourceCodeLine = 887;
                                                    if ( Functions.TestForTrue  ( ( LVVALUE)  ) ) 
                                                        {
                                                        __context__.SourceCodeLine = 888;
                                                        DELAYPOLLPLAYER (  __context__ , (ushort)( LVCOUNTER )) ; 
                                                        }
                                                    
                                                    } 
                                                
                                                } 
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            } 
        
        __context__.SourceCodeLine = 437;
        } 
    
    
    }
    
private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
    { 
    ushort LVINDEX = 0;
    ushort LVCOUNTER = 0;
    ushort LVVOIPNUM = 0;
    ushort LVVOIPCOUNTER = 0;
    
    short LVVOL = 0;
    
    CrestronString LVRX;
    CrestronString LVTRASH;
    CrestronString LVSTRING;
    CrestronString LVTAG;
    LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, this );
    LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
    LVTAG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    
    
    __context__.SourceCodeLine = 900;
    while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , AUDIODEVICE.RXQUEUE ))  ) ) 
        { 
        __context__.SourceCodeLine = 902;
        AUDIODEVICE . STATUSPARSING = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 903;
        LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , AUDIODEVICE . RXQUEUE )  ) ; 
        __context__.SourceCodeLine = 904;
        LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
        __context__.SourceCodeLine = 905;
        SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
        __context__.SourceCodeLine = 906;
        if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
            {
            __context__.SourceCodeLine = 907;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
            }
        
        __context__.SourceCodeLine = 908;
        if ( Functions.TestForTrue  ( ( Functions.Find( "Changes\u0022:[{" , LVRX ))  ) ) 
            { 
            __context__.SourceCodeLine = 910;
            LVTRASH  .UpdateValue ( Functions.Remove ( "Changes\u0022:[{" , LVRX )  ) ; 
            __context__.SourceCodeLine = 911;
            while ( Functions.TestForTrue  ( ( Functions.Find( "Component\u0022:\u0022" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 913;
                LVTRASH  .UpdateValue ( Functions.Remove ( "Component\u0022:\u0022" , LVRX )  ) ; 
                __context__.SourceCodeLine = 914;
                LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                __context__.SourceCodeLine = 915;
                LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                __context__.SourceCodeLine = 916;
                if ( Functions.TestForTrue  ( ( Functions.Find( ",{\u0022Component\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 918;
                    LVSTRING  .UpdateValue ( Functions.Remove ( ",{\u0022Component\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 919;
                    LVRX  .UpdateValue ( ",{\u0022Component\u0022:\u0022" + LVRX  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 922;
                    LVSTRING  .UpdateValue ( LVRX  ) ; 
                    }
                
                __context__.SourceCodeLine = 923;
                CHECKINSTANCETAGS (  __context__ , LVTAG, LVSTRING) ; 
                __context__.SourceCodeLine = 911;
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 926;
            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 928;
                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX )  ) ; 
                __context__.SourceCodeLine = 929;
                LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                __context__.SourceCodeLine = 930;
                LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                __context__.SourceCodeLine = 931;
                CHECKINSTANCETAGS (  __context__ , LVTAG, LVRX) ; 
                } 
            
            }
        
        __context__.SourceCodeLine = 900;
        } 
    
    __context__.SourceCodeLine = 934;
    AUDIODEVICE . STATUSPARSING = (ushort) ( 0 ) ; 
    
    }
    
private void PLAYTRACK (  SplusExecutionContext __context__, ushort LVPLAYER ) 
    { 
    CrestronString LVSTRING;
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    
    
    __context__.SourceCodeLine = 939;
    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVPLAYER ]  ) ; 
    __context__.SourceCodeLine = 940;
    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022play\u0022, \u0022Value\u0022: 1 }"  ) ; 
    __context__.SourceCodeLine = 941;
    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
    __context__.SourceCodeLine = 942;
    SETQUEUE (  __context__ , LVSTRING) ; 
    __context__.SourceCodeLine = 943;
    POLLPLAYER (  __context__ , (ushort)( LVPLAYER )) ; 
    
    }
    
private void STOPPLAYER (  SplusExecutionContext __context__, ushort LVPLAYER ) 
    { 
    CrestronString LVSTRING;
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    
    
    __context__.SourceCodeLine = 948;
    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVPLAYER ]  ) ; 
    __context__.SourceCodeLine = 949;
    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022stop\u0022, \u0022Value\u0022: 1 }"  ) ; 
    __context__.SourceCodeLine = 950;
    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
    __context__.SourceCodeLine = 951;
    SETQUEUE (  __context__ , LVSTRING) ; 
    
    }
    
private void SETPLAYERTRACK (  SplusExecutionContext __context__, ushort LVPLAYER , ushort LVTRACK ) 
    { 
    CrestronString LVSTRING;
    LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
    
    
    __context__.SourceCodeLine = 956;
    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVPLAYER ]  ) ; 
    __context__.SourceCodeLine = 957;
    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022filename\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . AUDIOTRACK [ LVTRACK ] + "\u0022 }"  ) ; 
    __context__.SourceCodeLine = 958;
    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
    __context__.SourceCodeLine = 959;
    SETQUEUE (  __context__ , LVSTRING) ; 
    __context__.SourceCodeLine = 960;
    POLLPLAYERFILE (  __context__ , (ushort)( LVPLAYER )) ; 
    __context__.SourceCodeLine = 961;
    PLAYTRACK (  __context__ , (ushort)( LVPLAYER )) ; 
    
    }
    
object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 970;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 971;
        CONNECT_FB  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 972;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        __context__.SourceCodeLine = 973;
        AUDIODEVICE . RECONNECTING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 974;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.LOGINPASSWORD ) > 3 ))  ) ) 
            { 
            __context__.SourceCodeLine = 976;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022:\u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022:\u0022Logon\u0022, \u0022params\u0022:{ \u0022User\u0022:\u0022" + AUDIODEVICE . LOGINNAME + "\u0022, "  ) ; 
            __context__.SourceCodeLine = 977;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022Password\u0022:\u0022" + AUDIODEVICE . LOGINPASSWORD + "\u0022 } }"  ) ; 
            __context__.SourceCodeLine = 978;
            SETQUEUE (  __context__ , LVSTRING) ; 
            } 
        
        __context__.SourceCodeLine = 980;
        CreateWait ( "__SPLS_TMPVAR__WAITLABEL_53__" , 200 , __SPLS_TMPVAR__WAITLABEL_53___Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public void __SPLS_TMPVAR__WAITLABEL_53___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 981;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 986;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 987;
        AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 988;
        AUDIODEVICE . STATUSPARSING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 989;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 990;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        __context__.SourceCodeLine = 991;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( AUDIODEVICE.STATUSCONNECTREQUEST ) && Functions.TestForTrue ( Functions.Not( AUDIODEVICE.RECONNECTING ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 993;
            AUDIODEVICE . RECONNECTING = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 994;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 995;
            while ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Not( AUDIODEVICE.STATUSCONNECTED ) ) && Functions.TestForTrue ( AUDIODEVICE.RECONNECTING )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 997;
                CreateWait ( "RECONNECTIP" , 3000 , RECONNECTIP_Callback ) ;
                __context__.SourceCodeLine = 995;
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public void RECONNECTIP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 998;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 1004;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 1022;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 1023;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 1024;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ) ) && Functions.TestForTrue ( Functions.Not( AUDIODEVICE.STATUSPARSING ) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 1025;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1029;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 1031;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1032;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1036;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 1037;
            AUDIODEVICE . RECONNECTING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 1038;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1043;
        SETQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLL) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1047;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 1048;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1052;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1056;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1060;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1064;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1068;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 1074;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1075;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 1076;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 1078;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 1079;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 1080;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 1082;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 1083;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1085;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 1086;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1089;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 1091;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 1093;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1100;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1101;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 1103;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1104;
            CreateWait ( "VOLUP" , 10 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 1101;
            } 
        
        __context__.SourceCodeLine = 1129;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 1107;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1109;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 1110;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 1111;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1112;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 1113;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1114;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 1115;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 1116;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1117;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1119;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1120;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + " }"  ) ; 
                            } 
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 1122;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1123;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1124;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 1125;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1133;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1134;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 1136;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1137;
            CreateWait ( "VOLDN" , 10 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 1134;
            } 
        
        __context__.SourceCodeLine = 1162;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 1140;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1142;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 1143;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 1144;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1145;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 1146;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 1147;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 1148;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 1149;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1150;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1152;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1153;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + " }"  ) ; 
                            } 
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 1155;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1156;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1157;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 1158;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1167;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1168;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1170;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1171;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1172;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1174;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1176;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1177;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1178;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1180;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1184;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1186;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1187;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1188;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                        __context__.SourceCodeLine = 1189;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1191;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1195;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1197;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                                __context__.SourceCodeLine = 1198;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022bypass\u0022, \u0022Value\u0022: 1 }"  ) ; 
                                __context__.SourceCodeLine = 1199;
                                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1202;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1203;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1208;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1209;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1211;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1212;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
            __context__.SourceCodeLine = 1213;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1215;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 1217;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1218;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1219;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1221;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1224;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1226;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1227;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1228;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                        __context__.SourceCodeLine = 1229;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1231;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1234;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1236;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                                __context__.SourceCodeLine = 1237;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022bypass\u0022, \u0022Value\u0022: 0 }"  ) ; 
                                __context__.SourceCodeLine = 1238;
                                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1240;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1241;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1246;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1247;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1249;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1251;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1252;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1253;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1255;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1257;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1258;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1259;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1261;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1264;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1266;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1267;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1268;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                            __context__.SourceCodeLine = 1269;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1271;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1274;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1276;
                                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                                    __context__.SourceCodeLine = 1277;
                                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022bypass\u0022, \u0022Value\u0022: 0 }"  ) ; 
                                    __context__.SourceCodeLine = 1278;
                                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1280;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1281;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1285;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1287;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1288;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1289;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1291;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1293;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1294;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1295;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1297;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1300;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1302;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1303;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1304;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                            __context__.SourceCodeLine = 1305;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1307;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 1310;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "DLAY" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 1312;
                                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                                    __context__.SourceCodeLine = 1313;
                                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022bypass\u0022, \u0022Value\u0022: 1 }"  ) ; 
                                    __context__.SourceCodeLine = 1314;
                                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1316;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1317;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVSTRINGVOL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1324;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1325;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1326;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1328;
            MakeString ( LVSTRINGVOL , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1329;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1331;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1332;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1333;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1335;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1337;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1338;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1339;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1341;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 1342;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1343;
            LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 1344;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
            __context__.SourceCodeLine = 1345;
            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + " }"  ) ; 
            __context__.SourceCodeLine = 1346;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1347;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1348;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        CrestronString LVWALLCONFIG;
        CrestronString LVNAME;
        CrestronString LVSTRINGWALL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVWALLCONFIG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVSTRINGWALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1355;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1356;
        LVNAME  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
        __context__.SourceCodeLine = 1357;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1359;
            if ( Functions.TestForTrue  ( ( Functions.Not( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ))  ) ) 
                { 
                __context__.SourceCodeLine = 1361;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1362;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 0 },"  ) ; 
                __context__.SourceCodeLine = 1363;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022String\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1364;
                LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1365;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1366;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1371;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)64; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 1373;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1375;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1377;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1379;
                                LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1371;
                    } 
                
                __context__.SourceCodeLine = 1384;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)64; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 1386;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1388;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1390;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1392;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ]  ) ; 
                                __context__.SourceCodeLine = 1393;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                                __context__.SourceCodeLine = 1394;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                                __context__.SourceCodeLine = 1395;
                                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ) + "\u0022 } ] } }"  ) ; 
                                __context__.SourceCodeLine = 1396;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1397;
                                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1384;
                    } 
                
                __context__.SourceCodeLine = 1402;
                LVWALLCONFIG  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 1403;
                AUDIODEVICE . STATUSGROUP [ AUDIODEVICE.LASTINDEX] = (ushort) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
                __context__.SourceCodeLine = 1404;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)64; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 1406;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1408;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1410;
                            LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1404;
                    } 
                
                __context__.SourceCodeLine = 1414;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1415;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                __context__.SourceCodeLine = 1416;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1417;
                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1418;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1419;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1423;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1425;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1426;
                if ( Functions.TestForTrue  ( ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1428;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: "  ) ; 
                    __context__.SourceCodeLine = 1429;
                    LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + " }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1432;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    }
                
                __context__.SourceCodeLine = 1433;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1434;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1435;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1437;
                if ( Functions.TestForTrue  ( ( Functions.Find( "PRST" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1439;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1440;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022load." + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1441;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    __context__.SourceCodeLine = 1442;
                    SETQUEUE (  __context__ , LVSTRING) ; 
                    __context__.SourceCodeLine = 1443;
                    POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1445;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1453;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1464;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1465;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1470;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1471;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1472;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1473;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1474;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1476;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1477;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1478;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1479;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1484;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1485;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1486;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1487;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1488;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1489;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1494;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1495;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1496;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1497;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1498;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1499;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1504;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1505;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1506;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1507;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1508;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1510;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1511;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1512;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1513;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1518;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1519;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1520;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1521;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1522;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1523;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1528;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1529;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1530;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1531;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1532;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1533;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1538;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1539;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1540;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1541;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1542;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1543;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1548;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1549;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1550;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1551;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1552;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1553;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1558;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1559;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1560;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022hook.flash\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1561;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1562;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1563;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1571;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1572;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.STATUSVOIPRECENTCALL[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1574;
            VOIP_DIALSTRING [ AUDIODEVICE.LASTINDEX]  .UpdateValue ( AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1575;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1576;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1577;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1578;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1579;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1580;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1581;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1582;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1583;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1589;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1593;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1597;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1601;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1605;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1609;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1613;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1617;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1621;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1625;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1629;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1633;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1641;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1642;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1644;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1645;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1646;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1647;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1648;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1649;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1650;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1651;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1652;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1658;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1659;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1660;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1661;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1662;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1663;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1667;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1668;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TRACK__POUND___PLAYER_OnChange_49 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVPLAYER = 0;
        
        
        __context__.SourceCodeLine = 1676;
        LVPLAYER = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1677;
        if ( Functions.TestForTrue  ( ( Functions.Not( TRACK__POUND___PLAYER[ LVPLAYER ] .UshortValue ))  ) ) 
            {
            __context__.SourceCodeLine = 1678;
            STOPPLAYER (  __context__ , (ushort)( LVPLAYER )) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1680;
            SETPLAYERTRACK (  __context__ , (ushort)( LVPLAYER ), (ushort)( TRACK__POUND___PLAYER[ LVPLAYER ] .UshortValue )) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1688;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1689;
        AUDIODEVICE . IPPORT = (ushort) ( 1710 ) ; 
        __context__.SourceCodeLine = 1690;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1691;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1692;
        AUDIODEVICE . ETX  .UpdateValue ( "\u0000"  ) ; 
        __context__.SourceCodeLine = 1693;
        AUDIODEVICE . COMMANDPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022StatusGet\u0022, \u0022params\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1694;
        AUDIODEVICE . COMMANDAUTOPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022id\u0022: 1234, \u0022method\u0022: \u0022ChangeGroup.AutoPoll\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022,\u0022Rate\u0022: 5 } }"  ) ; 
        __context__.SourceCodeLine = 1695;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1697;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 100 ) ) ) ; 
            __context__.SourceCodeLine = 1698;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 20 ) ; 
            __context__.SourceCodeLine = 1699;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1695;
            } 
        
        __context__.SourceCodeLine = 1701;
        GVBARGRAPHMAX = (ushort) ( 65535 ) ; 
        __context__.SourceCodeLine = 1702;
        GVVOLUMERANGE = (ushort) ( 110 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 65534, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    STATUS_PLAYER = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        STATUS_PLAYER[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( STATUS_PLAYER__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( STATUS_PLAYER__DigitalOutput__ + i, STATUS_PLAYER[i+1] );
    }
    
    USED_FILENAME = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        USED_FILENAME[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( USED_FILENAME__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( USED_FILENAME__DigitalOutput__ + i, USED_FILENAME[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    TRACK__POUND___PLAYER = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        TRACK__POUND___PLAYER[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( TRACK__POUND___PLAYER__AnalogSerialInput__ + i, TRACK__POUND___PLAYER__AnalogSerialInput__, this );
        m_AnalogInputList.Add( TRACK__POUND___PLAYER__AnalogSerialInput__ + i, TRACK__POUND___PLAYER[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 1023, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    FILENAME_PLAYER = new InOutArray<StringOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        FILENAME_PLAYER[i+1] = new Crestron.Logos.SplusObjects.StringOutput( FILENAME_PLAYER__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( FILENAME_PLAYER__AnalogSerialOutput__ + i, FILENAME_PLAYER[i+1] );
    }
    
    AUDIO_FILENAME = new InOutArray<StringOutput>( 32, this );
    for( uint i = 0; i < 32; i++ )
    {
        AUDIO_FILENAME[i+1] = new Crestron.Logos.SplusObjects.StringOutput( AUDIO_FILENAME__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( AUDIO_FILENAME__AnalogSerialOutput__ + i, AUDIO_FILENAME[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_51___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_51___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_52___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_52___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_53___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_53___CallbackFn );
    RECONNECTIP_Callback = new WaitFunction( RECONNECTIP_CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    for( uint i = 0; i < 64; i++ )
        TRACK__POUND___PLAYER[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( TRACK__POUND___PLAYER_OnChange_49, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_QSYS_V0_8 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_51___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_52___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_53___Callback;
private WaitFunction RECONNECTIP_Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint TRACK__POUND___PLAYER__AnalogSerialInput__ = 200;
const uint INSTANCETAGS__AnalogSerialInput__ = 264;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 328;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint STATUS_PLAYER__DigitalOutput__ = 73;
const uint USED_FILENAME__DigitalOutput__ = 137;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;
const uint FILENAME_PLAYER__AnalogSerialOutput__ = 136;
const uint AUDIO_FILENAME__AnalogSerialOutput__ = 200;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  RECONNECTING = 0;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  STATUSPARSING = 0;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [] STATUSWALL;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSGROUP;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(9, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(10, false, false)]
    public ushort  [] STATUSVOIPCALLSTATE;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  [] STATUSVOIPRECENTCALL;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(13, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(14, false, false)]
    public CrestronString  COMMANDAUTOPOLL;
    
    [SplusStructAttribute(15, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(16, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(17, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(18, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(19, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(20, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(21, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(22, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(23, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(25, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(26, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(27, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(28, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(29, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(30, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(31, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(32, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(33, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(34, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(35, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(36, false, false)]
    public CrestronString  RXQUEUE;
    
    [SplusStructAttribute(37, false, false)]
    public CrestronString  [] AUDIOTRACK;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSWALL  = new ushort[ 65 ];
        STATUSGROUP  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPCALLSTATE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMMANDAUTOPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 65534, Owner );
        STATUSVOIPRECENTCALL  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            STATUSVOIPRECENTCALL [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        AUDIOTRACK  = new CrestronString[ 17 ];
        for( uint i = 0; i < 17; i++ )
            AUDIOTRACK [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 123, Owner );
        
        
    }
    
}

}
