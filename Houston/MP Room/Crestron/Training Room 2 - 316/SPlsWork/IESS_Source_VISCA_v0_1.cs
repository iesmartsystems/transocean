using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_SOURCE_VISCA_V0_1
{
    public class UserModuleClass_IESS_SOURCE_VISCA_V0_1 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput PAN_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput PAN_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput TILT_UP;
        Crestron.Logos.SplusObjects.DigitalInput TILT_DOWN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_IN;
        Crestron.Logos.SplusObjects.DigitalInput ZOOM_OUT;
        Crestron.Logos.SplusObjects.DigitalInput POWER_ON;
        Crestron.Logos.SplusObjects.DigitalInput POWER_OFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> PRESET;
        Crestron.Logos.SplusObjects.StringInput DEVICE_RX__DOLLAR__;
        Crestron.Logos.SplusObjects.StringInput CAMERA_ID__DOLLAR__;
        Crestron.Logos.SplusObjects.StringOutput DEVICE_TX__DOLLAR__;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> PRESET_SELECTED;
        SSOURCE SOURCEDEV;
        CrestronString GLBL_STRING;
        ushort GLBL_INDEX = 0;
        private void BUILDSTRING (  SplusExecutionContext __context__, CrestronString LVINCOMING ) 
            { 
            
            __context__.SourceCodeLine = 102;
            DEVICE_TX__DOLLAR__  .UpdateValue ( SOURCEDEV . STX + LVINCOMING + SOURCEDEV . ETX  ) ; 
            
            }
            
        object PAN_LEFT_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort LVCOUNTER = 0;
                
                CrestronString LVSTRING;
                LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
                
                
                __context__.SourceCodeLine = 112;
                LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0001\u0003"  ) ; 
                __context__.SourceCodeLine = 113;
                BUILDSTRING (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 114;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)4; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    {
                    __context__.SourceCodeLine = 115;
                    PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 114;
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object PAN_LEFT_OnRelease_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 120;
            LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0003"  ) ; 
            __context__.SourceCodeLine = 121;
            BUILDSTRING (  __context__ , LVSTRING) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object PAN_RIGHT_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 128;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0002\u0003"  ) ; 
        __context__.SourceCodeLine = 129;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 130;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 131;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 130;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PAN_RIGHT_OnRelease_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 136;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 137;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 144;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0001"  ) ; 
        __context__.SourceCodeLine = 145;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 146;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 147;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 146;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_UP_OnRelease_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 152;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 153;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 160;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0002"  ) ; 
        __context__.SourceCodeLine = 161;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 162;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 163;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 162;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TILT_DOWN_OnRelease_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 168;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDMOVE + "\u0003\u0003"  ) ; 
        __context__.SourceCodeLine = 169;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 176;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDZOOM + "\u0024"  ) ; 
        __context__.SourceCodeLine = 177;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 178;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 179;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 178;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_IN_OnRelease_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 184;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDZOOM + "\u0000"  ) ; 
        __context__.SourceCodeLine = 185;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 192;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDZOOM + "\u0034"  ) ; 
        __context__.SourceCodeLine = 193;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 194;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 195;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 194;
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object ZOOM_OUT_OnRelease_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 200;
        LVSTRING  .UpdateValue ( SOURCEDEV . COMMANDZOOM + "\u0000"  ) ; 
        __context__.SourceCodeLine = 201;
        BUILDSTRING (  __context__ , LVSTRING) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object PRESET_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 206;
        GLBL_INDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 207;
        CreateWait ( "PRESETS" , 200 , PRESETS_Callback ) ;
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void PRESETS_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            __context__.SourceCodeLine = 209;
            if ( Functions.TestForTrue  ( ( PRESET[ GLBL_INDEX ] .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 211;
                GLBL_STRING  .UpdateValue ( SOURCEDEV . COMMANDPRESETSAVE + Functions.Chr (  (int) ( (GLBL_INDEX - 1) ) )  ) ; 
                __context__.SourceCodeLine = 212;
                BUILDSTRING (  __context__ , GLBL_STRING) ; 
                } 
            
            __context__.SourceCodeLine = 214;
            CancelWait ( "PRESETS" ) ; 
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object PRESET_OnRelease_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        
        
        __context__.SourceCodeLine = 220;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 221;
        GLBL_STRING  .UpdateValue ( SOURCEDEV . COMMANDPRESET + Functions.Chr (  (int) ( (LVINDEX - 1) ) )  ) ; 
        __context__.SourceCodeLine = 222;
        BUILDSTRING (  __context__ , GLBL_STRING) ; 
        __context__.SourceCodeLine = 223;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)4; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            {
            __context__.SourceCodeLine = 224;
            PRESET_SELECTED [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 223;
            }
        
        __context__.SourceCodeLine = 225;
        PRESET_SELECTED [ LVINDEX]  .Value = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DEVICE_RX__DOLLAR___OnChange_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object CAMERA_ID__DOLLAR___OnChange_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 232;
        SOURCEDEV . STX  .UpdateValue ( CAMERA_ID__DOLLAR__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWER_ON_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 236;
        BUILDSTRING (  __context__ , SOURCEDEV.POWERON) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POWER_OFF_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 240;
        BUILDSTRING (  __context__ , SOURCEDEV.POWEROFF) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 249;
        SOURCEDEV . STX  .UpdateValue ( "\u0081"  ) ; 
        __context__.SourceCodeLine = 250;
        SOURCEDEV . ETX  .UpdateValue ( "\u00FF"  ) ; 
        __context__.SourceCodeLine = 251;
        SOURCEDEV . COMMANDMOVE  .UpdateValue ( "\u0001\u0006\u0001\u0014\u0008"  ) ; 
        __context__.SourceCodeLine = 252;
        SOURCEDEV . COMMANDZOOM  .UpdateValue ( "\u0001\u0004\u0007"  ) ; 
        __context__.SourceCodeLine = 253;
        SOURCEDEV . COMMANDPRESET  .UpdateValue ( "\u0001\u0004\u003F\u0002"  ) ; 
        __context__.SourceCodeLine = 254;
        SOURCEDEV . COMMANDPRESETSAVE  .UpdateValue ( "\u0001\u0004\u003F\u0001"  ) ; 
        __context__.SourceCodeLine = 255;
        SOURCEDEV . POWERON  .UpdateValue ( "\u0001\u0004\u0000\u0002"  ) ; 
        __context__.SourceCodeLine = 256;
        SOURCEDEV . POWEROFF  .UpdateValue ( "\u0001\u0004\u0000\u0003"  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GLBL_STRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
    SOURCEDEV  = new SSOURCE( this, true );
    SOURCEDEV .PopulateCustomAttributeList( false );
    
    PAN_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_LEFT__DigitalInput__, PAN_LEFT );
    
    PAN_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( PAN_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( PAN_RIGHT__DigitalInput__, PAN_RIGHT );
    
    TILT_UP = new Crestron.Logos.SplusObjects.DigitalInput( TILT_UP__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_UP__DigitalInput__, TILT_UP );
    
    TILT_DOWN = new Crestron.Logos.SplusObjects.DigitalInput( TILT_DOWN__DigitalInput__, this );
    m_DigitalInputList.Add( TILT_DOWN__DigitalInput__, TILT_DOWN );
    
    ZOOM_IN = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_IN__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_IN__DigitalInput__, ZOOM_IN );
    
    ZOOM_OUT = new Crestron.Logos.SplusObjects.DigitalInput( ZOOM_OUT__DigitalInput__, this );
    m_DigitalInputList.Add( ZOOM_OUT__DigitalInput__, ZOOM_OUT );
    
    POWER_ON = new Crestron.Logos.SplusObjects.DigitalInput( POWER_ON__DigitalInput__, this );
    m_DigitalInputList.Add( POWER_ON__DigitalInput__, POWER_ON );
    
    POWER_OFF = new Crestron.Logos.SplusObjects.DigitalInput( POWER_OFF__DigitalInput__, this );
    m_DigitalInputList.Add( POWER_OFF__DigitalInput__, POWER_OFF );
    
    PRESET = new InOutArray<DigitalInput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        PRESET[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( PRESET__DigitalInput__ + i, PRESET__DigitalInput__, this );
        m_DigitalInputList.Add( PRESET__DigitalInput__ + i, PRESET[i+1] );
    }
    
    PRESET_SELECTED = new InOutArray<DigitalOutput>( 4, this );
    for( uint i = 0; i < 4; i++ )
    {
        PRESET_SELECTED[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( PRESET_SELECTED__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( PRESET_SELECTED__DigitalOutput__ + i, PRESET_SELECTED[i+1] );
    }
    
    DEVICE_RX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( DEVICE_RX__DOLLAR____AnalogSerialInput__, 100, this );
    m_StringInputList.Add( DEVICE_RX__DOLLAR____AnalogSerialInput__, DEVICE_RX__DOLLAR__ );
    
    CAMERA_ID__DOLLAR__ = new Crestron.Logos.SplusObjects.StringInput( CAMERA_ID__DOLLAR____AnalogSerialInput__, 10, this );
    m_StringInputList.Add( CAMERA_ID__DOLLAR____AnalogSerialInput__, CAMERA_ID__DOLLAR__ );
    
    DEVICE_TX__DOLLAR__ = new Crestron.Logos.SplusObjects.StringOutput( DEVICE_TX__DOLLAR____AnalogSerialOutput__, this );
    m_StringOutputList.Add( DEVICE_TX__DOLLAR____AnalogSerialOutput__, DEVICE_TX__DOLLAR__ );
    
    PRESETS_Callback = new WaitFunction( PRESETS_CallbackFn );
    
    PAN_LEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnPush_0, false ) );
    PAN_LEFT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_LEFT_OnRelease_1, false ) );
    PAN_RIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnPush_2, false ) );
    PAN_RIGHT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( PAN_RIGHT_OnRelease_3, false ) );
    TILT_UP.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_UP_OnPush_4, false ) );
    TILT_UP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_UP_OnRelease_5, false ) );
    TILT_DOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnPush_6, false ) );
    TILT_DOWN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( TILT_DOWN_OnRelease_7, false ) );
    ZOOM_IN.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnPush_8, false ) );
    ZOOM_IN.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_IN_OnRelease_9, false ) );
    ZOOM_OUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnPush_10, false ) );
    ZOOM_OUT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ZOOM_OUT_OnRelease_11, false ) );
    for( uint i = 0; i < 4; i++ )
        PRESET[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( PRESET_OnPush_12, false ) );
        
    for( uint i = 0; i < 4; i++ )
        PRESET[i+1].OnDigitalRelease.Add( new InputChangeHandlerWrapper( PRESET_OnRelease_13, false ) );
        
    DEVICE_RX__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( DEVICE_RX__DOLLAR___OnChange_14, false ) );
    CAMERA_ID__DOLLAR__.OnSerialChange.Add( new InputChangeHandlerWrapper( CAMERA_ID__DOLLAR___OnChange_15, false ) );
    POWER_ON.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWER_ON_OnPush_16, false ) );
    POWER_OFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( POWER_OFF_OnPush_17, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_SOURCE_VISCA_V0_1 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction PRESETS_Callback;


const uint PAN_LEFT__DigitalInput__ = 0;
const uint PAN_RIGHT__DigitalInput__ = 1;
const uint TILT_UP__DigitalInput__ = 2;
const uint TILT_DOWN__DigitalInput__ = 3;
const uint ZOOM_IN__DigitalInput__ = 4;
const uint ZOOM_OUT__DigitalInput__ = 5;
const uint POWER_ON__DigitalInput__ = 6;
const uint POWER_OFF__DigitalInput__ = 7;
const uint PRESET__DigitalInput__ = 8;
const uint DEVICE_RX__DOLLAR____AnalogSerialInput__ = 0;
const uint CAMERA_ID__DOLLAR____AnalogSerialInput__ = 1;
const uint DEVICE_TX__DOLLAR____AnalogSerialOutput__ = 0;
const uint PRESET_SELECTED__DigitalOutput__ = 0;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SSOURCE : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public CrestronString  COMMANDMOVE;
    
    [SplusStructAttribute(1, false, false)]
    public CrestronString  COMMANDZOOM;
    
    [SplusStructAttribute(2, false, false)]
    public CrestronString  COMMANDPRESET;
    
    [SplusStructAttribute(3, false, false)]
    public CrestronString  COMMANDPRESETSAVE;
    
    [SplusStructAttribute(4, false, false)]
    public CrestronString  POWERON;
    
    [SplusStructAttribute(5, false, false)]
    public CrestronString  POWEROFF;
    
    [SplusStructAttribute(6, false, false)]
    public CrestronString  STX;
    
    [SplusStructAttribute(7, false, false)]
    public CrestronString  ETX;
    
    
    public SSOURCE( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        COMMANDMOVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        COMMANDZOOM  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        COMMANDPRESET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        COMMANDPRESETSAVE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        POWERON  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        POWEROFF  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, Owner );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, Owner );
        
        
    }
    
}

}
