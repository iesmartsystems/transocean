/*
Dealer Name: i.e.SmartSytems
System Name: IESS TCP Client v0.2
System Number:
Programmer: Matthew Laletas
Comments: IESS TCP Client v0.2
*/
/*****************************************************************************************************************************
    i.e.SmartSystems LLC CONFIDENTIAL
    Copyright (c) 2011-2016 i.e.SmartSystems LLC, All Rights Reserved.
    NOTICE:  All information contained herein is, and remains the property of i.e.SmartSystems. The intellectual and 
    technical concepts contained herein are proprietary to i.e.SmartSystems and may be covered by U.S. and Foreign Patents, 
    patents in process, and are protected by trade secret or copyright law. Dissemination of this information or 
    reproduction of this material is strictly forbidden unless prior written permission is obtained from i.e.SmartSystems.  
    Access to the source code contained herein is hereby forbidden to anyone except current i.e.SmartSystems employees, 
    managers or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
    The copyright notice above does not evidence any actual or intended publication or disclosure of this source code, 
    which includes information that is confidential and/or proprietary, and is a trade secret, of i.e.SmartSystems.   
    ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS SOURCE 
    CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF i.e.SmartSystems IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
    LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY 
    OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  
    MAY DESCRIBE, IN WHOLE OR IN PART.
*****************************************************************************************************************************/
/*******************************************************************************************
  Compiler Directives
*******************************************************************************************/
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_DYNAMIC
#ENABLE_TRACE
#HELP "IESS - TCP Client v0.1"
#HELP ""
#HELP "INPUTS:"
#HELP "Working on it"
#HELP ""
#HELP "OUTPUTS:"
#HELP "Working on it"
/*******************************************************************************************
  Include Libraries
*******************************************************************************************/
/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
*******************************************************************************************/
DIGITAL_INPUT Connect, _SKIP_;
ANALOG_INPUT IP_Port;
STRING_INPUT IP_Address[31], _SKIP_;
STRING_INPUT TX$[511];
DIGITAL_OUTPUT Connect_FB;
ANALOG_OUTPUT Connect_Status_FB, _SKIP_;
STRING_OUTPUT RX$;
/*******************************************************************************************
  SOCKETS
*******************************************************************************************/
TCP_CLIENT tcpClient[2047];
/*******************************************************************************************
  Parameters
*******************************************************************************************/
/*******************************************************************************************
  Parameter Properties
*******************************************************************************************/
/*******************************************************************************************
  Structure Definitions
*******************************************************************************************/
STRUCTURE sTCPClient
{
	INTEGER StatusConnectRequest;
	INTEGER StatusConnected;
	STRING IPAddress[31];
	INTEGER IPPort;
};
sTCPClient GLBL_TCPClient;
/*******************************************************************************************
  Global Variables
*******************************************************************************************/
/*******************************************************************************************
  Functions
*******************************************************************************************/
FUNCTION ConnectDisconnect( INTEGER lvConnect )
{
	SIGNED_INTEGER lvStatus;
	IF( !lvConnect )
	{
		IF( GLBL_TCPCLient.StatusConnected )
			lvStatus = SOCKETDISCONNECTCLIENT( tcpClient );
	}
	ELSE IF( lvConnect )
	{
		IF( !GLBL_TCPCLient.StatusConnected )
		{
			IF( LEN( GLBL_TCPClient.IPAddress ) > 4 && GLBL_TCPClient.IPPort > 0 )
			{
				IF( GLBL_TCPClient.StatusConnectRequest )
					lvStatus = SOCKETCONNECTCLIENT( tcpClient, GLBL_TCPClient.IPAddress, GLBL_TCPClient.IPPort, 1 );
			}
		}
	}
}			
/*******************************************************************************************
  Event Handlers
*******************************************************************************************/
//Connected
SOCKETCONNECT tcpClient
{
	GLBL_TCPClient.StatusConnected = 1;
	Connect_FB = 1;
	Connect_Status_FB = tcpClient.SocketStatus;
}
//Disconnected
SOCKETDISCONNECT tcpClient
{
	GLBL_TCPClient.StatusConnected = 0;
	Connect_FB = 0;
	Connect_Status_FB = tcpClient.SocketStatus;
}
SOCKETSTATUS tcpClient
{ 
	Connect_Status_FB = SOCKETGETSTATUS();
	IF( Connect_Status_FB = 2 )
	{
		GLBL_TCPClient.StatusConnected = 1;
		Connect_FB = 1;
    }
	ELSE
	{
		GLBL_TCPClient.StatusConnected = 0;
		Connect_FB = 0;
	}
}
//Feedback
SOCKETRECEIVE tcpClient
{
	RX$ = tcpClient.SocketRxBuf;
	CLEARBUFFER( tcpClient.SocketRxBuf );
} 
CHANGE Connect
{
	IF( Connect )
	{	
		GLBL_TCPClient.StatusConnectRequest = 1;
		ConnectDisconnect( 1 );
	}
	ELSE
	{
		GLBL_TCPClient.StatusConnectRequest = 0;
		ConnectDisconnect( 0 );
	}					
}
CHANGE IP_Address
{
	GLBL_TCPClient.IPAddress = IP_Address;
}

CHANGE IP_Port
{
	GLBL_TCPClient.IPPort = IP_Port;
}

CHANGE TX$
{
	IF( GLBL_TCPClient.StatusConnected )
		SOCKETSEND( tcpClient, TX$ );
}
/*******************************************************************************************
  Main()
*******************************************************************************************/