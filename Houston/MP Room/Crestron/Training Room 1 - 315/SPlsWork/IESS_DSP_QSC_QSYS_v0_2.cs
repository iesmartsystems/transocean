using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_IESS_DSP_QSC_QSYS_V0_2
{
    public class UserModuleClass_IESS_DSP_QSC_QSYS_V0_2 : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput CONNECT;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        Crestron.Logos.SplusObjects.DigitalInput DEBUG;
        Crestron.Logos.SplusObjects.AnalogInput IP_PORT;
        Crestron.Logos.SplusObjects.StringInput IP_ADDRESS;
        Crestron.Logos.SplusObjects.StringInput LOGIN_NAME;
        Crestron.Logos.SplusObjects.StringInput LOGIN_PASSWORD;
        Crestron.Logos.SplusObjects.StringInput RX;
        Crestron.Logos.SplusObjects.StringInput MANUALCMD;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEUP;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEDOWN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTETOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOLUMEMUTEOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_0;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_1;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_2;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_3;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_4;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_5;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_6;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_7;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_8;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_9;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_STAR;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_NUM_POUND;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_BACKSPACE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWERON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_AUTOANSWEROFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDTOGGLE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDON;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DNDOFF;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DIAL;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_END;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_ACCEPT;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_DECLINE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_JOIN;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_CONFERENCE;
        InOutArray<Crestron.Logos.SplusObjects.DigitalInput> VOIP_REDIAL;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESET;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOLUMESTEP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> GROUP;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> VOIP_CALLAPPEARANCE;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> INSTANCETAGS;
        InOutArray<Crestron.Logos.SplusObjects.StringInput> VOIP_DIALENTRY;
        Crestron.Logos.SplusObjects.DigitalOutput CONNECT_FB;
        Crestron.Logos.SplusObjects.AnalogOutput CONNECT_STATUS_FB;
        Crestron.Logos.SplusObjects.StringOutput TX;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOLUMEMUTE_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLSTATUS_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_CALLINCOMING_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_AUTOANSWER_FB;
        InOutArray<Crestron.Logos.SplusObjects.DigitalOutput> VOIP_DND_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> VOLUMELEVEL_FB;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> GROUP_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_DIALSTRING;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNAME_FB;
        InOutArray<Crestron.Logos.SplusObjects.StringOutput> VOIP_CALLINCOMINGNUM;
        SplusTcpClient AUDIOCLIENT;
        SAUDIO AUDIODEVICE;
        ushort GVVOIPCOUNTER = 0;
        private void CONNECTDISCONNECT (  SplusExecutionContext __context__, ushort LVCONNECT ) 
            { 
            short LVSTATUS = 0;
            
            
            __context__.SourceCodeLine = 120;
            if ( Functions.TestForTrue  ( ( Functions.Not( LVCONNECT ))  ) ) 
                { 
                __context__.SourceCodeLine = 122;
                LVSTATUS = (short) ( Functions.SocketDisconnectClient( AUDIOCLIENT ) ) ; 
                __context__.SourceCodeLine = 123;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 124;
                AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 126;
                if ( Functions.TestForTrue  ( ( LVCONNECT)  ) ) 
                    { 
                    __context__.SourceCodeLine = 128;
                    LVSTATUS = (short) ( Functions.SocketConnectClient( AUDIOCLIENT , AUDIODEVICE.IPADDRESS , (ushort)( AUDIODEVICE.IPPORT ) , (ushort)( 1 ) ) ) ; 
                    } 
                
                }
            
            
            }
            
        private short VOLUMECONVERTER (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , ushort LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            
            uint LVVOLUMEMULTIPLIER = 0;
            
            short LVVOLUMELEVEL = 0;
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 138;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 140;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 142;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 143;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 145;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 146;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 148;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 149;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 152;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 154;
            LVVOLUMEMULTIPLIER = (uint) ( ((LVVOLUMEINCOMING * 100) / LVBARGRAPHMAX) ) ; 
            __context__.SourceCodeLine = 155;
            LVVOLUMEMULTIPLIER = (uint) ( (LVVOLUMEMULTIPLIER * LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 156;
            LVVOLUMELEVEL = (short) ( (LVVOLUMEMULTIPLIER / 100) ) ; 
            __context__.SourceCodeLine = 158;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 159;
                LVVOLUMELEVEL = (short) ( (LVVOLUMELEVEL + LVMINIMUM) ) ; 
                }
            
            __context__.SourceCodeLine = 161;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < LVMINIMUM ))  ) ) 
                {
                __context__.SourceCodeLine = 162;
                LVVOLUMELEVEL = (short) ( LVMINIMUM ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 163;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVMAXIMUM ))  ) ) 
                    {
                    __context__.SourceCodeLine = 164;
                    LVVOLUMELEVEL = (short) ( LVMAXIMUM ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 166;
            return (short)( LVVOLUMELEVEL) ; 
            
            }
            
        private uint VOLUMECONVERTERREVERSE (  SplusExecutionContext __context__, short LVMINIMUM , short LVMAXIMUM , short LVVOLUMEINCOMING ) 
            { 
            ushort LVVOLUMERANGE = 0;
            ushort LVBARGRAPHMAX = 0;
            ushort LVINC = 0;
            ushort LVMULT = 0;
            
            uint LVVOLUMELEVEL = 0;
            
            short LVFMIN = 0;
            short LVFMAX = 0;
            
            
            __context__.SourceCodeLine = 174;
            LVBARGRAPHMAX = (ushort) ( 65535 ) ; 
            __context__.SourceCodeLine = 176;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMINIMUM < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 178;
                LVFMIN = (short) ( (LVMINIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                __context__.SourceCodeLine = 179;
                LVINC = (ushort) ( (LVVOLUMEINCOMING + LVFMIN) ) ; 
                __context__.SourceCodeLine = 180;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM < 0 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 182;
                    LVFMAX = (short) ( (LVMAXIMUM * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                    __context__.SourceCodeLine = 183;
                    LVVOLUMERANGE = (ushort) ( (LVFMIN - LVFMAX) ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 185;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVMAXIMUM >= 0 ))  ) ) 
                        {
                        __context__.SourceCodeLine = 186;
                        LVVOLUMERANGE = (ushort) ( (LVFMIN + LVMAXIMUM) ) ; 
                        }
                    
                    }
                
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 190;
                LVVOLUMERANGE = (ushort) ( (LVMINIMUM + LVMAXIMUM) ) ; 
                __context__.SourceCodeLine = 191;
                LVINC = (ushort) ( LVVOLUMEINCOMING ) ; 
                } 
            
            __context__.SourceCodeLine = 194;
            LVMULT = (ushort) ( (LVBARGRAPHMAX / LVVOLUMERANGE) ) ; 
            __context__.SourceCodeLine = 195;
            LVVOLUMELEVEL = (uint) ( (LVINC * LVMULT) ) ; 
            __context__.SourceCodeLine = 197;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 198;
                LVVOLUMELEVEL = (uint) ( 0 ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 199;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LVVOLUMELEVEL > LVBARGRAPHMAX ))  ) ) 
                    {
                    __context__.SourceCodeLine = 200;
                    LVVOLUMELEVEL = (uint) ( LVBARGRAPHMAX ) ; 
                    }
                
                }
            
            __context__.SourceCodeLine = 201;
            return (uint)( LVVOLUMELEVEL) ; 
            
            }
            
        private void SETDEBUG (  SplusExecutionContext __context__, CrestronString LVSTRING , ushort LVTYPE ) 
            { 
            
            __context__.SourceCodeLine = 205;
            if ( Functions.TestForTrue  ( ( DEBUG  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 207;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVTYPE == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 208;
                    Trace( "QSYS RX: {0}", LVSTRING ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 210;
                    Trace( "QSYS TX: {0}", LVSTRING ) ; 
                    }
                
                } 
            
            
            }
            
        private void SETQUEUE (  SplusExecutionContext __context__, CrestronString LVSTRING ) 
            { 
            CrestronString LVTEMP;
            CrestronString LVTRASH;
            LVTEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
            
            ushort LVINDEX = 0;
            ushort LVINDEX2ND = 0;
            
            
            __context__.SourceCodeLine = 217;
            AUDIODEVICE . TXQUEUE  .UpdateValue ( AUDIODEVICE . TXQUEUE + LVSTRING + "\u000B\u000B"  ) ; 
            __context__.SourceCodeLine = 218;
            while ( Functions.TestForTrue  ( ( Functions.Find( "\u000B\u000B" , AUDIODEVICE.TXQUEUE ))  ) ) 
                { 
                __context__.SourceCodeLine = 220;
                LVTEMP  .UpdateValue ( Functions.Remove ( "\u000B\u000B" , AUDIODEVICE . TXQUEUE )  ) ; 
                __context__.SourceCodeLine = 221;
                LVTEMP  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTEMP ) - 2), LVTEMP )  ) ; 
                __context__.SourceCodeLine = 222;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( LVTEMP ) > 1 ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 224;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "Poll " , LVTEMP ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 226;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "Poll " , LVTEMP )  ) ; 
                        __context__.SourceCodeLine = 227;
                        LVTRASH  .UpdateValue ( Functions.Remove ( " " , LVTEMP )  ) ; 
                        __context__.SourceCodeLine = 228;
                        LVINDEX = (ushort) ( Functions.Atoi( LVTRASH ) ) ; 
                        __context__.SourceCodeLine = 229;
                        AUDIODEVICE . LASTPOLLINDEX = (ushort) ( LVINDEX ) ; 
                        __context__.SourceCodeLine = 230;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "2nd " , LVTEMP ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 232;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "2nd " , LVTEMP )  ) ; 
                            __context__.SourceCodeLine = 233;
                            LVTRASH  .UpdateValue ( Functions.Remove ( " " , LVTEMP )  ) ; 
                            __context__.SourceCodeLine = 234;
                            LVINDEX2ND = (ushort) ( Functions.Atoi( LVTRASH ) ) ; 
                            __context__.SourceCodeLine = 235;
                            AUDIODEVICE . LASTPOLLSECONDINDEX = (ushort) ( LVINDEX2ND ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 238;
                    LVTEMP  .UpdateValue ( LVTEMP + AUDIODEVICE . ETX  ) ; 
                    __context__.SourceCodeLine = 239;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.IPPORT == 1710))  ) ) 
                        { 
                        __context__.SourceCodeLine = 241;
                        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCONNECTED)  ) ) 
                            {
                            __context__.SourceCodeLine = 242;
                            Functions.SocketSend ( AUDIOCLIENT , LVTEMP ) ; 
                            }
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 245;
                        TX  .UpdateValue ( LVTEMP  ) ; 
                        }
                    
                    __context__.SourceCodeLine = 246;
                    SETDEBUG (  __context__ , LVTEMP, (ushort)( 0 )) ; 
                    } 
                
                __context__.SourceCodeLine = 218;
                } 
            
            
            }
            
        private void VOIP_NUM (  SplusExecutionContext __context__, ushort LVINDEX , CrestronString LVNUM ) 
            { 
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 253;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOIPCALLSTATE[ LVINDEX ] ))  ) ) 
                {
                __context__.SourceCodeLine = 254;
                AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ]  .UpdateValue ( AUDIODEVICE . VOIPDIALSTRING [ LVINDEX ] + LVNUM  ) ; 
                }
            
            else 
                { 
                __context__.SourceCodeLine = 257;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ LVINDEX ] ]  ) ; 
                __context__.SourceCodeLine = 258;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.pinpad." + LVNUM + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 259;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 260;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            
            }
            
        private void POLLINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 267;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Get\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  ) ; 
            __context__.SourceCodeLine = 268;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
            __context__.SourceCodeLine = 269;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 270;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 271;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 273;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 275;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 277;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                        __context__.SourceCodeLine = 278;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 280;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 283;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 285;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                                __context__.SourceCodeLine = 286;
                                LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 288;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 290;
                                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                    __context__.SourceCodeLine = 291;
                                    LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 293;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        } 
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 296;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 297;
            SETQUEUE (  __context__ , LVSTRING) ; 
            
            }
            
        private void INITIALIZEINSTANCE (  SplusExecutionContext __context__, ushort LVINDEX ) 
            { 
            ushort LVINDEXCOUNTER = 0;
            
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
            
            
            __context__.SourceCodeLine = 303;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) > 2 ))  ) ) 
                { 
                __context__.SourceCodeLine = 305;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022ChangeGroup.AddComponentControl\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022, \u0022Component\u0022 : "  ) ; 
                __context__.SourceCodeLine = 306;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 307;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 309;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022 }, { \u0022Name\u0022: \u0022mute\u0022 }"  ) ; 
                    __context__.SourceCodeLine = 316;
                    AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 318;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 320;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + "\u0022 }"  ) ; 
                        __context__.SourceCodeLine = 321;
                        AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 323;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 325;
                            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVINDEX ] ) )  ) ; 
                            __context__.SourceCodeLine = 326;
                            LVSTRING  .UpdateValue ( LVSTRING + ".gain\u0022 }"  ) ; 
                            __context__.SourceCodeLine = 327;
                            AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 329;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 331;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".gain\u0022 }, { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".mute\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 332;
                                LVSTRING  .UpdateValue ( LVSTRING + ", { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".open\u0022 }, { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVINDEX ] ) ) + ".config\u0022 }"  ) ; 
                                __context__.SourceCodeLine = 340;
                                AUDIODEVICE . INSTANCETAGSPOLL [ LVINDEX] = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 342;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                    { 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 345;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 347;
                                        GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                        __context__.SourceCodeLine = 348;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                            {
                                            __context__.SourceCodeLine = 349;
                                            AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                            }
                                        
                                        __context__.SourceCodeLine = 350;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022 }, { \u0022Name\u0022: \u0022call.autoanswer\u0022 }, { \u0022Name\u0022: \u0022call.number\u0022 }, "  ) ; 
                                        __context__.SourceCodeLine = 351;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.offhook\u0022 }, { \u0022Name\u0022: \u0022call.ringing\u0022 }, { \u0022Name\u0022: \u0022call.status\u0022 },"  ) ; 
                                        __context__.SourceCodeLine = 352;
                                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022recent.calls\u0022 }"  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 354;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "POTS" , AUDIODEVICE.INSTANCETAGSTYPE[ LVINDEX ] ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 356;
                                            GVVOIPCOUNTER = (ushort) ( (GVVOIPCOUNTER + 1) ) ; 
                                            __context__.SourceCodeLine = 357;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( GVVOIPCOUNTER <= 2 ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 358;
                                                AUDIODEVICE . VOIPINSTANCEINDEX [ GVVOIPCOUNTER] = (ushort) ( LVINDEX ) ; 
                                                }
                                            
                                            } 
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                __context__.SourceCodeLine = 360;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } } }"  ) ; 
                __context__.SourceCodeLine = 361;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 362;
                POLLINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            
            }
            
        private void DEVICEONLINE (  SplusExecutionContext __context__ ) 
            { 
            ushort LVCOUNTER = 0;
            
            
            __context__.SourceCodeLine = 368;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)64; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 370;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVCOUNTER )) ; 
                __context__.SourceCodeLine = 368;
                } 
            
            __context__.SourceCodeLine = 372;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_1__" , 200 , __SPLS_TMPVAR__WAITLABEL_1___Callback ) ;
            
            }
            
        public void __SPLS_TMPVAR__WAITLABEL_1___CallbackFn( object stateInfo )
        {
        
            try
            {
                Wait __LocalWait__ = (Wait)stateInfo;
                SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
                __LocalWait__.RemoveFromList();
                
            {
            __context__.SourceCodeLine = 373;
            SETQUEUE (  __context__ , AUDIODEVICE.COMMANDAUTOPOLL) ; 
            }
        
        
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler(); }
            
        }
        
    private void CHECKINSTANCETAGS (  SplusExecutionContext __context__, CrestronString LVINSTANCETAG , CrestronString LVPARSEDSTRING ) 
        { 
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVVOLUME;
        CrestronString LVSTRING;
        CrestronString LVTRASH;
        CrestronString LVINSTANCEINDEX;
        CrestronString LVCOMPARESTRING;
        LVVOLUME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        LVINSTANCEINDEX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        LVCOMPARESTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 380;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 382;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVINSTANCETAG == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                { 
                __context__.SourceCodeLine = 384;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 386;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022gain\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 388;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 389;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 390;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 392;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 393;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 395;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 396;
                                LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                __context__.SourceCodeLine = 397;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 398;
                                    LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                    }
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 400;
                                    LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                    }
                                
                                __context__.SourceCodeLine = 401;
                                AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                __context__.SourceCodeLine = 402;
                                VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                __context__.SourceCodeLine = 403;
                                if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                    {
                                    __context__.SourceCodeLine = 404;
                                    AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                    }
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 408;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute\u0022" , LVPARSEDSTRING ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 410;
                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                        __context__.SourceCodeLine = 411;
                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022gain\u0022" , LVSTRING )  ) ; 
                        __context__.SourceCodeLine = 412;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 414;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 415;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 417;
                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                __context__.SourceCodeLine = 418;
                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 420;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 422;
                                    AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                    __context__.SourceCodeLine = 423;
                                    VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                    } 
                                
                                }
                            
                            } 
                        
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 428;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 430;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022output." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 432;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 433;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022output." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 434;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 435;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 436;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 438;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "gain\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 440;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 441;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "gain\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 442;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 444;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 445;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 447;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 448;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 449;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 450;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 452;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 453;
                                            AUDIODEVICE . STATUSVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                            __context__.SourceCodeLine = 454;
                                            VOLUMELEVEL_FB [ LVCOUNTER]  .Value = (ushort) ( VOLUMECONVERTERREVERSE( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ LVCOUNTER ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ LVCOUNTER ] ) , (short)( LVVOL ) ) ) ; 
                                            __context__.SourceCodeLine = 455;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.VOLUMEINUSE ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 456;
                                                AUDIODEVICE . INTERNALVOLUME [ LVCOUNTER] = (short) ( LVVOL ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 460;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "mute\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 462;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 463;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "mute\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 464;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 466;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 467;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 468;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 470;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 471;
                                            VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 473;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 475;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 476;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        __context__.SourceCodeLine = 482;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "wall." , LVPARSEDSTRING ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 484;
                            LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                            __context__.SourceCodeLine = 485;
                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022wall." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 486;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "." , LVSTRING )  ) ; 
                            __context__.SourceCodeLine = 487;
                            LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                            __context__.SourceCodeLine = 488;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                { 
                                __context__.SourceCodeLine = 490;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "open\u0022,\u0022String\u0022:\u0022" , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 492;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 493;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "open\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 494;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 496;
                                        AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 498;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 500;
                                            AUDIODEVICE . STATUSWALL [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 501;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 502;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                            } 
                                        
                                        }
                                    
                                    } 
                                
                                __context__.SourceCodeLine = 505;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 507;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 508;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( "config\u0022,\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 509;
                                    LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 510;
                                    if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSWALL[ LVCOUNTER ])  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 512;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "G" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 514;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "G" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 515;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                            __context__.SourceCodeLine = 516;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( Functions.Atoi( LVSTRING ) ) ; 
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 523;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 525;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022select." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 527;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 528;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022select." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 529;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 530;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 531;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 533;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 535;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 536;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 538;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 539;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 1), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 540;
                                            AUDIODEVICE . STATUSGROUP [ LVCOUNTER] = (ushort) ( Functions.Atoi( LVVOLUME ) ) ; 
                                            __context__.SourceCodeLine = 541;
                                            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSVOLUMEMUTE[ LVCOUNTER ] ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 542;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            __context__.SourceCodeLine = 547;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022mute." , LVPARSEDSTRING ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 549;
                                LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                __context__.SourceCodeLine = 550;
                                LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022mute." , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 551;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                __context__.SourceCodeLine = 552;
                                LVINSTANCEINDEX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVINSTANCEINDEX ) - 1), LVINSTANCEINDEX )  ) ; 
                                __context__.SourceCodeLine = 553;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] == Functions.Atoi( LVINSTANCEINDEX )))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 555;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 557;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 558;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "unmuted" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 560;
                                            AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                            __context__.SourceCodeLine = 561;
                                            GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 563;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "muted" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 565;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 566;
                                                GROUP_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            }
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 572;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) ) || Functions.TestForTrue ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ) )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 574;
                                LVCOMPARESTRING  .UpdateValue ( "\u0022Name\u0022:\u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ LVCOUNTER ] ) ) + ".gain\u0022"  ) ; 
                                __context__.SourceCodeLine = 575;
                                if ( Functions.TestForTrue  ( ( Functions.Find( LVCOMPARESTRING , LVPARSEDSTRING ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 577;
                                    LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                    __context__.SourceCodeLine = 578;
                                    LVTRASH  .UpdateValue ( Functions.Remove ( LVCOMPARESTRING , LVSTRING )  ) ; 
                                    __context__.SourceCodeLine = 579;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 581;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 582;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "dB" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 584;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( "dB" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 585;
                                            LVVOLUME  .UpdateValue ( Functions.Remove ( (Functions.Length( LVVOLUME ) - 2), LVVOLUME )  ) ; 
                                            __context__.SourceCodeLine = 586;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVVOLUME ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 587;
                                                LVVOL = (short) ( (Functions.Atoi( LVVOLUME ) * Functions.ToSignedInteger( -( 1 ) )) ) ; 
                                                }
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 589;
                                                LVVOL = (short) ( Functions.Atoi( LVVOLUME ) ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 590;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVVOL == Functions.ToSignedLongInteger( -( 100 ) )))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 592;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 593;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                { 
                                                __context__.SourceCodeLine = 597;
                                                AUDIODEVICE . STATUSVOLUMEMUTE [ LVCOUNTER] = (ushort) ( 0 ) ; 
                                                __context__.SourceCodeLine = 598;
                                                VOLUMEMUTE_FB [ LVCOUNTER]  .Value = (ushort) ( 0 ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 604;
                                if ( Functions.TestForTrue  ( ( Functions.Find( "VOIP" , AUDIODEVICE.INSTANCETAGSTYPE[ LVCOUNTER ] ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 606;
                                    ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                                    ushort __FN_FOREND_VAL__2 = (ushort)2; 
                                    int __FN_FORSTEP_VAL__2 = (int)1; 
                                    for ( LVVOIPCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVVOIPCOUNTER  >= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVVOIPCOUNTER  <= __FN_FORSTART_VAL__2) && (LVVOIPCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVVOIPCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                                        { 
                                        __context__.SourceCodeLine = 608;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER == AUDIODEVICE.VOIPINSTANCEINDEX[ LVVOIPCOUNTER ]))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 610;
                                            LVVOIPNUM = (ushort) ( LVVOIPCOUNTER ) ; 
                                            __context__.SourceCodeLine = 611;
                                            break ; 
                                            } 
                                        
                                        __context__.SourceCodeLine = 606;
                                        } 
                                    
                                    __context__.SourceCodeLine = 614;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 616;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 617;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.dnd\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 618;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 620;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 621;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 622;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 623;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 625;
                                                AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 626;
                                                VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 628;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 630;
                                                    AUDIODEVICE . STATUSVOIPDND [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 631;
                                                    VOIP_DND_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 635;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 637;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 638;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.autoanswer\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 639;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 641;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 642;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 643;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 644;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "on" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 646;
                                                AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 647;
                                                VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 649;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "off" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 651;
                                                    AUDIODEVICE . STATUSVOIPAUTOANSWER [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 652;
                                                    VOIP_AUTOANSWER_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 656;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 658;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 659;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.offhook\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 660;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 662;
                                            LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022String\u0022:\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 663;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 664;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 665;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "true" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 667;
                                                AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 668;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 670;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( "false" , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 672;
                                                    AUDIODEVICE . STATUSVOIPCALLSTATE [ LVVOIPNUM] = (ushort) ( 0 ) ; 
                                                    __context__.SourceCodeLine = 673;
                                                    VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                    } 
                                                
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 691;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022call.status\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 693;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 694;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022call.status\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 695;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022String\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 697;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( "\u0022" , LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 698;
                                            LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 1), LVSTRING )  ) ; 
                                            __context__.SourceCodeLine = 699;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Dialing - " , LVSTRING ))  ) ) 
                                                {
                                                __context__.SourceCodeLine = 700;
                                                VOIP_CALLSTATUS_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                }
                                            
                                            __context__.SourceCodeLine = 701;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Incoming Call" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 703;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Incoming Call" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 704;
                                                VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 1 ) ; 
                                                __context__.SourceCodeLine = 705;
                                                if ( Functions.TestForTrue  ( ( Functions.Find( " - " , LVSTRING ))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 707;
                                                    LVTRASH  .UpdateValue ( Functions.Remove ( " - " , LVSTRING )  ) ; 
                                                    __context__.SourceCodeLine = 708;
                                                    VOIP_CALLINCOMINGNAME_FB [ LVVOIPNUM]  .UpdateValue ( LVSTRING  ) ; 
                                                    } 
                                                
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 712;
                                                VOIP_CALLINCOMING_FB [ LVVOIPNUM]  .Value = (ushort) ( 0 ) ; 
                                                }
                                            
                                            } 
                                        
                                        } 
                                    
                                    __context__.SourceCodeLine = 715;
                                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVPARSEDSTRING ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 717;
                                        LVSTRING  .UpdateValue ( LVPARSEDSTRING  ) ; 
                                        __context__.SourceCodeLine = 718;
                                        LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022Name\u0022:\u0022recent.calls\u0022" , LVSTRING )  ) ; 
                                        __context__.SourceCodeLine = 719;
                                        if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022Choices\u0022:\u0022" , LVSTRING ))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 721;
                                            if ( Functions.TestForTrue  ( ( Functions.Find( "Text\u005C\u0022:\u005C\u0022" , LVSTRING ))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 723;
                                                LVTRASH  .UpdateValue ( Functions.Remove ( "Text\u005C\u0022:\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 724;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( "\u005C\u0022" , LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 725;
                                                LVSTRING  .UpdateValue ( Functions.Remove ( (Functions.Length( LVSTRING ) - 2), LVSTRING )  ) ; 
                                                __context__.SourceCodeLine = 726;
                                                AUDIODEVICE . STATUSVOIPRECENTCALL [ LVVOIPNUM ]  .UpdateValue ( LVSTRING  ) ; 
                                                } 
                                            
                                            } 
                                        
                                        } 
                                    
                                    } 
                                
                                }
                            
                            }
                        
                        }
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 380;
            } 
        
        
        }
        
    private void PARSEFEEDBACK (  SplusExecutionContext __context__ ) 
        { 
        ushort LVINDEX = 0;
        ushort LVCOUNTER = 0;
        ushort LVVOIPNUM = 0;
        ushort LVVOIPCOUNTER = 0;
        
        short LVVOL = 0;
        
        CrestronString LVRX;
        CrestronString LVTRASH;
        CrestronString LVSTRING;
        CrestronString LVTAG;
        LVRX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTRASH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 8191, this );
        LVTAG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1023, this );
        
        
        __context__.SourceCodeLine = 739;
        while ( Functions.TestForTrue  ( ( Functions.Find( "\u0000" , AUDIODEVICE.RXQUEUE ))  ) ) 
            { 
            __context__.SourceCodeLine = 741;
            LVRX  .UpdateValue ( Functions.Remove ( "\u0000" , AUDIODEVICE . RXQUEUE )  ) ; 
            __context__.SourceCodeLine = 742;
            LVRX  .UpdateValue ( Functions.Remove ( (Functions.Length( LVRX ) - 1), LVRX )  ) ; 
            __context__.SourceCodeLine = 743;
            SETDEBUG (  __context__ , LVRX, (ushort)( 1 )) ; 
            __context__.SourceCodeLine = 744;
            if ( Functions.TestForTrue  ( ( Functions.Not( AUDIODEVICE.STATUSCOMMUNICATING ))  ) ) 
                {
                __context__.SourceCodeLine = 745;
                AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 1 ) ; 
                }
            
            __context__.SourceCodeLine = 746;
            if ( Functions.TestForTrue  ( ( Functions.Find( "Changes\u0022:[{" , LVRX ))  ) ) 
                { 
                __context__.SourceCodeLine = 748;
                LVTRASH  .UpdateValue ( Functions.Remove ( "Changes\u0022:[{" , LVRX )  ) ; 
                __context__.SourceCodeLine = 749;
                while ( Functions.TestForTrue  ( ( Functions.Find( "Component\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 751;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "Component\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 752;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 753;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 754;
                    if ( Functions.TestForTrue  ( ( Functions.Find( ",{\u0022Component\u0022:\u0022" , LVRX ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 756;
                        LVSTRING  .UpdateValue ( Functions.Remove ( ",{\u0022Component\u0022:\u0022" , LVRX )  ) ; 
                        __context__.SourceCodeLine = 757;
                        LVRX  .UpdateValue ( ",{\u0022Component\u0022:\u0022" + LVRX  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 760;
                        LVSTRING  .UpdateValue ( LVRX  ) ; 
                        }
                    
                    __context__.SourceCodeLine = 761;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVSTRING) ; 
                    __context__.SourceCodeLine = 749;
                    } 
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 764;
                if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 766;
                    LVTRASH  .UpdateValue ( Functions.Remove ( "\u0022result\u0022:{\u0022Name\u0022:\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 767;
                    LVTAG  .UpdateValue ( Functions.Remove ( "\u0022" , LVRX )  ) ; 
                    __context__.SourceCodeLine = 768;
                    LVTAG  .UpdateValue ( Functions.Remove ( (Functions.Length( LVTAG ) - 1), LVTAG )  ) ; 
                    __context__.SourceCodeLine = 769;
                    CHECKINSTANCETAGS (  __context__ , LVTAG, LVRX) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 739;
            } 
        
        
        }
        
    object AUDIOCLIENT_OnSocketConnect_0 ( Object __Info__ )
    
        { 
        SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
            CrestronString LVSTRING;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            
            
            __context__.SourceCodeLine = 780;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 781;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 782;
            CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
            __context__.SourceCodeLine = 783;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.LOGINPASSWORD ) > 3 ))  ) ) 
                { 
                __context__.SourceCodeLine = 785;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022:\u00222.0\u0022, \u0022method\u0022:\u0022Logon\u0022, \u0022params\u0022:{ \u0022User\u0022:\u0022" + AUDIODEVICE . LOGINNAME + "\u0022, "  ) ; 
                __context__.SourceCodeLine = 786;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022Password\u0022:\u0022" + AUDIODEVICE . LOGINPASSWORD + "\u0022 } }"  ) ; 
                __context__.SourceCodeLine = 787;
                SETQUEUE (  __context__ , LVSTRING) ; 
                } 
            
            __context__.SourceCodeLine = 789;
            CreateWait ( "__SPLS_TMPVAR__WAITLABEL_2__" , 200 , __SPLS_TMPVAR__WAITLABEL_2___Callback ) ;
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SocketInfo__ ); }
        return this;
        
    }
    
public void __SPLS_TMPVAR__WAITLABEL_2___CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            {
            __context__.SourceCodeLine = 790;
            DEVICEONLINE (  __context__  ) ; 
            }
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object AUDIOCLIENT_OnSocketDisconnect_1 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 795;
        AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 796;
        AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 797;
        CONNECT_FB  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 798;
        CONNECT_STATUS_FB  .Value = (ushort) ( AUDIOCLIENT.SocketStatus ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketStatus_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 802;
        CONNECT_STATUS_FB  .Value = (ushort) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 803;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (CONNECT_STATUS_FB  .Value == 2))  ) ) 
            { 
            __context__.SourceCodeLine = 805;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 806;
            CONNECT_FB  .Value = (ushort) ( 1 ) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 810;
            AUDIODEVICE . STATUSCONNECTED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 811;
            AUDIODEVICE . STATUSCOMMUNICATING = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 812;
            CONNECT_FB  .Value = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object AUDIOCLIENT_OnSocketReceive_3 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 818;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + AUDIOCLIENT .  SocketRxBuf  ) ; 
        __context__.SourceCodeLine = 819;
        Functions.ClearBuffer ( AUDIOCLIENT .  SocketRxBuf ) ; 
        __context__.SourceCodeLine = 820;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.RXQUEUE ) > 2 ))  ) ) 
            {
            __context__.SourceCodeLine = 821;
            PARSEFEEDBACK (  __context__  ) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object CONNECT_OnChange_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 825;
        if ( Functions.TestForTrue  ( ( CONNECT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 827;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 828;
            CONNECTDISCONNECT (  __context__ , (ushort)( 1 )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 832;
            AUDIODEVICE . STATUSCONNECTREQUEST = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 833;
            CONNECTDISCONNECT (  __context__ , (ushort)( 0 )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object POLL_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 838;
        SETQUEUE (  __context__ , AUDIODEVICE.COMMANDPOLL) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object RX_OnChange_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 842;
        AUDIODEVICE . RXQUEUE  .UpdateValue ( AUDIODEVICE . RXQUEUE + RX  ) ; 
        __context__.SourceCodeLine = 843;
        PARSEFEEDBACK (  __context__  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object MANUALCMD_OnChange_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 847;
        SETQUEUE (  __context__ , MANUALCMD) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_ADDRESS_OnChange_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 851;
        AUDIODEVICE . IPADDRESS  .UpdateValue ( IP_ADDRESS  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object IP_PORT_OnChange_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 855;
        AUDIODEVICE . IPPORT = (ushort) ( IP_PORT  .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_NAME_OnChange_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 859;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( LOGIN_NAME  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object LOGIN_PASSWORD_OnChange_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 863;
        AUDIODEVICE . LOGINPASSWORD  .UpdateValue ( LOGIN_PASSWORD  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object INSTANCETAGS_OnChange_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVINDEX = 0;
        
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 127, this );
        
        
        __context__.SourceCodeLine = 869;
        LVINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 870;
        LVSTRING  .UpdateValue ( INSTANCETAGS [ LVINDEX ]  ) ; 
        __context__.SourceCodeLine = 871;
        if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
            { 
            __context__.SourceCodeLine = 873;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( "-" , LVSTRING )  ) ; 
            __context__.SourceCodeLine = 874;
            AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ]  .UpdateValue ( Functions.Remove ( (Functions.Length( AUDIODEVICE.INSTANCETAGSNAME[ LVINDEX ] ) - 1), AUDIODEVICE . INSTANCETAGSNAME [ LVINDEX ] )  ) ; 
            __context__.SourceCodeLine = 875;
            if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                { 
                __context__.SourceCodeLine = 877;
                AUDIODEVICE . INSTANCETAGSINDEX [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                __context__.SourceCodeLine = 878;
                if ( Functions.TestForTrue  ( ( Functions.Find( "-" , LVSTRING ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 880;
                    AUDIODEVICE . INSTANCETAGSINDEXSECOND [ LVINDEX] = (ushort) ( Functions.Atoi( Functions.Remove( "-" , LVSTRING ) ) ) ; 
                    __context__.SourceCodeLine = 881;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 884;
                    AUDIODEVICE . INSTANCETAGSTYPE [ LVINDEX ]  .UpdateValue ( LVSTRING  ) ; 
                    }
                
                } 
            
            __context__.SourceCodeLine = 886;
            if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSCOMMUNICATING)  ) ) 
                { 
                __context__.SourceCodeLine = 888;
                INITIALIZEINSTANCE (  __context__ , (ushort)( LVINDEX )) ; 
                } 
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEUP_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 895;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 896;
        while ( Functions.TestForTrue  ( ( VOLUMEUP[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 898;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 899;
            CreateWait ( "VOLUP" , 10 , VOLUP_Callback ) ;
            __context__.SourceCodeLine = 896;
            } 
        
        __context__.SourceCodeLine = 919;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLUP_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 902;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) > AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 904;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] + AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 905;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 906;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 907;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 908;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 909;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 910;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 911;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 912;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 913;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 914;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 915;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEDOWN_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 923;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 924;
        while ( Functions.TestForTrue  ( ( VOLUMEDOWN[ AUDIODEVICE.LASTINDEX ] .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 926;
            AUDIODEVICE . VOLUMEINUSE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 927;
            CreateWait ( "VOLDN" , 10 , VOLDN_Callback ) ;
            __context__.SourceCodeLine = 924;
            } 
        
        __context__.SourceCodeLine = 947;
        AUDIODEVICE . VOLUMEINUSE = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public void VOLDN_CallbackFn( object stateInfo )
{

    try
    {
        Wait __LocalWait__ = (Wait)stateInfo;
        SplusExecutionContext __context__ = SplusThreadStartCode(__LocalWait__);
        __LocalWait__.RemoveFromList();
        
            
            CrestronString LVSTRING;
            CrestronString LVSTRINGVOL;
            LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
            LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
            
            __context__.SourceCodeLine = 930;
            if ( Functions.TestForTrue  ( ( Functions.Not( Functions.BoolToInt ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) < AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ))  ) ) 
                { 
                __context__.SourceCodeLine = 932;
                AUDIODEVICE . INTERNALVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( (AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] - AUDIODEVICE.VOLUMESTEP[ AUDIODEVICE.LASTINDEX ]) ) ; 
                __context__.SourceCodeLine = 933;
                MakeString ( LVSTRINGVOL , "{0:d}", (short)AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ]) ; 
                __context__.SourceCodeLine = 934;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 935;
                LVSTRING  .UpdateValue ( LVSTRING + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ] + "\u0022, \u0022Controls\u0022: [ "  ) ; 
                __context__.SourceCodeLine = 936;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 937;
                    LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 938;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        {
                        __context__.SourceCodeLine = 939;
                        LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 940;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 941;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 942;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                __context__.SourceCodeLine = 943;
                AUDIODEVICE . STATUSVOLUME [ AUDIODEVICE.LASTINDEX] = (short) ( AUDIODEVICE.INTERNALVOLUME[ AUDIODEVICE.LASTINDEX ] ) ; 
                } 
            
            
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    
}

object VOLUMEMUTEON_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 952;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 953;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 955;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 956;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 957;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 959;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 961;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 962;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 963;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 965;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 969;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 971;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 972;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 973;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                        __context__.SourceCodeLine = 974;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 976;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 980;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 981;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTEOFF_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 986;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 987;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 989;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 990;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
            __context__.SourceCodeLine = 991;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            } 
        
        else 
            {
            __context__.SourceCodeLine = 993;
            if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                { 
                __context__.SourceCodeLine = 995;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 996;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 997;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 999;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1002;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1004;
                        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                        __context__.SourceCodeLine = 1005;
                        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                        __context__.SourceCodeLine = 1006;
                        LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                        __context__.SourceCodeLine = 1007;
                        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1009;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            } 
                        
                        }
                    
                    }
                
                }
            
            }
        
        __context__.SourceCodeLine = 1012;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1013;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMEMUTETOGGLE_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1018;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1019;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOLUMEMUTE[ AUDIODEVICE.LASTINDEX ])  ) ) 
            { 
            __context__.SourceCodeLine = 1021;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1023;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1024;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1025;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1027;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1029;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1030;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1031;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1033;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1036;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1038;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1039;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1040;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: 0 }"  ) ; 
                            __context__.SourceCodeLine = 1041;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1043;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1046;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1047;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1051;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1053;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1054;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                __context__.SourceCodeLine = 1055;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1057;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1059;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1060;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".mute\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    __context__.SourceCodeLine = 1061;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1063;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    else 
                        {
                        __context__.SourceCodeLine = 1066;
                        if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1068;
                            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                            __context__.SourceCodeLine = 1069;
                            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022input." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".output."  ) ; 
                            __context__.SourceCodeLine = 1070;
                            LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEXSECOND[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: -100 }"  ) ; 
                            __context__.SourceCodeLine = 1071;
                            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 1073;
                            if ( Functions.TestForTrue  ( ( Functions.Find( "LGCS" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                                { 
                                } 
                            
                            }
                        
                        }
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 1076;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1077;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESET_OnChange_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        CrestronString LVSTRINGVOL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVSTRINGVOL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 15, this );
        
        short LVVOL = 0;
        
        
        __context__.SourceCodeLine = 1084;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1085;
        LVVOL = (short) ( VOLUMECONVERTER( __context__ , (short)( AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) , (short)( AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) , (ushort)( VOLUMESET[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) ) ; 
        __context__.SourceCodeLine = 1086;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LVVOL >= AUDIODEVICE.VOLUMEMIN[ AUDIODEVICE.LASTINDEX ] ) ) && Functions.TestForTrue ( Functions.BoolToInt ( LVVOL <= AUDIODEVICE.VOLUMEMAX[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1088;
            MakeString ( LVSTRINGVOL , "{0:d}", (short)LVVOL) ; 
            __context__.SourceCodeLine = 1089;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "SVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "MVOL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1091;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1092;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                __context__.SourceCodeLine = 1093;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1095;
                if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1097;
                    LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                    __context__.SourceCodeLine = 1098;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022output." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".gain\u0022, \u0022Value\u0022: " + LVSTRINGVOL + ", \u0022Ramp\u0022: 0 }"  ) ; 
                    __context__.SourceCodeLine = 1099;
                    LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                    } 
                
                }
            
            __context__.SourceCodeLine = 1101;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1102;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object GROUP_OnChange_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort LVCOUNTER = 0;
        
        CrestronString LVSTRING;
        CrestronString LVWALLCONFIG;
        CrestronString LVNAME;
        CrestronString LVSTRINGWALL;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        LVWALLCONFIG  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, this );
        LVSTRINGWALL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1109;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1110;
        LVNAME  .UpdateValue ( AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
        __context__.SourceCodeLine = 1111;
        if ( Functions.TestForTrue  ( ( Functions.Find( "RCMB" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
            { 
            __context__.SourceCodeLine = 1113;
            if ( Functions.TestForTrue  ( ( Functions.Not( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ))  ) ) 
                { 
                __context__.SourceCodeLine = 1115;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1116;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 0 },"  ) ; 
                __context__.SourceCodeLine = 1117;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022String\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1118;
                LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1119;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1120;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 1125;
                ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__1 = (ushort)64; 
                int __FN_FORSTEP_VAL__1 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
                    { 
                    __context__.SourceCodeLine = 1127;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1129;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1131;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1133;
                                LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1125;
                    } 
                
                __context__.SourceCodeLine = 1138;
                ushort __FN_FORSTART_VAL__2 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__2 = (ushort)64; 
                int __FN_FORSTEP_VAL__2 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__2) && (LVCOUNTER  <= __FN_FOREND_VAL__2) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__2) && (LVCOUNTER  >= __FN_FOREND_VAL__2) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__2) 
                    { 
                    __context__.SourceCodeLine = 1140;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1142;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]) ) && Functions.TestForTrue ( Functions.BoolToInt (GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue != AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ]) )) ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1144;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVCOUNTER != AUDIODEVICE.LASTINDEX))  ) ) 
                                { 
                                __context__.SourceCodeLine = 1146;
                                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ LVCOUNTER ]  ) ; 
                                __context__.SourceCodeLine = 1147;
                                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                                __context__.SourceCodeLine = 1148;
                                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                                __context__.SourceCodeLine = 1149;
                                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( AUDIODEVICE.STATUSGROUP[ LVCOUNTER ] ) ) + "\u0022 } ] } }"  ) ; 
                                __context__.SourceCodeLine = 1150;
                                SETQUEUE (  __context__ , LVSTRING) ; 
                                __context__.SourceCodeLine = 1151;
                                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                                } 
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1138;
                    } 
                
                __context__.SourceCodeLine = 1156;
                LVWALLCONFIG  .UpdateValue ( ""  ) ; 
                __context__.SourceCodeLine = 1157;
                AUDIODEVICE . STATUSGROUP [ AUDIODEVICE.LASTINDEX] = (ushort) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
                __context__.SourceCodeLine = 1158;
                ushort __FN_FORSTART_VAL__3 = (ushort) ( 1 ) ;
                ushort __FN_FOREND_VAL__3 = (ushort)64; 
                int __FN_FORSTEP_VAL__3 = (int)1; 
                for ( LVCOUNTER  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__3) && (LVCOUNTER  <= __FN_FOREND_VAL__3) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__3) && (LVCOUNTER  >= __FN_FOREND_VAL__3) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__3) 
                    { 
                    __context__.SourceCodeLine = 1160;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (LVNAME == AUDIODEVICE.INSTANCETAGSNAME[ LVCOUNTER ]))  ) ) 
                        { 
                        __context__.SourceCodeLine = 1162;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (AUDIODEVICE.STATUSGROUP[ AUDIODEVICE.LASTINDEX ] == AUDIODEVICE.STATUSGROUP[ LVCOUNTER ]))  ) ) 
                            { 
                            __context__.SourceCodeLine = 1164;
                            LVWALLCONFIG  .UpdateValue ( LVWALLCONFIG + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ LVCOUNTER ] ) ) + " "  ) ; 
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 1158;
                    } 
                
                __context__.SourceCodeLine = 1168;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1169;
                LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".open\u0022, \u0022Value\u0022: 1 },"  ) ; 
                __context__.SourceCodeLine = 1170;
                LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022wall." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + ".config\u0022, \u0022Value\u0022: \u0022"  ) ; 
                __context__.SourceCodeLine = 1171;
                LVSTRING  .UpdateValue ( LVSTRING + LVWALLCONFIG + "G" + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + "\u0022 } ] } }"  ) ; 
                __context__.SourceCodeLine = 1172;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1173;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            } 
        
        else 
            {
            __context__.SourceCodeLine = 1177;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "ROUT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) ) || Functions.TestForTrue ( Functions.Find( "SSEL" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ) )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 1179;
                LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.LASTINDEX ]  ) ; 
                __context__.SourceCodeLine = 1180;
                if ( Functions.TestForTrue  ( ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue)  ) ) 
                    { 
                    __context__.SourceCodeLine = 1182;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022select." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: "  ) ; 
                    __context__.SourceCodeLine = 1183;
                    LVSTRING  .UpdateValue ( LVSTRING + Functions.ItoA (  (int) ( GROUP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ) + " }, { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 0 }"  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1186;
                    LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022mute." + Functions.ItoA (  (int) ( AUDIODEVICE.INSTANCETAGSINDEX[ AUDIODEVICE.LASTINDEX ] ) ) + "\u0022, \u0022Value\u0022: 1 }"  ) ; 
                    }
                
                __context__.SourceCodeLine = 1187;
                LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
                __context__.SourceCodeLine = 1188;
                SETQUEUE (  __context__ , LVSTRING) ; 
                __context__.SourceCodeLine = 1189;
                POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.LASTINDEX )) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 1191;
                if ( Functions.TestForTrue  ( ( Functions.Find( "SMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                    { 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 1199;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "MMAT" , AUDIODEVICE.INSTANCETAGSTYPE[ AUDIODEVICE.LASTINDEX ] ))  ) ) 
                        { 
                        } 
                    
                    }
                
                }
            
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOLUMESTEP_OnChange_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1210;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1211;
        AUDIODEVICE . VOLUMESTEP [ AUDIODEVICE.LASTINDEX] = (ushort) ( VOLUMESTEP[ AUDIODEVICE.LASTINDEX ] .UshortValue ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERTOGGLE_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1216;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1217;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1218;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1219;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPAUTOANSWER[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1220;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1222;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1223;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1224;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1225;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWERON_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1230;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1231;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1232;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1233;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1234;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1235;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_AUTOANSWEROFF_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1240;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1241;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1242;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.autoanswer\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1243;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1244;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1245;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDTOGGLE_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1250;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1251;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1252;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ "  ) ; 
        __context__.SourceCodeLine = 1253;
        if ( Functions.TestForTrue  ( ( AUDIODEVICE.STATUSVOIPDND[ AUDIODEVICE.LASTINDEX ])  ) ) 
            {
            __context__.SourceCodeLine = 1254;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 1256;
            LVSTRING  .UpdateValue ( LVSTRING + "{ \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
            }
        
        __context__.SourceCodeLine = 1257;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1258;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1259;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDON_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1264;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1265;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1266;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1267;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1268;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1269;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DNDOFF_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1274;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1275;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1276;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.dnd\u0022, \u0022Value\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1277;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1278;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1279;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_ACCEPT_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1284;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1285;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1286;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1287;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1288;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1289;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DECLINE_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1294;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1295;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1296;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1297;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1298;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1299;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_JOIN_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, this );
        
        
        __context__.SourceCodeLine = 1304;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1305;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1306;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022hook.flash\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1307;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1308;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1309;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CONFERENCE_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_REDIAL_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1317;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1318;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.STATUSVOIPRECENTCALL[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1320;
            VOIP_DIALSTRING [ AUDIODEVICE.LASTINDEX]  .UpdateValue ( AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ]  ) ; 
            __context__.SourceCodeLine = 1321;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1322;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . STATUSVOIPRECENTCALL [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1323;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1324;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1325;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1326;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1327;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1328;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1329;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_0_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1335;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "0") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_1_OnPush_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1339;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_2_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1343;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "2") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_3_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1347;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "3") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_4_OnPush_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1351;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "4") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_5_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1355;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "5") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_6_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1359;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "6") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_7_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1363;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "7") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_8_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1367;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "8") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_9_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1371;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "9") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_STAR_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1375;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "*") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_NUM_POUND_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1379;
        VOIP_NUM (  __context__ , (ushort)( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ), "#") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_BACKSPACE_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIAL_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1387;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1388;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( AUDIODEVICE.VOIPDIALSTRING[ AUDIODEVICE.LASTINDEX ] ) > 1 ))  ) ) 
            { 
            __context__.SourceCodeLine = 1390;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1391;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.number\u0022, \u0022Value\u0022: \u0022" + AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ] + "\u0022 }"  ) ; 
            __context__.SourceCodeLine = 1392;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1393;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1394;
            LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
            __context__.SourceCodeLine = 1395;
            LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.connect\u0022, \u0022Value\u0022: 1 }"  ) ; 
            __context__.SourceCodeLine = 1396;
            LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
            __context__.SourceCodeLine = 1397;
            SETQUEUE (  __context__ , LVSTRING) ; 
            __context__.SourceCodeLine = 1398;
            POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_END_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString LVSTRING;
        LVSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 511, this );
        
        
        __context__.SourceCodeLine = 1404;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1405;
        LVSTRING  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022Component.Set\u0022, \u0022params\u0022: { \u0022Name\u0022: \u0022" + AUDIODEVICE . INSTANCETAGSNAME [ AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] ]  ) ; 
        __context__.SourceCodeLine = 1406;
        LVSTRING  .UpdateValue ( LVSTRING + "\u0022, \u0022Controls\u0022: [ { \u0022Name\u0022: \u0022call.disconnect\u0022, \u0022Value\u0022: 1 }"  ) ; 
        __context__.SourceCodeLine = 1407;
        LVSTRING  .UpdateValue ( LVSTRING + " ] } }"  ) ; 
        __context__.SourceCodeLine = 1408;
        SETQUEUE (  __context__ , LVSTRING) ; 
        __context__.SourceCodeLine = 1409;
        POLLINSTANCE (  __context__ , (ushort)( AUDIODEVICE.VOIPINSTANCEINDEX[ AUDIODEVICE.LASTINDEX ] )) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_DIALENTRY_OnChange_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1413;
        AUDIODEVICE . LASTINDEX = (ushort) ( Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) ) ; 
        __context__.SourceCodeLine = 1414;
        AUDIODEVICE . VOIPDIALSTRING [ AUDIODEVICE.LASTINDEX ]  .UpdateValue ( VOIP_DIALENTRY [ AUDIODEVICE.LASTINDEX ]  ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object VOIP_CALLAPPEARANCE_OnChange_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    ushort LVCOUNTER = 0;
    
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1425;
        GVVOIPCOUNTER = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1426;
        AUDIODEVICE . IPPORT = (ushort) ( 1710 ) ; 
        __context__.SourceCodeLine = 1427;
        AUDIODEVICE . BAUD = (uint) ( 9600 ) ; 
        __context__.SourceCodeLine = 1428;
        AUDIODEVICE . LOGINNAME  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1429;
        AUDIODEVICE . ETX  .UpdateValue ( "\u0000"  ) ; 
        __context__.SourceCodeLine = 1430;
        AUDIODEVICE . COMMANDPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022StatusGet\u0022, \u0022params\u0022: 0 }"  ) ; 
        __context__.SourceCodeLine = 1431;
        AUDIODEVICE . COMMANDAUTOPOLL  .UpdateValue ( "{ \u0022jsonrpc\u0022: \u00222.0\u0022, \u0022method\u0022: \u0022ChangeGroup.AutoPoll\u0022, \u0022params\u0022: { \u0022Id\u0022: \u0022MainGroup\u0022,\u0022Rate\u0022: 5 } }"  ) ; 
        __context__.SourceCodeLine = 1432;
        ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
        ushort __FN_FOREND_VAL__1 = (ushort)64; 
        int __FN_FORSTEP_VAL__1 = (int)1; 
        for ( LVCOUNTER  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (LVCOUNTER  >= __FN_FORSTART_VAL__1) && (LVCOUNTER  <= __FN_FOREND_VAL__1) ) : ( (LVCOUNTER  <= __FN_FORSTART_VAL__1) && (LVCOUNTER  >= __FN_FOREND_VAL__1) ) ; LVCOUNTER  += (ushort)__FN_FORSTEP_VAL__1) 
            { 
            __context__.SourceCodeLine = 1434;
            AUDIODEVICE . VOLUMEMIN [ LVCOUNTER] = (short) ( Functions.ToInteger( -( 100 ) ) ) ; 
            __context__.SourceCodeLine = 1435;
            AUDIODEVICE . VOLUMEMAX [ LVCOUNTER] = (short) ( 20 ) ; 
            __context__.SourceCodeLine = 1436;
            AUDIODEVICE . VOLUMESTEP [ LVCOUNTER] = (ushort) ( 3 ) ; 
            __context__.SourceCodeLine = 1432;
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    AUDIOCLIENT  = new SplusTcpClient ( 16383, this );
    AUDIODEVICE  = new SAUDIO( this, true );
    AUDIODEVICE .PopulateCustomAttributeList( false );
    
    CONNECT = new Crestron.Logos.SplusObjects.DigitalInput( CONNECT__DigitalInput__, this );
    m_DigitalInputList.Add( CONNECT__DigitalInput__, CONNECT );
    
    POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
    m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
    
    DEBUG = new Crestron.Logos.SplusObjects.DigitalInput( DEBUG__DigitalInput__, this );
    m_DigitalInputList.Add( DEBUG__DigitalInput__, DEBUG );
    
    VOLUMEUP = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEUP[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEUP__DigitalInput__ + i, VOLUMEUP__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEUP__DigitalInput__ + i, VOLUMEUP[i+1] );
    }
    
    VOLUMEDOWN = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEDOWN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEDOWN__DigitalInput__ + i, VOLUMEDOWN[i+1] );
    }
    
    VOLUMEMUTETOGGLE = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTETOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTETOGGLE__DigitalInput__ + i, VOLUMEMUTETOGGLE[i+1] );
    }
    
    VOLUMEMUTEON = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEON__DigitalInput__ + i, VOLUMEMUTEON[i+1] );
    }
    
    VOLUMEMUTEOFF = new InOutArray<DigitalInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTEOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOLUMEMUTEOFF__DigitalInput__ + i, VOLUMEMUTEOFF[i+1] );
    }
    
    VOIP_NUM_0 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_0[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_0__DigitalInput__ + i, VOIP_NUM_0[i+1] );
    }
    
    VOIP_NUM_1 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_1[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_1__DigitalInput__ + i, VOIP_NUM_1[i+1] );
    }
    
    VOIP_NUM_2 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_2[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_2__DigitalInput__ + i, VOIP_NUM_2[i+1] );
    }
    
    VOIP_NUM_3 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_3[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_3__DigitalInput__ + i, VOIP_NUM_3[i+1] );
    }
    
    VOIP_NUM_4 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_4[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_4__DigitalInput__ + i, VOIP_NUM_4[i+1] );
    }
    
    VOIP_NUM_5 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_5[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_5__DigitalInput__ + i, VOIP_NUM_5[i+1] );
    }
    
    VOIP_NUM_6 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_6[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_6__DigitalInput__ + i, VOIP_NUM_6[i+1] );
    }
    
    VOIP_NUM_7 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_7[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_7__DigitalInput__ + i, VOIP_NUM_7[i+1] );
    }
    
    VOIP_NUM_8 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_8[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_8__DigitalInput__ + i, VOIP_NUM_8[i+1] );
    }
    
    VOIP_NUM_9 = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_9[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_9__DigitalInput__ + i, VOIP_NUM_9[i+1] );
    }
    
    VOIP_NUM_STAR = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_STAR[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_STAR__DigitalInput__ + i, VOIP_NUM_STAR[i+1] );
    }
    
    VOIP_NUM_POUND = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_NUM_POUND[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_NUM_POUND__DigitalInput__ + i, VOIP_NUM_POUND[i+1] );
    }
    
    VOIP_BACKSPACE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_BACKSPACE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_BACKSPACE__DigitalInput__ + i, VOIP_BACKSPACE[i+1] );
    }
    
    VOIP_AUTOANSWERTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERTOGGLE__DigitalInput__ + i, VOIP_AUTOANSWERTOGGLE[i+1] );
    }
    
    VOIP_AUTOANSWERON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWERON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWERON__DigitalInput__ + i, VOIP_AUTOANSWERON[i+1] );
    }
    
    VOIP_AUTOANSWEROFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWEROFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_AUTOANSWEROFF__DigitalInput__ + i, VOIP_AUTOANSWEROFF[i+1] );
    }
    
    VOIP_DNDTOGGLE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDTOGGLE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDTOGGLE__DigitalInput__ + i, VOIP_DNDTOGGLE[i+1] );
    }
    
    VOIP_DNDON = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDON[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDON__DigitalInput__ + i, VOIP_DNDON[i+1] );
    }
    
    VOIP_DNDOFF = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DNDOFF[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DNDOFF__DigitalInput__ + i, VOIP_DNDOFF[i+1] );
    }
    
    VOIP_DIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DIAL__DigitalInput__ + i, VOIP_DIAL[i+1] );
    }
    
    VOIP_END = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_END[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_END__DigitalInput__ + i, VOIP_END__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_END__DigitalInput__ + i, VOIP_END[i+1] );
    }
    
    VOIP_ACCEPT = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_ACCEPT[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_ACCEPT__DigitalInput__ + i, VOIP_ACCEPT[i+1] );
    }
    
    VOIP_DECLINE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DECLINE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_DECLINE__DigitalInput__ + i, VOIP_DECLINE[i+1] );
    }
    
    VOIP_JOIN = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_JOIN[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_JOIN__DigitalInput__ + i, VOIP_JOIN[i+1] );
    }
    
    VOIP_CONFERENCE = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CONFERENCE[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_CONFERENCE__DigitalInput__ + i, VOIP_CONFERENCE[i+1] );
    }
    
    VOIP_REDIAL = new InOutArray<DigitalInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_REDIAL[i+1] = new Crestron.Logos.SplusObjects.DigitalInput( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL__DigitalInput__, this );
        m_DigitalInputList.Add( VOIP_REDIAL__DigitalInput__ + i, VOIP_REDIAL[i+1] );
    }
    
    CONNECT_FB = new Crestron.Logos.SplusObjects.DigitalOutput( CONNECT_FB__DigitalOutput__, this );
    m_DigitalOutputList.Add( CONNECT_FB__DigitalOutput__, CONNECT_FB );
    
    VOLUMEMUTE_FB = new InOutArray<DigitalOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMEMUTE_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOLUMEMUTE_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOLUMEMUTE_FB__DigitalOutput__ + i, VOLUMEMUTE_FB[i+1] );
    }
    
    VOIP_CALLSTATUS_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLSTATUS_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLSTATUS_FB__DigitalOutput__ + i, VOIP_CALLSTATUS_FB[i+1] );
    }
    
    VOIP_CALLINCOMING_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMING_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_CALLINCOMING_FB__DigitalOutput__ + i, VOIP_CALLINCOMING_FB[i+1] );
    }
    
    VOIP_AUTOANSWER_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_AUTOANSWER_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_AUTOANSWER_FB__DigitalOutput__ + i, VOIP_AUTOANSWER_FB[i+1] );
    }
    
    VOIP_DND_FB = new InOutArray<DigitalOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DND_FB[i+1] = new Crestron.Logos.SplusObjects.DigitalOutput( VOIP_DND_FB__DigitalOutput__ + i, this );
        m_DigitalOutputList.Add( VOIP_DND_FB__DigitalOutput__ + i, VOIP_DND_FB[i+1] );
    }
    
    IP_PORT = new Crestron.Logos.SplusObjects.AnalogInput( IP_PORT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( IP_PORT__AnalogSerialInput__, IP_PORT );
    
    VOLUMESET = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESET[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESET__AnalogSerialInput__ + i, VOLUMESET[i+1] );
    }
    
    VOLUMESTEP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMESTEP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOLUMESTEP__AnalogSerialInput__ + i, VOLUMESTEP[i+1] );
    }
    
    GROUP = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( GROUP__AnalogSerialInput__ + i, GROUP__AnalogSerialInput__, this );
        m_AnalogInputList.Add( GROUP__AnalogSerialInput__ + i, GROUP[i+1] );
    }
    
    VOIP_CALLAPPEARANCE = new InOutArray<AnalogInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLAPPEARANCE[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE__AnalogSerialInput__, this );
        m_AnalogInputList.Add( VOIP_CALLAPPEARANCE__AnalogSerialInput__ + i, VOIP_CALLAPPEARANCE[i+1] );
    }
    
    CONNECT_STATUS_FB = new Crestron.Logos.SplusObjects.AnalogOutput( CONNECT_STATUS_FB__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( CONNECT_STATUS_FB__AnalogSerialOutput__, CONNECT_STATUS_FB );
    
    VOLUMELEVEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        VOLUMELEVEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( VOLUMELEVEL_FB__AnalogSerialOutput__ + i, VOLUMELEVEL_FB[i+1] );
    }
    
    GROUP_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        GROUP_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( GROUP_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( GROUP_FB__AnalogSerialOutput__ + i, GROUP_FB[i+1] );
    }
    
    IP_ADDRESS = new Crestron.Logos.SplusObjects.StringInput( IP_ADDRESS__AnalogSerialInput__, 16, this );
    m_StringInputList.Add( IP_ADDRESS__AnalogSerialInput__, IP_ADDRESS );
    
    LOGIN_NAME = new Crestron.Logos.SplusObjects.StringInput( LOGIN_NAME__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_NAME__AnalogSerialInput__, LOGIN_NAME );
    
    LOGIN_PASSWORD = new Crestron.Logos.SplusObjects.StringInput( LOGIN_PASSWORD__AnalogSerialInput__, 32, this );
    m_StringInputList.Add( LOGIN_PASSWORD__AnalogSerialInput__, LOGIN_PASSWORD );
    
    RX = new Crestron.Logos.SplusObjects.StringInput( RX__AnalogSerialInput__, 1023, this );
    m_StringInputList.Add( RX__AnalogSerialInput__, RX );
    
    MANUALCMD = new Crestron.Logos.SplusObjects.StringInput( MANUALCMD__AnalogSerialInput__, 255, this );
    m_StringInputList.Add( MANUALCMD__AnalogSerialInput__, MANUALCMD );
    
    INSTANCETAGS = new InOutArray<StringInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        INSTANCETAGS[i+1] = new Crestron.Logos.SplusObjects.StringInput( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( INSTANCETAGS__AnalogSerialInput__ + i, INSTANCETAGS[i+1] );
    }
    
    VOIP_DIALENTRY = new InOutArray<StringInput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALENTRY[i+1] = new Crestron.Logos.SplusObjects.StringInput( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY__AnalogSerialInput__, 31, this );
        m_StringInputList.Add( VOIP_DIALENTRY__AnalogSerialInput__ + i, VOIP_DIALENTRY[i+1] );
    }
    
    TX = new Crestron.Logos.SplusObjects.StringOutput( TX__AnalogSerialOutput__, this );
    m_StringOutputList.Add( TX__AnalogSerialOutput__, TX );
    
    VOIP_DIALSTRING = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_DIALSTRING[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_DIALSTRING__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_DIALSTRING__AnalogSerialOutput__ + i, VOIP_DIALSTRING[i+1] );
    }
    
    VOIP_CALLINCOMINGNAME_FB = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNAME_FB[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNAME_FB[i+1] );
    }
    
    VOIP_CALLINCOMINGNUM = new InOutArray<StringOutput>( 2, this );
    for( uint i = 0; i < 2; i++ )
    {
        VOIP_CALLINCOMINGNUM[i+1] = new Crestron.Logos.SplusObjects.StringOutput( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, this );
        m_StringOutputList.Add( VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ + i, VOIP_CALLINCOMINGNUM[i+1] );
    }
    
    __SPLS_TMPVAR__WAITLABEL_1___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_1___CallbackFn );
    __SPLS_TMPVAR__WAITLABEL_2___Callback = new WaitFunction( __SPLS_TMPVAR__WAITLABEL_2___CallbackFn );
    VOLUP_Callback = new WaitFunction( VOLUP_CallbackFn );
    VOLDN_Callback = new WaitFunction( VOLDN_CallbackFn );
    
    AUDIOCLIENT.OnSocketConnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketConnect_0, false ) );
    AUDIOCLIENT.OnSocketDisconnect.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketDisconnect_1, false ) );
    AUDIOCLIENT.OnSocketStatus.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketStatus_2, false ) );
    AUDIOCLIENT.OnSocketReceive.Add( new SocketHandlerWrapper( AUDIOCLIENT_OnSocketReceive_3, false ) );
    CONNECT.OnDigitalChange.Add( new InputChangeHandlerWrapper( CONNECT_OnChange_4, false ) );
    POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_5, false ) );
    RX.OnSerialChange.Add( new InputChangeHandlerWrapper( RX_OnChange_6, false ) );
    MANUALCMD.OnSerialChange.Add( new InputChangeHandlerWrapper( MANUALCMD_OnChange_7, false ) );
    IP_ADDRESS.OnSerialChange.Add( new InputChangeHandlerWrapper( IP_ADDRESS_OnChange_8, false ) );
    IP_PORT.OnAnalogChange.Add( new InputChangeHandlerWrapper( IP_PORT_OnChange_9, false ) );
    LOGIN_NAME.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_NAME_OnChange_10, false ) );
    LOGIN_PASSWORD.OnSerialChange.Add( new InputChangeHandlerWrapper( LOGIN_PASSWORD_OnChange_11, false ) );
    for( uint i = 0; i < 64; i++ )
        INSTANCETAGS[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( INSTANCETAGS_OnChange_12, true ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEUP[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEUP_OnPush_13, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEDOWN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEDOWN_OnPush_14, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEON_OnPush_15, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTEOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTEOFF_OnPush_16, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMEMUTETOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOLUMEMUTETOGGLE_OnPush_17, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESET[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESET_OnChange_18, false ) );
        
    for( uint i = 0; i < 64; i++ )
        GROUP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( GROUP_OnChange_19, false ) );
        
    for( uint i = 0; i < 64; i++ )
        VOLUMESTEP[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOLUMESTEP_OnChange_20, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERTOGGLE_OnPush_21, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWERON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWERON_OnPush_22, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_AUTOANSWEROFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_AUTOANSWEROFF_OnPush_23, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDTOGGLE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDTOGGLE_OnPush_24, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDON[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDON_OnPush_25, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DNDOFF[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DNDOFF_OnPush_26, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_ACCEPT[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_ACCEPT_OnPush_27, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DECLINE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DECLINE_OnPush_28, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_JOIN[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_JOIN_OnPush_29, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CONFERENCE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_CONFERENCE_OnPush_30, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_REDIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_REDIAL_OnPush_31, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_0[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_0_OnPush_32, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_1[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_1_OnPush_33, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_2[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_2_OnPush_34, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_3[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_3_OnPush_35, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_4[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_4_OnPush_36, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_5[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_5_OnPush_37, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_6[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_6_OnPush_38, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_7[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_7_OnPush_39, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_8[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_8_OnPush_40, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_9[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_9_OnPush_41, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_STAR[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_STAR_OnPush_42, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_NUM_POUND[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_NUM_POUND_OnPush_43, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_BACKSPACE[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_BACKSPACE_OnPush_44, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIAL[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_DIAL_OnPush_45, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_END[i+1].OnDigitalPush.Add( new InputChangeHandlerWrapper( VOIP_END_OnPush_46, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_DIALENTRY[i+1].OnSerialChange.Add( new InputChangeHandlerWrapper( VOIP_DIALENTRY_OnChange_47, false ) );
        
    for( uint i = 0; i < 2; i++ )
        VOIP_CALLAPPEARANCE[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( VOIP_CALLAPPEARANCE_OnChange_48, false ) );
        
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_IESS_DSP_QSC_QSYS_V0_2 ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}


private WaitFunction __SPLS_TMPVAR__WAITLABEL_1___Callback;
private WaitFunction __SPLS_TMPVAR__WAITLABEL_2___Callback;
private WaitFunction VOLUP_Callback;
private WaitFunction VOLDN_Callback;


const uint CONNECT__DigitalInput__ = 0;
const uint POLL__DigitalInput__ = 1;
const uint DEBUG__DigitalInput__ = 2;
const uint IP_PORT__AnalogSerialInput__ = 0;
const uint IP_ADDRESS__AnalogSerialInput__ = 1;
const uint LOGIN_NAME__AnalogSerialInput__ = 2;
const uint LOGIN_PASSWORD__AnalogSerialInput__ = 3;
const uint RX__AnalogSerialInput__ = 4;
const uint MANUALCMD__AnalogSerialInput__ = 5;
const uint VOLUMEUP__DigitalInput__ = 3;
const uint VOLUMEDOWN__DigitalInput__ = 67;
const uint VOLUMEMUTETOGGLE__DigitalInput__ = 131;
const uint VOLUMEMUTEON__DigitalInput__ = 195;
const uint VOLUMEMUTEOFF__DigitalInput__ = 259;
const uint VOIP_NUM_0__DigitalInput__ = 323;
const uint VOIP_NUM_1__DigitalInput__ = 325;
const uint VOIP_NUM_2__DigitalInput__ = 327;
const uint VOIP_NUM_3__DigitalInput__ = 329;
const uint VOIP_NUM_4__DigitalInput__ = 331;
const uint VOIP_NUM_5__DigitalInput__ = 333;
const uint VOIP_NUM_6__DigitalInput__ = 335;
const uint VOIP_NUM_7__DigitalInput__ = 337;
const uint VOIP_NUM_8__DigitalInput__ = 339;
const uint VOIP_NUM_9__DigitalInput__ = 341;
const uint VOIP_NUM_STAR__DigitalInput__ = 343;
const uint VOIP_NUM_POUND__DigitalInput__ = 345;
const uint VOIP_BACKSPACE__DigitalInput__ = 347;
const uint VOIP_AUTOANSWERTOGGLE__DigitalInput__ = 349;
const uint VOIP_AUTOANSWERON__DigitalInput__ = 351;
const uint VOIP_AUTOANSWEROFF__DigitalInput__ = 353;
const uint VOIP_DNDTOGGLE__DigitalInput__ = 355;
const uint VOIP_DNDON__DigitalInput__ = 357;
const uint VOIP_DNDOFF__DigitalInput__ = 359;
const uint VOIP_DIAL__DigitalInput__ = 361;
const uint VOIP_END__DigitalInput__ = 363;
const uint VOIP_ACCEPT__DigitalInput__ = 365;
const uint VOIP_DECLINE__DigitalInput__ = 367;
const uint VOIP_JOIN__DigitalInput__ = 369;
const uint VOIP_CONFERENCE__DigitalInput__ = 371;
const uint VOIP_REDIAL__DigitalInput__ = 373;
const uint VOLUMESET__AnalogSerialInput__ = 6;
const uint VOLUMESTEP__AnalogSerialInput__ = 70;
const uint GROUP__AnalogSerialInput__ = 134;
const uint VOIP_CALLAPPEARANCE__AnalogSerialInput__ = 198;
const uint INSTANCETAGS__AnalogSerialInput__ = 200;
const uint VOIP_DIALENTRY__AnalogSerialInput__ = 264;
const uint CONNECT_FB__DigitalOutput__ = 0;
const uint CONNECT_STATUS_FB__AnalogSerialOutput__ = 0;
const uint TX__AnalogSerialOutput__ = 1;
const uint VOLUMEMUTE_FB__DigitalOutput__ = 1;
const uint VOIP_CALLSTATUS_FB__DigitalOutput__ = 65;
const uint VOIP_CALLINCOMING_FB__DigitalOutput__ = 67;
const uint VOIP_AUTOANSWER_FB__DigitalOutput__ = 69;
const uint VOIP_DND_FB__DigitalOutput__ = 71;
const uint VOLUMELEVEL_FB__AnalogSerialOutput__ = 2;
const uint GROUP_FB__AnalogSerialOutput__ = 66;
const uint VOIP_DIALSTRING__AnalogSerialOutput__ = 130;
const uint VOIP_CALLINCOMINGNAME_FB__AnalogSerialOutput__ = 132;
const uint VOIP_CALLINCOMINGNUM__AnalogSerialOutput__ = 134;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}

[SplusStructAttribute(-1, true, false)]
public class SAUDIO : SplusStructureBase
{

    [SplusStructAttribute(0, false, false)]
    public ushort  STATUSCONNECTREQUEST = 0;
    
    [SplusStructAttribute(1, false, false)]
    public ushort  STATUSCONNECTED = 0;
    
    [SplusStructAttribute(2, false, false)]
    public ushort  STATUSCOMMUNICATING = 0;
    
    [SplusStructAttribute(3, false, false)]
    public ushort  [] STATUSVOLUMEMUTE;
    
    [SplusStructAttribute(4, false, false)]
    public ushort  [] STATUSWALL;
    
    [SplusStructAttribute(5, false, false)]
    public ushort  [] STATUSGROUP;
    
    [SplusStructAttribute(6, false, false)]
    public ushort  [] STATUSVOIPAUTOANSWER;
    
    [SplusStructAttribute(7, false, false)]
    public ushort  [] STATUSVOIPDND;
    
    [SplusStructAttribute(8, false, false)]
    public ushort  [] STATUSVOIPCALLSTATE;
    
    [SplusStructAttribute(9, false, false)]
    public CrestronString  [] STATUSVOIPRECENTCALL;
    
    [SplusStructAttribute(10, false, false)]
    public CrestronString  [] VOIPDIALSTRING;
    
    [SplusStructAttribute(11, false, false)]
    public CrestronString  COMMANDPOLL;
    
    [SplusStructAttribute(12, false, false)]
    public CrestronString  COMMANDAUTOPOLL;
    
    [SplusStructAttribute(13, false, false)]
    public ushort  [] VOIPINSTANCEINDEX;
    
    [SplusStructAttribute(14, false, false)]
    public ushort  VOLUMEINUSE = 0;
    
    [SplusStructAttribute(15, false, false)]
    public ushort  LASTINDEX = 0;
    
    [SplusStructAttribute(16, false, false)]
    public ushort  LASTPOLLINDEX = 0;
    
    [SplusStructAttribute(17, false, false)]
    public ushort  LASTPOLLSECONDINDEX = 0;
    
    [SplusStructAttribute(18, false, false)]
    public uint  BAUD = 0;
    
    [SplusStructAttribute(19, false, false)]
    public short  [] STATUSVOLUME;
    
    [SplusStructAttribute(20, false, false)]
    public short  [] INTERNALVOLUME;
    
    [SplusStructAttribute(21, false, false)]
    public CrestronString  ETX;
    
    [SplusStructAttribute(22, false, false)]
    public CrestronString  IPADDRESS;
    
    [SplusStructAttribute(23, false, false)]
    public ushort  IPPORT = 0;
    
    [SplusStructAttribute(24, false, false)]
    public CrestronString  LOGINNAME;
    
    [SplusStructAttribute(25, false, false)]
    public CrestronString  LOGINPASSWORD;
    
    [SplusStructAttribute(26, false, false)]
    public CrestronString  [] INSTANCETAGSNAME;
    
    [SplusStructAttribute(27, false, false)]
    public CrestronString  [] INSTANCETAGSTYPE;
    
    [SplusStructAttribute(28, false, false)]
    public ushort  [] INSTANCETAGSINDEX;
    
    [SplusStructAttribute(29, false, false)]
    public ushort  [] INSTANCETAGSINDEXSECOND;
    
    [SplusStructAttribute(30, false, false)]
    public ushort  [] INSTANCETAGSPOLL;
    
    [SplusStructAttribute(31, false, false)]
    public short  [] VOLUMEMIN;
    
    [SplusStructAttribute(32, false, false)]
    public short  [] VOLUMEMAX;
    
    [SplusStructAttribute(33, false, false)]
    public ushort  [] VOLUMESTEP;
    
    [SplusStructAttribute(34, false, false)]
    public CrestronString  TXQUEUE;
    
    [SplusStructAttribute(35, false, false)]
    public CrestronString  RXQUEUE;
    
    
    public SAUDIO( SplusObject __caller__, bool bIsStructureVolatile ) : base ( __caller__, bIsStructureVolatile )
    {
        STATUSVOLUMEMUTE  = new ushort[ 65 ];
        STATUSWALL  = new ushort[ 65 ];
        STATUSGROUP  = new ushort[ 65 ];
        STATUSVOIPAUTOANSWER  = new ushort[ 3 ];
        STATUSVOIPDND  = new ushort[ 3 ];
        STATUSVOIPCALLSTATE  = new ushort[ 3 ];
        VOIPINSTANCEINDEX  = new ushort[ 3 ];
        INSTANCETAGSINDEX  = new ushort[ 65 ];
        INSTANCETAGSINDEXSECOND  = new ushort[ 65 ];
        INSTANCETAGSPOLL  = new ushort[ 65 ];
        VOLUMESTEP  = new ushort[ 65 ];
        STATUSVOLUME  = new short[ 65 ];
        INTERNALVOLUME  = new short[ 65 ];
        VOLUMEMIN  = new short[ 65 ];
        VOLUMEMAX  = new short[ 65 ];
        COMMANDPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        COMMANDAUTOPOLL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 255, Owner );
        ETX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2, Owner );
        IPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINNAME  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        LOGINPASSWORD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        TXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, Owner );
        RXQUEUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 16383, Owner );
        STATUSVOIPRECENTCALL  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            STATUSVOIPRECENTCALL [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        VOIPDIALSTRING  = new CrestronString[ 3 ];
        for( uint i = 0; i < 3; i++ )
            VOIPDIALSTRING [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSNAME  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSNAME [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 31, Owner );
        INSTANCETAGSTYPE  = new CrestronString[ 65 ];
        for( uint i = 0; i < 65; i++ )
            INSTANCETAGSTYPE [i] = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 7, Owner );
        
        
    }
    
}

}
